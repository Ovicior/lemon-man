﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="2587888364">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1431953291">
      <_items dataType="Array" type="Duality.Component[]" id="2199808118">
        <item dataType="Struct" type="Duality.Components.Transform" id="653236000">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">2587888364</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3" />
          <posAbs dataType="Struct" type="Duality.Vector3" />
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2295323745">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">5</animDuration>
          <animFirstFrame dataType="Int">0</animFirstFrame>
          <animFrameCount dataType="Int">4</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
          <animPaused dataType="Bool">false</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence />
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">2587888364</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">30</H>
            <W dataType="Float">30</W>
            <X dataType="Float">-15</X>
            <Y dataType="Float">-15</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="Manager.AnimationManager" id="1064132052">
          <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
          <_x003C_animationDatabase_x003E_k__BackingField />
          <active dataType="Bool">true</active>
          <animationDuration dataType="Float">0</animationDuration>
          <animationToReturnTo />
          <animSpriteRenderer />
          <currentAnimationName />
          <currentTime dataType="Float">0</currentTime>
          <gameobj dataType="ObjectRef">2587888364</gameobj>
        </item>
        <item dataType="Struct" type="Behavior.PeaHit" id="4279815177">
          <active dataType="Bool">true</active>
          <animationManager dataType="ObjectRef">1064132052</animationManager>
          <checkForDelete dataType="Bool">true</checkForDelete>
          <deleteDelay dataType="Float">400</deleteDelay>
          <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
          <gameobj dataType="ObjectRef">2587888364</gameobj>
          <targetDeleteTime dataType="Float">400</targetDeleteTime>
        </item>
      </_items>
      <_size dataType="Int">4</_size>
      <_version dataType="Int">4</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2326526664" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="4030456609">
          <item dataType="Type" id="1233624686" value="Duality.Components.Transform" />
          <item dataType="Type" id="784658378" value="Duality.Components.Renderers.AnimSpriteRenderer" />
          <item dataType="Type" id="111526494" value="Manager.AnimationManager" />
          <item dataType="Type" id="2659686234" value="Behavior.PeaHit" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="3682683168">
          <item dataType="ObjectRef">653236000</item>
          <item dataType="ObjectRef">2295323745</item>
          <item dataType="ObjectRef">1064132052</item>
          <item dataType="ObjectRef">4279815177</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">653236000</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="3629489331">Ck7SEJL1vESv9H/59a22Wg==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Pea_Hit</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
