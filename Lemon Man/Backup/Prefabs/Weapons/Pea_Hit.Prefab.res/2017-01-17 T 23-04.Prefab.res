﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="1098242473">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="4145129090">
      <_items dataType="Array" type="Duality.Component[]" id="3684403856">
        <item dataType="Struct" type="Duality.Components.Transform" id="3458557405">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">1098242473</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3" />
          <posAbs dataType="Struct" type="Duality.Vector3" />
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="805677854">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">0.7</animDuration>
          <animFirstFrame dataType="Int">0</animFirstFrame>
          <animFrameCount dataType="Int">4</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
          <animPaused dataType="Bool">false</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence />
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">1098242473</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">30</H>
            <W dataType="Float">30</W>
            <X dataType="Float">-15</X>
            <Y dataType="Float">-15</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="Manager.AnimationManager" id="3869453457">
          <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
          <_x003C_animationDatabase_x003E_k__BackingField />
          <active dataType="Bool">true</active>
          <animationDuration dataType="Float">0</animationDuration>
          <animationToReturnTo />
          <animSpriteRenderer />
          <currentAnimationName />
          <currentTime dataType="Float">0</currentTime>
          <gameobj dataType="ObjectRef">1098242473</gameobj>
        </item>
        <item dataType="Struct" type="Behavior.PeaHit" id="2790169286">
          <active dataType="Bool">true</active>
          <animationManager dataType="Struct" type="Manager.AnimationManager" id="3668503896">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="2059056302">
              <_items dataType="Array" type="Misc.Animation[]" id="433234256" length="4">
                <item dataType="Struct" type="Misc.Animation" id="1546820540">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">4</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.7</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">hit</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">1</_version>
            </_x003C_animationDatabase_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3686248008">
              <active dataType="Bool">true</active>
              <animDuration dataType="Float">0.7</animDuration>
              <animFirstFrame dataType="Int">0</animFirstFrame>
              <animFrameCount dataType="Int">4</animFrameCount>
              <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
              <animPaused dataType="Bool">false</animPaused>
              <animTime dataType="Float">0</animTime>
              <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                <A dataType="Byte">255</A>
                <B dataType="Byte">255</B>
                <G dataType="Byte">255</G>
                <R dataType="Byte">255</R>
              </colorTint>
              <customFrameSequence />
              <customMat />
              <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
              <gameobj dataType="Struct" type="Duality.GameObject" id="3978812627">
                <active dataType="Bool">true</active>
                <children />
                <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2195536081">
                  <_items dataType="Array" type="Duality.Component[]" id="1976846574">
                    <item dataType="Struct" type="Duality.Components.Transform" id="2044160263">
                      <active dataType="Bool">true</active>
                      <angle dataType="Float">0</angle>
                      <angleAbs dataType="Float">0</angleAbs>
                      <angleVel dataType="Float">0</angleVel>
                      <angleVelAbs dataType="Float">0</angleVelAbs>
                      <deriveAngle dataType="Bool">true</deriveAngle>
                      <gameobj dataType="ObjectRef">3978812627</gameobj>
                      <ignoreParent dataType="Bool">false</ignoreParent>
                      <parentTransform />
                      <pos dataType="Struct" type="Duality.Vector3" />
                      <posAbs dataType="Struct" type="Duality.Vector3" />
                      <scale dataType="Float">1</scale>
                      <scaleAbs dataType="Float">1</scaleAbs>
                      <vel dataType="Struct" type="Duality.Vector3" />
                      <velAbs dataType="Struct" type="Duality.Vector3" />
                    </item>
                    <item dataType="ObjectRef">3686248008</item>
                    <item dataType="Struct" type="Behavior.PeaHit" id="1375772144">
                      <active dataType="Bool">true</active>
                      <animationManager dataType="ObjectRef">3668503896</animationManager>
                      <checkForDelete dataType="Bool">true</checkForDelete>
                      <deleteDelay dataType="Float">400</deleteDelay>
                      <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
                      <gameobj dataType="ObjectRef">3978812627</gameobj>
                      <targetDeleteTime dataType="Float">400</targetDeleteTime>
                    </item>
                    <item dataType="Struct" type="Manager.AnimationManager" id="2455056315">
                      <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
                      <_x003C_animationDatabase_x003E_k__BackingField />
                      <active dataType="Bool">true</active>
                      <animationDuration dataType="Float">0</animationDuration>
                      <animationToReturnTo />
                      <animSpriteRenderer />
                      <currentAnimationName />
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">3978812627</gameobj>
                    </item>
                  </_items>
                  <_size dataType="Int">4</_size>
                  <_version dataType="Int">6</_version>
                </compList>
                <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1985220000" surrogate="true">
                  <header />
                  <body>
                    <keys dataType="Array" type="System.Object[]" id="2980963579">
                      <item dataType="Type" id="3522049622" value="Duality.Components.Transform" />
                      <item dataType="Type" id="3631615706" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                      <item dataType="Type" id="917543542" value="Manager.AnimationManager" />
                      <item dataType="Type" id="3462547578" value="Behavior.PeaHit" />
                    </keys>
                    <values dataType="Array" type="System.Object[]" id="1269486504">
                      <item dataType="ObjectRef">2044160263</item>
                      <item dataType="ObjectRef">3686248008</item>
                      <item dataType="ObjectRef">2455056315</item>
                      <item dataType="ObjectRef">1375772144</item>
                    </values>
                  </body>
                </compMap>
                <compTransform dataType="ObjectRef">2044160263</compTransform>
                <identifier dataType="Struct" type="System.Guid" surrogate="true">
                  <header>
                    <data dataType="Array" type="System.Byte[]" id="420467953">e50l2gJ7B0GPhkwG4P3Krw==</data>
                  </header>
                  <body />
                </identifier>
                <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                <name dataType="String">Pea_Hit</name>
                <parent />
                <prefabLink />
              </gameobj>
              <offset dataType="Int">0</offset>
              <pixelGrid dataType="Bool">false</pixelGrid>
              <rect dataType="Struct" type="Duality.Rect">
                <H dataType="Float">30</H>
                <W dataType="Float">30</W>
                <X dataType="Float">-15</X>
                <Y dataType="Float">-15</Y>
              </rect>
              <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
              <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
              </sharedMat>
              <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
            </animSpriteRenderer>
            <currentAnimationName dataType="String">hit</currentAnimationName>
            <currentTime dataType="Float">0</currentTime>
            <gameobj />
          </animationManager>
          <checkForDelete dataType="Bool">true</checkForDelete>
          <deleteDelay dataType="Float">400</deleteDelay>
          <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
          <gameobj dataType="ObjectRef">1098242473</gameobj>
          <targetDeleteTime dataType="Float">400</targetDeleteTime>
        </item>
      </_items>
      <_size dataType="Int">4</_size>
      <_version dataType="Int">4</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2147502218" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="2248248664">
          <item dataType="ObjectRef">3522049622</item>
          <item dataType="ObjectRef">3631615706</item>
          <item dataType="ObjectRef">917543542</item>
          <item dataType="ObjectRef">3462547578</item>
        </keys>
        <values dataType="Array" type="System.Object[]" id="3432220574">
          <item dataType="ObjectRef">3458557405</item>
          <item dataType="ObjectRef">805677854</item>
          <item dataType="ObjectRef">3869453457</item>
          <item dataType="ObjectRef">2790169286</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">3458557405</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="2474173444">XJ1V3CB9dUSR0+BBisqjgA==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Pea_Hit</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
