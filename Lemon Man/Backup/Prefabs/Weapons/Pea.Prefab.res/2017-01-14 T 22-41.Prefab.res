﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="201652214">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1360445921">
      <_items dataType="Array" type="Duality.Component[]" id="1623380334">
        <item dataType="Struct" type="Duality.Components.Transform" id="2561967146">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">201652214</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">-0.1</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">-0.1</Z>
          </posAbs>
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="1843818782">
          <active dataType="Bool">true</active>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">201652214</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">4</H>
            <W dataType="Float">12</W>
            <X dataType="Float">-6</X>
            <Y dataType="Float">-2</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3264428738">
          <active dataType="Bool">true</active>
          <angularDamp dataType="Float">0.3</angularDamp>
          <angularVel dataType="Float">0</angularVel>
          <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
          <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
          <colFilter />
          <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
          <continous dataType="Bool">false</continous>
          <explicitInertia dataType="Float">0</explicitInertia>
          <explicitMass dataType="Float">0</explicitMass>
          <fixedAngle dataType="Bool">true</fixedAngle>
          <gameobj dataType="ObjectRef">201652214</gameobj>
          <ignoreGravity dataType="Bool">true</ignoreGravity>
          <joints />
          <linearDamp dataType="Float">0.3</linearDamp>
          <linearVel dataType="Struct" type="Duality.Vector2" />
          <revolutions dataType="Float">0</revolutions>
          <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3578151302">
            <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3644196736">
              <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2834284956">
                <density dataType="Float">1</density>
                <friction dataType="Float">0.3</friction>
                <parent dataType="ObjectRef">3264428738</parent>
                <restitution dataType="Float">0.3</restitution>
                <sensor dataType="Bool">false</sensor>
                <vertices dataType="Array" type="Duality.Vector2[]" id="1932756420">
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-1</X>
                    <Y dataType="Float">-2</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-1</X>
                    <Y dataType="Float">2</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">6</X>
                    <Y dataType="Float">2</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">6</X>
                    <Y dataType="Float">-2</Y>
                  </item>
                </vertices>
              </item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">10</_version>
          </shapes>
        </item>
        <item dataType="Struct" type="Behavior.Pea" id="2261962146">
          <_x003C_Creator_x003E_k__BackingField dataType="Struct" type="Duality.GameObject" id="787527030">
            <active dataType="Bool">true</active>
            <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="1558397648">
              <_items dataType="Array" type="Duality.GameObject[]" id="1894399676" length="4" />
              <_size dataType="Int">0</_size>
              <_version dataType="Int">2</_version>
            </children>
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3132524142">
              <_items dataType="Array" type="Duality.Component[]" id="462846370" length="8">
                <item dataType="Struct" type="Duality.Components.Transform" id="3147841962">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-34.3</Y>
                    <Z dataType="Float">0</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-34.3</Y>
                    <Z dataType="Float">0</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item />
                <item dataType="Struct" type="Player.PlayerController" id="1575548244">
                  <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
                  <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
                  <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">4</X>
                    <Y dataType="Float">0.1</Y>
                    <Z dataType="Float">-0.1</Z>
                  </_x003C_BulletSpawnOffset_x003E_k__BackingField>
                  <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Player.FacingDirection" name="Left" value="0" />
                  <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">300</_x003C_FiringDelay_x003E_k__BackingField>
                  <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
                  <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
                  <_x003C_PlayerAnimationState_x003E_k__BackingField dataType="Enum" type="Player.PlayerAnimationState" name="Idle" value="0" />
                  <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <animationManager dataType="Struct" type="Manager.AnimationManager" id="3558738014">
                    <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">LemonMan</_x003C_animatedObjectName_x003E_k__BackingField>
                    <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="1229126178">
                      <_items dataType="Array" type="Misc.Animation[]" id="665900304">
                        <item dataType="Struct" type="Misc.Animation" id="1893976892">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">6</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                        <item dataType="Struct" type="Misc.Animation" id="294488982">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">4</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.5</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">7</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                        <item dataType="Struct" type="Misc.Animation" id="3766369768">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">jump</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                        <item dataType="Struct" type="Misc.Animation" id="1682333810">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">fall</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                      </_items>
                      <_size dataType="Int">4</_size>
                      <_version dataType="Int">4</_version>
                    </_x003C_animationDatabase_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationLength dataType="Float">0</animationLength>
                    <animationToReturnTo />
                    <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="494962411">
                      <active dataType="Bool">true</active>
                      <animDuration dataType="Float">2</animDuration>
                      <animFirstFrame dataType="Int">6</animFirstFrame>
                      <animFrameCount dataType="Int">4</animFrameCount>
                      <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                      <animPaused dataType="Bool">false</animPaused>
                      <animTime dataType="Float">0</animTime>
                      <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                        <A dataType="Byte">255</A>
                        <B dataType="Byte">255</B>
                        <G dataType="Byte">255</G>
                        <R dataType="Byte">255</R>
                      </colorTint>
                      <customFrameSequence />
                      <customMat />
                      <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                      <offset dataType="Int">0</offset>
                      <pixelGrid dataType="Bool">false</pixelGrid>
                      <rect dataType="Struct" type="Duality.Rect">
                        <H dataType="Float">26</H>
                        <W dataType="Float">24</W>
                        <X dataType="Float">-12</X>
                        <Y dataType="Float">-13</Y>
                      </rect>
                      <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                      <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                        <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                      </sharedMat>
                      <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                    </animSpriteRenderer>
                    <currentAnimationName />
                    <currentTime dataType="Float">0</currentTime>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                  </animationManager>
                  <firingDelayCounter dataType="Float">0</firingDelayCounter>
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <gravity dataType="Float">444.444458</gravity>
                  <heldWeapon dataType="Struct" type="Behavior.HeldWeapon" id="971760278">
                    <_x003C_CurrentWeapon_x003E_k__BackingField dataType="Struct" type="Misc.Weapon" id="3957022490">
                      <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">0</_x003C_BottomLeft_x003E_k__BackingField>
                      <_x003C_BottomRight_x003E_k__BackingField dataType="Float">0</_x003C_BottomRight_x003E_k__BackingField>
                      <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                      <_x003C_ID_x003E_k__BackingField dataType="Int">0</_x003C_ID_x003E_k__BackingField>
                      <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">3</_x003C_Inaccuracy_x003E_k__BackingField>
                      <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabXOffset_x003E_k__BackingField>
                      <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabYOffset_x003E_k__BackingField>
                      <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">3</_x003C_RateOfFire_x003E_k__BackingField>
                      <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                      <_x003C_Slug_x003E_k__BackingField dataType="String">peastol</_x003C_Slug_x003E_k__BackingField>
                      <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                        <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\peastol.Texture.res</contentPath>
                      </_x003C_Sprite_x003E_k__BackingField>
                      <_x003C_Title_x003E_k__BackingField dataType="String">Peastol</_x003C_Title_x003E_k__BackingField>
                      <_x003C_TopLeft_x003E_k__BackingField dataType="Float">0</_x003C_TopLeft_x003E_k__BackingField>
                      <_x003C_TopRight_x003E_k__BackingField dataType="Float">0</_x003C_TopRight_x003E_k__BackingField>
                      <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                      <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                      <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
                    </_x003C_CurrentWeapon_x003E_k__BackingField>
                    <_x003C_prefabSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                    <_x003C_weaponOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">10.5</X>
                      <Y dataType="Float">3.2</Y>
                    </_x003C_weaponOffset_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationState dataType="Enum" type="Behavior.WeaponAnimationState" name="Idle" value="0" />
                    <firingDelay dataType="Float">300</firingDelay>
                    <firingDelayCounter dataType="Float">0</firingDelayCounter>
                    <gameobj dataType="Struct" type="Duality.GameObject" id="1343490243">
                      <active dataType="Bool">true</active>
                      <children />
                      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1584958573">
                        <_items dataType="Array" type="Duality.Component[]" id="3310475494">
                          <item dataType="Struct" type="Duality.Components.Transform" id="3703805175">
                            <active dataType="Bool">true</active>
                            <angle dataType="Float">0</angle>
                            <angleAbs dataType="Float">0</angleAbs>
                            <angleVel dataType="Float">0</angleVel>
                            <angleVelAbs dataType="Float">0</angleVelAbs>
                            <deriveAngle dataType="Bool">true</deriveAngle>
                            <gameobj dataType="ObjectRef">1343490243</gameobj>
                            <ignoreParent dataType="Bool">false</ignoreParent>
                            <parentTransform />
                            <pos dataType="Struct" type="Duality.Vector3">
                              <X dataType="Float">0</X>
                              <Y dataType="Float">0</Y>
                              <Z dataType="Float">-0.1</Z>
                            </pos>
                            <posAbs dataType="Struct" type="Duality.Vector3">
                              <X dataType="Float">0</X>
                              <Y dataType="Float">0</Y>
                              <Z dataType="Float">-0.1</Z>
                            </posAbs>
                            <scale dataType="Float">1</scale>
                            <scaleAbs dataType="Float">1</scaleAbs>
                            <vel dataType="Struct" type="Duality.Vector3" />
                            <velAbs dataType="Struct" type="Duality.Vector3" />
                          </item>
                          <item dataType="ObjectRef">971760278</item>
                          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1050925624">
                            <active dataType="Bool">true</active>
                            <animDuration dataType="Float">5</animDuration>
                            <animFirstFrame dataType="Int">0</animFirstFrame>
                            <animFrameCount dataType="Int">0</animFrameCount>
                            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                            <animPaused dataType="Bool">false</animPaused>
                            <animTime dataType="Float">0</animTime>
                            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                              <A dataType="Byte">255</A>
                              <B dataType="Byte">255</B>
                              <G dataType="Byte">255</G>
                              <R dataType="Byte">255</R>
                            </colorTint>
                            <customFrameSequence />
                            <customMat />
                            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                            <gameobj dataType="ObjectRef">1343490243</gameobj>
                            <offset dataType="Int">0</offset>
                            <pixelGrid dataType="Bool">false</pixelGrid>
                            <rect dataType="Struct" type="Duality.Rect">
                              <H dataType="Float">12</H>
                              <W dataType="Float">27</W>
                              <X dataType="Float">-13.5</X>
                              <Y dataType="Float">-6</Y>
                            </rect>
                            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\heldWeapon.Material.res</contentPath>
                            </sharedMat>
                            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                          </item>
                          <item dataType="Struct" type="Manager.AnimationManager" id="4114701227">
                            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peastol</_x003C_animatedObjectName_x003E_k__BackingField>
                            <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="1387961595">
                              <_items dataType="Array" type="Misc.Animation[]" id="1907078742" length="4">
                                <item dataType="Struct" type="Misc.Animation" id="4029247520">
                                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                                </item>
                                <item dataType="Struct" type="Misc.Animation" id="1216275342">
                                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">2</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.2</_x003C_AnimationDuration_x003E_k__BackingField>
                                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">fire</_x003C_AnimationName_x003E_k__BackingField>
                                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">2</_x003C_StartFrame_x003E_k__BackingField>
                                </item>
                                <item dataType="Struct" type="Misc.Animation" id="182688572">
                                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">playerMoving</_x003C_AnimationName_x003E_k__BackingField>
                                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                                </item>
                              </_items>
                              <_size dataType="Int">3</_size>
                              <_version dataType="Int">3</_version>
                            </_x003C_animationDatabase_x003E_k__BackingField>
                            <active dataType="Bool">true</active>
                            <animationLength dataType="Float">0</animationLength>
                            <animationToReturnTo />
                            <animSpriteRenderer dataType="ObjectRef">1050925624</animSpriteRenderer>
                            <currentAnimationName />
                            <currentTime dataType="Float">0</currentTime>
                            <gameobj dataType="ObjectRef">1343490243</gameobj>
                          </item>
                        </_items>
                        <_size dataType="Int">4</_size>
                        <_version dataType="Int">12</_version>
                      </compList>
                      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="132646648" surrogate="true">
                        <header />
                        <body>
                          <keys dataType="Array" type="System.Object[]" id="1683961351">
                            <item dataType="Type" id="2624026190" value="Duality.Components.Transform" />
                            <item dataType="Type" id="185084490" value="Manager.AnimationManager" />
                            <item dataType="Type" id="827226110" value="Behavior.HeldWeapon" />
                            <item dataType="Type" id="2406367066" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                          </keys>
                          <values dataType="Array" type="System.Object[]" id="1814180992">
                            <item dataType="ObjectRef">3703805175</item>
                            <item dataType="ObjectRef">4114701227</item>
                            <item dataType="ObjectRef">971760278</item>
                            <item dataType="ObjectRef">1050925624</item>
                          </values>
                        </body>
                      </compMap>
                      <compTransform dataType="ObjectRef">3703805175</compTransform>
                      <identifier dataType="Struct" type="System.Guid" surrogate="true">
                        <header>
                          <data dataType="Array" type="System.Byte[]" id="2876087301">A0P5xu17sEuEt+3rLf8XcA==</data>
                        </header>
                        <body />
                      </identifier>
                      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                      <name dataType="String">Weapon</name>
                      <parent />
                      <prefabLink />
                    </gameobj>
                    <heldWeaponAnimRenderer dataType="ObjectRef">1050925624</heldWeaponAnimRenderer>
                    <hydroLaserPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                    <idleAnimationDelayCounter dataType="Float">0</idleAnimationDelayCounter>
                    <lemonadePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                    <peaPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                    <pepperFlamePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                    <player dataType="ObjectRef">787527030</player>
                    <playerAnimRenderer dataType="ObjectRef">494962411</playerAnimRenderer>
                    <playerController dataType="ObjectRef">1575548244</playerController>
                    <prefabToBeFired dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Weapons\Pea.Prefab.res</contentPath>
                    </prefabToBeFired>
                    <transform dataType="ObjectRef">3703805175</transform>
                    <weaponAnimationManager dataType="ObjectRef">4114701227</weaponAnimationManager>
                    <weaponDatabase dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Weapon]]" id="3362912570">
                      <_items dataType="Array" type="Misc.Weapon[]" id="1489718880" length="4">
                        <item dataType="ObjectRef">3957022490</item>
                        <item dataType="Struct" type="Misc.Weapon" id="2213984988">
                          <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">0</_x003C_BottomLeft_x003E_k__BackingField>
                          <_x003C_BottomRight_x003E_k__BackingField dataType="Float">0</_x003C_BottomRight_x003E_k__BackingField>
                          <_x003C_BurstCount_x003E_k__BackingField dataType="Int">5</_x003C_BurstCount_x003E_k__BackingField>
                          <_x003C_ID_x003E_k__BackingField dataType="Int">1</_x003C_ID_x003E_k__BackingField>
                          <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">10</_x003C_Inaccuracy_x003E_k__BackingField>
                          <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabXOffset_x003E_k__BackingField>
                          <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabYOffset_x003E_k__BackingField>
                          <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">5</_x003C_RateOfFire_x003E_k__BackingField>
                          <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Heavy" value="2" />
                          <_x003C_Slug_x003E_k__BackingField dataType="String">shotgun</_x003C_Slug_x003E_k__BackingField>
                          <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                            <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\shotgun.Texture.res</contentPath>
                          </_x003C_Sprite_x003E_k__BackingField>
                          <_x003C_Title_x003E_k__BackingField dataType="String">Shotgun</_x003C_Title_x003E_k__BackingField>
                          <_x003C_TopLeft_x003E_k__BackingField dataType="Float">0</_x003C_TopLeft_x003E_k__BackingField>
                          <_x003C_TopRight_x003E_k__BackingField dataType="Float">0</_x003C_TopRight_x003E_k__BackingField>
                          <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                          <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                          <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
                        </item>
                      </_items>
                      <_size dataType="Int">2</_size>
                      <_version dataType="Int">2</_version>
                    </weaponDatabase>
                    <weaponDatabaseObject dataType="Struct" type="Duality.GameObject" id="2834260959">
                      <active dataType="Bool">true</active>
                      <children />
                      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="205347489">
                        <_items dataType="Array" type="Duality.Component[]" id="1830603886" length="4">
                          <item dataType="Struct" type="Manager.WeaponDatabaseManager" id="3611639126">
                            <_x003C_weaponDatabase_x003E_k__BackingField dataType="ObjectRef">3362912570</_x003C_weaponDatabase_x003E_k__BackingField>
                            <active dataType="Bool">true</active>
                            <gameobj dataType="ObjectRef">2834260959</gameobj>
                          </item>
                        </_items>
                        <_size dataType="Int">1</_size>
                        <_version dataType="Int">11</_version>
                      </compList>
                      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="142578464" surrogate="true">
                        <header />
                        <body>
                          <keys dataType="Array" type="System.Object[]" id="2792031147">
                            <item dataType="Type" id="3804364022" value="Manager.WeaponDatabaseManager" />
                          </keys>
                          <values dataType="Array" type="System.Object[]" id="3778473288">
                            <item dataType="ObjectRef">3611639126</item>
                          </values>
                        </body>
                      </compMap>
                      <compTransform />
                      <identifier dataType="Struct" type="System.Guid" surrogate="true">
                        <header>
                          <data dataType="Array" type="System.Byte[]" id="258502049">biwph8Z5kk+28lYMuemNIw==</data>
                        </header>
                        <body />
                      </identifier>
                      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                      <name dataType="String">WorldManagers</name>
                      <parent />
                      <prefabLink />
                    </weaponDatabaseObject>
                  </heldWeapon>
                  <jumpVelocity dataType="Float">200</jumpVelocity>
                  <spriteRenderer dataType="ObjectRef">494962411</spriteRenderer>
                  <velocity dataType="Struct" type="Duality.Vector2" />
                </item>
                <item dataType="Struct" type="Behavior.EntityStats" id="1425810874">
                  <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">35</_x003C_CurrentHealth_x003E_k__BackingField>
                  <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
                  <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">35</_x003C_MaxHealth_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <animationManager />
                  <damageAnimationLength dataType="Float">0.1</damageAnimationLength>
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                  <timer />
                </item>
                <item dataType="ObjectRef">494962411</item>
                <item dataType="ObjectRef">3558738014</item>
              </_items>
              <_size dataType="Int">6</_size>
              <_version dataType="Int">20</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1385808044" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="3797709688">
                  <item dataType="ObjectRef">2624026190</item>
                  <item dataType="ObjectRef">2406367066</item>
                  <item dataType="Type" id="2684378988" value="Player.PlayerController" />
                  <item dataType="Type" id="1543971894" value="Behavior.EntityStats" />
                  <item dataType="ObjectRef">185084490</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="2094603230">
                  <item dataType="ObjectRef">3147841962</item>
                  <item dataType="ObjectRef">494962411</item>
                  <item dataType="ObjectRef">1575548244</item>
                  <item dataType="ObjectRef">1425810874</item>
                  <item dataType="ObjectRef">3558738014</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">3147841962</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="1913843236">UPF8jIwOT0SjSKyQzVYuaQ==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Player</name>
            <parent />
            <prefabLink />
          </_x003C_Creator_x003E_k__BackingField>
          <_x003C_LinearVelocityToSet_x003E_k__BackingField dataType="Struct" type="Duality.Vector2" />
          <_x003C_PeaSpriteRenderer_x003E_k__BackingField dataType="ObjectRef">1843818782</_x003C_PeaSpriteRenderer_x003E_k__BackingField>
          <_x003C_Speed_x003E_k__BackingField dataType="Float">3</_x003C_Speed_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">201652214</gameobj>
          <hasCollided dataType="Bool">false</hasCollided>
          <heldWeapon dataType="ObjectRef">971760278</heldWeapon>
          <hitAnimation dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
          <lifetimeCounter dataType="Float">0</lifetimeCounter>
          <player />
          <playerController dataType="ObjectRef">1575548244</playerController>
          <prefabDamage dataType="Int">3</prefabDamage>
          <prefabLifetime dataType="Float">6</prefabLifetime>
          <rigidBody dataType="ObjectRef">3264428738</rigidBody>
          <stats />
          <transform dataType="ObjectRef">2561967146</transform>
        </item>
      </_items>
      <_size dataType="Int">4</_size>
      <_version dataType="Int">4</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3173345312" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="2519328363">
          <item dataType="ObjectRef">2624026190</item>
          <item dataType="Type" id="2241044086" value="Duality.Components.Renderers.SpriteRenderer" />
          <item dataType="Type" id="1383987482" value="Duality.Components.Physics.RigidBody" />
          <item dataType="Type" id="732901782" value="Behavior.Pea" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="333128904">
          <item dataType="ObjectRef">2561967146</item>
          <item dataType="ObjectRef">1843818782</item>
          <item dataType="ObjectRef">3264428738</item>
          <item dataType="ObjectRef">2261962146</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">2561967146</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="3646557025">6rBvgWtuyEePpeOhqR1z1A==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Pea</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
