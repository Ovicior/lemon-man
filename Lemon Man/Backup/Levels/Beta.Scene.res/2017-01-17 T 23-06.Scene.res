﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">48.8</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="2511523052">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2647580066">
        <_items dataType="Array" type="Duality.Component[]" id="178379024">
          <item dataType="Struct" type="Duality.Components.Transform" id="576870688">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-300</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-300</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Camera" id="3048798859">
            <active dataType="Bool">true</active>
            <farZ dataType="Float">10000</farZ>
            <focusDist dataType="Float">500</focusDist>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
            <nearZ dataType="Float">0</nearZ>
            <passes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Camera+Pass]]" id="4189425567">
              <_items dataType="Array" type="Duality.Components.Camera+Pass[]" id="2743093614" length="4">
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="2592933968">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="All" value="3" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="PerspectiveWorld" value="0" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="AllGroups" value="2147483647" />
                </item>
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="3724855662">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="None" value="0" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="OrthoScreen" value="1" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </passes>
            <perspective dataType="Enum" type="Duality.Drawing.PerspectiveMode" name="Parallax" value="1" />
            <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
          </item>
          <item dataType="Struct" type="Duality.Components.SoundListener" id="3165004423">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
          </item>
          <item dataType="Struct" type="Camera.CameraFollow" id="1051768081">
            <_x003C_PlayerObject_x003E_k__BackingField dataType="Struct" type="Duality.GameObject" id="787527030">
              <active dataType="Bool">true</active>
              <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="3740027105">
                <_items dataType="Array" type="Duality.GameObject[]" id="3062691694" length="4" />
                <_size dataType="Int">0</_size>
                <_version dataType="Int">2</_version>
              </children>
              <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2174773792">
                <_items dataType="Array" type="Duality.Component[]" id="330374507">
                  <item dataType="Struct" type="Duality.Components.Transform" id="3147841962">
                    <active dataType="Bool">true</active>
                    <angle dataType="Float">0</angle>
                    <angleAbs dataType="Float">0</angleAbs>
                    <angleVel dataType="Float">0</angleVel>
                    <angleVelAbs dataType="Float">0</angleVelAbs>
                    <deriveAngle dataType="Bool">true</deriveAngle>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <ignoreParent dataType="Bool">false</ignoreParent>
                    <parentTransform />
                    <pos dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">0</X>
                      <Y dataType="Float">-34.3</Y>
                      <Z dataType="Float">-1</Z>
                    </pos>
                    <posAbs dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">0</X>
                      <Y dataType="Float">-34.3</Y>
                      <Z dataType="Float">-1</Z>
                    </posAbs>
                    <scale dataType="Float">1</scale>
                    <scaleAbs dataType="Float">1</scaleAbs>
                    <vel dataType="Struct" type="Duality.Vector3" />
                    <velAbs dataType="Struct" type="Duality.Vector3" />
                  </item>
                  <item dataType="Struct" type="Player.PlayerController" id="1575548244">
                    <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
                    <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
                    <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">4</X>
                      <Y dataType="Float">0.1</Y>
                      <Z dataType="Float">-0.1</Z>
                    </_x003C_BulletSpawnOffset_x003E_k__BackingField>
                    <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Player.FacingDirection" name="Left" value="0" />
                    <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">300</_x003C_FiringDelay_x003E_k__BackingField>
                    <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
                    <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
                    <_x003C_PlayerAnimationState_x003E_k__BackingField dataType="Enum" type="Player.PlayerAnimationState" name="Idle" value="0" />
                    <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationManager dataType="Struct" type="Manager.AnimationManager" id="3558738014">
                      <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">LemonMan</_x003C_animatedObjectName_x003E_k__BackingField>
                      <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="2099245324">
                        <_items dataType="Array" type="Misc.Animation[]" id="2903414948">
                          <item dataType="Struct" type="Misc.Animation" id="547758276">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">6</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="3392830358">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">4</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.5</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">7</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="1621268864">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">jump</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="3338486306">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">fall</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                        </_items>
                        <_size dataType="Int">4</_size>
                        <_version dataType="Int">4</_version>
                      </_x003C_animationDatabase_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <animationDuration dataType="Float">0</animationDuration>
                      <animationToReturnTo />
                      <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="494962411">
                        <active dataType="Bool">true</active>
                        <animDuration dataType="Float">2</animDuration>
                        <animFirstFrame dataType="Int">6</animFirstFrame>
                        <animFrameCount dataType="Int">4</animFrameCount>
                        <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                        <animPaused dataType="Bool">false</animPaused>
                        <animTime dataType="Float">0</animTime>
                        <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                          <A dataType="Byte">255</A>
                          <B dataType="Byte">255</B>
                          <G dataType="Byte">255</G>
                          <R dataType="Byte">255</R>
                        </colorTint>
                        <customFrameSequence />
                        <customMat />
                        <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                        <gameobj dataType="ObjectRef">787527030</gameobj>
                        <offset dataType="Int">0</offset>
                        <pixelGrid dataType="Bool">false</pixelGrid>
                        <rect dataType="Struct" type="Duality.Rect">
                          <H dataType="Float">26</H>
                          <W dataType="Float">24</W>
                          <X dataType="Float">-12</X>
                          <Y dataType="Float">-13</Y>
                        </rect>
                        <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                        <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                          <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                        </sharedMat>
                        <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                      </animSpriteRenderer>
                      <currentAnimationName />
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                    </animationManager>
                    <firingDelayCounter dataType="Float">0</firingDelayCounter>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <gravity dataType="Float">444.444458</gravity>
                    <heldWeapon dataType="Struct" type="Behavior.HeldWeapon" id="301082255">
                      <active dataType="Bool">true</active>
                      <gameobj dataType="Struct" type="Duality.GameObject" id="672812220">
                        <active dataType="Bool">true</active>
                        <children />
                        <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2119560839">
                          <_items dataType="Array" type="Duality.Component[]" id="3898180430">
                            <item dataType="Struct" type="Duality.Components.Transform" id="3033127152">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">672812220</gameobj>
                            </item>
                            <item dataType="Struct" type="Manager.AnimationManager" id="3444023204">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">672812220</gameobj>
                            </item>
                            <item dataType="ObjectRef">301082255</item>
                            <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="380247601">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">672812220</gameobj>
                            </item>
                          </_items>
                          <_size dataType="Int">4</_size>
                          <_version dataType="Int">4</_version>
                        </compList>
                        <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2569033088" surrogate="true">
                          <header />
                          <body>
                            <keys dataType="Array" type="System.Object[]" id="1200891693">
                              <item dataType="Type" id="1715766886" value="Duality.Components.Transform" />
                              <item dataType="Type" id="198886202" value="Manager.AnimationManager" />
                              <item dataType="Type" id="2584806118" value="Behavior.HeldWeapon" />
                              <item dataType="Type" id="891076794" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                            </keys>
                            <values dataType="Array" type="System.Object[]" id="618863224">
                              <item dataType="ObjectRef">3033127152</item>
                              <item dataType="ObjectRef">3444023204</item>
                              <item dataType="ObjectRef">301082255</item>
                              <item dataType="ObjectRef">380247601</item>
                            </values>
                          </body>
                        </compMap>
                        <compTransform dataType="ObjectRef">3033127152</compTransform>
                        <identifier dataType="Struct" type="System.Guid" surrogate="true">
                          <header>
                            <data dataType="Array" type="System.Byte[]" id="3278327623">ww7ZFsN1MUKIHkuvVynjTg==</data>
                          </header>
                          <body />
                        </identifier>
                        <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                        <name dataType="String">Weapon</name>
                        <parent />
                        <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3862741381">
                          <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2173869076">
                            <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="4285230692" length="4">
                              <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                                <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4208132808">
                                  <_items dataType="Array" type="System.Int32[]" id="143691372"></_items>
                                  <_size dataType="Int">0</_size>
                                  <_version dataType="Int">1</_version>
                                </childIndex>
                                <componentType dataType="ObjectRef">1715766886</componentType>
                                <prop dataType="MemberInfo" id="3163653854" value="P:Duality.Components.Transform:RelativePos" />
                                <val dataType="Struct" type="Duality.Vector3">
                                  <X dataType="Float">-22.4</X>
                                  <Y dataType="Float">0</Y>
                                  <Z dataType="Float">-0.1</Z>
                                </val>
                              </item>
                            </_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">391</_version>
                          </changes>
                          <obj dataType="ObjectRef">672812220</obj>
                          <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                            <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                          </prefab>
                        </prefabLink>
                      </gameobj>
                    </heldWeapon>
                    <jumpVelocity dataType="Float">200</jumpVelocity>
                    <spriteRenderer dataType="ObjectRef">494962411</spriteRenderer>
                    <velocity dataType="Struct" type="Duality.Vector2" />
                  </item>
                  <item dataType="Struct" type="Behavior.EntityStats" id="1425810874">
                    <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">35</_x003C_CurrentHealth_x003E_k__BackingField>
                    <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
                    <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">35</_x003C_MaxHealth_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationManager dataType="ObjectRef">3558738014</animationManager>
                    <damageEffectLength dataType="Float">0.1</damageEffectLength>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                    </material>
                    <timer dataType="Struct" type="Misc.Timer" id="856554177">
                      <_x003C_LengthOfCycle_x003E_k__BackingField dataType="Float">0.1</_x003C_LengthOfCycle_x003E_k__BackingField>
                      <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                      <timerStart dataType="Bool">false</timerStart>
                    </timer>
                  </item>
                  <item dataType="ObjectRef">494962411</item>
                  <item dataType="ObjectRef">3558738014</item>
                  <item dataType="Struct" type="Manager.WeaponSpawner" id="1877854119">
                    <active dataType="Bool">true</active>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <heldWeaponPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                    </heldWeaponPrefab>
                  </item>
                  <item dataType="Struct" type="Behavior.Controller2D" id="3211926186">
                    <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                    <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="Player" value="0" />
                    <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
                    <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
                    <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">14.4</X>
                      <Y dataType="Float">10.12</Y>
                    </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
                    <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">7.4</X>
                      <Y dataType="Float">10</Y>
                    </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
                    <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <bounds dataType="Struct" type="Duality.Rect">
                      <H dataType="Float">15.88</H>
                      <W dataType="Float">9.6</W>
                      <X dataType="Float">-4.6</X>
                      <Y dataType="Float">-37.3</Y>
                    </bounds>
                    <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
                    <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <horizontalRaySpacing dataType="Float">7.925</horizontalRaySpacing>
                    <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
                    <verticalRaySpacing dataType="Float">4.78500032</verticalRaySpacing>
                  </item>
                  <item dataType="ObjectRef">856554177</item>
                </_items>
                <_size dataType="Int">8</_size>
                <_version dataType="Int">24</_version>
              </compList>
              <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="200955507" surrogate="true">
                <header />
                <body>
                  <keys dataType="Array" type="System.Object[]" id="4117914532">
                    <item dataType="ObjectRef">1715766886</item>
                    <item dataType="ObjectRef">891076794</item>
                    <item dataType="Type" id="533349572" value="Player.PlayerController" />
                    <item dataType="Type" id="4154674070" value="Behavior.EntityStats" />
                    <item dataType="ObjectRef">198886202</item>
                    <item dataType="Type" id="808633728" value="Manager.WeaponSpawner" />
                    <item dataType="Type" id="2066733602" value="Behavior.Controller2D" />
                    <item dataType="Type" id="2343907548" value="Misc.Timer" />
                  </keys>
                  <values dataType="Array" type="System.Object[]" id="1922096918">
                    <item dataType="ObjectRef">3147841962</item>
                    <item dataType="ObjectRef">494962411</item>
                    <item dataType="ObjectRef">1575548244</item>
                    <item dataType="ObjectRef">1425810874</item>
                    <item dataType="ObjectRef">3558738014</item>
                    <item dataType="ObjectRef">1877854119</item>
                    <item dataType="ObjectRef">3211926186</item>
                    <item dataType="ObjectRef">856554177</item>
                  </values>
                </body>
              </compMap>
              <compTransform dataType="ObjectRef">3147841962</compTransform>
              <identifier dataType="Struct" type="System.Guid" surrogate="true">
                <header>
                  <data dataType="Array" type="System.Byte[]" id="3123626656">UPF8jIwOT0SjSKyQzVYuaQ==</data>
                </header>
                <body />
              </identifier>
              <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
              <name dataType="String">Player</name>
              <parent />
              <prefabLink />
            </_x003C_PlayerObject_x003E_k__BackingField>
            <_x003C_ZoomLevel_x003E_k__BackingField dataType="Int">-160</_x003C_ZoomLevel_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="81641738" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3606603832">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="Type" id="3815977580" value="Duality.Components.Camera" />
            <item dataType="Type" id="605605942" value="Duality.Components.SoundListener" />
            <item dataType="Type" id="2748295992" value="Camera.CameraFollow" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1649367774">
            <item dataType="ObjectRef">576870688</item>
            <item dataType="ObjectRef">3048798859</item>
            <item dataType="ObjectRef">3165004423</item>
            <item dataType="ObjectRef">1051768081</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">576870688</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="4262995556">S42dOS8++kGkGJLsYQCCXQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">MainCamera</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="ObjectRef">787527030</item>
    <item dataType="Struct" type="Duality.GameObject" id="2834260959">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2398124317">
        <_items dataType="Array" type="Duality.Component[]" id="2201392358" length="4">
          <item dataType="Struct" type="Manager.WeaponDatabaseManager" id="3611639126">
            <_x003C_weaponDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Weapon]]" id="1634047002">
              <_items dataType="Array" type="Misc.Weapon[]" id="3350915456" length="4">
                <item dataType="Struct" type="Misc.Weapon" id="591297948">
                  <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">27</_x003C_BottomLeft_x003E_k__BackingField>
                  <_x003C_BottomRight_x003E_k__BackingField dataType="Float">12</_x003C_BottomRight_x003E_k__BackingField>
                  <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                  <_x003C_ID_x003E_k__BackingField dataType="Int">0</_x003C_ID_x003E_k__BackingField>
                  <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">3</_x003C_Inaccuracy_x003E_k__BackingField>
                  <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                  <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                  <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">3</_x003C_RateOfFire_x003E_k__BackingField>
                  <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                  <_x003C_Slug_x003E_k__BackingField dataType="String">peastol</_x003C_Slug_x003E_k__BackingField>
                  <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\peastol.Texture.res</contentPath>
                  </_x003C_Sprite_x003E_k__BackingField>
                  <_x003C_Title_x003E_k__BackingField dataType="String">Peastol</_x003C_Title_x003E_k__BackingField>
                  <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-13.5</_x003C_TopLeft_x003E_k__BackingField>
                  <_x003C_TopRight_x003E_k__BackingField dataType="Float">-6</_x003C_TopRight_x003E_k__BackingField>
                  <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                  <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                  <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Weapon" id="3814594582">
                  <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">33</_x003C_BottomLeft_x003E_k__BackingField>
                  <_x003C_BottomRight_x003E_k__BackingField dataType="Float">18</_x003C_BottomRight_x003E_k__BackingField>
                  <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                  <_x003C_ID_x003E_k__BackingField dataType="Int">1</_x003C_ID_x003E_k__BackingField>
                  <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">5</_x003C_Inaccuracy_x003E_k__BackingField>
                  <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">17.9</_x003C_PrefabXOffset_x003E_k__BackingField>
                  <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                  <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">5</_x003C_RateOfFire_x003E_k__BackingField>
                  <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Heavy" value="2" />
                  <_x003C_Slug_x003E_k__BackingField dataType="String">lemonade_launcher</_x003C_Slug_x003E_k__BackingField>
                  <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\lemonade_launcher.Texture.res</contentPath>
                  </_x003C_Sprite_x003E_k__BackingField>
                  <_x003C_Title_x003E_k__BackingField dataType="String">Lemonade Launcher</_x003C_Title_x003E_k__BackingField>
                  <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-16.5</_x003C_TopLeft_x003E_k__BackingField>
                  <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9</_x003C_TopRight_x003E_k__BackingField>
                  <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Lemonade" value="1" />
                  <_x003C_XOffset_x003E_k__BackingField dataType="Float">8.45</_x003C_XOffset_x003E_k__BackingField>
                  <_x003C_YOffset_x003E_k__BackingField dataType="Float">2</_x003C_YOffset_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Weapon" id="4133481480">
                  <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">29</_x003C_BottomLeft_x003E_k__BackingField>
                  <_x003C_BottomRight_x003E_k__BackingField dataType="Float">19</_x003C_BottomRight_x003E_k__BackingField>
                  <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                  <_x003C_ID_x003E_k__BackingField dataType="Int">2</_x003C_ID_x003E_k__BackingField>
                  <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">0</_x003C_Inaccuracy_x003E_k__BackingField>
                  <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                  <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">2.8</_x003C_PrefabYOffset_x003E_k__BackingField>
                  <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">0.5</_x003C_RateOfFire_x003E_k__BackingField>
                  <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                  <_x003C_Slug_x003E_k__BackingField dataType="String">pepper_thrower</_x003C_Slug_x003E_k__BackingField>
                  <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\pepper_thrower.Texture.res</contentPath>
                  </_x003C_Sprite_x003E_k__BackingField>
                  <_x003C_Title_x003E_k__BackingField dataType="String">Pepper Thrower</_x003C_Title_x003E_k__BackingField>
                  <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-14.5</_x003C_TopLeft_x003E_k__BackingField>
                  <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9.5</_x003C_TopRight_x003E_k__BackingField>
                  <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pepper" value="2" />
                  <_x003C_XOffset_x003E_k__BackingField dataType="Float">14.5</_x003C_XOffset_x003E_k__BackingField>
                  <_x003C_YOffset_x003E_k__BackingField dataType="Float">2.4</_x003C_YOffset_x003E_k__BackingField>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </_x003C_weaponDatabase_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2834260959</gameobj>
          </item>
        </_items>
        <_size dataType="Int">1</_size>
        <_version dataType="Int">11</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1283085048" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3464028791">
            <item dataType="Type" id="2364103054" value="Manager.WeaponDatabaseManager" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1109499200">
            <item dataType="ObjectRef">3611639126</item>
          </values>
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3936879317">biwph8Z5kk+28lYMuemNIw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">WorldManagers</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1366558956">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="534897058">
        <_items dataType="Array" type="Duality.Component[]" id="95631632" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="3726873888">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">1366558956</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-0.07501602</X>
              <Y dataType="Float">65</Y>
              <Z dataType="Float">1.6</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-0.07501602</X>
              <Y dataType="Float">65</Y>
              <Z dataType="Float">1.6</Z>
            </posAbs>
            <scale dataType="Float">50</scale>
            <scaleAbs dataType="Float">50</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="3008725524">
            <active dataType="Bool">true</active>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">223</B>
              <G dataType="Byte">96</G>
              <R dataType="Byte">249</R>
            </colorTint>
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">1366558956</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">16</H>
              <W dataType="Float">160</W>
              <X dataType="Float">-80</X>
              <Y dataType="Float">-8</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Platform.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="600875274" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="268923960">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="Type" id="2652621420" value="Duality.Components.Renderers.SpriteRenderer" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="3587396318">
            <item dataType="ObjectRef">3726873888</item>
            <item dataType="ObjectRef">3008725524</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3726873888</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2372010596">/Eu7es8On0O4YyXmTJvmjw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Background</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="ObjectRef">672812220</item>
    <item dataType="Struct" type="Duality.GameObject" id="876377208">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3412445086">
        <_items dataType="Array" type="Duality.Component[]" id="3098904464">
          <item dataType="Struct" type="Duality.Components.Transform" id="3236692140">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">876377208</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">85</X>
              <Y dataType="Float">-34</Y>
              <Z dataType="Float">0</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">85</X>
              <Y dataType="Float">-34</Y>
              <Z dataType="Float">0</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="583812589">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">5</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">51</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">876377208</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">39</H>
              <W dataType="Float">39</W>
              <X dataType="Float">-19.5</X>
              <Y dataType="Float">-19.5</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3647588192">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">greenAppowl</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="1653798032">
              <_items dataType="Array" type="Misc.Animation[]" id="3934033212" length="8">
                <item dataType="Struct" type="Misc.Animation" id="3644432196">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">14</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1.12</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="3136728726">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">startRun</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="3433924352">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">9</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.72</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="1358882850">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">12</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.96</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">death</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">34</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="578305116">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">10</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">deathStill</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">45</_x003C_StartFrame_x003E_k__BackingField>
                </item>
              </_items>
              <_size dataType="Int">5</_size>
              <_version dataType="Int">5</_version>
            </_x003C_animationDatabase_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer dataType="ObjectRef">583812589</animSpriteRenderer>
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">876377208</gameobj>
          </item>
          <item dataType="Struct" type="Enemy.EnemyController" id="3129822232">
            <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
            <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
            <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">4</X>
              <Y dataType="Float">0.1</Y>
              <Z dataType="Float">-0.1</Z>
            </_x003C_BulletSpawnOffset_x003E_k__BackingField>
            <_x003C_EnemyAnimationState_x003E_k__BackingField dataType="Enum" type="Enemy.EnemyAnimationState" name="Idle" value="0" />
            <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
            <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">10</_x003C_FiringDelay_x003E_k__BackingField>
            <_x003C_HorizontalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.HorizontalMovement" name="None" value="2" />
            <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
            <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
            <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
            <_x003C_VerticalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.VerticalMovement" name="None" value="2" />
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3647588192</animationManager>
            <checkLeftMovement dataType="Bool">false</checkLeftMovement>
            <checkRightMovement dataType="Bool">false</checkRightMovement>
            <currentXPosition dataType="Float">0</currentXPosition>
            <entityStats dataType="Struct" type="Behavior.EntityStats" id="1514661052">
              <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">100</_x003C_CurrentHealth_x003E_k__BackingField>
              <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
              <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">100</_x003C_MaxHealth_x003E_k__BackingField>
              <active dataType="Bool">true</active>
              <animationManager dataType="ObjectRef">3647588192</animationManager>
              <damageEffectLength dataType="Float">0.1</damageEffectLength>
              <gameobj dataType="ObjectRef">876377208</gameobj>
              <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
              </material>
              <timer dataType="Struct" type="Misc.Timer" id="945404355">
                <_x003C_LengthOfCycle_x003E_k__BackingField dataType="Float">0.1</_x003C_LengthOfCycle_x003E_k__BackingField>
                <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <currentTime dataType="Float">0</currentTime>
                <gameobj dataType="ObjectRef">876377208</gameobj>
                <timerStart dataType="Bool">false</timerStart>
              </timer>
            </entityStats>
            <firingDelayCounter dataType="Float">0</firingDelayCounter>
            <gameobj dataType="ObjectRef">876377208</gameobj>
            <gravity dataType="Float">444.444458</gravity>
            <heldWeapon dataType="ObjectRef">301082255</heldWeapon>
            <jumpVelocity dataType="Float">200</jumpVelocity>
            <shapeInfo />
            <spriteRenderer dataType="ObjectRef">583812589</spriteRenderer>
            <targetXPosition dataType="Float">0</targetXPosition>
            <velocity dataType="Struct" type="Duality.Vector2" />
          </item>
          <item dataType="Struct" type="Behavior.Controller2D" id="3300776364">
            <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
            <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="GreenAppowl" value="1" />
            <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
            <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
            <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">14.4</X>
              <Y dataType="Float">17</Y>
            </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
            <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">7.4</X>
              <Y dataType="Float">10</Y>
            </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
            <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <bounds dataType="Struct" type="Duality.Rect">
              <H dataType="Float">22</H>
              <W dataType="Float">24.6</W>
              <X dataType="Float">72.9</X>
              <Y dataType="Float">-43.5</Y>
            </bounds>
            <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
            <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
            <gameobj dataType="ObjectRef">876377208</gameobj>
            <horizontalRaySpacing dataType="Float">10.985</horizontalRaySpacing>
            <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
            <verticalRaySpacing dataType="Float">12.285</verticalRaySpacing>
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3939153732">
            <active dataType="Bool">true</active>
            <angularDamp dataType="Float">0.3</angularDamp>
            <angularVel dataType="Float">0</angularVel>
            <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
            <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
            <colFilter />
            <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
            <continous dataType="Bool">false</continous>
            <explicitInertia dataType="Float">0</explicitInertia>
            <explicitMass dataType="Float">0</explicitMass>
            <fixedAngle dataType="Bool">true</fixedAngle>
            <gameobj dataType="ObjectRef">876377208</gameobj>
            <ignoreGravity dataType="Bool">true</ignoreGravity>
            <joints />
            <linearDamp dataType="Float">0.3</linearDamp>
            <linearVel dataType="Struct" type="Duality.Vector2" />
            <revolutions dataType="Float">0</revolutions>
            <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="4117605804">
              <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3573857508">
                <item dataType="Struct" type="Duality.Components.Physics.CircleShapeInfo" id="2423195588">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">3939153732</parent>
                  <position dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-1</X>
                    <Y dataType="Float">2</Y>
                  </position>
                  <radius dataType="Float">8.944272</radius>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">1</_version>
            </shapes>
          </item>
          <item dataType="ObjectRef">945404355</item>
          <item dataType="ObjectRef">1514661052</item>
        </_items>
        <_size dataType="Int">8</_size>
        <_version dataType="Int">12</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2114992522" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1358371516">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="Type" id="1508246084" value="Enemy.EnemyController" />
            <item dataType="ObjectRef">2066733602</item>
            <item dataType="Type" id="4046539414" value="Duality.Components.Physics.RigidBody" />
            <item dataType="ObjectRef">2343907548</item>
            <item dataType="ObjectRef">4154674070</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="4227203734">
            <item dataType="ObjectRef">3236692140</item>
            <item dataType="ObjectRef">583812589</item>
            <item dataType="ObjectRef">3647588192</item>
            <item dataType="ObjectRef">3129822232</item>
            <item dataType="ObjectRef">3300776364</item>
            <item dataType="ObjectRef">3939153732</item>
            <item dataType="ObjectRef">945404355</item>
            <item dataType="ObjectRef">1514661052</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3236692140</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="560977512">JT+kx5gynEuOOMFOLMhhwQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">GreenAppowl</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="674216840">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1466994702">
        <_items dataType="Array" type="Duality.Component[]" id="2093177808" length="8">
          <item dataType="Struct" type="Duality.Plugins.Tilemaps.Tilemap" id="2676068857">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <tileData dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapData" id="2182523037" custom="true">
              <body>
                <version dataType="Int">3</version>
                <data dataType="Array" type="System.Byte[]" id="2668734182">H4sIAAAAAAAEAO2WSwqAMAxE4xcr6soDeP9TVmw2QhlKoNipDZS3C/MgNLlE5LzfIk+tAUMFeBs1PzY0P25Q+2l1ANR+Y8AE4Jj9toAdwOqnFZ2J3hg3R0/rfKLRmI2p03vm90OjcRhTp/cs6f/Mkbokv2/R/LiB/NBdwIKony5FdBewILrfXXX4iV/CQccJXbQevoj6i4gRAAA=</data>
              </body>
            </tileData>
            <tileset dataType="Struct" type="Duality.ContentRef`1[[Duality.Plugins.Tilemaps.Tileset]]">
              <contentPath dataType="String">Data\Tilesets\Grasslands.Tileset.res</contentPath>
            </tileset>
          </item>
          <item dataType="Struct" type="Duality.Components.Transform" id="3034531772">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapRenderer" id="3667292288">
            <active dataType="Bool">true</active>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <externalTilemap />
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <offset dataType="Float">0</offset>
            <origin dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
            <tileDepthMode dataType="Enum" type="Duality.Plugins.Tilemaps.TileDepthOffsetMode" name="Flat" value="0" />
            <tileDepthOffset dataType="Int">0</tileDepthOffset>
            <tileDepthScale dataType="Float">0.01</tileDepthScale>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3736993364">
            <active dataType="Bool">true</active>
            <angularDamp dataType="Float">0.3</angularDamp>
            <angularVel dataType="Float">0</angularVel>
            <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
            <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
            <colFilter />
            <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
            <continous dataType="Bool">false</continous>
            <explicitInertia dataType="Float">0</explicitInertia>
            <explicitMass dataType="Float">0.01</explicitMass>
            <fixedAngle dataType="Bool">true</fixedAngle>
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <ignoreGravity dataType="Bool">true</ignoreGravity>
            <joints />
            <linearDamp dataType="Float">0.3</linearDamp>
            <linearVel dataType="Struct" type="Duality.Vector2" />
            <revolutions dataType="Float">0</revolutions>
            <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3755750940">
              <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2103280068" length="16">
                <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="246139204">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">3736993364</parent>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                  <vertices dataType="Array" type="Duality.Vector2[]" id="100213316">
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-64</X>
                      <Y dataType="Float">0</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">16</X>
                      <Y dataType="Float">0</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">16</X>
                      <Y dataType="Float">32</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-64</X>
                      <Y dataType="Float">32</Y>
                    </item>
                  </vertices>
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3352994454">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">3736993364</parent>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                  <vertices dataType="Array" type="Duality.Vector2[]" id="2865938638">
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-160</X>
                      <Y dataType="Float">-16</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-80</X>
                      <Y dataType="Float">-16</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-80</X>
                      <Y dataType="Float">16</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-160</X>
                      <Y dataType="Float">16</Y>
                    </item>
                  </vertices>
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2346850560">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">3736993364</parent>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                  <vertices dataType="Array" type="Duality.Vector2[]" id="2776959560">
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">32</X>
                      <Y dataType="Float">-16</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">112</X>
                      <Y dataType="Float">-16</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">112</X>
                      <Y dataType="Float">16</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">32</X>
                      <Y dataType="Float">16</Y>
                    </item>
                  </vertices>
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3180540962">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">3736993364</parent>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                  <vertices dataType="Array" type="Duality.Vector2[]" id="3844247314">
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">144</X>
                      <Y dataType="Float">-64</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">256</X>
                      <Y dataType="Float">-64</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">256</X>
                      <Y dataType="Float">-32</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">144</X>
                      <Y dataType="Float">-32</Y>
                    </item>
                  </vertices>
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3579710044">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">3736993364</parent>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                  <vertices dataType="Array" type="Duality.Vector2[]" id="1003766796">
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-256</X>
                      <Y dataType="Float">96</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">256</X>
                      <Y dataType="Float">96</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">256</X>
                      <Y dataType="Float">160</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-256</X>
                      <Y dataType="Float">160</Y>
                    </item>
                  </vertices>
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2303921918">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">3736993364</parent>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                  <vertices dataType="Array" type="Duality.Vector2[]" id="1807637030">
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-224</X>
                      <Y dataType="Float">96</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-256</X>
                      <Y dataType="Float">96</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-256</X>
                      <Y dataType="Float">-161</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-224</X>
                      <Y dataType="Float">-160</Y>
                    </item>
                  </vertices>
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2665595064">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">3736993364</parent>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                  <vertices dataType="Array" type="Duality.Vector2[]" id="887720304">
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">256</X>
                      <Y dataType="Float">96</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">224.25</X>
                      <Y dataType="Float">96</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">224.25</X>
                      <Y dataType="Float">-160</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">256.25</X>
                      <Y dataType="Float">-160</Y>
                    </item>
                  </vertices>
                </item>
              </_items>
              <_size dataType="Int">7</_size>
              <_version dataType="Int">881</_version>
            </shapes>
          </item>
          <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollider" id="3083478055">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <origin dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
            <roundedCorners dataType="Bool">false</roundedCorners>
            <shapeFriction dataType="Float">0.299999982</shapeFriction>
            <shapeRestitution dataType="Float">0.299999982</shapeRestitution>
            <solidOuterEdges dataType="Bool">true</solidOuterEdges>
            <source dataType="Array" type="Duality.Plugins.Tilemaps.TilemapCollisionSource[]" id="3358612099">
              <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollisionSource">
                <Layers dataType="Enum" type="Duality.Plugins.Tilemaps.TileCollisionLayer" name="Layer0" value="1" />
                <SourceTilemap />
              </item>
            </source>
          </item>
        </_items>
        <_size dataType="Int">5</_size>
        <_version dataType="Int">5</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1993390922" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1140621772">
            <item dataType="Type" id="2476887204" value="Duality.Plugins.Tilemaps.Tilemap" />
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="Type" id="141779734" value="Duality.Plugins.Tilemaps.TilemapRenderer" />
            <item dataType="ObjectRef">4046539414</item>
            <item dataType="Type" id="1229872032" value="Duality.Plugins.Tilemaps.TilemapCollider" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="540318454">
            <item dataType="ObjectRef">2676068857</item>
            <item dataType="ObjectRef">3034531772</item>
            <item dataType="ObjectRef">3667292288</item>
            <item dataType="ObjectRef">3736993364</item>
            <item dataType="ObjectRef">3083478055</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3034531772</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="342318552">EPthak7nzUCHnaWjvJd8VQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Map</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3592681272">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2685700062">
        <_items dataType="Array" type="Duality.Component[]" id="2526261520">
          <item dataType="Struct" type="Duality.Components.Transform" id="1658028908">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">3592681272</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3300116653">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">3592681272</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="2068924960">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">3592681272</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="989640789">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">2068924960</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">3592681272</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2609846538" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1917813756">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="Type" id="537754436" value="Behavior.PeaHit" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="703389590">
            <item dataType="ObjectRef">1658028908</item>
            <item dataType="ObjectRef">3300116653</item>
            <item dataType="ObjectRef">2068924960</item>
            <item dataType="ObjectRef">989640789</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1658028908</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3785724584">KLVAfGNXXEKbNf132yPqTA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3917001774">
        <changes />
        <obj dataType="ObjectRef">3592681272</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1893228347">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="459709001">
        <_items dataType="Array" type="Duality.Component[]" id="2017547662">
          <item dataType="Struct" type="Duality.Components.Transform" id="4253543279">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">1893228347</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1600663728">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">1893228347</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="369472035">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">1893228347</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="3585155160">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">369472035</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">1893228347</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3043748160" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1533525507">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="2135355320">
            <item dataType="ObjectRef">4253543279</item>
            <item dataType="ObjectRef">1600663728</item>
            <item dataType="ObjectRef">369472035</item>
            <item dataType="ObjectRef">3585155160</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">4253543279</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3401194281">UhYAHNgdWEm3P3FHqmBfMQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1218951787">
        <changes />
        <obj dataType="ObjectRef">1893228347</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1173548462">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2232214776">
        <_items dataType="Array" type="Duality.Component[]" id="1591348076">
          <item dataType="Struct" type="Duality.Components.Transform" id="3533863394">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">1173548462</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="880983843">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">1173548462</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3944759446">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">1173548462</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="2865475275">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3944759446</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">1173548462</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2909206494" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="574833978">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="1867187898">
            <item dataType="ObjectRef">3533863394</item>
            <item dataType="ObjectRef">880983843</item>
            <item dataType="ObjectRef">3944759446</item>
            <item dataType="ObjectRef">2865475275</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3533863394</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1129048634">oK2P5n0nQUCyqoSd8K+WTw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3385671588">
        <changes />
        <obj dataType="ObjectRef">1173548462</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1008207989">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3198491655">
        <_items dataType="Array" type="Duality.Component[]" id="3140147790">
          <item dataType="Struct" type="Duality.Components.Transform" id="3368522921">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">1008207989</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="715643370">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">1008207989</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3779418973">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">1008207989</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="2700134802">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3779418973</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">1008207989</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3855452288" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2673452461">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="3704674680">
            <item dataType="ObjectRef">3368522921</item>
            <item dataType="ObjectRef">715643370</item>
            <item dataType="ObjectRef">3779418973</item>
            <item dataType="ObjectRef">2700134802</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3368522921</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3657406919">nkZWNE44h06IRjDFGEX3bA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="370028037">
        <changes />
        <obj dataType="ObjectRef">1008207989</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2146683584">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3744723590">
        <_items dataType="Array" type="Duality.Component[]" id="3242041216">
          <item dataType="Struct" type="Duality.Components.Transform" id="212031220">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2146683584</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1854118965">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">2146683584</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="622927272">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">2146683584</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="3838610397">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">622927272</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">2146683584</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="387383098" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1290197236">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="1812674294">
            <item dataType="ObjectRef">212031220</item>
            <item dataType="ObjectRef">1854118965</item>
            <item dataType="ObjectRef">622927272</item>
            <item dataType="ObjectRef">3838610397</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">212031220</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1493420496">9Yz7KgjmmUaPFbMwz94arA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2645378566">
        <changes />
        <obj dataType="ObjectRef">2146683584</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1185622201">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1671295003">
        <_items dataType="Array" type="Duality.Component[]" id="306312854">
          <item dataType="Struct" type="Duality.Components.Transform" id="3545937133">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">1185622201</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="893057582">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">1185622201</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3956833185">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">1185622201</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="2877549014">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3956833185</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">1185622201</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3814934888" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="132044145">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="2867429600">
            <item dataType="ObjectRef">3545937133</item>
            <item dataType="ObjectRef">893057582</item>
            <item dataType="ObjectRef">3956833185</item>
            <item dataType="ObjectRef">2877549014</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3545937133</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3137069859">HGkAtquuvEGbPV7WfsX/GQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1841451729">
        <changes />
        <obj dataType="ObjectRef">1185622201</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2351755121">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1135639059">
        <_items dataType="Array" type="Duality.Component[]" id="2449060582">
          <item dataType="Struct" type="Duality.Components.Transform" id="417102757">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2351755121</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2059190502">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">2351755121</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="827998809">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">2351755121</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="4043681934">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">827998809</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">2351755121</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1348928760" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3979778681">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="3549142400">
            <item dataType="ObjectRef">417102757</item>
            <item dataType="ObjectRef">2059190502</item>
            <item dataType="ObjectRef">827998809</item>
            <item dataType="ObjectRef">4043681934</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">417102757</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3756588923">h6g2tRqMdUedRLWSRr5dgw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1214826233">
        <changes />
        <obj dataType="ObjectRef">2351755121</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="198508874">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="795380244">
        <_items dataType="Array" type="Duality.Component[]" id="140223076">
          <item dataType="Struct" type="Duality.Components.Transform" id="2558823806">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">198508874</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="4200911551">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">198508874</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="2969719858">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">198508874</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="1890435687">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">2969719858</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">198508874</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="137793334" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="4084810942">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="2505093130">
            <item dataType="ObjectRef">2558823806</item>
            <item dataType="ObjectRef">4200911551</item>
            <item dataType="ObjectRef">2969719858</item>
            <item dataType="ObjectRef">1890435687</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2558823806</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="148054734">6k5FcpCA80+hz/mks+9VUw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3721165488">
        <changes />
        <obj dataType="ObjectRef">198508874</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="4060610736">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="4020448278">
        <_items dataType="Array" type="Duality.Component[]" id="767317280">
          <item dataType="Struct" type="Duality.Components.Transform" id="2125958372">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">4060610736</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3768046117">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">4060610736</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="2536854424">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">4060610736</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="1457570253">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">2536854424</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">4060610736</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="815753178" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1074426084">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="1487033878">
            <item dataType="ObjectRef">2125958372</item>
            <item dataType="ObjectRef">3768046117</item>
            <item dataType="ObjectRef">2536854424</item>
            <item dataType="ObjectRef">1457570253</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2125958372</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2595159776">UUN5cHGLfkONnAm76ZZDnw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2357561270">
        <changes />
        <obj dataType="ObjectRef">4060610736</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2106564293">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1871942583">
        <_items dataType="Array" type="Duality.Component[]" id="224827790">
          <item dataType="Struct" type="Duality.Components.Transform" id="171911929">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2106564293</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1813999674">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">2106564293</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="582807981">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">2106564293</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="3798491106">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">582807981</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">2106564293</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1372314944" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="4238047229">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="2920473528">
            <item dataType="ObjectRef">171911929</item>
            <item dataType="ObjectRef">1813999674</item>
            <item dataType="ObjectRef">582807981</item>
            <item dataType="ObjectRef">3798491106</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">171911929</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2061730519">ONiESwvz+0aOaMXVxONkBg==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3130505109">
        <changes />
        <obj dataType="ObjectRef">2106564293</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="515416777">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3563611787">
        <_items dataType="Array" type="Duality.Component[]" id="581155446">
          <item dataType="Struct" type="Duality.Components.Transform" id="2875731709">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">515416777</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="222852158">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">515416777</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3286627761">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">515416777</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="2207343590">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3286627761</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">515416777</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2200588488" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3963915809">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="3938602272">
            <item dataType="ObjectRef">2875731709</item>
            <item dataType="ObjectRef">222852158</item>
            <item dataType="ObjectRef">3286627761</item>
            <item dataType="ObjectRef">2207343590</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2875731709</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3135306675">zOC3Jw9K20K1G3PMFKnJ3g==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3018177601">
        <changes />
        <obj dataType="ObjectRef">515416777</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3339943234">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1178001100">
        <_items dataType="Array" type="Duality.Component[]" id="3510505636">
          <item dataType="Struct" type="Duality.Components.Transform" id="1405290870">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">3339943234</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3047378615">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">3339943234</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="1816186922">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">3339943234</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="736902751">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">1816186922</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">3339943234</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1129631478" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3109920070">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="2546133946">
            <item dataType="ObjectRef">1405290870</item>
            <item dataType="ObjectRef">3047378615</item>
            <item dataType="ObjectRef">1816186922</item>
            <item dataType="ObjectRef">736902751</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1405290870</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3683159878">9zthb3IPcUaoh6ctnVz2xA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1772600536">
        <changes />
        <obj dataType="ObjectRef">3339943234</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="639875823">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3047221517">
        <_items dataType="Array" type="Duality.Component[]" id="24176422">
          <item dataType="Struct" type="Duality.Components.Transform" id="3000190755">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">639875823</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="347311204">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">639875823</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3411086807">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">639875823</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="2331802636">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3411086807</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">639875823</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2454806968" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1008703079">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="2734128768">
            <item dataType="ObjectRef">3000190755</item>
            <item dataType="ObjectRef">347311204</item>
            <item dataType="ObjectRef">3411086807</item>
            <item dataType="ObjectRef">2331802636</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3000190755</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="248090405">RPh/wFwtkEKhk8uxT2kHOw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3278635495">
        <changes />
        <obj dataType="ObjectRef">639875823</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="687867730">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="997966780">
        <_items dataType="Array" type="Duality.Component[]" id="1620692548">
          <item dataType="Struct" type="Duality.Components.Transform" id="3048182662">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">687867730</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="395303111">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">687867730</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3459078714">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">687867730</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="2379794543">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3459078714</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">687867730</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="451059350" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1702520470">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="2813918426">
            <item dataType="ObjectRef">3048182662</item>
            <item dataType="ObjectRef">395303111</item>
            <item dataType="ObjectRef">3459078714</item>
            <item dataType="ObjectRef">2379794543</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3048182662</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="534779702">kU3GS8bKy0qAP6jp7R+LCA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2188941672">
        <changes />
        <obj dataType="ObjectRef">687867730</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="523539291">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2432072489">
        <_items dataType="Array" type="Duality.Component[]" id="2581180430">
          <item dataType="Struct" type="Duality.Components.Transform" id="2883854223">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">523539291</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="230974672">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">523539291</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3294750275">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">523539291</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="2215466104">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3294750275</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">523539291</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="799161792" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="317440675">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="905828728">
            <item dataType="ObjectRef">2883854223</item>
            <item dataType="ObjectRef">230974672</item>
            <item dataType="ObjectRef">3294750275</item>
            <item dataType="ObjectRef">2215466104</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2883854223</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="291726601">s9+WWqK9lEygbPi2jvFsfA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1769818379">
        <changes />
        <obj dataType="ObjectRef">523539291</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2939076110">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1500518552">
        <_items dataType="Array" type="Duality.Component[]" id="3926628908">
          <item dataType="Struct" type="Duality.Components.Transform" id="1004423746">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2939076110</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2646511491">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">2939076110</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="1415319798">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">2939076110</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="336035627">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">1415319798</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">2939076110</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3748750622" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="911987546">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="641051578">
            <item dataType="ObjectRef">1004423746</item>
            <item dataType="ObjectRef">2646511491</item>
            <item dataType="ObjectRef">1415319798</item>
            <item dataType="ObjectRef">336035627</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1004423746</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2820022618">N/kNXSMPc0exQv28sXm2Sg==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3174019780">
        <changes />
        <obj dataType="ObjectRef">2939076110</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="531187803">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3880647209">
        <_items dataType="Array" type="Duality.Component[]" id="1605337614">
          <item dataType="Struct" type="Duality.Components.Transform" id="2891502735">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">531187803</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="238623184">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">0.7</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">531187803</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3302398787">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">531187803</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="2223114616">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3302398787</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">531187803</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3018595264" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3032297379">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="632077176">
            <item dataType="ObjectRef">2891502735</item>
            <item dataType="ObjectRef">238623184</item>
            <item dataType="ObjectRef">3302398787</item>
            <item dataType="ObjectRef">2223114616</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2891502735</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1278370825">sArZjXCNy0Kdex9f9AwQIA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1916554763">
        <changes />
        <obj dataType="ObjectRef">531187803</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Weapons\Pea_Hit.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="990562905">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1954520315">
        <_items dataType="Array" type="Duality.Component[]" id="3970414166">
          <item dataType="Struct" type="Duality.Components.Transform" id="3350877837">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">990562905</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="697998286">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">5</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">4</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">990562905</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">30</H>
              <W dataType="Float">30</W>
              <X dataType="Float">-15</X>
              <Y dataType="Float">-15</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3761773889">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField />
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer />
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">990562905</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.PeaHit" id="2682489718">
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3761773889</animationManager>
            <checkForDelete dataType="Bool">true</checkForDelete>
            <deleteDelay dataType="Float">400</deleteDelay>
            <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
            <gameobj dataType="ObjectRef">990562905</gameobj>
            <targetDeleteTime dataType="Float">400</targetDeleteTime>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3352908712" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2754631953">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">537754436</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="2369847200">
            <item dataType="ObjectRef">3350877837</item>
            <item dataType="ObjectRef">697998286</item>
            <item dataType="ObjectRef">3761773889</item>
            <item dataType="ObjectRef">2682489718</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3350877837</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1527306371">II8YyxK5bkWqm0lxmzs3Fw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Pea_Hit</name>
      <parent />
      <prefabLink />
    </item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
