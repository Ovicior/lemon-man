﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">48.8</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="2511523052">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2647580066">
        <_items dataType="Array" type="Duality.Component[]" id="178379024">
          <item dataType="Struct" type="Duality.Components.Transform" id="576870688">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-300</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">0</Y>
              <Z dataType="Float">-300</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Camera" id="3048798859">
            <active dataType="Bool">true</active>
            <farZ dataType="Float">10000</farZ>
            <focusDist dataType="Float">500</focusDist>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
            <nearZ dataType="Float">0</nearZ>
            <passes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Camera+Pass]]" id="4189425567">
              <_items dataType="Array" type="Duality.Components.Camera+Pass[]" id="2743093614" length="4">
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="2592933968">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="All" value="3" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="PerspectiveWorld" value="0" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="AllGroups" value="2147483647" />
                </item>
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="3724855662">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="None" value="0" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="OrthoScreen" value="1" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </passes>
            <perspective dataType="Enum" type="Duality.Drawing.PerspectiveMode" name="Parallax" value="1" />
            <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
          </item>
          <item dataType="Struct" type="Duality.Components.SoundListener" id="3165004423">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
          </item>
          <item dataType="Struct" type="Camera.CameraFollow" id="1051768081">
            <_x003C_PlayerObject_x003E_k__BackingField dataType="Struct" type="Duality.GameObject" id="787527030">
              <active dataType="Bool">true</active>
              <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="3740027105">
                <_items dataType="Array" type="Duality.GameObject[]" id="3062691694" length="4" />
                <_size dataType="Int">0</_size>
                <_version dataType="Int">2</_version>
              </children>
              <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2174773792">
                <_items dataType="Array" type="Duality.Component[]" id="330374507">
                  <item dataType="Struct" type="Duality.Components.Transform" id="3147841962">
                    <active dataType="Bool">true</active>
                    <angle dataType="Float">0</angle>
                    <angleAbs dataType="Float">0</angleAbs>
                    <angleVel dataType="Float">0</angleVel>
                    <angleVelAbs dataType="Float">0</angleVelAbs>
                    <deriveAngle dataType="Bool">true</deriveAngle>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <ignoreParent dataType="Bool">false</ignoreParent>
                    <parentTransform />
                    <pos dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">0</X>
                      <Y dataType="Float">-34.3</Y>
                      <Z dataType="Float">0</Z>
                    </pos>
                    <posAbs dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">0</X>
                      <Y dataType="Float">-34.3</Y>
                      <Z dataType="Float">0</Z>
                    </posAbs>
                    <scale dataType="Float">1</scale>
                    <scaleAbs dataType="Float">1</scaleAbs>
                    <vel dataType="Struct" type="Duality.Vector3" />
                    <velAbs dataType="Struct" type="Duality.Vector3" />
                  </item>
                  <item dataType="Struct" type="Player.PlayerController" id="1575548244">
                    <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
                    <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
                    <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">4</X>
                      <Y dataType="Float">0.1</Y>
                      <Z dataType="Float">-0.1</Z>
                    </_x003C_BulletSpawnOffset_x003E_k__BackingField>
                    <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Player.FacingDirection" name="Left" value="0" />
                    <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">300</_x003C_FiringDelay_x003E_k__BackingField>
                    <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
                    <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
                    <_x003C_PlayerAnimationState_x003E_k__BackingField dataType="Enum" type="Player.PlayerAnimationState" name="Idle" value="0" />
                    <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationManager dataType="Struct" type="Manager.AnimationManager" id="3558738014">
                      <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">LemonMan</_x003C_animatedObjectName_x003E_k__BackingField>
                      <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="2099245324">
                        <_items dataType="Array" type="Misc.Animation[]" id="2903414948">
                          <item dataType="Struct" type="Misc.Animation" id="547758276">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">6</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="3392830358">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">4</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.5</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">7</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="1621268864">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">jump</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="3338486306">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">fall</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                        </_items>
                        <_size dataType="Int">4</_size>
                        <_version dataType="Int">4</_version>
                      </_x003C_animationDatabase_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <animationLength dataType="Float">0</animationLength>
                      <animationToReturnTo />
                      <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="494962411">
                        <active dataType="Bool">true</active>
                        <animDuration dataType="Float">2</animDuration>
                        <animFirstFrame dataType="Int">6</animFirstFrame>
                        <animFrameCount dataType="Int">4</animFrameCount>
                        <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                        <animPaused dataType="Bool">false</animPaused>
                        <animTime dataType="Float">0</animTime>
                        <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                          <A dataType="Byte">255</A>
                          <B dataType="Byte">255</B>
                          <G dataType="Byte">255</G>
                          <R dataType="Byte">255</R>
                        </colorTint>
                        <customFrameSequence />
                        <customMat />
                        <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                        <gameobj dataType="ObjectRef">787527030</gameobj>
                        <offset dataType="Int">0</offset>
                        <pixelGrid dataType="Bool">false</pixelGrid>
                        <rect dataType="Struct" type="Duality.Rect">
                          <H dataType="Float">26</H>
                          <W dataType="Float">24</W>
                          <X dataType="Float">-12</X>
                          <Y dataType="Float">-13</Y>
                        </rect>
                        <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                        <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                          <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                        </sharedMat>
                        <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                      </animSpriteRenderer>
                      <currentAnimationName />
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                    </animationManager>
                    <firingDelayCounter dataType="Float">0</firingDelayCounter>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <gravity dataType="Float">444.444458</gravity>
                    <heldWeapon dataType="Struct" type="Behavior.HeldWeapon" id="301082255">
                      <active dataType="Bool">true</active>
                      <gameobj dataType="Struct" type="Duality.GameObject" id="672812220">
                        <active dataType="Bool">true</active>
                        <children />
                        <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2119560839">
                          <_items dataType="Array" type="Duality.Component[]" id="3898180430">
                            <item dataType="Struct" type="Duality.Components.Transform" id="3033127152">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">672812220</gameobj>
                            </item>
                            <item dataType="Struct" type="Manager.AnimationManager" id="3444023204">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">672812220</gameobj>
                            </item>
                            <item dataType="ObjectRef">301082255</item>
                            <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="380247601">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">672812220</gameobj>
                            </item>
                          </_items>
                          <_size dataType="Int">4</_size>
                          <_version dataType="Int">4</_version>
                        </compList>
                        <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2569033088" surrogate="true">
                          <header />
                          <body>
                            <keys dataType="Array" type="System.Object[]" id="1200891693">
                              <item dataType="Type" id="1715766886" value="Duality.Components.Transform" />
                              <item dataType="Type" id="198886202" value="Manager.AnimationManager" />
                              <item dataType="Type" id="2584806118" value="Behavior.HeldWeapon" />
                              <item dataType="Type" id="891076794" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                            </keys>
                            <values dataType="Array" type="System.Object[]" id="618863224">
                              <item dataType="ObjectRef">3033127152</item>
                              <item dataType="ObjectRef">3444023204</item>
                              <item dataType="ObjectRef">301082255</item>
                              <item dataType="ObjectRef">380247601</item>
                            </values>
                          </body>
                        </compMap>
                        <compTransform dataType="ObjectRef">3033127152</compTransform>
                        <identifier dataType="Struct" type="System.Guid" surrogate="true">
                          <header>
                            <data dataType="Array" type="System.Byte[]" id="3278327623">ww7ZFsN1MUKIHkuvVynjTg==</data>
                          </header>
                          <body />
                        </identifier>
                        <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                        <name dataType="String">Weapon</name>
                        <parent />
                        <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3862741381">
                          <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2173869076">
                            <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="4285230692" length="4">
                              <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                                <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4208132808">
                                  <_items dataType="Array" type="System.Int32[]" id="143691372"></_items>
                                  <_size dataType="Int">0</_size>
                                  <_version dataType="Int">1</_version>
                                </childIndex>
                                <componentType dataType="ObjectRef">1715766886</componentType>
                                <prop dataType="MemberInfo" id="3163653854" value="P:Duality.Components.Transform:RelativePos" />
                                <val dataType="Struct" type="Duality.Vector3">
                                  <X dataType="Float">-22.4</X>
                                  <Y dataType="Float">0</Y>
                                  <Z dataType="Float">-0.1</Z>
                                </val>
                              </item>
                            </_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">391</_version>
                          </changes>
                          <obj dataType="ObjectRef">672812220</obj>
                          <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                            <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                          </prefab>
                        </prefabLink>
                      </gameobj>
                    </heldWeapon>
                    <jumpVelocity dataType="Float">200</jumpVelocity>
                    <spriteRenderer dataType="ObjectRef">494962411</spriteRenderer>
                    <velocity dataType="Struct" type="Duality.Vector2" />
                  </item>
                  <item dataType="Struct" type="Behavior.EntityStats" id="1425810874">
                    <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">35</_x003C_CurrentHealth_x003E_k__BackingField>
                    <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
                    <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">35</_x003C_MaxHealth_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationManager dataType="ObjectRef">3558738014</animationManager>
                    <currentTime dataType="Float">0</currentTime>
                    <damageAnimationLength dataType="Float">0.3</damageAnimationLength>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                    </material>
                    <timer dataType="Struct" type="Misc.Timer" id="856554177">
                      <_x003C_CycleLength_x003E_k__BackingField dataType="Float">0.3</_x003C_CycleLength_x003E_k__BackingField>
                      <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                      <timerStart dataType="Bool">false</timerStart>
                    </timer>
                  </item>
                  <item dataType="ObjectRef">494962411</item>
                  <item dataType="ObjectRef">3558738014</item>
                  <item dataType="Struct" type="Manager.WeaponSpawner" id="1877854119">
                    <active dataType="Bool">true</active>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <heldWeaponPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                    </heldWeaponPrefab>
                  </item>
                  <item dataType="Struct" type="Behavior.Controller2D" id="3211926186">
                    <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                    <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="Player" value="0" />
                    <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
                    <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
                    <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">14.4</X>
                      <Y dataType="Float">10.12</Y>
                    </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
                    <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">7.4</X>
                      <Y dataType="Float">10</Y>
                    </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
                    <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <bounds dataType="Struct" type="Duality.Rect">
                      <H dataType="Float">15.88</H>
                      <W dataType="Float">9.6</W>
                      <X dataType="Float">-4.6</X>
                      <Y dataType="Float">-37.3</Y>
                    </bounds>
                    <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
                    <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <horizontalRaySpacing dataType="Float">7.925</horizontalRaySpacing>
                    <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
                    <verticalRaySpacing dataType="Float">4.78500032</verticalRaySpacing>
                  </item>
                  <item dataType="ObjectRef">856554177</item>
                </_items>
                <_size dataType="Int">8</_size>
                <_version dataType="Int">24</_version>
              </compList>
              <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="200955507" surrogate="true">
                <header />
                <body>
                  <keys dataType="Array" type="System.Object[]" id="4117914532">
                    <item dataType="ObjectRef">1715766886</item>
                    <item dataType="ObjectRef">891076794</item>
                    <item dataType="Type" id="533349572" value="Player.PlayerController" />
                    <item dataType="Type" id="4154674070" value="Behavior.EntityStats" />
                    <item dataType="ObjectRef">198886202</item>
                    <item dataType="Type" id="808633728" value="Manager.WeaponSpawner" />
                    <item dataType="Type" id="2066733602" value="Behavior.Controller2D" />
                    <item dataType="Type" id="2343907548" value="Misc.Timer" />
                  </keys>
                  <values dataType="Array" type="System.Object[]" id="1922096918">
                    <item dataType="ObjectRef">3147841962</item>
                    <item dataType="ObjectRef">494962411</item>
                    <item dataType="ObjectRef">1575548244</item>
                    <item dataType="ObjectRef">1425810874</item>
                    <item dataType="ObjectRef">3558738014</item>
                    <item dataType="ObjectRef">1877854119</item>
                    <item dataType="ObjectRef">3211926186</item>
                    <item dataType="ObjectRef">856554177</item>
                  </values>
                </body>
              </compMap>
              <compTransform dataType="ObjectRef">3147841962</compTransform>
              <identifier dataType="Struct" type="System.Guid" surrogate="true">
                <header>
                  <data dataType="Array" type="System.Byte[]" id="3123626656">UPF8jIwOT0SjSKyQzVYuaQ==</data>
                </header>
                <body />
              </identifier>
              <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
              <name dataType="String">Player</name>
              <parent />
              <prefabLink />
            </_x003C_PlayerObject_x003E_k__BackingField>
            <_x003C_ZoomLevel_x003E_k__BackingField dataType="Int">-160</_x003C_ZoomLevel_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="81641738" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3606603832">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="Type" id="3815977580" value="Duality.Components.Camera" />
            <item dataType="Type" id="605605942" value="Duality.Components.SoundListener" />
            <item dataType="Type" id="2748295992" value="Camera.CameraFollow" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1649367774">
            <item dataType="ObjectRef">576870688</item>
            <item dataType="ObjectRef">3048798859</item>
            <item dataType="ObjectRef">3165004423</item>
            <item dataType="ObjectRef">1051768081</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">576870688</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="4262995556">S42dOS8++kGkGJLsYQCCXQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">MainCamera</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="13729924">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1271375162">
        <_items dataType="Array" type="Duality.Component[]" id="1424437504" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="2374044856">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">13729924</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">12</Y>
              <Z dataType="Float">0.2</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0</X>
              <Y dataType="Float">12</Y>
              <Z dataType="Float">0.2</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="1655896492">
            <active dataType="Bool">true</active>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">13729924</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">16</H>
              <W dataType="Float">160</W>
              <X dataType="Float">-80</X>
              <Y dataType="Float">-8</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Platform.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3076506448">
            <active dataType="Bool">true</active>
            <angularDamp dataType="Float">0.3</angularDamp>
            <angularVel dataType="Float">0</angularVel>
            <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
            <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
            <colFilter />
            <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
            <continous dataType="Bool">false</continous>
            <explicitInertia dataType="Float">0</explicitInertia>
            <explicitMass dataType="Float">0</explicitMass>
            <fixedAngle dataType="Bool">true</fixedAngle>
            <gameobj dataType="ObjectRef">13729924</gameobj>
            <ignoreGravity dataType="Bool">true</ignoreGravity>
            <joints />
            <linearDamp dataType="Float">0.3</linearDamp>
            <linearVel dataType="Struct" type="Duality.Vector2" />
            <revolutions dataType="Float">0</revolutions>
            <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="31240448">
              <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2961683100" length="4">
                <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2427157444">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">3076506448</parent>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                  <vertices dataType="Array" type="Duality.Vector2[]" id="551759172">
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-80</X>
                      <Y dataType="Float">-8</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">80</X>
                      <Y dataType="Float">-8</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">80</X>
                      <Y dataType="Float">8</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-80</X>
                      <Y dataType="Float">8</Y>
                    </item>
                  </vertices>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">9</_version>
            </shapes>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2873476794" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1239920256">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="Type" id="2606366108" value="Duality.Components.Renderers.SpriteRenderer" />
            <item dataType="Type" id="3875346454" value="Duality.Components.Physics.RigidBody" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="3394408654">
            <item dataType="ObjectRef">2374044856</item>
            <item dataType="ObjectRef">1655896492</item>
            <item dataType="ObjectRef">3076506448</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2374044856</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="4252642844">GMCuwH+1UECfDHU4lEiqzA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Platform</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="ObjectRef">787527030</item>
    <item dataType="Struct" type="Duality.GameObject" id="2834260959">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2398124317">
        <_items dataType="Array" type="Duality.Component[]" id="2201392358" length="4">
          <item dataType="Struct" type="Manager.WeaponDatabaseManager" id="3611639126">
            <_x003C_weaponDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Weapon]]" id="1634047002">
              <_items dataType="Array" type="Misc.Weapon[]" id="3350915456" length="4">
                <item dataType="Struct" type="Misc.Weapon" id="591297948">
                  <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">27</_x003C_BottomLeft_x003E_k__BackingField>
                  <_x003C_BottomRight_x003E_k__BackingField dataType="Float">12</_x003C_BottomRight_x003E_k__BackingField>
                  <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                  <_x003C_ID_x003E_k__BackingField dataType="Int">0</_x003C_ID_x003E_k__BackingField>
                  <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">3</_x003C_Inaccuracy_x003E_k__BackingField>
                  <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                  <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                  <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">3</_x003C_RateOfFire_x003E_k__BackingField>
                  <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                  <_x003C_Slug_x003E_k__BackingField dataType="String">peastol</_x003C_Slug_x003E_k__BackingField>
                  <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\peastol.Texture.res</contentPath>
                  </_x003C_Sprite_x003E_k__BackingField>
                  <_x003C_Title_x003E_k__BackingField dataType="String">Peastol</_x003C_Title_x003E_k__BackingField>
                  <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-13.5</_x003C_TopLeft_x003E_k__BackingField>
                  <_x003C_TopRight_x003E_k__BackingField dataType="Float">-6</_x003C_TopRight_x003E_k__BackingField>
                  <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                  <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                  <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Weapon" id="3814594582">
                  <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">33</_x003C_BottomLeft_x003E_k__BackingField>
                  <_x003C_BottomRight_x003E_k__BackingField dataType="Float">18</_x003C_BottomRight_x003E_k__BackingField>
                  <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                  <_x003C_ID_x003E_k__BackingField dataType="Int">1</_x003C_ID_x003E_k__BackingField>
                  <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">5</_x003C_Inaccuracy_x003E_k__BackingField>
                  <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">17.9</_x003C_PrefabXOffset_x003E_k__BackingField>
                  <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                  <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">5</_x003C_RateOfFire_x003E_k__BackingField>
                  <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Heavy" value="2" />
                  <_x003C_Slug_x003E_k__BackingField dataType="String">lemonade_launcher</_x003C_Slug_x003E_k__BackingField>
                  <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\lemonade_launcher.Texture.res</contentPath>
                  </_x003C_Sprite_x003E_k__BackingField>
                  <_x003C_Title_x003E_k__BackingField dataType="String">Lemonade Launcher</_x003C_Title_x003E_k__BackingField>
                  <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-16.5</_x003C_TopLeft_x003E_k__BackingField>
                  <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9</_x003C_TopRight_x003E_k__BackingField>
                  <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Lemonade" value="1" />
                  <_x003C_XOffset_x003E_k__BackingField dataType="Float">8.45</_x003C_XOffset_x003E_k__BackingField>
                  <_x003C_YOffset_x003E_k__BackingField dataType="Float">2</_x003C_YOffset_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Weapon" id="4133481480">
                  <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">29</_x003C_BottomLeft_x003E_k__BackingField>
                  <_x003C_BottomRight_x003E_k__BackingField dataType="Float">19</_x003C_BottomRight_x003E_k__BackingField>
                  <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                  <_x003C_ID_x003E_k__BackingField dataType="Int">2</_x003C_ID_x003E_k__BackingField>
                  <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">0</_x003C_Inaccuracy_x003E_k__BackingField>
                  <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                  <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">2.8</_x003C_PrefabYOffset_x003E_k__BackingField>
                  <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">0.5</_x003C_RateOfFire_x003E_k__BackingField>
                  <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                  <_x003C_Slug_x003E_k__BackingField dataType="String">pepper_thrower</_x003C_Slug_x003E_k__BackingField>
                  <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\pepper_thrower.Texture.res</contentPath>
                  </_x003C_Sprite_x003E_k__BackingField>
                  <_x003C_Title_x003E_k__BackingField dataType="String">Pepper Thrower</_x003C_Title_x003E_k__BackingField>
                  <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-14.5</_x003C_TopLeft_x003E_k__BackingField>
                  <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9.5</_x003C_TopRight_x003E_k__BackingField>
                  <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pepper" value="2" />
                  <_x003C_XOffset_x003E_k__BackingField dataType="Float">14.5</_x003C_XOffset_x003E_k__BackingField>
                  <_x003C_YOffset_x003E_k__BackingField dataType="Float">2.4</_x003C_YOffset_x003E_k__BackingField>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </_x003C_weaponDatabase_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2834260959</gameobj>
          </item>
        </_items>
        <_size dataType="Int">1</_size>
        <_version dataType="Int">11</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1283085048" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3464028791">
            <item dataType="Type" id="2364103054" value="Manager.WeaponDatabaseManager" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1109499200">
            <item dataType="ObjectRef">3611639126</item>
          </values>
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3936879317">biwph8Z5kk+28lYMuemNIw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">WorldManagers</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1366558956">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="534897058">
        <_items dataType="Array" type="Duality.Component[]" id="95631632" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="3726873888">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">1366558956</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-0.07501602</X>
              <Y dataType="Float">-28.7749538</Y>
              <Z dataType="Float">1.6</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-0.07501602</X>
              <Y dataType="Float">-28.7749538</Y>
              <Z dataType="Float">1.6</Z>
            </posAbs>
            <scale dataType="Float">12.1</scale>
            <scaleAbs dataType="Float">12.1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="3008725524">
            <active dataType="Bool">true</active>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">223</B>
              <G dataType="Byte">96</G>
              <R dataType="Byte">249</R>
            </colorTint>
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">1366558956</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">16</H>
              <W dataType="Float">160</W>
              <X dataType="Float">-80</X>
              <Y dataType="Float">-8</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Platform.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="600875274" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="268923960">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">2606366108</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="3587396318">
            <item dataType="ObjectRef">3726873888</item>
            <item dataType="ObjectRef">3008725524</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3726873888</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2372010596">/Eu7es8On0O4YyXmTJvmjw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Background</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1171716376">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="91919102">
        <_items dataType="Array" type="Duality.Component[]" id="1397143952" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="3532031308">
            <active dataType="Bool">true</active>
            <angle dataType="Float">4.712389</angle>
            <angleAbs dataType="Float">4.712389</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">1171716376</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-87.599</X>
              <Y dataType="Float">-60</Y>
              <Z dataType="Float">0.2</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-87.599</X>
              <Y dataType="Float">-60</Y>
              <Z dataType="Float">0.2</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="2813882944">
            <active dataType="Bool">true</active>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">1171716376</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">16</H>
              <W dataType="Float">160</W>
              <X dataType="Float">-80</X>
              <Y dataType="Float">-8</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Platform.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="4234492900">
            <active dataType="Bool">true</active>
            <angularDamp dataType="Float">0.3</angularDamp>
            <angularVel dataType="Float">0</angularVel>
            <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
            <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
            <colFilter />
            <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
            <continous dataType="Bool">false</continous>
            <explicitInertia dataType="Float">0</explicitInertia>
            <explicitMass dataType="Float">0</explicitMass>
            <fixedAngle dataType="Bool">false</fixedAngle>
            <gameobj dataType="ObjectRef">1171716376</gameobj>
            <ignoreGravity dataType="Bool">true</ignoreGravity>
            <joints />
            <linearDamp dataType="Float">0.3</linearDamp>
            <linearVel dataType="Struct" type="Duality.Vector2" />
            <revolutions dataType="Float">0</revolutions>
            <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="2229353292">
              <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2979429796">
                <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3157343428">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">4234492900</parent>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                  <vertices dataType="Array" type="Duality.Vector2[]" id="1355679556">
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-80</X>
                      <Y dataType="Float">-8</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">80</X>
                      <Y dataType="Float">-8</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">80</X>
                      <Y dataType="Float">8</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-80</X>
                      <Y dataType="Float">8</Y>
                    </item>
                  </vertices>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">1</_version>
            </shapes>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="526907274" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="849086684">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">2606366108</item>
            <item dataType="ObjectRef">3875346454</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="2392127766">
            <item dataType="ObjectRef">3532031308</item>
            <item dataType="ObjectRef">2813882944</item>
            <item dataType="ObjectRef">4234492900</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3532031308</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3971025480">KreqjnxVBk6pU55iHm5+NA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Wall</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="ObjectRef">672812220</item>
    <item dataType="Struct" type="Duality.GameObject" id="2562805337">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="922556667">
        <_items dataType="Array" type="Duality.Component[]" id="969047638" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="628152973">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2562805337</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">160</X>
              <Y dataType="Float">12</Y>
              <Z dataType="Float">0.2</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">160</X>
              <Y dataType="Float">12</Y>
              <Z dataType="Float">0.2</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="4204971905">
            <active dataType="Bool">true</active>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">2562805337</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">16</H>
              <W dataType="Float">160</W>
              <X dataType="Float">-80</X>
              <Y dataType="Float">-8</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Platform.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1330614565">
            <active dataType="Bool">true</active>
            <angularDamp dataType="Float">0.3</angularDamp>
            <angularVel dataType="Float">0</angularVel>
            <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
            <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
            <colFilter />
            <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
            <continous dataType="Bool">false</continous>
            <explicitInertia dataType="Float">0</explicitInertia>
            <explicitMass dataType="Float">0</explicitMass>
            <fixedAngle dataType="Bool">true</fixedAngle>
            <gameobj dataType="ObjectRef">2562805337</gameobj>
            <ignoreGravity dataType="Bool">true</ignoreGravity>
            <joints />
            <linearDamp dataType="Float">0.3</linearDamp>
            <linearVel dataType="Struct" type="Duality.Vector2" />
            <revolutions dataType="Float">0</revolutions>
            <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="2830236533">
              <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2884124278">
                <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2920626144">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">1330614565</parent>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                  <vertices dataType="Array" type="Duality.Vector2[]" id="1148070876">
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-80</X>
                      <Y dataType="Float">-8</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">80</X>
                      <Y dataType="Float">-8</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">80</X>
                      <Y dataType="Float">8</Y>
                    </item>
                    <item dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-80</X>
                      <Y dataType="Float">8</Y>
                    </item>
                  </vertices>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">1</_version>
            </shapes>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1795308456" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="224273681">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">2606366108</item>
            <item dataType="ObjectRef">3875346454</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="1462626208">
            <item dataType="ObjectRef">628152973</item>
            <item dataType="ObjectRef">4204971905</item>
            <item dataType="ObjectRef">1330614565</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">628152973</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1338928259">owWIP0mOhEu5LymRxQFRfg==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Platform 2</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2684920694">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2191740640">
        <_items dataType="Array" type="Duality.Component[]" id="4241184732">
          <item dataType="Struct" type="Duality.Components.Transform" id="750268330">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2684920694</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">85</X>
              <Y dataType="Float">-34</Y>
              <Z dataType="Float">0</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">85</X>
              <Y dataType="Float">-34</Y>
              <Z dataType="Float">0</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2392356075">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">5</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">51</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">2684920694</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">39</H>
              <W dataType="Float">39</W>
              <X dataType="Float">-19.5</X>
              <Y dataType="Float">-19.5</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="1161164382">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">greenAppowl</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="2863787206">
              <_items dataType="Array" type="Misc.Animation[]" id="173914368" length="8">
                <item dataType="Struct" type="Misc.Animation" id="522965660">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">14</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1.12</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="332501526">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">startRun</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="4083918088">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">9</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.72</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="3900353458">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">12</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.96</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">death</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">34</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="1487370548">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">deathStill</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">45</_x003C_StartFrame_x003E_k__BackingField>
                </item>
              </_items>
              <_size dataType="Int">5</_size>
              <_version dataType="Int">5</_version>
            </_x003C_animationDatabase_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <animationLength dataType="Float">0</animationLength>
            <animationToReturnTo />
            <animSpriteRenderer dataType="ObjectRef">2392356075</animSpriteRenderer>
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">2684920694</gameobj>
          </item>
          <item dataType="Struct" type="Behavior.Controller2D" id="814352554">
            <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
            <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="GreenAppowl" value="1" />
            <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
            <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
            <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">14.4</X>
              <Y dataType="Float">17</Y>
            </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
            <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">7.4</X>
              <Y dataType="Float">10</Y>
            </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
            <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <bounds dataType="Struct" type="Duality.Rect">
              <H dataType="Float">22</H>
              <W dataType="Float">24.6</W>
              <X dataType="Float">72.9</X>
              <Y dataType="Float">-43.5</Y>
            </bounds>
            <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
            <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
            <gameobj dataType="ObjectRef">2684920694</gameobj>
            <horizontalRaySpacing dataType="Float">10.985</horizontalRaySpacing>
            <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
            <verticalRaySpacing dataType="Float">12.285</verticalRaySpacing>
          </item>
          <item dataType="Struct" type="Enemy.EnemyController" id="643398422">
            <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
            <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
            <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">4</X>
              <Y dataType="Float">0.1</Y>
              <Z dataType="Float">-0.1</Z>
            </_x003C_BulletSpawnOffset_x003E_k__BackingField>
            <_x003C_EnemyAnimationState_x003E_k__BackingField dataType="Enum" type="Enemy.EnemyAnimationState" name="Idle" value="0" />
            <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
            <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">10</_x003C_FiringDelay_x003E_k__BackingField>
            <_x003C_HorizontalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.HorizontalMovement" name="None" value="2" />
            <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
            <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
            <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
            <_x003C_VerticalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.VerticalMovement" name="None" value="2" />
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">1161164382</animationManager>
            <checkLeftMovement dataType="Bool">false</checkLeftMovement>
            <checkRightMovement dataType="Bool">false</checkRightMovement>
            <currentXPosition dataType="Float">0</currentXPosition>
            <entityStats dataType="Struct" type="Behavior.EntityStats" id="3323204538">
              <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">50</_x003C_CurrentHealth_x003E_k__BackingField>
              <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
              <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">50</_x003C_MaxHealth_x003E_k__BackingField>
              <active dataType="Bool">true</active>
              <animationManager dataType="ObjectRef">1161164382</animationManager>
              <currentTime dataType="Float">0</currentTime>
              <damageAnimationLength dataType="Float">0.3</damageAnimationLength>
              <gameobj dataType="ObjectRef">2684920694</gameobj>
              <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
              </material>
              <timer dataType="Struct" type="Misc.Timer" id="2753947841">
                <_x003C_CycleLength_x003E_k__BackingField dataType="Float">0.1</_x003C_CycleLength_x003E_k__BackingField>
                <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <currentTime dataType="Float">0</currentTime>
                <gameobj dataType="ObjectRef">2684920694</gameobj>
                <timerStart dataType="Bool">false</timerStart>
              </timer>
            </entityStats>
            <firingDelayCounter dataType="Float">0</firingDelayCounter>
            <gameobj dataType="ObjectRef">2684920694</gameobj>
            <gravity dataType="Float">444.444458</gravity>
            <heldWeapon dataType="ObjectRef">301082255</heldWeapon>
            <jumpVelocity dataType="Float">200</jumpVelocity>
            <shapeInfo />
            <spriteRenderer dataType="ObjectRef">2392356075</spriteRenderer>
            <targetXPosition dataType="Float">0</targetXPosition>
            <velocity dataType="Struct" type="Duality.Vector2" />
          </item>
          <item dataType="ObjectRef">3323204538</item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1452729922">
            <active dataType="Bool">true</active>
            <angularDamp dataType="Float">0.3</angularDamp>
            <angularVel dataType="Float">0</angularVel>
            <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
            <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
            <colFilter />
            <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
            <continous dataType="Bool">false</continous>
            <explicitInertia dataType="Float">0</explicitInertia>
            <explicitMass dataType="Float">0</explicitMass>
            <fixedAngle dataType="Bool">true</fixedAngle>
            <gameobj dataType="ObjectRef">2684920694</gameobj>
            <ignoreGravity dataType="Bool">true</ignoreGravity>
            <joints />
            <linearDamp dataType="Float">0.3</linearDamp>
            <linearVel dataType="Struct" type="Duality.Vector2" />
            <revolutions dataType="Float">0</revolutions>
            <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="1869764338">
              <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3831772624" length="4">
                <item dataType="Struct" type="Duality.Components.Physics.CircleShapeInfo" id="365440700">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">1452729922</parent>
                  <position dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-1</X>
                    <Y dataType="Float">2</Y>
                  </position>
                  <radius dataType="Float">8.944272</radius>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">3</_version>
            </shapes>
          </item>
          <item dataType="ObjectRef">2753947841</item>
        </_items>
        <_size dataType="Int">8</_size>
        <_version dataType="Int">12</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="792711054" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2430382642">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="Type" id="971987408" value="Enemy.EnemyController" />
            <item dataType="ObjectRef">2066733602</item>
            <item dataType="ObjectRef">4154674070</item>
            <item dataType="ObjectRef">3875346454</item>
            <item dataType="ObjectRef">2343907548</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="1966984522">
            <item dataType="ObjectRef">750268330</item>
            <item dataType="ObjectRef">2392356075</item>
            <item dataType="ObjectRef">1161164382</item>
            <item dataType="ObjectRef">643398422</item>
            <item dataType="ObjectRef">814352554</item>
            <item dataType="ObjectRef">3323204538</item>
            <item dataType="ObjectRef">1452729922</item>
            <item dataType="ObjectRef">2753947841</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">750268330</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3184427650">TODrO+PM/UOKxlc5k5Uhxw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">GreenAppowl</name>
      <parent />
      <prefabLink />
    </item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
