﻿<root dataType="Struct" type="Duality.Resources.FragmentShader" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicShaderAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.frag</item>
    </sourceFileHint>
  </assetInfo>
  <source dataType="String">uniform sampler2D mainTex;
uniform float shaderActive;

void main()
{
    vec4 color = texture2D(mainTex, gl_TexCoord[0].st);

    if (shaderActive == 1)
    {
        /*
        if (color.a == 0)
        {
            gl_FragColor = vec4(0, 0, 0, 0);
        }
        else
        {
            gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
        */
        gl_FragColor = vec4(1.0, 1.0, 1.0, color.a);
    }
    else
    {
        gl_FragColor = gl_Color * texture2D(mainTex, gl_TexCoord[0].st);
    }
}</source>
</root>
<!-- XmlFormatterBase Document Separator -->
