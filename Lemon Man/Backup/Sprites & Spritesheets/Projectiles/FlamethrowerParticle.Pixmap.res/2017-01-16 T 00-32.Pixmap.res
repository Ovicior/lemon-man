﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">7</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">1</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">14</H>
        <W dataType="Float">16</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">14</H>
        <W dataType="Float">16</W>
        <X dataType="Float">16</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">14</H>
        <W dataType="Float">16</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">14</H>
        <W dataType="Float">16</W>
        <X dataType="Float">48</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">14</H>
        <W dataType="Float">16</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">14</H>
        <W dataType="Float">16</W>
        <X dataType="Float">80</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">14</H>
        <W dataType="Float">16</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">0</Y>
      </item>
    </_items>
    <_size dataType="Int">7</_size>
    <_version dataType="Int">7</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAHAAAAAOCAYAAAD3yJj6AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAO7SURBVFhH1Vg9axZBGLy/oNgIFhaigmJnoYgmGj9CgjEY1IhYGEQQwd5Oa/+ApY0gCHYiWvkrbKwkhYX2YvO68zjPZvbzLkFFB4a7fT72bmfevbtk+BfwZt+u4cP8bhxnSsZY1cbdjUeR28XJ4/PD9bV76J0JmZ0OzBM4U3JeVvwH+Ph0T8ExwCA3L9QnpJFWk9MBgUgTfruCufDstzkgPONaE8c1aE1gMt9OoPP9FcCszy9P4KgmjJpIQ7THiFiLvjOxuEsLa26ecW11I5oK4Hhl+fawdPFGIooLw/Nkx0gsr7FxD6jJdnSMO6eAtXbNqT07Bkxy875vPjGKGZZ3KmDC+6PjBtZyMPHBkWO+SBMLRxFvuHXzYRTT65wwPhwLoXICtTjYAq6tBNhj15kCqZ/hB8ix8bcjiJqYVzHRSJPZ1d99vbHHHh86GBcJLi+uu2FgNHXhzOXh/NyK1Wo8F4ZELjEAdGgNmMPjea4VbwF1/nSR+7TrjiKIX7CHIGhhoI81jnM1sWZgzaxaHOPn+/cmC8NCsWAlczM8QrNHmwmj4FzGkNdaViQGRjEZU8bcGHgdYw7N6fxdBKGHH99e4RiF55gVJYKgZqBS+3XMvPXBwNrXp8+h1JzUxkXx1zqsX7uPcTQODHEzSwQxU0D0OhVSZ49lz7PW5p07tRh7GdOx1feAGt9l+o7eMYLA0TwFxW+aGERtmqjnMmZn3IVdE/McaX0OFzx73JjI+mGjBKTWxg6voXkx7/UhZ/PjB4N3rV+3BvbEvI4DbR6+k4v72BZoEm3bgovf24kw5dOzA2ai16sJztZ7ENQ6jzWYmAe44CCFiZRHK6u3oHU15HkcYRrnU1q+BvabOUIzjtQ4u36hFa8C5kD4FmgMq0tsvp6zfiXM8GNgYh5AM6JxSpplzGLsroO7IQp07vRyNLcGxNHT20UKzpUYeHXlTrMXcRC7THZ0Yl4LzFvdKILQhYE6xjlqWvjydql4jDrzneeAGWqQkmbVYuwuAXF9h3Dh3cWjnrTaC2dXXbSmsN5Ted+yog55EsT6sR6vqbEAzOm8A6d+zNRMrJoHwIzaf2JoVGIgzv2P+BpcWBdJhWqhUZ/04ehUSDypb8GfDLiOfhiNAfco78j+tdTEaNzXFxYHxxCErrIHGFL7GgXfHd56R/bMA7BQNwLEQrlwVpSo9bhA3sfzqmheNyFv9Ec1rjsVfo+hv7i3Ktws5Z8GjJnCHrhQ49iXoSPvqX3O+7g1Vy/PeNXcqcC9YS3lvQ3DT2Knrqc/W3O+AAAAAElFTkSuQmCC</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
