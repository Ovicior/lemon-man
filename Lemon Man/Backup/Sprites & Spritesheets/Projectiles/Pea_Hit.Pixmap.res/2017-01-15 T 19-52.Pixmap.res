﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">4</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">1</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">30</H>
        <W dataType="Float">30</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">30</H>
        <W dataType="Float">30</W>
        <X dataType="Float">30</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">30</H>
        <W dataType="Float">30</W>
        <X dataType="Float">60</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">30</H>
        <W dataType="Float">30</W>
        <X dataType="Float">90</X>
        <Y dataType="Float">0</Y>
      </item>
    </_items>
    <_size dataType="Int">4</_size>
    <_version dataType="Int">4</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAHgAAAAeCAYAAADnydqVAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAMJSURBVGhD7Zm9axRBGMa3FETExv/EykpIY+0/kCqdEOwtBJF0YiGoYCsYxBQiWGgEg/gVPL9iDqJIFCwsLOxl3Wd43uHdubm9m7259T7eHzzcXnYyz+z77MxONoVhGIZhGIZhGMvGyvOTA+qS7b9HnO78OVpcOjw+tv/LezeKgxcPnH5/f8ufGjVQzPP9E/gsRfzOFuODImuNA3yu/zqGgEuIY+DZZhBwpbLyKg/fPbaQQ1BICVcKnFpkAcVmkX3B8bNRwEf7p3ij/73tTR/yTAbMC6wpB7h4LF3fdh8VP/d33HFYcPrVwk0tsoC+VaFLHLP4bBFHxiBKXT3Q/8wt0xgI9LB/bWB5xMWlXOAwGGhjwenlfINw3ThSQL/BbHKeGEcT8Ak11yBYqrzdX8MFTVzcGPvP7jsPLRRcP6vgozxr/lAqsZBHBbxwoLhy8SimhBuEzNbtCWdwIDcOCZKe3htqC0KGNx4N4rNU7H656wLWRQ3Chdi6PbrQPz4+jYXt2sFLa/3Jmf8akB73sL3DzIICXtk7i08dZijXLiexWRULDu3CJXbc3XAOYv44xti7AmNofYMzvNiMddp4v1psfr6c1ukIqn4HFCIXEpvp+I6L7gL4hP4Q9hNdERkDz4wBCxwNWHWY1mkD8Ivt0tG/CN9vfrqA43AMLtxw5z1N4DP3MzhScF3U9E4bgB/61zfU6utT4uH8pM3F3rniw9ct7z/J86/ycq8cU147CrrAc/cMBiyo19qb035ZhnJCj1rAt16tS7i1gCn+ZnsQLpWtT2MIKK5eMa72VmrLH2YI2lBZwsCs1TfUqD7p7WS0QBdwq7fhZy+ClmebbjMpXJZduPLaEUusKIS+WW6upQdLsgTMHTPP5IOBeQE+R6MbNrazgHOgA8ZxFyDQ/o7bvEV3xHIjWMAZYMBeXSCzFwFD2B0bCwT/nvQBV+IZYyHATl0HjO9dgEeDyJgieonGZ/gMnhZNGzsjIyguio33ySw6z0wXeTQgZAt4AanCdQF3+d8wo0MYsNNkFMU/gtO6Ake/AI4AAAAASUVORK5CYII=</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
