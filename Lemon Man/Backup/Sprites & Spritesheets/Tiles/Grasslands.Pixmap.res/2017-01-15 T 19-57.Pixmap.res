﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">0</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">0</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas />
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="2035693768">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="2696347487" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="1485019246" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2204481104">iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATdSURBVHhe7ZnBrhw1EEVnEZSQQEhEEoKIBEF6EhsQiA35gSj7SCzZsIfPYMGP5C9f3rX7tq7v2G7PTHmERJdU6qrrKtdxt/RmXs9ht91222233Xbbbbf/sd0u1659+ttrXG6Xa9VGanp2Rv8Q+5Zhk9sHP7863Hv5OOWt+NWHP1Pt/R9vkBe2aGtNaw8ezg872q/x4hfbOnQrhr/456+U8wAaY411XKvFrPd8tN/ii20dfHj/S7q24nT4JUbfuk6gu7WtvVIuvZqP9DNOLHd9d36xpc10cNpcYNaBBsK+ms6c69RqtaP9rEOc8qgbwAGnukKe4tpzTj8czIn96cN8irMNGy3OzQGFnAf03Ndc6/XqIVo1rX72IV/99bPlIOdb2sihHLC35lqvF84DqY/2I4anw4M94gZwUx3kkJ5rrcbwU3prea8ftXSw5yNcZnmzuyE66Pdfbwow1jDvxd6HvZizppVv9WOdNWDPR7jMuFEBgqF+E5CzhmsKiDXGcK7VtFo+0o+agjnQ0iAMxSDEf797c6QxZ1zTFJ57qFbLT+kHa0aOteLAiOmqIYbruusKT3etNmO0H6wZOdC+e/4El2KwQjHH9d8/3lZdaxjXcvi5/WBcWKdYGuRQvVx9pJcHUl3Xt3IwZtRge/LwAS6bEFtPSGu9t9Wjsdd7DsaFNdxWELrCqdbzWr1r1Gteq3MNrBk51ta77EM19yfS0lp7aJ1ro/1gzcixVgA4VA/WtZFa5rW6rX6wZuRY6w6nPuIje/T22+oHa0YONH4MEkIdgx1G85rWO6D6qf1gnPoxiMHuGKxwntc05K291E/tB+Nn9z/JtNH29RePcFmHE1KdOoE89jr3Wo/GXucOHYwADbflzhbD/YlQZ64ac9Vm9IPx2eeXvgmqWwHA4QRwoFp+rX6wZuQg4zdBAhCC7lDUmVO7Vj9YARxpxUAOVZAWVC+f1Q/ejB1n1aeBq2stKMS6x6x+sM74KCyGE8o1Baam64xbeUQ/WDNyoP3wTXq7WgBq7F6DRHyNfnACdoalAQ5Rc13XOtVUd9d1rVNNdXVwhv9LvHyupgEY7E+FunpPm90PVgBH2joQTgDVRvTRupY+WgfejB1n6zC/4xoTQGtVpza7H7wZO86OhikI45ZrzzX6wZuxg0z/HdanoTmcQITSmPnsfnAu/7iF29HAXu6xH8BrPD+3H5wZN95WCAJ5jqsDaa3Xe47rpf3gzLjxdjTMnbrCaex17rUejb3OHYzT3gjx74AO1ydVg1SNuWrR/eCb9S6AtgJwOAEcqJbrAbjWq/d8pB+MGXWOFRB0h6LOnBp7Z/aDMaMGG78O61AFaUH18hn9YAToDCueCAcy70Exnt0PxqmvxRVAhzNXYK+p1Xt+aT8YZ/04Cjt6Iozda5CIr9EPzowbaPrzuEPUXNe1TjXV3XVd61RTXR2cgI22Yrg/FerqPW1mP1gzcqytAxVAtRF9tK6l9+qYgzUjx9o6zO+4xoTQWtWpRfdrH1gzcqwVQA7C2L3Wo7HW9HyrXw//07dfZeJIu3n5JS7FMIIwhwMIdXCF1Xy0n+tw1bSfOn3K4Wm8CQRArDk1vIzgD6n07188TbkegPWeo5+fOnR8uXGN+lVtucNpOH4r4Ndjeu8J8Aaq636S/0fscPgI0riNbOXXVLIAAAAASUVORK5CYII=</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
