﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">20</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">1</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487" length="32">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">24</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">48</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">72</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">120</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">144</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">168</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">216</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">240</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">264</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">312</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">336</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">360</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">384</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">408</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">432</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">26</H>
        <W dataType="Float">24</W>
        <X dataType="Float">456</X>
        <Y dataType="Float">0</Y>
      </item>
    </_items>
    <_size dataType="Int">20</_size>
    <_version dataType="Int">23</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAeAAAAAaCAYAAABvqo9eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAegSURBVHhe7ZzNrhw1EIWvhEAQhBT+JDYIkILEKwSJDWzIgmVWPATvyZPwFmGO3adV9lS7/VedcW4d6WimPeWv3GXPrZmbqzy5XC6Xy+VyuVwul8vlcrlcLpfL5XK5XC7X4nr54lM8vCv5x29f3h5cLpfL5XpmOmuSPQ3y5+++xkOY/8+fr4tm3DbH5XK5XK4PW7VNkjG1DZJcjVUy5nzzxQvMdblcLpfr/Us2Ss093xx7miTiKxukOv/Mv/3yA+/J5XK5XK73q5pGidc7mrDKKrmmQfLX2dr8I4NLYy4Az1ZHn7Z6PmXlOmLDlvwZbNe5rPfXWquv3/VhiedRa1q5Edf461uVo7mxQarz3717Uv3fv1/tXqEBa/9ePu2PyEobjvGRH0JnhwmvWfExPsKmmEPzc+eTrdUfxmuzcmh+7uuHLPmrsqkrchhIPYuaO5qXyig1yZ4GXGq+tGzCmP/T9x9FUoes9lly5f3BHB86R0yQw6Xxes8/ktewYcRY8fH6SIHOcuC158o/Y9OI6f0jC19/We+zPnjtEdnUFTmMpK43N5rcaAM+a5SyQcbphzpkvn79925trCGHqpp9fsT+VZ2gY5MplZfbmo846w3oeSM/Er+zwai83L376+svy5K/Kpu6IoehkjXi/KFByaYGY6zjbIY5ZOfNkGw5xuZ48g01cGEyMAZLljYmG3Drt+Cafe6oEaXycg/wzxOwqIiNU5qkMqWt+bA1H0ZsnNIklaUZsXFKk1RWbt/fY6++friTr7JyPyCbUpm5B3NYKVmfbFZag0R8nFalEA8u711hJWOyQd58pMDj+qQ1ljZWkSNRbfPlfSK4USoz9wA/TQAQiiCLh+tZN2DN1wy2JR9emT+zPtjL1fcXrJXXn3uAr/KkH5RNqVzpCTmsFNaFNeK5bFa41sYavjkm5xuWLL6HtTHMjQhVd1w6rzvN+pNfkSOXypS5wRzY4zt+bt4DYuOUNiUgLFYuXt4EYuOUJl3GP8qB64ECJXzNYK/CB0fWhh7lYz7qvPr+HtVnpfVP5K/Kpsxz8BtYyVtMq8K6uGatGeZjmBOnniqpAS1rldeMxtyIUHXK08z7pMGJuCrdseQa6Mb6SN3xwcrZHeveFSYDjudyU3Etx7brVpnzS8WRRUJsnNKknc8cGn9gAy7jH7FpxMYpTdrXVtpbMdaqZP2W+3t2H9vzVl26/sn8VdmUaQ7jf2NW1yy5fJ3GnDj1VAkz5+aWeTA3IlTdcWss76EiR667tS71/v3y88/wsMPl4lkQXiMOwS2y5t8U2CgQnks+ruXYdt2qZAO04tOIjVOaZM4HF8bz2fXh/p7tLYw4BDfKfH/Jx2PpPhAbpzTpsvXj+WT+qmzKvPZgnxlrQGycUq279/8Rm2cUc+LUUx1y5TgMPl2R425+jbn+jvuAwtpYC1juM5kje0w+nks2ruXYdt2lsEhZFLkplcUvyYx/VYOH8VzycS3HtutWmfIvqA90urcw4mJ4va7+gFi6D8QhuEVXr38mf1U2dUGO5IxoRm4YsXFKtQ7PIsfI5v1hTpxa1ps34SHhl8xaYc7vv34cJmtq5dLk1+TIJfcYllxZM7yGOAS36IpzCh0ufPQGNpnzUYijHCwe4mJ4vaw34KINDhyL+mxSuTP51uvHfJlD8mnExfBmXbJ+I/6qbMp0/TlH5qE7+SpLM/gdOVSW5ka+yiiZ/I57oO7qL/eYRlwMb1bCzvkzzqkK5hgSwIiL4c26jJ/noEf5xhtgzj9iyxyIi+HNuuPmuVbaX8nl2KOv35C/KpuyzBHmgoHn8sMzruXYdl2t2m+TOLc04sOsCr19Gx5UprRk13wzreXS+fr/+uOTAGhUmE+m3FuODZ6jnZPz6dFzmiTILQsUw5ulcumZfFkUjqE4D74Bl/Elk2Oz65N7hf3F+sjLvdL5NOCvyqbMcvC3V/nZkXnAHjg7xXMJy7PZ8qtb6KxZkk1+iK5Qa3Mnv3X9QoGh5YBljhjerJ0l95ZjM86p+Q1Y8zUuPZNvtAGX8TXPqA/ma2x4Zv01j/BfvQoPKpd+5PVvUrn0IF9l0g/MplQ2PSFHwpPvX7r3vbt9G0z40lz7yPrZLGHJ5lgvv6W59/CpD+L9u33yOIX3fkKx5t8UGCU+c8TwZjm/rISP65yPsc5fL0EJP7fMEcObpDKlB/mQ5fohS/6qbMo6R8Jj0+X16IfnvJHhufSkGu2NjMavwLf/2GMfw0WLjpp7tmbpHiVszRNqFOZrbHgCP2gH4jk9C36TGT9v8HhOS/5Igyf7iD94D6Z86/rwkzo4ModkDzTfqg9wzIPARiU8XOce5Fuv35Rf+hb2yGzqghxhvuTmDXj0HmQj2xw02iCvUt7c6d6fN5mS2uM69+geX3FOoQDE4/YHAPsGz4DftCxf/kEEHo/4vQfKmr/JtP75IcVzcrfXRpWwaeQYzJMwWRtqYo2s1k+Z8eX5LPFvblbpG94om7JcP889TL7MMeseXIdK6m31/r3inAbAlmiXPGB4MiDnl7U6X/ukPqv5QjtTvMlm5MmZd5pUo32tk9dPLctXzs3+g1N4VDvLoj4Wv8J1VWmv95EmvX8HzunT0/9PMC7JBLMctwAAAABJRU5ErkJggg==</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
