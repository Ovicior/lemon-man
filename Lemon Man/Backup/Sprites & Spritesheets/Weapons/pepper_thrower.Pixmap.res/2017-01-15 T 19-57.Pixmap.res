﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">30</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">1</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487" length="48">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">29</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">58</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">87</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">116</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">145</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">174</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">203</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">232</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">261</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">290</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">319</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">348</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">377</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">406</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">435</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">464</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">493</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">522</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">551</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">580</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">609</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">638</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">667</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">696</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">725</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">754</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">783</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">812</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">19</H>
        <W dataType="Float">29</W>
        <X dataType="Float">841</X>
        <Y dataType="Float">0</Y>
      </item>
    </_items>
    <_size dataType="Int">30</_size>
    <_version dataType="Int">34</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAA2YAAAATCAYAAAD70h4RAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAW7SURBVHhe7Z2/iyRFGIYH3bvdZXdd9zwQVO4QD9ZbODzFRHENDPTARMRLFJP7C0wUMVMTA0HkQDC5UPwBhqIYKUaKRoKJGIiBf4LZ2G/N9/V801PV073TX/VMz/vAS09PVdez1VPUfXO7Ozsiq80fH14skwNxjXP5CCGEEEIIIaQ1+iYp1xsXcYU3Sjmc6snhIsSbPtZzH84+6GOedA6LPubZh5MQQogTsqGXb5Q0XhjHjDMX1pnTO3T6uKfWmdML+vAaZx//sZH1u87GG+KNcWWbZ9WZw7sKzlwYb4g3xjXoNUQIIb1jNj95xg/ryuzDxp5lc1eHJOs/KMbZhzeLz7py+IC4ynuaw2ucWV9LYNzZvMZZztmbijPLXNUjzuzz/PPTK+HcG+uUSIsfK+AM596oR5zZ5znkNUQIIb2Cje7vL54Km58cpcUHjA+XRM+l1QdxlJt7xnmW3hxOUPXmcnINdY84S2+uNQSsN7fzv3/ey+ZVp4m0+NGnc1PuLddQ96hz3e/t5Yt34zBO5cmdc8WhW+ichM6zQ+ckQ3HWohufbrb/fvuitPggvpnNNqcz1zxBH06gXuSvOyfyrB/q4xrqnj6cQL1IjjUE1Klzxbk3dPpBpx90tscWXzJWmW8eOiqjfU53z6P/UtBJ57LQOSxnI4ovYIRi79fPboyw+X13+xlp8QNOSdhwczn1uzo5nbnvLYD3x48fz/p6cg35AOcmrCEA5y9fvhScOOLcGzr9oNMPOpvTtPiqBv1Pzm/h2tass7PoX2YRvLd0IujPNeTAq88/isNYju589PrB6K2bj+DhoJ0g970Fm+DkGvKFTj/o9INOP9bJ+fQLOzjMFV+xguuVg51o2hZhQ3AW1yIYQ56Zh/eWzmq4hiJYoU2Tn42879xdOIw/eHjyjhAnTaCTToXOeuikU6GzHjrpVJZ1fv37/bUFmBZbbxztjYpjNE3p0llcV6YOj3mqF4mx7ve2KVxD7Z2KepEYG/F6Pnh8gEMQWikkeB6JbYIPbE02PQQbX5vNj046AZ2LoZNOQOdi6KQTeDltigJrpvhahi6cRf9qMIa0zuPhXMS63luA+dnUwTV0duciNmINqfDlN49rhegTC9rsxndtb/Evt9FJpw2daeik04bONHTSadOVE9crKLwwri3AYhTXLS6+BAen3qukvwunjo9rJdISp6t5AvGWSeF5b1NwDU1o4tTxca1EWuJsxBqywiuvXR0/9/nVcIFK0KbSVPAFIujbduOjk06Ezjh00lkNnXHopLOarp0KHkvCdVWKaxr/+RcPp4m0ztKl07qQFB0759wxHO9teC4G1xDXUAyM32gN7R+FjSoIrRTCJtFNr83GR2fcUQ2daeiMO6qhMw2dcUc1dKahM+6oZqjOuiIMn4KL4gufBKlFWAxvZ8zbpRN/BkVcyTkCj3lab8w9xNczBtdQe+dKrqH9S/fgEDYsK7TndmOri46zaOOjk85UdBw6J9BJZyo6Dp0T6KQzFR2nK2esCLt1uDvzY0vXtrfC+ffvXg8fz//zJ8+OkGoR5ulE0ffb7SeyOn94/2T01a17pXWK9zx/eudC1nn28XpyDW3GGgJBUhUeXtoPbQikddF+x7vzv3SbgE4ZK+ax0X50zkGnjBXz2Gg/OuegU8aKeWy0H51z0CljxTw22m8ozlghpr9TopHCLBR9OCY+GtvV+fbpHsapwnl24Oxjnry3GzHPqVSF2xfKd3jjQ/NpRqm02PQUOovHdE5DZxI6i8d0TkNnEjqLx3RO4+XUQqxajKHQqgbPowjDdYuKTTrppJPOmW/TReICnSEu0BniAp0hLtAZ4gKdIS7QGeLCqjrlxyHDc6lirBr0vfHYNq6bg86QAJ3poC+dm+EkhBBCCCEt0D9WjdQVY2g/3Q2F29LQSeey0DksJyGEEEIIEWwxFotHAUbnJHSeHTonWT3naPQ/pC+jgcX6TWMAAAAASUVORK5CYII=</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
