﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">8</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">1</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">384</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">448</X>
        <Y dataType="Float">0</Y>
      </item>
    </_items>
    <_size dataType="Int">8</_size>
    <_version dataType="Int">8</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAgAAAABACAYAAABsv8+/AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABGgSURBVHhe7Z3NsjS5UYZP2MOMsR0muAc2XntDsCDYsmPDLbDjHrgGdr4FX8vswGwGBxgHxsP8eLCHwfbmoDf1pipLJXVXd6skVXc+8b31dUmpzJRKVeqf6j5vjuM4juM4juM4juM4juM4juM4juM4juM4juM4juM4juM4juM4juPUeH+v69mJ/fyHoB8HvVMfs4xGkxP7sOgW8rZWlyjZWx1Nq5glP9BeSm1zHUWLGNbHo75Ow3Ki75PjPCuY3u/vf4v/C1Nfyp+W2MmfoI/hYY5Uoy6h+7lGEnOwx++vWXY9t9hA+79REZTHJ0d/j8eldlDYsMFBMMZDMbUN26987QW2WdtczYFPo4diGB/N8qXPlYZje0jtZdXuiUBn9sh5YvhKF/O8wLmmPHKN+lEQXtHjlfzfsYxGBnawglarLVR8ooSyYTB+yEORnFRVUBcXfyW1UW1A2TIGitiWdCjwn8W7KSZsqVV71V5ob33kag58Wv+PxND2Vo9CP4eOwW5sFkGPknydFCT+iJwnAtM4vpKrsUz1aBs1IzE3u0Arur/NPT5BqLFut134gNRDw4i51fOCSrBOrBekDST1OSwXywWxtwqbcvuWMMYm9l5ga9upbsW2DbK5JN8tgT9KfLf23wKTI9R8DHZhj0BQa5Lvk4BEW8o5AZieNZl6mdBlULdqt9qfiR3vZEAr+O6AWKxJ9hBsIFRkLDajKOeW8oKKoDzWK3i8fHxQYtsGYF+k9V1grE38vaj9re1q5P6sWkKfqc8zw1xF3bAzIuhoUqxJQWJHypkUTMnKW9ZBUq4K+zXijXDRZqOp4E18Fco5Yz+W5yz2RlKzRmygYZRzS3lBqN/A8lCPd4D23fS4tCmqK4iX6xV4xT7vxszG3kjciUAyPeVMxp7P9qlYtAHl8oqQjxXdZ6BJ2Pt2vgX7pi5X2JTsLFoW7UaA2PUnekkbUKbai21TkuMMw8z2UUj8CUASI+RMAqbhns/2o/6xYmdf/VuwL5qK+tv5oJwz9ilrI3aQwn3WWaRM6keiOVCal5XjPC9mpo9m9NmG4CPlTACm4OXTQaYpFDa6cOb6q7DRxxYtY7BJQD7bXJV6zmy3kYVl1g8VP0qZBeRiZHN1OsLxT3pFuo2BmeWzIPkMAEFnkDMYTL/Lp4RMUcjYbhQ2+D9nqZ+JmO/tb4XvAe0yP+ILmhHNzaoHo+LOhOn79PPkSLqMgXoPmo3Y634g2ExyBoKpVz81tDwpbIplUHics9jMBnOGbJ6i3/3+Wyi/C7QryYlk42LH/eUojMErjwN0zBiY0Z2VA3pdBEFm1FTgUFzSM8E+cQpatGylsFmXff7rD99++d/fMeUWKUPdlGhu0De/+7b04+Of/un7Jz//Pv6nVRtsrFyvQuxv9Z2XlwN9zscAekW07zoOzTCjOzuNe74BzmfWcDD88Ya26z9dCj0Dpj+lvq6EV8Zff/NB2FnKsGBCtmxR/K74zGChN0r9wX4L0P8rc+pl2H7bZBmDVxoHhf1+6TGw6Bg0HQeO7llg1s2B4zNoGHHi6auTGqhLgm1C93OdAeaa928jfYVsy3TBtGVGU4NFnrILv4pW94H20H/813fDQGAslNX4QC9DvInUgm+VnOc8aY32/ZXH4FDCDFOdBcn3AOD0DBoChvz64q/IIVKhDVV6a/Pyr6XNguYYc65LF8drZZB+jl7SLGCB1vwzSd0j0If4C30OsmBfNNV4HM3yDgAkf1uANY5zAJxpZ4PZNwMOz6TuYMj3TxWdVklh82dBeJyjNvLkYGq+/J8P3z794qO3z7/6kH0pa1nQlsXflmn5v/3n994++/IvQsHmre9pCHmWngA8vPgD+jH9tsRxGD0W2tcW/d2D9nmGvjtPjp5lON1OhuTdEDg7k7qCoa4v4DXkEGW6BOrHv+JB+Jp0wbIK5RdVs12XWaS+C4hTkxJyzPvcbDGkL+2ziDHeP/3ib1Z5jID5SY7433GeCp51Z4W9eBg4OqO6gaG+faro9LLahRj3BiGv3YiGu/m5GBRVaJNk67dtgNr2eScEMcofyYjCZrMAqujhcQq+raR+JMwh5eM4TwXP9rPCXjwMHJ1R3eCCwGHfi04vq73gnQAG7wTilXNE2VpmURCVbK4pb/fVb/787X//79thhwkdzIW7zVWlBbrpokx/1n8uWrYFfcuVg9jUobk4zjB4pp8Vyb8BcHJGdUEvkLdNFTk0Be1F7LuBWNc/4pCcdqhEyW5RvsiEsqJasr3bXEGxqLRAs3UZzTNXDfrPYyT987/+4CZ/e4g+Nu98bAjxbV6y3wON1Sue88Jw9p8Z9uRu4ODMOhwMMS+S1B6sfa5rJNtuINaNuVV0ia19uMivFH8s6EdBxY8hUNaM9d3mFi27bRGEPWV9iB9VCfXLGCutfzwpCWWie2Db4EfBY1aSENvmJI97MSqu84Jw9p8Z9uRu4ODMOhwMMS+SRpfIbUuqsbLrBmLFmNdIuVV0iZJ9TTmpLmyY9IOor+jXgv1Yp4vQtcVI7dm2ovq9DSbG6gnA1sdKYUMHO4lt8nd68JgGJMuFpdeJ/re6hXtjt4Q5iJwnhrP/zLAndwMHZ9bh8CKGoc5UomRXU4mVTTcQa/lb/TUkpyRcoPOyOrndJdVINs2Ar+3b4bffhBj9aPsaUl/FLDqFsbXYcjbeCexjO6Xsx+Qhj/ey+F8JZbsxsTdPANTXrT5vxeTAEucp4Qw9M+zJ3cDBmXU4vNhgqDPllGweUlcQbx0/Z8/NfyVym0u6RLJrCvzluhW2kyzroJ4NKmDBWX4VUFUi1d8E7Nc+o4/cD/LgMWbJPhb/G+2GsVN8CO2331C57Suz0cdaNWx854nhTDoz7MndwMGZdTjmgoHh7qmuIN4ifA7/4yBN5idvuEufF0WbY6YSJbuSLrGy7QYXgo1ykFPM7RL7cl98qUqk+puAvWkbVH63g/0c8gQAmPiSA9pHPzn7vy2z+Egqwth39d85GZwJZ4Y9uRs4OLO6gGGmMOS9NBTEz1TKsaFqrOzChgl2wCwEVqxdYF5ItoLkDl1l8QXV0Ho22gnsbdtae/RRdQtr/0Af02AnjJ3GG79Aufi0RN/X/KsN7VO7EnnskTAXkXMAnAlnhj25Gzg4s7qBoaYw7EcrbBh4ABqfuTQXL66idV2JVB82TLATvPiu8kVZTsxN7yXIQVnSVWCzbpOT6sKGjXbCNne13YPxH6TI/k1gjCkZ8+XbEBaNE/1DNVi/aVPCxJXHtwK/qkexueS0jGPRfpdiPh2cCWeGPbkbODizusITDsN+pMKGAQcQ4+c3xrURL2YblWwzhQ0T7ETIS5XnS4s1MUe9kRIC+lgUNjQ2wJ8KqB3bBOVIudjcira7p+0e6Js5KrJ/MxwTGfNf/OqP6deicZKqoC63hUqYuCy5Dfq+GGMvNpc8n5ZxLDbm08PZcGbYk7uBgzOrK3qyxWlzmIaB2FTII0dyKwoXjFJ5TbzArFSyCwqbqKOxsfCLhJ/8/Pt6IbTaXIgtbF/tRwn6TL4htWfbTLd/S6EXyGvJU5H9mzHj8r79c8lA4yRVQd1eWz0G0D1ksR6CeaS5Z8njXIql9ZdsFBOTJU8MR+/MsCd3AwdnVnfMyYThv6Kcks1KYcNAA2B85lJC6xbxYpGUl1vbXGrzT5/8ydtnX34UClMOST0w8Ta5ZWKLMiU/WlYDPksxtB197PI1GuZn8w2Kf+76Vjguop/94nvBx9V3paqgLtdR0P/VnPZgxmAz97I4e/q/K59avKeEI3dm2JO7gYMzawg8oVQ4DAXVKNmKwoYBBsEcQi41JM8k3JzFi4XI1pXKSvrV59+RC/woQg5B5cXF9i1ILo7XCO1U8CGPL0G/xTjGV1IPNAfNYy8mTzuOTaBfyPq2mgLk0TInHgeZF5YszsVYxvYqetzzeE8JR+7MsCd3Awdn1lB4YqlwOKg9WHvRcJBDzKVGyhWSu7Px6h0XqKzOLmibuoLChkl0hrFDDiVSP266IGp/oGvQ92q8UNYTmy+f1GlOtNgP/cjYqc9WqD8jjSOxZiDL62FqxyKLczGWsT0d7L+oBPsmuhmO3FmR/BsAJ2fUNHAC6iHBsdmBtY8+RsM8mFOOlovCJur3f/gW/s/r7WK2qatoCIgb45d4e//qt39Uvfi0AL7tWFGsPR72n2Pw9v71Nx/YfGi1H+sLj4+CcVaaAebSrP84BioL4+yKZWy70DIe+16di4xz31hz5M4Ke/EwcHRGTQMnIcRDcw3YJaHdFGguMa8clK1sIC3f6PNfx1eS21+2q0l+o747iLnkYNEyGh6EucBZsfZ40D/T1yTeBEmr/YS2kPq5CcRTnRX2X8fgUHrGuoWWOXE++BOAAuzFw8DRGTUVy0S8hk671GYqmJPNk4p3nxsVbFay/qCsPkfKwya264XGZHzJJCL7Unck5gLX/QlA7J/+caBFyAHfvb+H6HPRLaDfPft/FOy7zJ+j0XHuEWsvzKdJ/zknRCUeipVm/Pm4s8dV4OxMmg4cjuVmskvEQ9f28LVDc7si7Qc6lJHqEnhsyqmcVBc2bNgJjcn4VJ+v25kL3KAnAOgrwP9Rn37xUfWCeySIqf0fGP/h2BxX0SvCvmMuHc7DsTjrzwazbwYcnklTEifjta8r9VlYjgB5L/2oofVsFMBjytSXSPXdQcxcveCikxb/RxegvcS/+RD+Cfg/vvr/919+lxZ9Yd/TOByJHmNIMfFZ4twDxxXz6XD0GN4dK8388yD5HgCcnkFTg0NzTWeF+cdZWAX1235qWayvgTrRdDD3jVpgFh792dtmvi/BP/hEVjdu0qIviNsrB44x+i2PgYkfC5y70DHVcZ0emf3ngVk3B47PIGcQPKk5DWvI6VRkaV/zoXVsMAnIp/LODsoe5l9+9gNZeI7wfYn3948RiNz3lceWMPbhTwB0bNFnSvZNfFo6L4HOgqDZibP1OOB8ZjkDwdS7fKqgvP4RB9tD0XwFypKmIv4teuRlWXJ9JF+0/eq3P8T/1mcT39dY+oUfdfrL4YsfF+DD8+C4rsYaP//cI/Y1mIPI6YiZDbMi+XUAQWaUMxhMv8unyfW/z774yEGZSGxmYv1WuWXJ+V7YX/EWST5VhwHfKvMDQKxdsHZWLVGf/ProO94VOQrGWo0zb3yUxb80Br1g/OF5vCRmRsyG5NURBJtJzgRgCi6nSC4s/tcPFUyu3Cg5HUwso03OaBt95DzuuwXMD7I5Nc0Lvqjk/zdff5DKW0O/q77g56h14R0JF37kMTyXl8TMilmIM7Q/CDqDnEnANFxOj5XChkY7UHu2vdtPL9aflSttcmZ78bhGfENDWfLbqBnwtfgF+ljUHPi0/iGz8NJqDMzj0Fy0z5Bi4rLkhTGzbzSSx0AQfKScicBUrOlWSj6gGdneA4DHIpQ/BH2I1wXxrRoGYi8/FAQkH1Uz4Gvxa4lxoJaoT+t7pgVQbwrl39lgaTvYbxlbhX33dx0UnX1Bo1gfoXEgiRFynCnAabj+2CL+idsWp+fat4LHIpQPg/ElowXJC2rGEgdSdF/UHcS06gVi4e9PhP/lY4B7f5HxEuzTalz9CUABMwN7sz4640EyPeU4U4HT0aol0efmvgjsD4U5hFws7XNTf9G3gsdJ3UFMG79XDllc+WNUrWObGAk+ARDNwDT5pCMRdTQp1qQgsSPlOC8JTvlco4l5yLsd5tJU/5rnI8RY1ZtDu4OYI3IoxG0eG/5Us8LFf553JLIj0prk+yQg0ZZyHGdCcEnSS5PqKGwMKsXtjYnf9dJciNst9kzofRBHfh30LrIj8yjJ10lB4o/IcZyJwaVJ1ZNRcRXG7n6JZlyoe+yZ+OzLj2QM8P+U2KND7WXV7olAZ/bIcRxnanBpztULxhu6TDAH0QgY+xxLpD1Se+Q4juM4JbBE5OoN4w5brkbHdxzHcZyXZPQCzPgix3Ecx3E6wcXXX4E7juM4ziuhr779CYDjOI7jOI7jOI7jOI7jOI7jOI7jOI7jOGN5e/t/TEBEQw5nnzwAAAAASUVORK5CYII=</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
