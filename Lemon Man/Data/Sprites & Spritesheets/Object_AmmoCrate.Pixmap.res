﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">15</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">1</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487" length="16">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">30</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">60</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">90</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">120</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">150</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">180</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">210</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">240</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">270</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">300</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">330</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">360</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">390</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">20</H>
        <W dataType="Float">30</W>
        <X dataType="Float">420</X>
        <Y dataType="Float">0</Y>
      </item>
    </_items>
    <_size dataType="Int">15</_size>
    <_version dataType="Int">17</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAcIAAAAUCAYAAAAeCD3DAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAVMSURBVHhe7Zk/rxtFFMVfQ4kllEQiKPAsJSgCiQIoAFEQ0SBEikg0SHwKekRDQ8NX4lPwMWipDWfeHPv6vjPj9Xpmx36+R/pp17N37pk/++Y6zk0oFApdqlarleQYqf4gFAqFQqGzlStaG4d/PoW9HLePH6f2Q0KM4hip/iAUCoVCoaJysdgrXq349vY2XWvFEO0Gn8M/n4LMEQqFQqEzFw9szzFS/UlJ+fnm7z9+2vz716/NQD7kPVQM6d8Z6R0KhUKhMxAP6Yw8wI/E5zj4L7I3X7xM/VjAWoF8yMtiqMaANjxjfCuQL3sWvUcJY+lJSSq2JaFQKDRL+RDxh3YTUIBYhN5/9Kh4YKHt5+8/T+NgEWkF8iFvJjvuhLbvPn0+zLsnJeXndmzNyF9qstO+0G5jW7Lni5sp1KTiFVbquaImFa+wUs8VNal4hZV6rqhJxSus1HNFTSpeYaWeK2pS8YprlloPRU0qXkHlz9uDuxXIh7zA/Ius+hPlAsUwu+0L7Z19pTd9ezClIHGMrUC+7C+9l/BNJnyRahxaoGNz4Bq+9wnfndA+Jwfup1CTileMFsYwan/z5/QMh0prmNv+RFniw/VTXDd/vv6qKciJ/Epof/Pqk+TLg7UV2Vd6ow3PEOcP91MwvsnDi76MbQXyZd/qfHv67pkQ1Sl3YLxiTo45fTxzcszp45mTY04fz5wcc/p45uSY08dzdI5rKcDm89FrJJidY/30SeoPSn3ngnzMDS9zvyR78xXIInoKyJlJe21Fz95r7fWQfZMBsAEHNqbI1ByvPlqH74E+NabmuDbfz9bvwTd54yUntj/JfRivODrH0gX4lx92sXZctTWqMTXHx8/ePTjGTiRfjMXuDXnnx7c33/z+IoF7FaNAPjt3uwbWdwBpn71yexq7GvdckC/7Jm+vh+x7w4YeeGPAlwv3qk8LvCcI3/Z4TzDal/hnHhtbYmqOUQUYBRGfbXxLvC+w88a96te7IKkY8s/6SQL3Pr+PJfjp1X6GH3A/yQ6ZrxLHgxyq/1yQL/tyzort+rQC3sibUZ5A9j0F65sMguChwD/oHvDAsOAPyvvbZx4bW2Jqjq9ffjD079eO0zOiINGToA3FyT6z8QT5rK/1t74qhjA/7n1+H0smzrfENr4VGAPyXiPbBfWbB3p/swvfO8L3fozikK/9v6qlwTjUmFvg5wvMWi++zv9T3V8WBYI2jME+s/Fkqi/aVP/f3lolkB9XzhlXtvk+wBckci4FeBSl+XZ8r4b5bl+sEtxI3HsDH0smftPZi/GEb/h6ztV3ZAEGdmye3uvsc4JRBQmxnC8+sxiRki/yeU+AduuLNtV/1HyRv2dBUjGE64x7n9/HEj9f+AEz32G+1YWmKUEbXy7b5kE+a2wHkI3D12A96RG+9/uF72X4IlZ5kl4FqUZprsAflMQclMPnW4K+uPf5fSyZUhhAab70JGhr9F4N862+WNhAAANcW3/TQZvqH77h6/uA8L0M3xrwVe1gqi8OsJYgJzw82XPri7GocddoNV/VH7ktaGtZkNCm+nd8r4b5VhcawTAA+Nz6m0743oHY8N09D987LsG3JciJw8mTPZPvl8+fJe8Fgd+w+eKKtVf7hP0D2F9cGxaG7XxVf8T2fq9Uf8T28q0udA0ORjF1odGm+tcI3x3hG76jfAcWpCGMLsBqn7An2EfuZeuChDbVv8alvs/bhW4JcsLEk02TcfieTviG70DfxRkpNZ6FSIe2OuBrtChI6t04BeS07xPJnkN9h33TCd/uhO8yXKVvqL/yWqs96M2Vvc+rm/8Adc3ZMOTOMgoAAAAASUVORK5CYII=</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
