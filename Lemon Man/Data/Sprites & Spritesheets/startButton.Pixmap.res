﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">0</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">0</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas />
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="2035693768">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="2696347487" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="1485019246" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2204481104">iVBORw0KGgoAAAANSUhEUgAAAPoAAAAjCAYAAAC93RfaAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAHYSURBVHhe7dohToNBEMXxXoAToDgCFgdHqCEcAARBIZC9ABaF5A44FAqH4QwIHAJH8pER0wzL2822UBYmf/EL+d5Ot9tkX6jobLazmAAkJ0MAucgQQC4yBJCLDAHkIkMAucgQQC4yBJCLDAHkIkMAucgQTYfHZ9Pj026Tzdnf92lWdX0zn04vTuRaZHvZrFpz/p5qLbL388/hfvsMir/2+WVbrpv9+fnyzFiRDNHUWwyK3o+ib5gM0dRbDGcX1HO/0C7uZUXyPF54e44l87K29r28OlquxX2V2hlKvWcon83t3cEyb722/Nz4ITJEU28xHEWn6MPJEE29xXAUnaIPJ0M0xWLYBbbnUpyn6BR9OBmiKRajJs5TdIo+nAzRFItRE+d7i15jc7FkJSt13NN8p+jqTDbXOoP56aKXbN73w4pkiCa7cHbpS7X/Rpsu+v3D3peyU3R8IkOsxS6rX8qY9xY9FvIvfHVf9wzls+Gr+2AyxFrssqpLStEp+nAyRFPtJ7Cvb1vyklJ0ij6cDNEUi1ET5/9T0Wts7reLXrJ53w8rkiGaeovhKDpFH06GAHKRIYBcZAggFxkCyEWGAHKRIYBcZAggFxkCyEWGAHKRIYBcZAggkcX0ATIgcPP8OnbhAAAAAElFTkSuQmCC</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
