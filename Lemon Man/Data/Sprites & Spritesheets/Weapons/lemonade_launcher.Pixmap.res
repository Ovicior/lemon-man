﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">1</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">1</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">18</H>
        <W dataType="Float">33</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">3</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAACEAAAASCAYAAADVCrdsAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFrSURBVEhLvZO9SgNBFIXnGbRSsUohBm1CJIWFIgoiWFmIWNvtg9j6CEKexEpsfI28xbjn5t7JmZvZzayBXPiY5P6dk51s2HWMRsdr7DxUOBJa+Wfowk5KobUYY9jOhC4KV5dHsrDE69Nh6jNodjsTGGqafVkEFj97spBBzuolM8hTj26uCFtgBnJhC87lZjyLz1UtLR+AE+Pg/BIz8vX9m4E8TtSyR1vLSgSRi3qqTNhS3+R5/5jH55fGmegHQmYC8wyuA2e1CR5GPxZ7QcbEB5lA4yZxQ/rbBV6oE/oDdpFMlAQZHsK1VBlpe+wKcTK6C/ry6slASRhoc7y+uZWB8fhEZrCo1wgZKDG9mC0N6PsvQyzMwyZuA/q5V8Dwv9iTov2SXPPiyWS63tyG5lLv2flp6jM0l5ng8N/D2+OBDBhd4hbI66PMrstjteyxdwUaPJsCPXf3Dziza2RQ0x6dGhC1Q2ykRL2BEP4AdYoOPDarog8AAAAASUVORK5CYII=</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
