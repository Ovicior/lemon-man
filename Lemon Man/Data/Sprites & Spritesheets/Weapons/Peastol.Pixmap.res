﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">3</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">1</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487" length="6">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">12</H>
        <W dataType="Float">27</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">12</H>
        <W dataType="Float">27</W>
        <X dataType="Float">27</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">12</H>
        <W dataType="Float">27</W>
        <X dataType="Float">54</X>
        <Y dataType="Float">0</Y>
      </item>
    </_items>
    <_size dataType="Int">3</_size>
    <_version dataType="Int">12</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAFEAAAAMCAYAAAAan1EfAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAGPSURBVFhHYxgO4N2jc3BMDwC16z+97KMpOLFmBsPt45sZHl7YQ1dPQQMRjAkCK08OEPUfhDUd2eBsPVdWONslkAXO9gxhBrHJAqTaFZ0owTCluQQUkKDAAwcgUZ4CAnr6CwT+r7sj8T9xhyDYMGT2rb8cYAwTh7GBmFxAsl2wAHx8aR/D/TM7wCmTSECyXUBMNoAbjoxhFoEwzCJdbTOYPLmAJLuQUiAYg/j3Tm8HZ28iApMq/gKlfBDGC2R02UHUf1sN+f/5HpZwjGwRtQKRVLvQAxGGQeKEApFa/gLaB8IE/Qy2SFVWEkzDMCgWYRaB2IQsIxKQZBd6IIL4pKREUuwCqYdoQwXQQITycAO4ZSA2MgZZAsLobCAmF5BsFywgn10/THKZSEd/YVgGAv+5hJjANAgr2CJqNyibXECSXRa+0mTXzkBAT39BLAPR7GysEBGIwbQAJNsFSnlkthMp8hcswoi1D1uSpxUg2y5SPQUEFPkLahdxkcbDBWmUcnOAazOagqFkFyzCIIHIwAAAiGifto1sheMAAAAASUVORK5CYII=</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
