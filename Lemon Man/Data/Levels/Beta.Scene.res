﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">48.8</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="2511523052">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2647580066">
        <_items dataType="Array" type="Duality.Component[]" id="178379024" length="8">
          <item dataType="Struct" type="Duality.Components.Transform" id="576870688">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-550</X>
              <Y dataType="Float">-235</Y>
              <Z dataType="Float">-300</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-550</X>
              <Y dataType="Float">-235</Y>
              <Z dataType="Float">-300</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Camera" id="3048798859">
            <active dataType="Bool">true</active>
            <farZ dataType="Float">10000</farZ>
            <focusDist dataType="Float">500</focusDist>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
            <nearZ dataType="Float">0</nearZ>
            <passes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Camera+Pass]]" id="4189425567">
              <_items dataType="Array" type="Duality.Components.Camera+Pass[]" id="2743093614" length="4">
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="2592933968">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="All" value="3" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="PerspectiveWorld" value="0" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="AllGroups" value="2147483647" />
                </item>
                <item dataType="Struct" type="Duality.Components.Camera+Pass" id="3724855662">
                  <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                  <clearDepth dataType="Float">1</clearDepth>
                  <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="None" value="0" />
                  <input />
                  <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="OrthoScreen" value="1" />
                  <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                  <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </passes>
            <perspective dataType="Enum" type="Duality.Drawing.PerspectiveMode" name="Parallax" value="1" />
            <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
          </item>
          <item dataType="Struct" type="Duality.Components.SoundListener" id="3165004423">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
          </item>
          <item dataType="Struct" type="Camera.CameraFollow" id="1051768081">
            <_x003C_PlayerObject_x003E_k__BackingField dataType="Struct" type="Duality.GameObject" id="787527030">
              <active dataType="Bool">true</active>
              <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="3740027105">
                <_items dataType="Array" type="Duality.GameObject[]" id="3062691694" length="4" />
                <_size dataType="Int">0</_size>
                <_version dataType="Int">2</_version>
              </children>
              <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2174773792">
                <_items dataType="Array" type="Duality.Component[]" id="330374507" length="16">
                  <item dataType="Struct" type="Duality.Components.Transform" id="3147841962">
                    <active dataType="Bool">true</active>
                    <angle dataType="Float">0</angle>
                    <angleAbs dataType="Float">0</angleAbs>
                    <angleVel dataType="Float">0</angleVel>
                    <angleVelAbs dataType="Float">0</angleVelAbs>
                    <deriveAngle dataType="Bool">true</deriveAngle>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <ignoreParent dataType="Bool">false</ignoreParent>
                    <parentTransform />
                    <pos dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">-685</X>
                      <Y dataType="Float">-241</Y>
                      <Z dataType="Float">-1</Z>
                    </pos>
                    <posAbs dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">-685</X>
                      <Y dataType="Float">-241</Y>
                      <Z dataType="Float">-1</Z>
                    </posAbs>
                    <scale dataType="Float">1</scale>
                    <scaleAbs dataType="Float">1</scaleAbs>
                    <vel dataType="Struct" type="Duality.Vector3" />
                    <velAbs dataType="Struct" type="Duality.Vector3" />
                  </item>
                  <item dataType="Struct" type="Player.PlayerController" id="1575548244">
                    <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
                    <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
                    <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">4</X>
                      <Y dataType="Float">0.1</Y>
                      <Z dataType="Float">-0.1</Z>
                    </_x003C_BulletSpawnOffset_x003E_k__BackingField>
                    <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Player.FacingDirection" name="Left" value="0" />
                    <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">300</_x003C_FiringDelay_x003E_k__BackingField>
                    <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
                    <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
                    <_x003C_PlayerAnimationState_x003E_k__BackingField dataType="Enum" type="Player.PlayerAnimationState" name="Idle" value="0" />
                    <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationManager dataType="Struct" type="Manager.AnimationManager" id="3558738014">
                      <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">LemonMan</_x003C_animatedObjectName_x003E_k__BackingField>
                      <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="2099245324">
                        <_items dataType="Array" type="Misc.Animation[]" id="2903414948">
                          <item dataType="Struct" type="Misc.Animation" id="547758276">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">6</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="3392830358">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">4</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.5</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">7</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="1621268864">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">jump</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="3338486306">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">fall</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                        </_items>
                        <_size dataType="Int">4</_size>
                        <_version dataType="Int">4</_version>
                      </_x003C_animationDatabase_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <animationDuration dataType="Float">0</animationDuration>
                      <animationToReturnTo />
                      <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="494962411">
                        <active dataType="Bool">true</active>
                        <animDuration dataType="Float">2</animDuration>
                        <animFirstFrame dataType="Int">6</animFirstFrame>
                        <animFrameCount dataType="Int">4</animFrameCount>
                        <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                        <animPaused dataType="Bool">false</animPaused>
                        <animTime dataType="Float">0</animTime>
                        <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                          <A dataType="Byte">255</A>
                          <B dataType="Byte">255</B>
                          <G dataType="Byte">255</G>
                          <R dataType="Byte">255</R>
                        </colorTint>
                        <customFrameSequence />
                        <customMat />
                        <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                        <gameobj dataType="ObjectRef">787527030</gameobj>
                        <offset dataType="Int">0</offset>
                        <pixelGrid dataType="Bool">false</pixelGrid>
                        <rect dataType="Struct" type="Duality.Rect">
                          <H dataType="Float">26</H>
                          <W dataType="Float">24</W>
                          <X dataType="Float">-12</X>
                          <Y dataType="Float">-13</Y>
                        </rect>
                        <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                        <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                          <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                        </sharedMat>
                        <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                      </animSpriteRenderer>
                      <currentAnimationName />
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                    </animationManager>
                    <firingDelayCounter dataType="Float">0</firingDelayCounter>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <gravity dataType="Float">444.444458</gravity>
                    <heldWeapon dataType="Struct" type="Behavior.HeldWeapon" id="301082255">
                      <active dataType="Bool">true</active>
                      <gameobj dataType="Struct" type="Duality.GameObject" id="672812220">
                        <active dataType="Bool">true</active>
                        <children />
                        <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2119560839">
                          <_items dataType="Array" type="Duality.Component[]" id="3898180430">
                            <item dataType="Struct" type="Duality.Components.Transform" id="3033127152">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">672812220</gameobj>
                            </item>
                            <item dataType="Struct" type="Manager.AnimationManager" id="3444023204">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">672812220</gameobj>
                            </item>
                            <item dataType="ObjectRef">301082255</item>
                            <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="380247601">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">672812220</gameobj>
                            </item>
                          </_items>
                          <_size dataType="Int">4</_size>
                          <_version dataType="Int">4</_version>
                        </compList>
                        <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2569033088" surrogate="true">
                          <header />
                          <body>
                            <keys dataType="Array" type="System.Object[]" id="1200891693">
                              <item dataType="Type" id="1715766886" value="Duality.Components.Transform" />
                              <item dataType="Type" id="198886202" value="Manager.AnimationManager" />
                              <item dataType="Type" id="2584806118" value="Behavior.HeldWeapon" />
                              <item dataType="Type" id="891076794" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                            </keys>
                            <values dataType="Array" type="System.Object[]" id="618863224">
                              <item dataType="ObjectRef">3033127152</item>
                              <item dataType="ObjectRef">3444023204</item>
                              <item dataType="ObjectRef">301082255</item>
                              <item dataType="ObjectRef">380247601</item>
                            </values>
                          </body>
                        </compMap>
                        <compTransform dataType="ObjectRef">3033127152</compTransform>
                        <identifier dataType="Struct" type="System.Guid" surrogate="true">
                          <header>
                            <data dataType="Array" type="System.Byte[]" id="3278327623">ww7ZFsN1MUKIHkuvVynjTg==</data>
                          </header>
                          <body />
                        </identifier>
                        <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                        <name dataType="String">Weapon</name>
                        <parent />
                        <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3862741381">
                          <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2173869076">
                            <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="4285230692" length="4">
                              <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                                <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4208132808">
                                  <_items dataType="Array" type="System.Int32[]" id="143691372"></_items>
                                  <_size dataType="Int">0</_size>
                                  <_version dataType="Int">1</_version>
                                </childIndex>
                                <componentType dataType="ObjectRef">1715766886</componentType>
                                <prop dataType="MemberInfo" id="3163653854" value="P:Duality.Components.Transform:RelativePos" />
                                <val dataType="Struct" type="Duality.Vector3">
                                  <X dataType="Float">0</X>
                                  <Y dataType="Float">0</Y>
                                  <Z dataType="Float">-1</Z>
                                </val>
                              </item>
                            </_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">393</_version>
                          </changes>
                          <obj dataType="ObjectRef">672812220</obj>
                          <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                            <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                          </prefab>
                        </prefabLink>
                      </gameobj>
                    </heldWeapon>
                    <jumpVelocity dataType="Float">200</jumpVelocity>
                    <rigidBody dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3850303554">
                      <active dataType="Bool">true</active>
                      <angularDamp dataType="Float">0.3</angularDamp>
                      <angularVel dataType="Float">0</angularVel>
                      <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                      <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1, Cat2" value="3" />
                      <colFilter />
                      <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
                      <continous dataType="Bool">false</continous>
                      <explicitInertia dataType="Float">0</explicitInertia>
                      <explicitMass dataType="Float">0</explicitMass>
                      <fixedAngle dataType="Bool">true</fixedAngle>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                      <ignoreGravity dataType="Bool">true</ignoreGravity>
                      <joints />
                      <linearDamp dataType="Float">0.3</linearDamp>
                      <linearVel dataType="Struct" type="Duality.Vector2" />
                      <revolutions dataType="Float">0</revolutions>
                      <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="278714248">
                        <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="4276663660" length="4">
                          <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="49208164">
                            <density dataType="Float">1</density>
                            <friction dataType="Float">0.3</friction>
                            <parent dataType="ObjectRef">3850303554</parent>
                            <restitution dataType="Float">0.3</restitution>
                            <sensor dataType="Bool">false</sensor>
                            <vertices dataType="Array" type="Duality.Vector2[]" id="1099912132">
                              <item dataType="Struct" type="Duality.Vector2">
                                <X dataType="Float">-5</X>
                                <Y dataType="Float">-3</Y>
                              </item>
                              <item dataType="Struct" type="Duality.Vector2">
                                <X dataType="Float">5</X>
                                <Y dataType="Float">-3</Y>
                              </item>
                              <item dataType="Struct" type="Duality.Vector2">
                                <X dataType="Float">5</X>
                                <Y dataType="Float">9</Y>
                              </item>
                              <item dataType="Struct" type="Duality.Vector2">
                                <X dataType="Float">-5</X>
                                <Y dataType="Float">9</Y>
                              </item>
                            </vertices>
                          </item>
                        </_items>
                        <_size dataType="Int">1</_size>
                        <_version dataType="Int">11</_version>
                      </shapes>
                    </rigidBody>
                    <spriteRenderer dataType="ObjectRef">494962411</spriteRenderer>
                    <velocity dataType="Struct" type="Duality.Vector2" />
                  </item>
                  <item dataType="Struct" type="Behavior.EntityStats" id="1425810874">
                    <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">35</_x003C_CurrentHealth_x003E_k__BackingField>
                    <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
                    <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">35</_x003C_MaxHealth_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationManager dataType="ObjectRef">3558738014</animationManager>
                    <currentTime dataType="Float">0</currentTime>
                    <damageEffectLength dataType="Float">0.1</damageEffectLength>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                    </material>
                    <soundManager dataType="Struct" type="Manager.SoundManager" id="243598982">
                      <active dataType="Bool">true</active>
                      <eightiesTrack dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\80s.Sound.res</contentPath>
                      </eightiesTrack>
                      <gameobj dataType="Struct" type="Duality.GameObject" id="2834260959">
                        <active dataType="Bool">true</active>
                        <children />
                        <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="470140773">
                          <_items dataType="Array" type="Duality.Component[]" id="2451953558" length="4">
                            <item dataType="Struct" type="Manager.WeaponDatabaseManager" id="3611639126">
                              <_x003C_weaponDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Weapon]]" id="3106059642">
                                <_items dataType="Array" type="Misc.Weapon[]" id="4154130304" length="4">
                                  <item dataType="Struct" type="Misc.Weapon" id="1343783324">
                                    <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">27</_x003C_BottomLeft_x003E_k__BackingField>
                                    <_x003C_BottomRight_x003E_k__BackingField dataType="Float">12</_x003C_BottomRight_x003E_k__BackingField>
                                    <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                                    <_x003C_ID_x003E_k__BackingField dataType="Int">0</_x003C_ID_x003E_k__BackingField>
                                    <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">3</_x003C_Inaccuracy_x003E_k__BackingField>
                                    <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                                    <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                                    <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">3</_x003C_RateOfFire_x003E_k__BackingField>
                                    <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                                    <_x003C_Slug_x003E_k__BackingField dataType="String">peastol</_x003C_Slug_x003E_k__BackingField>
                                    <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\peastol.Texture.res</contentPath>
                                    </_x003C_Sprite_x003E_k__BackingField>
                                    <_x003C_Title_x003E_k__BackingField dataType="String">Peastol</_x003C_Title_x003E_k__BackingField>
                                    <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-13.5</_x003C_TopLeft_x003E_k__BackingField>
                                    <_x003C_TopRight_x003E_k__BackingField dataType="Float">-6</_x003C_TopRight_x003E_k__BackingField>
                                    <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                                    <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                                    <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
                                  </item>
                                  <item dataType="Struct" type="Misc.Weapon" id="2564317206">
                                    <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">33</_x003C_BottomLeft_x003E_k__BackingField>
                                    <_x003C_BottomRight_x003E_k__BackingField dataType="Float">18</_x003C_BottomRight_x003E_k__BackingField>
                                    <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                                    <_x003C_ID_x003E_k__BackingField dataType="Int">1</_x003C_ID_x003E_k__BackingField>
                                    <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">5</_x003C_Inaccuracy_x003E_k__BackingField>
                                    <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">17.9</_x003C_PrefabXOffset_x003E_k__BackingField>
                                    <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                                    <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">5</_x003C_RateOfFire_x003E_k__BackingField>
                                    <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Heavy" value="2" />
                                    <_x003C_Slug_x003E_k__BackingField dataType="String">lemonade_launcher</_x003C_Slug_x003E_k__BackingField>
                                    <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\lemonade_launcher.Texture.res</contentPath>
                                    </_x003C_Sprite_x003E_k__BackingField>
                                    <_x003C_Title_x003E_k__BackingField dataType="String">Lemonade Launcher</_x003C_Title_x003E_k__BackingField>
                                    <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-16.5</_x003C_TopLeft_x003E_k__BackingField>
                                    <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9</_x003C_TopRight_x003E_k__BackingField>
                                    <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Lemonade" value="1" />
                                    <_x003C_XOffset_x003E_k__BackingField dataType="Float">8.45</_x003C_XOffset_x003E_k__BackingField>
                                    <_x003C_YOffset_x003E_k__BackingField dataType="Float">2</_x003C_YOffset_x003E_k__BackingField>
                                  </item>
                                  <item dataType="Struct" type="Misc.Weapon" id="2583183368">
                                    <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">29</_x003C_BottomLeft_x003E_k__BackingField>
                                    <_x003C_BottomRight_x003E_k__BackingField dataType="Float">19</_x003C_BottomRight_x003E_k__BackingField>
                                    <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                                    <_x003C_ID_x003E_k__BackingField dataType="Int">2</_x003C_ID_x003E_k__BackingField>
                                    <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">0</_x003C_Inaccuracy_x003E_k__BackingField>
                                    <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                                    <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">2.8</_x003C_PrefabYOffset_x003E_k__BackingField>
                                    <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">0.5</_x003C_RateOfFire_x003E_k__BackingField>
                                    <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                                    <_x003C_Slug_x003E_k__BackingField dataType="String">pepper_thrower</_x003C_Slug_x003E_k__BackingField>
                                    <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\pepper_thrower.Texture.res</contentPath>
                                    </_x003C_Sprite_x003E_k__BackingField>
                                    <_x003C_Title_x003E_k__BackingField dataType="String">Pepper Thrower</_x003C_Title_x003E_k__BackingField>
                                    <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-14.5</_x003C_TopLeft_x003E_k__BackingField>
                                    <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9.5</_x003C_TopRight_x003E_k__BackingField>
                                    <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pepper" value="2" />
                                    <_x003C_XOffset_x003E_k__BackingField dataType="Float">14.5</_x003C_XOffset_x003E_k__BackingField>
                                    <_x003C_YOffset_x003E_k__BackingField dataType="Float">2.4</_x003C_YOffset_x003E_k__BackingField>
                                  </item>
                                </_items>
                                <_size dataType="Int">3</_size>
                                <_version dataType="Int">3</_version>
                              </_x003C_weaponDatabase_x003E_k__BackingField>
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">2834260959</gameobj>
                            </item>
                            <item dataType="ObjectRef">243598982</item>
                          </_items>
                          <_size dataType="Int">2</_size>
                          <_version dataType="Int">12</_version>
                        </compList>
                        <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="4029559400" surrogate="true">
                          <header />
                          <body>
                            <keys dataType="Array" type="System.Object[]" id="1367010319">
                              <item dataType="Type" id="1980106670" value="Manager.WeaponDatabaseManager" />
                              <item dataType="Type" id="2365249226" value="Manager.SoundManager" />
                            </keys>
                            <values dataType="Array" type="System.Object[]" id="3288576992">
                              <item dataType="ObjectRef">3611639126</item>
                              <item dataType="ObjectRef">243598982</item>
                            </values>
                          </body>
                        </compMap>
                        <compTransform />
                        <identifier dataType="Struct" type="System.Guid" surrogate="true">
                          <header>
                            <data dataType="Array" type="System.Byte[]" id="621482333">biwph8Z5kk+28lYMuemNIw==</data>
                          </header>
                          <body />
                        </identifier>
                        <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                        <name dataType="String">WorldManagers</name>
                        <parent />
                        <prefabLink />
                      </gameobj>
                      <happy dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\Happy.Sound.res</contentPath>
                      </happy>
                      <hurt dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\Hurt.Sound.res</contentPath>
                      </hurt>
                      <lemonade dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\LemonLauncher1.Sound.res</contentPath>
                      </lemonade>
                      <lemonLauncher dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\LemonlauncherShoot1.Sound.res</contentPath>
                      </lemonLauncher>
                      <peastol dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\Peastol.Sound.res</contentPath>
                      </peastol>
                      <pop dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\PopMaybe.Sound.res</contentPath>
                      </pop>
                    </soundManager>
                    <timer dataType="Struct" type="Misc.Timer" id="856554177">
                      <_x003C_LengthOfCycle_x003E_k__BackingField dataType="Float">0.1</_x003C_LengthOfCycle_x003E_k__BackingField>
                      <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                      <timerStart dataType="Bool">false</timerStart>
                    </timer>
                  </item>
                  <item dataType="ObjectRef">494962411</item>
                  <item dataType="ObjectRef">3558738014</item>
                  <item dataType="Struct" type="Manager.WeaponSpawner" id="1877854119">
                    <active dataType="Bool">true</active>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <heldWeaponPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                    </heldWeaponPrefab>
                  </item>
                  <item dataType="Struct" type="Behavior.Controller2D" id="3211926186">
                    <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                    <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="Player" value="0" />
                    <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
                    <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
                    <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">14.4</X>
                      <Y dataType="Float">10.12</Y>
                    </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
                    <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">7.4</X>
                      <Y dataType="Float">10</Y>
                    </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
                    <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <bounds dataType="Struct" type="Duality.Rect">
                      <H dataType="Float">15.88</H>
                      <W dataType="Float">9.6</W>
                      <X dataType="Float">-689.6</X>
                      <Y dataType="Float">-244</Y>
                    </bounds>
                    <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
                    <enemyController />
                    <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <horizontalRaySpacing dataType="Float">7.925</horizontalRaySpacing>
                    <maxClimbAngle dataType="Float">60</maxClimbAngle>
                    <playerController dataType="ObjectRef">1575548244</playerController>
                    <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
                    <rigidBody dataType="ObjectRef">3850303554</rigidBody>
                    <verticalRaySpacing dataType="Float">4.78500032</verticalRaySpacing>
                  </item>
                  <item dataType="ObjectRef">856554177</item>
                  <item dataType="ObjectRef">3850303554</item>
                </_items>
                <_size dataType="Int">9</_size>
                <_version dataType="Int">29</_version>
              </compList>
              <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="200955507" surrogate="true">
                <header />
                <body>
                  <keys dataType="Array" type="System.Object[]" id="4117914532">
                    <item dataType="ObjectRef">1715766886</item>
                    <item dataType="ObjectRef">891076794</item>
                    <item dataType="Type" id="533349572" value="Player.PlayerController" />
                    <item dataType="Type" id="4154674070" value="Behavior.EntityStats" />
                    <item dataType="ObjectRef">198886202</item>
                    <item dataType="Type" id="808633728" value="Manager.WeaponSpawner" />
                    <item dataType="Type" id="2066733602" value="Behavior.Controller2D" />
                    <item dataType="Type" id="2343907548" value="Misc.Timer" />
                    <item dataType="Type" id="12090110" value="Duality.Components.Physics.RigidBody" />
                  </keys>
                  <values dataType="Array" type="System.Object[]" id="1922096918">
                    <item dataType="ObjectRef">3147841962</item>
                    <item dataType="ObjectRef">494962411</item>
                    <item dataType="ObjectRef">1575548244</item>
                    <item dataType="ObjectRef">1425810874</item>
                    <item dataType="ObjectRef">3558738014</item>
                    <item dataType="ObjectRef">1877854119</item>
                    <item dataType="ObjectRef">3211926186</item>
                    <item dataType="ObjectRef">856554177</item>
                    <item dataType="ObjectRef">3850303554</item>
                  </values>
                </body>
              </compMap>
              <compTransform dataType="ObjectRef">3147841962</compTransform>
              <identifier dataType="Struct" type="System.Guid" surrogate="true">
                <header>
                  <data dataType="Array" type="System.Byte[]" id="3123626656">UPF8jIwOT0SjSKyQzVYuaQ==</data>
                </header>
                <body />
              </identifier>
              <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
              <name dataType="String">Player</name>
              <parent />
              <prefabLink />
            </_x003C_PlayerObject_x003E_k__BackingField>
            <_x003C_ZoomLevel_x003E_k__BackingField dataType="Int">-160</_x003C_ZoomLevel_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2511523052</gameobj>
          </item>
        </_items>
        <_size dataType="Int">4</_size>
        <_version dataType="Int">6</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="81641738" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3606603832">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="Type" id="3815977580" value="Duality.Components.Camera" />
            <item dataType="Type" id="605605942" value="Duality.Components.SoundListener" />
            <item dataType="Type" id="2748295992" value="Camera.CameraFollow" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1649367774">
            <item dataType="ObjectRef">576870688</item>
            <item dataType="ObjectRef">3048798859</item>
            <item dataType="ObjectRef">3165004423</item>
            <item dataType="ObjectRef">1051768081</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">576870688</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="4262995556">S42dOS8++kGkGJLsYQCCXQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">MainCamera</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="ObjectRef">787527030</item>
    <item dataType="ObjectRef">2834260959</item>
    <item dataType="ObjectRef">672812220</item>
    <item dataType="Struct" type="Duality.GameObject" id="876377208">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3412445086">
        <_items dataType="Array" type="Duality.Component[]" id="3098904464" length="16">
          <item dataType="Struct" type="Duality.Components.Transform" id="3236692140">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">876377208</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-224.79277</X>
              <Y dataType="Float">-265.231781</Y>
              <Z dataType="Float">-0.9</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-224.79277</X>
              <Y dataType="Float">-265.231781</Y>
              <Z dataType="Float">-0.9</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="583812589">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">5</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">51</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">876377208</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">39</H>
              <W dataType="Float">39</W>
              <X dataType="Float">-19.5</X>
              <Y dataType="Float">-19.5</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="3647588192">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">greenAppowl</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="1653798032">
              <_items dataType="Array" type="Misc.Animation[]" id="3934033212" length="8">
                <item dataType="Struct" type="Misc.Animation" id="3644432196">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">14</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1.12</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="3136728726">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">startRun</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="3433924352">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">9</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.72</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="1358882850">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">12</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.96</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">death</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">34</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="578305116">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">10</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">deathStill</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">45</_x003C_StartFrame_x003E_k__BackingField>
                </item>
              </_items>
              <_size dataType="Int">5</_size>
              <_version dataType="Int">5</_version>
            </_x003C_animationDatabase_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer dataType="ObjectRef">583812589</animSpriteRenderer>
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">876377208</gameobj>
          </item>
          <item dataType="Struct" type="Enemy.EnemyController" id="3129822232">
            <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
            <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
            <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">4</X>
              <Y dataType="Float">0.1</Y>
              <Z dataType="Float">-0.1</Z>
            </_x003C_BulletSpawnOffset_x003E_k__BackingField>
            <_x003C_EnemyAnimationState_x003E_k__BackingField dataType="Enum" type="Enemy.EnemyAnimationState" name="Idle" value="0" />
            <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
            <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">10</_x003C_FiringDelay_x003E_k__BackingField>
            <_x003C_HorizontalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.HorizontalMovement" name="None" value="2" />
            <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
            <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
            <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
            <_x003C_VerticalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.VerticalMovement" name="None" value="2" />
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">3647588192</animationManager>
            <checkLeftMovement dataType="Bool">false</checkLeftMovement>
            <checkRightMovement dataType="Bool">false</checkRightMovement>
            <currentXPosition dataType="Float">0</currentXPosition>
            <entityStats dataType="Struct" type="Behavior.EntityStats" id="1514661052">
              <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">25</_x003C_CurrentHealth_x003E_k__BackingField>
              <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
              <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">25</_x003C_MaxHealth_x003E_k__BackingField>
              <active dataType="Bool">true</active>
              <animationManager dataType="ObjectRef">3647588192</animationManager>
              <currentTime dataType="Float">0</currentTime>
              <damageEffectLength dataType="Float">0.1</damageEffectLength>
              <gameobj dataType="ObjectRef">876377208</gameobj>
              <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
              </material>
              <soundManager dataType="ObjectRef">243598982</soundManager>
              <timer dataType="Struct" type="Misc.Timer" id="945404355">
                <_x003C_LengthOfCycle_x003E_k__BackingField dataType="Float">0.1</_x003C_LengthOfCycle_x003E_k__BackingField>
                <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <currentTime dataType="Float">0</currentTime>
                <gameobj dataType="ObjectRef">876377208</gameobj>
                <timerStart dataType="Bool">false</timerStart>
              </timer>
            </entityStats>
            <firingDelayCounter dataType="Float">0</firingDelayCounter>
            <gameobj dataType="ObjectRef">876377208</gameobj>
            <gravity dataType="Float">444.444458</gravity>
            <heldWeapon dataType="ObjectRef">301082255</heldWeapon>
            <jumpVelocity dataType="Float">200</jumpVelocity>
            <shapeInfo />
            <spriteRenderer dataType="ObjectRef">583812589</spriteRenderer>
            <targetXPosition dataType="Float">0</targetXPosition>
            <velocity dataType="Struct" type="Duality.Vector2" />
          </item>
          <item dataType="Struct" type="Behavior.Controller2D" id="3300776364">
            <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
            <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="GreenAppowl" value="1" />
            <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
            <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
            <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">14.4</X>
              <Y dataType="Float">17</Y>
            </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
            <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">7.4</X>
              <Y dataType="Float">10</Y>
            </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
            <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <bounds dataType="Struct" type="Duality.Rect">
              <H dataType="Float">22</H>
              <W dataType="Float">24.6</W>
              <X dataType="Float">-236.892776</X>
              <Y dataType="Float">-274.731781</Y>
            </bounds>
            <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
            <enemyController dataType="ObjectRef">3129822232</enemyController>
            <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
            <gameobj dataType="ObjectRef">876377208</gameobj>
            <horizontalRaySpacing dataType="Float">10.985</horizontalRaySpacing>
            <maxClimbAngle dataType="Float">60</maxClimbAngle>
            <playerController />
            <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
            <rigidBody dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3939153732">
              <active dataType="Bool">true</active>
              <angularDamp dataType="Float">0.3</angularDamp>
              <angularVel dataType="Float">0</angularVel>
              <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
              <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
              <colFilter />
              <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
              <continous dataType="Bool">false</continous>
              <explicitInertia dataType="Float">0</explicitInertia>
              <explicitMass dataType="Float">0</explicitMass>
              <fixedAngle dataType="Bool">true</fixedAngle>
              <gameobj dataType="ObjectRef">876377208</gameobj>
              <ignoreGravity dataType="Bool">true</ignoreGravity>
              <joints />
              <linearDamp dataType="Float">0.3</linearDamp>
              <linearVel dataType="Struct" type="Duality.Vector2" />
              <revolutions dataType="Float">0</revolutions>
              <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="4026522356">
                <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2730344612">
                  <item dataType="Struct" type="Duality.Components.Physics.CircleShapeInfo" id="233140420">
                    <density dataType="Float">1</density>
                    <friction dataType="Float">0.3</friction>
                    <parent dataType="ObjectRef">3939153732</parent>
                    <position dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-1</X>
                      <Y dataType="Float">2</Y>
                    </position>
                    <radius dataType="Float">8.944272</radius>
                    <restitution dataType="Float">0.3</restitution>
                    <sensor dataType="Bool">false</sensor>
                  </item>
                </_items>
                <_size dataType="Int">1</_size>
                <_version dataType="Int">1</_version>
              </shapes>
            </rigidBody>
            <verticalRaySpacing dataType="Float">12.285</verticalRaySpacing>
          </item>
          <item dataType="ObjectRef">3939153732</item>
          <item dataType="ObjectRef">945404355</item>
          <item dataType="ObjectRef">1514661052</item>
          <item dataType="Struct" type="Enemy.GreenAppowl" id="1755638200">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">876377208</gameobj>
            <stats />
          </item>
        </_items>
        <_size dataType="Int">9</_size>
        <_version dataType="Int">13</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2114992522" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1358371516">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="Type" id="1508246084" value="Enemy.EnemyController" />
            <item dataType="ObjectRef">2066733602</item>
            <item dataType="ObjectRef">12090110</item>
            <item dataType="ObjectRef">2343907548</item>
            <item dataType="ObjectRef">4154674070</item>
            <item dataType="Type" id="4046539414" value="Enemy.GreenAppowl" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="4227203734">
            <item dataType="ObjectRef">3236692140</item>
            <item dataType="ObjectRef">583812589</item>
            <item dataType="ObjectRef">3647588192</item>
            <item dataType="ObjectRef">3129822232</item>
            <item dataType="ObjectRef">3300776364</item>
            <item dataType="ObjectRef">3939153732</item>
            <item dataType="ObjectRef">945404355</item>
            <item dataType="ObjectRef">1514661052</item>
            <item dataType="ObjectRef">1755638200</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3236692140</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="560977512">JT+kx5gynEuOOMFOLMhhwQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">GreenAppowl</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="674216840">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1466994702">
        <_items dataType="Array" type="Duality.Component[]" id="2093177808" length="8">
          <item dataType="Struct" type="Duality.Plugins.Tilemaps.Tilemap" id="2676068857">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <tileData dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapData" id="2182523037" custom="true">
              <body>
                <version dataType="Int">3</version>
                <data dataType="Array" type="System.Byte[]" id="2668734182">H4sIAAAAAAAEAO3cXW4UMQwH8IFdCi2UBYrK10t5gUv0MNz/FCDiF1eW6yR2YideCf3FqnQz5DdONpOZP8dxPP77czn+v04ZDuNDiY99cRs5XPRDhqXP0Exd9EPGAJ8x7V5ntMZCPv1qddHTMcPe5+X5WN0nvF6WcNHvUWIuzNYQ+Lwr8a3EXJ9XJd6UcNHvUSKmT0FAIz6VmKv1XYn3JVz0e5RYDya32DhJa/pMnyKfk7Smz/TZ4HOY1vSZPj1rTZ+uwoXIi+YRdWpNn08CXi+CBCwMOoSpozV9Ht9LPJR4HTJ+lsB2HTKtngmE9gmy4FVdezDMXyVcXDXBIb/yw6F1wVQ+b4Ww8AkvPAL5LXl+YZI+W9FWa7Wvn4LAPg90DNWzoIcS3Aj0g2m84NMxTBd6XKCVExZMCAxHvFaft6jxFpXodwm4kEqesFcqH7RJyPdyVE8IuBoy16d9QKEmfY5uy14hr7S4tm7p8xUV6dNHwFCHi+omPiHeUuGic3Tia4k7uxi2QwO06jCN4nMFIVx8LmH4TYNcjTG0q8NUDnP0mDrai70QvwF2HTKV+4S5sNxnJ7CdvcxlalFUW5mq+sQiVwdmv/i3nlbMtPpKTLXPvURimKv75LTqfC2rvklI7hNgbiKy2udedltdV+shYZ5LYJ8x94dYME2f44L0iWty+nxicNLGyC0DV0xYUQKYWT8zpgUpEkf6zBhoEAcnEkf6zKiJE/UmSVGwPCXYvJo+M/jg6qCAYuezoNJnBh/wED2dcpg+M2x8jn7IY/qMHeSwe0397Zr6SfJHyMCbTdLnunF+/k0BInhTp/vlC7v3JUYzTZ/j4qbEFyomlafqfh/NNH36iNGzvNYNF/hWsfTpKsgBWj6T8+Szs8OHldH0yQbGBwM0efNU5w6wYR2uulPNvtWb+JR/eeUM2ne4fRm16Ol7u+Yu65NzxoWg9uA3VffMGhYk+y2+FpPS9XzeoONTjWHbty3KqH0XW5xcC/mEby+qPif9v8T0acE0ik9yyohvnooyWxvdxZPu3dEZ7bFPcvfUXIrcYdpvVph73uqU0dDnGPZJXmyzP5Qzaoug1Zgi+XicYDNOC58ubnrsLKPcPyfRdhrUabz9Zi8XE57OEuTpGCx8yrVy64rwCa1rOp1aQ/tsLaMuCqfOqTbsQqpFjWyNYEyr+9ZFq3VGe4c+BZYsKLrvaXkZ9dTqzlON9DlsE3/1JwyD6bCnBX3rcGDvLKOcz9bqVi2r1bXgKbcL+RSUUYfN7Syj1T45pq0GVbGr+nRYiVbwKS+jnfNPVRPVv8WQqd+e5mqP31a3llGdFUidyV71rzasnw4Lp6CMBvOJnzEfxaf8gywWltzDXM8nV0Ydri+1ng+dTKPAXM8nxzSKT3lUf40P1qnL+jxRB/YXVEEKuoidAAA=</data>
              </body>
            </tileData>
            <tileset dataType="Struct" type="Duality.ContentRef`1[[Duality.Plugins.Tilemaps.Tileset]]">
              <contentPath dataType="String">Data\Tilesets\Grasslands.Tileset.res</contentPath>
            </tileset>
          </item>
          <item dataType="Struct" type="Duality.Components.Transform" id="3034531772">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3" />
            <posAbs dataType="Struct" type="Duality.Vector3" />
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapRenderer" id="3667292288">
            <active dataType="Bool">true</active>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <externalTilemap />
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <offset dataType="Float">0</offset>
            <origin dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
            <tileDepthMode dataType="Enum" type="Duality.Plugins.Tilemaps.TileDepthOffsetMode" name="Flat" value="0" />
            <tileDepthOffset dataType="Int">0</tileDepthOffset>
            <tileDepthScale dataType="Float">0.01</tileDepthScale>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3736993364">
            <active dataType="Bool">true</active>
            <angularDamp dataType="Float">0.3</angularDamp>
            <angularVel dataType="Float">0</angularVel>
            <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
            <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
            <colFilter />
            <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1, Cat2" value="3" />
            <continous dataType="Bool">true</continous>
            <explicitInertia dataType="Float">0</explicitInertia>
            <explicitMass dataType="Float">0.01</explicitMass>
            <fixedAngle dataType="Bool">true</fixedAngle>
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <ignoreGravity dataType="Bool">true</ignoreGravity>
            <joints />
            <linearDamp dataType="Float">0.3</linearDamp>
            <linearVel dataType="Struct" type="Duality.Vector2" />
            <revolutions dataType="Float">0</revolutions>
            <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3755750940">
              <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2103280068" length="32" />
              <_size dataType="Int">0</_size>
              <_version dataType="Int">10078</_version>
            </shapes>
          </item>
          <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollider" id="3083478055">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <origin dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
            <roundedCorners dataType="Bool">false</roundedCorners>
            <shapeFriction dataType="Float">0.299999982</shapeFriction>
            <shapeRestitution dataType="Float">0.299999982</shapeRestitution>
            <solidOuterEdges dataType="Bool">true</solidOuterEdges>
            <source dataType="Array" type="Duality.Plugins.Tilemaps.TilemapCollisionSource[]" id="3358612099">
              <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollisionSource">
                <Layers dataType="Enum" type="Duality.Plugins.Tilemaps.TileCollisionLayer" name="Layer0" value="1" />
                <SourceTilemap />
              </item>
            </source>
          </item>
          <item dataType="Struct" type="Misc.TilemapPositionMaintainer" id="1512622023">
            <_x003C_positionToMaintain_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">674216840</gameobj>
            <transform dataType="ObjectRef">3034531772</transform>
          </item>
        </_items>
        <_size dataType="Int">6</_size>
        <_version dataType="Int">6</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1993390922" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1140621772">
            <item dataType="Type" id="2476887204" value="Duality.Plugins.Tilemaps.Tilemap" />
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="Type" id="141779734" value="Duality.Plugins.Tilemaps.TilemapRenderer" />
            <item dataType="ObjectRef">12090110</item>
            <item dataType="Type" id="1229872032" value="Duality.Plugins.Tilemaps.TilemapCollider" />
            <item dataType="Type" id="1648253538" value="Misc.TilemapPositionMaintainer" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="540318454">
            <item dataType="ObjectRef">2676068857</item>
            <item dataType="ObjectRef">3034531772</item>
            <item dataType="ObjectRef">3667292288</item>
            <item dataType="ObjectRef">3736993364</item>
            <item dataType="ObjectRef">3083478055</item>
            <item dataType="ObjectRef">1512622023</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3034531772</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="342318552">EPthak7nzUCHnaWjvJd8VQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Foreground Map</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3790891982">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2544453592">
        <_items dataType="Array" type="Duality.Component[]" id="664579500" length="16">
          <item dataType="Struct" type="Duality.Components.Transform" id="1856239618">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">3790891982</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">292</X>
              <Y dataType="Float">63</Y>
              <Z dataType="Float">0</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">292</X>
              <Y dataType="Float">63</Y>
              <Z dataType="Float">0</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3498327363">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">5</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">51</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">3790891982</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">39</H>
              <W dataType="Float">39</W>
              <X dataType="Float">-19.5</X>
              <Y dataType="Float">-19.5</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="2267135670">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">greenAppowl</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="1030423246">
              <_items dataType="Array" type="Misc.Animation[]" id="4271981520" length="8">
                <item dataType="Struct" type="Misc.Animation" id="1320499900">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">14</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1.12</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="446579350">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">startRun</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="3159520872">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">9</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.72</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="1762948466">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">12</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.96</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">death</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">34</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="3484635412">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">10</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">deathStill</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">45</_x003C_StartFrame_x003E_k__BackingField>
                </item>
              </_items>
              <_size dataType="Int">5</_size>
              <_version dataType="Int">5</_version>
            </_x003C_animationDatabase_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer dataType="ObjectRef">3498327363</animSpriteRenderer>
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">3790891982</gameobj>
          </item>
          <item dataType="Struct" type="Enemy.EnemyController" id="1749369710">
            <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
            <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
            <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">4</X>
              <Y dataType="Float">0.1</Y>
              <Z dataType="Float">-0.1</Z>
            </_x003C_BulletSpawnOffset_x003E_k__BackingField>
            <_x003C_EnemyAnimationState_x003E_k__BackingField dataType="Enum" type="Enemy.EnemyAnimationState" name="Idle" value="0" />
            <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Enemy.FacingDirection" name="Right" value="1" />
            <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">10</_x003C_FiringDelay_x003E_k__BackingField>
            <_x003C_HorizontalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.HorizontalMovement" name="None" value="2" />
            <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
            <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
            <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
            <_x003C_VerticalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.VerticalMovement" name="None" value="2" />
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">2267135670</animationManager>
            <checkLeftMovement dataType="Bool">false</checkLeftMovement>
            <checkRightMovement dataType="Bool">false</checkRightMovement>
            <currentXPosition dataType="Float">0</currentXPosition>
            <entityStats dataType="Struct" type="Behavior.EntityStats" id="134208530">
              <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">25</_x003C_CurrentHealth_x003E_k__BackingField>
              <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
              <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">25</_x003C_MaxHealth_x003E_k__BackingField>
              <active dataType="Bool">true</active>
              <animationManager dataType="ObjectRef">2267135670</animationManager>
              <currentTime dataType="Float">0</currentTime>
              <damageEffectLength dataType="Float">0.1</damageEffectLength>
              <gameobj dataType="ObjectRef">3790891982</gameobj>
              <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
              </material>
              <soundManager dataType="ObjectRef">243598982</soundManager>
              <timer dataType="Struct" type="Misc.Timer" id="3859919129">
                <_x003C_LengthOfCycle_x003E_k__BackingField dataType="Float">0.1</_x003C_LengthOfCycle_x003E_k__BackingField>
                <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <currentTime dataType="Float">0</currentTime>
                <gameobj dataType="ObjectRef">3790891982</gameobj>
                <timerStart dataType="Bool">false</timerStart>
              </timer>
            </entityStats>
            <firingDelayCounter dataType="Float">0</firingDelayCounter>
            <gameobj dataType="ObjectRef">3790891982</gameobj>
            <gravity dataType="Float">444.444458</gravity>
            <heldWeapon dataType="ObjectRef">301082255</heldWeapon>
            <jumpVelocity dataType="Float">200</jumpVelocity>
            <shapeInfo />
            <spriteRenderer dataType="ObjectRef">3498327363</spriteRenderer>
            <targetXPosition dataType="Float">0</targetXPosition>
            <velocity dataType="Struct" type="Duality.Vector2" />
          </item>
          <item dataType="Struct" type="Behavior.Controller2D" id="1920323842">
            <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
            <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="GreenAppowl" value="1" />
            <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
            <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
            <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">14.4</X>
              <Y dataType="Float">17</Y>
            </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
            <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">7.4</X>
              <Y dataType="Float">10</Y>
            </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
            <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <bounds dataType="Struct" type="Duality.Rect">
              <H dataType="Float">22</H>
              <W dataType="Float">24.6</W>
              <X dataType="Float">279.9</X>
              <Y dataType="Float">53.5</Y>
            </bounds>
            <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
            <enemyController dataType="ObjectRef">1749369710</enemyController>
            <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
            <gameobj dataType="ObjectRef">3790891982</gameobj>
            <horizontalRaySpacing dataType="Float">10.985</horizontalRaySpacing>
            <maxClimbAngle dataType="Float">60</maxClimbAngle>
            <playerController />
            <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
            <rigidBody dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2558701210">
              <active dataType="Bool">true</active>
              <angularDamp dataType="Float">0.3</angularDamp>
              <angularVel dataType="Float">0</angularVel>
              <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
              <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
              <colFilter />
              <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
              <continous dataType="Bool">false</continous>
              <explicitInertia dataType="Float">0</explicitInertia>
              <explicitMass dataType="Float">0</explicitMass>
              <fixedAngle dataType="Bool">true</fixedAngle>
              <gameobj dataType="ObjectRef">3790891982</gameobj>
              <ignoreGravity dataType="Bool">true</ignoreGravity>
              <joints />
              <linearDamp dataType="Float">0.3</linearDamp>
              <linearVel dataType="Struct" type="Duality.Vector2" />
              <revolutions dataType="Float">0</revolutions>
              <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="4234991512">
                <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3416912940">
                  <item dataType="Struct" type="Duality.Components.Physics.CircleShapeInfo" id="829936868">
                    <density dataType="Float">1</density>
                    <friction dataType="Float">0.3</friction>
                    <parent dataType="ObjectRef">2558701210</parent>
                    <position dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-1</X>
                      <Y dataType="Float">2</Y>
                    </position>
                    <radius dataType="Float">8.944272</radius>
                    <restitution dataType="Float">0.3</restitution>
                    <sensor dataType="Bool">false</sensor>
                  </item>
                </_items>
                <_size dataType="Int">1</_size>
                <_version dataType="Int">2</_version>
              </shapes>
            </rigidBody>
            <verticalRaySpacing dataType="Float">12.285</verticalRaySpacing>
          </item>
          <item dataType="ObjectRef">2558701210</item>
          <item dataType="ObjectRef">3859919129</item>
          <item dataType="ObjectRef">134208530</item>
          <item dataType="Struct" type="Enemy.GreenAppowl" id="375185678">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3790891982</gameobj>
            <stats />
          </item>
        </_items>
        <_size dataType="Int">9</_size>
        <_version dataType="Int">9</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="225819294" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3159028634">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">1508246084</item>
            <item dataType="ObjectRef">2066733602</item>
            <item dataType="ObjectRef">12090110</item>
            <item dataType="ObjectRef">2343907548</item>
            <item dataType="ObjectRef">4154674070</item>
            <item dataType="ObjectRef">4046539414</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="1112774458">
            <item dataType="ObjectRef">1856239618</item>
            <item dataType="ObjectRef">3498327363</item>
            <item dataType="ObjectRef">2267135670</item>
            <item dataType="ObjectRef">1749369710</item>
            <item dataType="ObjectRef">1920323842</item>
            <item dataType="ObjectRef">2558701210</item>
            <item dataType="ObjectRef">3859919129</item>
            <item dataType="ObjectRef">134208530</item>
            <item dataType="ObjectRef">375185678</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1856239618</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2959654682">kik5CM0qy0K8ssNd/LHJlw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">GreenAppowl</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3441102175">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3420273309">
        <_items dataType="Array" type="Duality.Component[]" id="1013247718" length="16">
          <item dataType="Struct" type="Duality.Components.Transform" id="1506449811">
            <active dataType="Bool">true</active>
            <angle dataType="Float">0</angle>
            <angleAbs dataType="Float">0</angleAbs>
            <angleVel dataType="Float">0</angleVel>
            <angleVelAbs dataType="Float">0</angleVelAbs>
            <deriveAngle dataType="Bool">true</deriveAngle>
            <gameobj dataType="ObjectRef">3441102175</gameobj>
            <ignoreParent dataType="Bool">false</ignoreParent>
            <parentTransform />
            <pos dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-376</X>
              <Y dataType="Float">233</Y>
              <Z dataType="Float">0</Z>
            </pos>
            <posAbs dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">-376</X>
              <Y dataType="Float">233</Y>
              <Z dataType="Float">0</Z>
            </posAbs>
            <scale dataType="Float">1</scale>
            <scaleAbs dataType="Float">1</scaleAbs>
            <vel dataType="Struct" type="Duality.Vector3" />
            <velAbs dataType="Struct" type="Duality.Vector3" />
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3148537556">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">5</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">51</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">3441102175</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">39</H>
              <W dataType="Float">39</W>
              <X dataType="Float">-19.5</X>
              <Y dataType="Float">-19.5</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </item>
          <item dataType="Struct" type="Manager.AnimationManager" id="1917345863">
            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">greenAppowl</_x003C_animatedObjectName_x003E_k__BackingField>
            <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="3949972167">
              <_items dataType="Array" type="Misc.Animation[]" id="1028741838" length="8">
                <item dataType="Struct" type="Misc.Animation" id="935248848">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">14</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1.12</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="1976177262">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">startRun</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="4207729580">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">9</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.72</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="4003090962">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">12</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.96</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">death</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">34</_x003C_StartFrame_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Animation" id="3317820808">
                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">10</_x003C_AnimationDuration_x003E_k__BackingField>
                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">deathStill</_x003C_AnimationName_x003E_k__BackingField>
                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">45</_x003C_StartFrame_x003E_k__BackingField>
                </item>
              </_items>
              <_size dataType="Int">5</_size>
              <_version dataType="Int">5</_version>
            </_x003C_animationDatabase_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <animationDuration dataType="Float">0</animationDuration>
            <animationToReturnTo />
            <animSpriteRenderer dataType="ObjectRef">3148537556</animSpriteRenderer>
            <currentAnimationName />
            <currentTime dataType="Float">0</currentTime>
            <gameobj dataType="ObjectRef">3441102175</gameobj>
          </item>
          <item dataType="Struct" type="Enemy.EnemyController" id="1399579903">
            <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
            <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
            <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">4</X>
              <Y dataType="Float">0.1</Y>
              <Z dataType="Float">-0.1</Z>
            </_x003C_BulletSpawnOffset_x003E_k__BackingField>
            <_x003C_EnemyAnimationState_x003E_k__BackingField dataType="Enum" type="Enemy.EnemyAnimationState" name="Idle" value="0" />
            <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Enemy.FacingDirection" name="Right" value="1" />
            <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">10</_x003C_FiringDelay_x003E_k__BackingField>
            <_x003C_HorizontalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.HorizontalMovement" name="None" value="2" />
            <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
            <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
            <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
            <_x003C_VerticalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.VerticalMovement" name="None" value="2" />
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">1917345863</animationManager>
            <checkLeftMovement dataType="Bool">false</checkLeftMovement>
            <checkRightMovement dataType="Bool">false</checkRightMovement>
            <currentXPosition dataType="Float">0</currentXPosition>
            <entityStats dataType="Struct" type="Behavior.EntityStats" id="4079386019">
              <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">25</_x003C_CurrentHealth_x003E_k__BackingField>
              <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
              <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">25</_x003C_MaxHealth_x003E_k__BackingField>
              <active dataType="Bool">true</active>
              <animationManager dataType="ObjectRef">1917345863</animationManager>
              <currentTime dataType="Float">0</currentTime>
              <damageEffectLength dataType="Float">0.1</damageEffectLength>
              <gameobj dataType="ObjectRef">3441102175</gameobj>
              <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
              </material>
              <soundManager dataType="ObjectRef">243598982</soundManager>
              <timer dataType="Struct" type="Misc.Timer" id="3510129322">
                <_x003C_LengthOfCycle_x003E_k__BackingField dataType="Float">0.1</_x003C_LengthOfCycle_x003E_k__BackingField>
                <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <currentTime dataType="Float">0</currentTime>
                <gameobj dataType="ObjectRef">3441102175</gameobj>
                <timerStart dataType="Bool">false</timerStart>
              </timer>
            </entityStats>
            <firingDelayCounter dataType="Float">0</firingDelayCounter>
            <gameobj dataType="ObjectRef">3441102175</gameobj>
            <gravity dataType="Float">444.444458</gravity>
            <heldWeapon dataType="ObjectRef">301082255</heldWeapon>
            <jumpVelocity dataType="Float">200</jumpVelocity>
            <shapeInfo />
            <spriteRenderer dataType="ObjectRef">3148537556</spriteRenderer>
            <targetXPosition dataType="Float">0</targetXPosition>
            <velocity dataType="Struct" type="Duality.Vector2" />
          </item>
          <item dataType="Struct" type="Behavior.Controller2D" id="1570534035">
            <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
            <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="GreenAppowl" value="1" />
            <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
            <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
            <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">14.4</X>
              <Y dataType="Float">17</Y>
            </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
            <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">7.4</X>
              <Y dataType="Float">10</Y>
            </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
            <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <bounds dataType="Struct" type="Duality.Rect">
              <H dataType="Float">22</H>
              <W dataType="Float">24.6</W>
              <X dataType="Float">-388.1</X>
              <Y dataType="Float">223.5</Y>
            </bounds>
            <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
            <enemyController dataType="ObjectRef">1399579903</enemyController>
            <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
            <gameobj dataType="ObjectRef">3441102175</gameobj>
            <horizontalRaySpacing dataType="Float">10.985</horizontalRaySpacing>
            <maxClimbAngle dataType="Float">60</maxClimbAngle>
            <playerController />
            <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
            <rigidBody dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2208911403">
              <active dataType="Bool">true</active>
              <angularDamp dataType="Float">0.3</angularDamp>
              <angularVel dataType="Float">0</angularVel>
              <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
              <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
              <colFilter />
              <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
              <continous dataType="Bool">false</continous>
              <explicitInertia dataType="Float">0</explicitInertia>
              <explicitMass dataType="Float">0</explicitMass>
              <fixedAngle dataType="Bool">true</fixedAngle>
              <gameobj dataType="ObjectRef">3441102175</gameobj>
              <ignoreGravity dataType="Bool">true</ignoreGravity>
              <joints />
              <linearDamp dataType="Float">0.3</linearDamp>
              <linearVel dataType="Struct" type="Duality.Vector2" />
              <revolutions dataType="Float">0</revolutions>
              <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="1338290478">
                <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3182544720">
                  <item dataType="Struct" type="Duality.Components.Physics.CircleShapeInfo" id="1734048700">
                    <density dataType="Float">1</density>
                    <friction dataType="Float">0.3</friction>
                    <parent dataType="ObjectRef">2208911403</parent>
                    <position dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">-1</X>
                      <Y dataType="Float">2</Y>
                    </position>
                    <radius dataType="Float">8.944272</radius>
                    <restitution dataType="Float">0.3</restitution>
                    <sensor dataType="Bool">false</sensor>
                  </item>
                </_items>
                <_size dataType="Int">1</_size>
                <_version dataType="Int">2</_version>
              </shapes>
            </rigidBody>
            <verticalRaySpacing dataType="Float">12.285</verticalRaySpacing>
          </item>
          <item dataType="ObjectRef">2208911403</item>
          <item dataType="ObjectRef">3510129322</item>
          <item dataType="ObjectRef">4079386019</item>
          <item dataType="Struct" type="Enemy.GreenAppowl" id="25395871">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3441102175</gameobj>
            <stats />
          </item>
        </_items>
        <_size dataType="Int">9</_size>
        <_version dataType="Int">9</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2499989752" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1367993847">
            <item dataType="ObjectRef">1715766886</item>
            <item dataType="ObjectRef">891076794</item>
            <item dataType="ObjectRef">198886202</item>
            <item dataType="ObjectRef">1508246084</item>
            <item dataType="ObjectRef">2066733602</item>
            <item dataType="ObjectRef">12090110</item>
            <item dataType="ObjectRef">2343907548</item>
            <item dataType="ObjectRef">4154674070</item>
            <item dataType="ObjectRef">4046539414</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="3109469760">
            <item dataType="ObjectRef">1506449811</item>
            <item dataType="ObjectRef">3148537556</item>
            <item dataType="ObjectRef">1917345863</item>
            <item dataType="ObjectRef">1399579903</item>
            <item dataType="ObjectRef">1570534035</item>
            <item dataType="ObjectRef">2208911403</item>
            <item dataType="ObjectRef">3510129322</item>
            <item dataType="ObjectRef">4079386019</item>
            <item dataType="ObjectRef">25395871</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1506449811</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2610926677">gEbUo8/wr0uVW54/I4QEcA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">GreenAppowl</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3809856721">
      <active dataType="Bool">true</active>
      <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="2335165491">
        <_items dataType="Array" type="Duality.GameObject[]" id="4294466086" length="8">
          <item dataType="Struct" type="Duality.GameObject" id="4157747002">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3041916494">
              <_items dataType="Array" type="Duality.Component[]" id="1868906192" length="8">
                <item dataType="Struct" type="Duality.Plugins.Tilemaps.Tilemap" id="1864631723">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">4157747002</gameobj>
                  <tileData dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapData" id="1243283903" custom="true">
                    <body>
                      <version dataType="Int">3</version>
                      <data dataType="Array" type="System.Byte[]" id="2976771502">H4sIAAAAAAAEAO3VsQ2EMAxA0WzAAtBwazAM+0/B6VJeAQo4MvCQ0Gsd/BXWUsryfYfyewAAAAAAAAAgnM8+KebEWxgrB8JUKzoWOVVaw1QrYpgrl4apT5zm5K9cpggl8OLUJ9rpdnGKFg10uzjVimf0KVPo84+EI0Gfj+ozxeIivmCK4fWpz/ucL+FuUzR4l/3pE5n3F7/UFCcCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXskG4n5eeIidAAA=</data>
                    </body>
                  </tileData>
                  <tileset dataType="Struct" type="Duality.ContentRef`1[[Duality.Plugins.Tilemaps.Tileset]]">
                    <contentPath dataType="String">Data\Tilesets\Grasslands.Tileset.res</contentPath>
                  </tileset>
                </item>
                <item dataType="Struct" type="Duality.Components.Transform" id="2223094638">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">4157747002</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0.001</X>
                    <Y dataType="Float">0</Y>
                    <Z dataType="Float">0.1</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0.001</X>
                    <Y dataType="Float">0</Y>
                    <Z dataType="Float">0.1</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapRenderer" id="2855855154">
                  <active dataType="Bool">true</active>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <externalTilemap />
                  <gameobj dataType="ObjectRef">4157747002</gameobj>
                  <offset dataType="Float">0</offset>
                  <origin dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                  <tileDepthMode dataType="Enum" type="Duality.Plugins.Tilemaps.TileDepthOffsetMode" name="Flat" value="0" />
                  <tileDepthOffset dataType="Int">0</tileDepthOffset>
                  <tileDepthScale dataType="Float">0.01</tileDepthScale>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2925556230">
                  <active dataType="Bool">true</active>
                  <angularDamp dataType="Float">0.3</angularDamp>
                  <angularVel dataType="Float">0</angularVel>
                  <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
                  <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                  <colFilter />
                  <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="None" value="0" />
                  <continous dataType="Bool">false</continous>
                  <explicitInertia dataType="Float">0</explicitInertia>
                  <explicitMass dataType="Float">0.01</explicitMass>
                  <fixedAngle dataType="Bool">true</fixedAngle>
                  <gameobj dataType="ObjectRef">4157747002</gameobj>
                  <ignoreGravity dataType="Bool">true</ignoreGravity>
                  <joints />
                  <linearDamp dataType="Float">0.3</linearDamp>
                  <linearVel dataType="Struct" type="Duality.Vector2" />
                  <revolutions dataType="Float">0</revolutions>
                  <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3935711526">
                    <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="4198794496" length="40">
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="1595021980">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">2925556230</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="3984649156">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="1285923350">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">2925556230</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="1030158390">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-384</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-736</X>
                            <Y dataType="Float">-384</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-736</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-592</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-576</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-528</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-512</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-496</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-496</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-448</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-448</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-352</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-320</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-112</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-512</X>
                            <Y dataType="Float">-112</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-512</X>
                            <Y dataType="Float">-96</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-656</X>
                            <Y dataType="Float">-96</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-656</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-688</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-688</X>
                            <Y dataType="Float">-64</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-720</X>
                            <Y dataType="Float">-64</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-720</X>
                            <Y dataType="Float">-48</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-736</X>
                            <Y dataType="Float">-48</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-736</X>
                            <Y dataType="Float">-32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-384</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="4053126408">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">2925556230</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="770748056">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="2617342898">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">2925556230</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2801054602">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.LoopShapeInfo" id="139079988">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">2925556230</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="941379036">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-144</X>
                            <Y dataType="Float">-272</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">192</X>
                            <Y dataType="Float">-272</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">208</X>
                            <Y dataType="Float">-256</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-256</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">240</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">240</X>
                            <Y dataType="Float">-144</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">32</X>
                            <Y dataType="Float">-144</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">32</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-176</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="2086336910">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">2925556230</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2710573838">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="2657627328">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">2925556230</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="3257660688">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-256</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">272</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">320</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">336</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">368</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">384</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">416</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">432</X>
                            <Y dataType="Float">-192</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">448</X>
                            <Y dataType="Float">-192</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">464</X>
                            <Y dataType="Float">-176</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">480</X>
                            <Y dataType="Float">-176</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">576</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">592</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">592</X>
                            <Y dataType="Float">-48</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">512</X>
                            <Y dataType="Float">-48</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">512</X>
                            <Y dataType="Float">-64</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">384</X>
                            <Y dataType="Float">-64</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">384</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">352</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">352</X>
                            <Y dataType="Float">-96</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">320</X>
                            <Y dataType="Float">-96</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">320</X>
                            <Y dataType="Float">-112</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">304</X>
                            <Y dataType="Float">-112</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">304</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">272</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="3838758154">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">2925556230</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="3468059650">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="4052514956">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">2925556230</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="245927380">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="4048634598">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">2925556230</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2883139046">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                        </vertices>
                      </item>
                    </_items>
                    <_size dataType="Int">10</_size>
                    <_version dataType="Int">4238</_version>
                  </shapes>
                </item>
                <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollider" id="2272040921">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">4157747002</gameobj>
                  <origin dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                  <roundedCorners dataType="Bool">false</roundedCorners>
                  <shapeFriction dataType="Float">0.299999982</shapeFriction>
                  <shapeRestitution dataType="Float">0.299999982</shapeRestitution>
                  <solidOuterEdges dataType="Bool">true</solidOuterEdges>
                  <source dataType="Array" type="Duality.Plugins.Tilemaps.TilemapCollisionSource[]" id="1970197885">
                    <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollisionSource">
                      <Layers dataType="Enum" type="Duality.Plugins.Tilemaps.TileCollisionLayer" name="Layer0" value="1" />
                      <SourceTilemap />
                    </item>
                  </source>
                </item>
                <item dataType="Struct" type="Misc.TilemapPositionMaintainer" id="701184889">
                  <_x003C_positionToMaintain_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">0</Y>
                    <Z dataType="Float">0.1</Z>
                  </_x003C_positionToMaintain_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">4157747002</gameobj>
                  <transform dataType="ObjectRef">2223094638</transform>
                </item>
              </_items>
              <_size dataType="Int">6</_size>
              <_version dataType="Int">6</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2852596298" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="1329518860">
                  <item dataType="ObjectRef">2476887204</item>
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">141779734</item>
                  <item dataType="ObjectRef">12090110</item>
                  <item dataType="ObjectRef">1229872032</item>
                  <item dataType="ObjectRef">1648253538</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="1767696118">
                  <item dataType="ObjectRef">1864631723</item>
                  <item dataType="ObjectRef">2223094638</item>
                  <item dataType="ObjectRef">2855855154</item>
                  <item dataType="ObjectRef">2925556230</item>
                  <item dataType="ObjectRef">2272040921</item>
                  <item dataType="ObjectRef">701184889</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">2223094638</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="2473840536">PGpg6rL5XUWZI+RjjdAGWw==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Background Map</name>
            <parent dataType="ObjectRef">3809856721</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="1132339076">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="599512416">
              <_items dataType="Array" type="Duality.Component[]" id="1106439388" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="3492654008">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">1132339076</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-655.97</X>
                    <Y dataType="Float">-235.35</Y>
                    <Z dataType="Float">0</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-655.97</X>
                    <Y dataType="Float">-235.35</Y>
                    <Z dataType="Float">0</Z>
                  </posAbs>
                  <scale dataType="Float">0.5</scale>
                  <scaleAbs dataType="Float">0.5</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="2874967898">
                  <active dataType="Bool">true</active>
                  <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customMat />
                  <gameobj dataType="ObjectRef">1132339076</gameobj>
                  <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                  <offset dataType="Int">0</offset>
                  <text dataType="Struct" type="Duality.Drawing.FormattedText" id="119681850">
                    <flowAreas />
                    <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="1637742848">
                      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                        <contentPath dataType="String">Default:Font:GenericMonospace10</contentPath>
                      </item>
                    </fonts>
                    <icons />
                    <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                    <maxHeight dataType="Int">0</maxHeight>
                    <maxWidth dataType="Int">0</maxWidth>
                    <sourceText dataType="String">Don't fall</sourceText>
                    <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                  </text>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1354019982" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="910558642">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="Type" id="3750530768" value="Duality.Components.Renderers.TextRenderer" />
                </keys>
                <values dataType="Array" type="System.Object[]" id="3245529674">
                  <item dataType="ObjectRef">3492654008</item>
                  <item dataType="ObjectRef">2874967898</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">3492654008</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="1243597826">flcCz3zQyUiD/lxdtiMfMw==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">DontFall</name>
            <parent dataType="ObjectRef">3809856721</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="1182226281">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1926761833">
              <_items dataType="Array" type="Duality.Component[]" id="174982414" length="8">
                <item dataType="Struct" type="Duality.Plugins.Tilemaps.Tilemap" id="3184078298">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">1182226281</gameobj>
                  <tileData dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapData" id="3978846398" custom="true">
                    <body>
                      <version dataType="Int">3</version>
                      <data dataType="Array" type="System.Byte[]" id="510759952">H4sIAAAAAAAEAO3FAQ0AMAgEsfeCtWlCL2Q26CWXdpK3V34AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJxkAM+HhH0wmgEA</data>
                    </body>
                  </tileData>
                  <tileset dataType="Struct" type="Duality.ContentRef`1[[Duality.Plugins.Tilemaps.Tileset]]">
                    <contentPath dataType="String">Data\Tilesets\Grasslands.Tileset.res</contentPath>
                  </tileset>
                </item>
                <item dataType="Struct" type="Duality.Components.Transform" id="3542541213">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">1182226281</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0.002</X>
                    <Y dataType="Float">0</Y>
                    <Z dataType="Float">1.2</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0.002</X>
                    <Y dataType="Float">0</Y>
                    <Z dataType="Float">1.2</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapRenderer" id="4175301729">
                  <active dataType="Bool">true</active>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <externalTilemap />
                  <gameobj dataType="ObjectRef">1182226281</gameobj>
                  <offset dataType="Float">0</offset>
                  <origin dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                  <tileDepthMode dataType="Enum" type="Duality.Plugins.Tilemaps.TileDepthOffsetMode" name="Flat" value="0" />
                  <tileDepthOffset dataType="Int">0</tileDepthOffset>
                  <tileDepthScale dataType="Float">0.01</tileDepthScale>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="4245002805">
                  <active dataType="Bool">true</active>
                  <angularDamp dataType="Float">0.3</angularDamp>
                  <angularVel dataType="Float">0</angularVel>
                  <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
                  <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                  <colFilter />
                  <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="None" value="0" />
                  <continous dataType="Bool">false</continous>
                  <explicitInertia dataType="Float">0</explicitInertia>
                  <explicitMass dataType="Float">0.01</explicitMass>
                  <fixedAngle dataType="Bool">true</fixedAngle>
                  <gameobj dataType="ObjectRef">1182226281</gameobj>
                  <ignoreGravity dataType="Bool">true</ignoreGravity>
                  <joints />
                  <linearDamp dataType="Float">0.3</linearDamp>
                  <linearVel dataType="Struct" type="Duality.Vector2" />
                  <revolutions dataType="Float">0</revolutions>
                  <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="968787957">
                    <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="448311414" length="42">
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="2722013152">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2896337884">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="978428814">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="668976946">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-384</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-736</X>
                            <Y dataType="Float">-384</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-736</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-592</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-576</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-528</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-512</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-496</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-496</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-448</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-448</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-352</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-320</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-112</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-512</X>
                            <Y dataType="Float">-112</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-512</X>
                            <Y dataType="Float">-96</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-656</X>
                            <Y dataType="Float">-96</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-656</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-688</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-688</X>
                            <Y dataType="Float">-64</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-720</X>
                            <Y dataType="Float">-64</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-720</X>
                            <Y dataType="Float">-48</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-736</X>
                            <Y dataType="Float">-48</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-736</X>
                            <Y dataType="Float">-32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-384</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="1449496316">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2887992952">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="1720248082">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="3663683206">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.LoopShapeInfo" id="3812277912">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="141805588">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-144</X>
                            <Y dataType="Float">-272</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">192</X>
                            <Y dataType="Float">-272</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">208</X>
                            <Y dataType="Float">-256</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-256</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">240</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">240</X>
                            <Y dataType="Float">-144</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">32</X>
                            <Y dataType="Float">-144</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">32</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-176</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="3906214310">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="1512505546">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="961564660">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="411619376">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-256</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">272</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">320</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">336</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">368</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">384</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">416</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">432</X>
                            <Y dataType="Float">-192</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">448</X>
                            <Y dataType="Float">-192</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">464</X>
                            <Y dataType="Float">-176</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">480</X>
                            <Y dataType="Float">-176</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">576</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">592</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">592</X>
                            <Y dataType="Float">-48</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">512</X>
                            <Y dataType="Float">-48</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">512</X>
                            <Y dataType="Float">-64</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">384</X>
                            <Y dataType="Float">-64</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">384</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">352</X>
                            <Y dataType="Float">-80</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">352</X>
                            <Y dataType="Float">-96</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">320</X>
                            <Y dataType="Float">-96</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">320</X>
                            <Y dataType="Float">-112</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">304</X>
                            <Y dataType="Float">-112</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">304</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-128</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">272</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="4175012010">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="4181895838">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="1229727760">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="970652492">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="387791550">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2715522594">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="3711402796">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="3878429608">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="875125506">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="415608182">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.LoopShapeInfo" id="2752525256">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2618031172">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-320</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-432</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-432</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-352</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.LoopShapeInfo" id="1879613590">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2088567674">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-576</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-528</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-512</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-512</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-736</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-736</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-592</X>
                            <Y dataType="Float">-208</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="4082036580">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2123966368">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.LoopShapeInfo" id="2642022490">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="3612876302">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-144</X>
                            <Y dataType="Float">-272</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">192</X>
                            <Y dataType="Float">-272</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">208</X>
                            <Y dataType="Float">-256</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-256</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">64</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">64</X>
                            <Y dataType="Float">-192</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">32</X>
                            <Y dataType="Float">-192</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">32</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-176</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="124159936">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="743273724">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">-480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="3694373166">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="3097750674">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-256</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">272</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">320</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">336</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">352</X>
                            <Y dataType="Float">-224</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">352</X>
                            <Y dataType="Float">-176</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">368</X>
                            <Y dataType="Float">-176</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">368</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-160</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">272</X>
                            <Y dataType="Float">-240</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="3199494620">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="1897517720">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-768</X>
                            <Y dataType="Float">480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="612994994">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2964749926">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                        </vertices>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Physics.ChainShapeInfo" id="2609050232">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.299999982</friction>
                        <parent dataType="ObjectRef">4245002805</parent>
                        <restitution dataType="Float">0.299999982</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="1452635828">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">32</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">768</X>
                            <Y dataType="Float">480</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">256</X>
                            <Y dataType="Float">480</Y>
                          </item>
                        </vertices>
                      </item>
                    </_items>
                    <_size dataType="Int">21</_size>
                    <_version dataType="Int">4663</_version>
                  </shapes>
                </item>
                <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollider" id="3591487496">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">1182226281</gameobj>
                  <origin dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                  <roundedCorners dataType="Bool">false</roundedCorners>
                  <shapeFriction dataType="Float">0.3</shapeFriction>
                  <shapeRestitution dataType="Float">0.3</shapeRestitution>
                  <solidOuterEdges dataType="Bool">true</solidOuterEdges>
                  <source dataType="Array" type="Duality.Plugins.Tilemaps.TilemapCollisionSource[]" id="3170780628">
                    <item dataType="Struct" type="Duality.Plugins.Tilemaps.TilemapCollisionSource">
                      <Layers dataType="Enum" type="Duality.Plugins.Tilemaps.TileCollisionLayer" name="Layer0" value="1" />
                      <SourceTilemap />
                    </item>
                  </source>
                </item>
              </_items>
              <_size dataType="Int">5</_size>
              <_version dataType="Int">5</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2210900672" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="4289024483">
                  <item dataType="ObjectRef">2476887204</item>
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">141779734</item>
                  <item dataType="ObjectRef">12090110</item>
                  <item dataType="ObjectRef">1229872032</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="2551223032">
                  <item dataType="ObjectRef">3184078298</item>
                  <item dataType="ObjectRef">3542541213</item>
                  <item dataType="ObjectRef">4175301729</item>
                  <item dataType="ObjectRef">4245002805</item>
                  <item dataType="ObjectRef">3591487496</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">3542541213</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="213261641">ixeARvcZ1kCUBRkyVtBYNA==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Extended Background</name>
            <parent dataType="ObjectRef">3809856721</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="266672357">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2380463253">
              <_items dataType="Array" type="Duality.Component[]" id="2505608310" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="2626987289">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">266672357</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-563.8</Y>
                    <Z dataType="Float">0.3</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-563.8</Y>
                    <Z dataType="Float">0.3</Z>
                  </posAbs>
                  <scale dataType="Float">1.2</scale>
                  <scaleAbs dataType="Float">1.2</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="1908838925">
                  <active dataType="Bool">true</active>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customMat />
                  <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                  <gameobj dataType="ObjectRef">266672357</gameobj>
                  <offset dataType="Int">0</offset>
                  <pixelGrid dataType="Bool">false</pixelGrid>
                  <rect dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">640</H>
                    <W dataType="Float">1536</W>
                    <X dataType="Float">-768</X>
                    <Y dataType="Float">-320</Y>
                  </rect>
                  <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                  <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Backgrounds\Grasslands Background.Material.res</contentPath>
                  </sharedMat>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="4033810120" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="3636080703">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="Type" id="2030083246" value="Duality.Components.Renderers.SpriteRenderer" />
                </keys>
                <values dataType="Array" type="System.Object[]" id="3938351328">
                  <item dataType="ObjectRef">2626987289</item>
                  <item dataType="ObjectRef">1908838925</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">2626987289</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="2043309293">81eGV2MqYk+Vd3JyGdWDdA==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Grasslands Background</name>
            <parent dataType="ObjectRef">3809856721</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="3981402552">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2176119956">
              <_items dataType="Array" type="Duality.Component[]" id="3918024548" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="2046750188">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">3981402552</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-551</X>
                    <Y dataType="Float">-238</Y>
                    <Z dataType="Float">0</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-551</X>
                    <Y dataType="Float">-238</Y>
                    <Z dataType="Float">0</Z>
                  </posAbs>
                  <scale dataType="Float">0.5</scale>
                  <scaleAbs dataType="Float">0.5</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="1429064078">
                  <active dataType="Bool">true</active>
                  <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customMat />
                  <gameobj dataType="ObjectRef">3981402552</gameobj>
                  <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                  <offset dataType="Int">0</offset>
                  <text dataType="Struct" type="Duality.Drawing.FormattedText" id="3133017078">
                    <flowAreas />
                    <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="2591004384">
                      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                        <contentPath dataType="String">Default:Font:GenericMonospace10</contentPath>
                      </item>
                    </fonts>
                    <icons />
                    <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                    <maxHeight dataType="Int">0</maxHeight>
                    <maxWidth dataType="Int">0</maxWidth>
                    <sourceText dataType="String">If you get stuck, push R</sourceText>
                    <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                  </text>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2822097974" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="3463950654">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">3750530768</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="23564810">
                  <item dataType="ObjectRef">2046750188</item>
                  <item dataType="ObjectRef">1429064078</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">2046750188</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="2842619982">/yB+izGAi0GdaebqMrmJzA==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">push r if you get stuck</name>
            <parent dataType="ObjectRef">3809856721</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="3937918291">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3663771619">
              <_items dataType="Array" type="Duality.Component[]" id="940079334" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="2003265927">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">3937918291</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-291</X>
                    <Y dataType="Float">-271</Y>
                    <Z dataType="Float">0</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-291</X>
                    <Y dataType="Float">-271</Y>
                    <Z dataType="Float">0</Z>
                  </posAbs>
                  <scale dataType="Float">0.5</scale>
                  <scaleAbs dataType="Float">0.5</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="1385579817">
                  <active dataType="Bool">true</active>
                  <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customMat />
                  <gameobj dataType="ObjectRef">3937918291</gameobj>
                  <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                  <offset dataType="Int">0</offset>
                  <text dataType="Struct" type="Duality.Drawing.FormattedText" id="1925064361">
                    <flowAreas />
                    <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="1641548814">
                      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                        <contentPath dataType="String">Default:Font:GenericMonospace10</contentPath>
                      </item>
                    </fonts>
                    <icons />
                    <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                    <maxHeight dataType="Int">0</maxHeight>
                    <maxWidth dataType="Int">0</maxWidth>
                    <sourceText dataType="String">Push X to shoot the enemy</sourceText>
                    <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                  </text>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1893541624" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="2496807049">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">3750530768</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="626814784">
                  <item dataType="ObjectRef">2003265927</item>
                  <item dataType="ObjectRef">1385579817</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">2003265927</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="192195115">RZ3Rx2Z19kCPa1jki3354A==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Push X to shoot this enemy</name>
            <parent dataType="ObjectRef">3809856721</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="3150687493">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3502871605">
              <_items dataType="Array" type="Duality.Component[]" id="1541911030" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="1216035129">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">3150687493</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-552</X>
                    <Y dataType="Float">-251</Y>
                    <Z dataType="Float">0</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-552</X>
                    <Y dataType="Float">-251</Y>
                    <Z dataType="Float">0</Z>
                  </posAbs>
                  <scale dataType="Float">0.5</scale>
                  <scaleAbs dataType="Float">0.5</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="598349019">
                  <active dataType="Bool">true</active>
                  <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customMat />
                  <gameobj dataType="ObjectRef">3150687493</gameobj>
                  <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                  <offset dataType="Int">0</offset>
                  <text dataType="Struct" type="Duality.Drawing.FormattedText" id="737715019">
                    <flowAreas />
                    <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="1029168886">
                      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                        <contentPath dataType="String">Default:Font:GenericMonospace10</contentPath>
                      </item>
                    </fonts>
                    <icons />
                    <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                    <maxHeight dataType="Int">0</maxHeight>
                    <maxWidth dataType="Int">0</maxWidth>
                    <sourceText dataType="String">Push Z to jump</sourceText>
                    <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                  </text>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2546825800" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="4043470367">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">3750530768</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="3141621792">
                  <item dataType="ObjectRef">1216035129</item>
                  <item dataType="ObjectRef">598349019</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">1216035129</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="1550009485">d+crGeCkEkqjvwbRxkILYA==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">pushZtojump</name>
            <parent dataType="ObjectRef">3809856721</parent>
            <prefabLink />
          </item>
        </_items>
        <_size dataType="Int">7</_size>
        <_version dataType="Int">7</_version>
      </children>
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3351494840">
        <_items dataType="Array" type="Duality.Component[]" id="466742873" length="0" />
        <_size dataType="Int">0</_size>
        <_version dataType="Int">0</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1092460121" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="4035541652" length="0" />
          <values dataType="Array" type="System.Object[]" id="892192822" length="0" />
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="336830256">poQgBzXNOEieSSmEO8wlTw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">AESTHETICS</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2103822484">
      <active dataType="Bool">true</active>
      <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="83698730">
        <_items dataType="Array" type="Duality.GameObject[]" id="1795873056" length="8">
          <item dataType="Struct" type="Duality.GameObject" id="3129138373">
            <active dataType="Bool">true</active>
            <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="572924721">
              <_items dataType="Array" type="Duality.GameObject[]" id="759236654" length="4" />
              <_size dataType="Int">0</_size>
              <_version dataType="Int">2</_version>
            </children>
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="690964064">
              <_items dataType="Array" type="Duality.Component[]" id="530243355" length="8">
                <item dataType="Struct" type="Duality.Components.Transform" id="1194486009">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">3129138373</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-373.6653</X>
                    <Y dataType="Float">-228.990219</Y>
                    <Z dataType="Float">-1</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-373.6653</X>
                    <Y dataType="Float">-228.990219</Y>
                    <Z dataType="Float">-1</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2836573754">
                  <active dataType="Bool">true</active>
                  <animDuration dataType="Float">0.8</animDuration>
                  <animFirstFrame dataType="Int">0</animFirstFrame>
                  <animFrameCount dataType="Int">1</animFrameCount>
                  <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                  <animPaused dataType="Bool">false</animPaused>
                  <animTime dataType="Float">0</animTime>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customFrameSequence />
                  <customMat />
                  <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                  <gameobj dataType="ObjectRef">3129138373</gameobj>
                  <offset dataType="Int">0</offset>
                  <pixelGrid dataType="Bool">false</pixelGrid>
                  <rect dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">20</H>
                    <W dataType="Float">30</W>
                    <X dataType="Float">-15</X>
                    <Y dataType="Float">-13</Y>
                  </rect>
                  <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                  <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Object_AmmoCrate.Material.res</contentPath>
                  </sharedMat>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1896947601">
                  <active dataType="Bool">true</active>
                  <angularDamp dataType="Float">0.3</angularDamp>
                  <angularVel dataType="Float">0</angularVel>
                  <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                  <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1, Cat2" value="3" />
                  <colFilter />
                  <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                  <continous dataType="Bool">false</continous>
                  <explicitInertia dataType="Float">0</explicitInertia>
                  <explicitMass dataType="Float">0</explicitMass>
                  <fixedAngle dataType="Bool">false</fixedAngle>
                  <gameobj dataType="ObjectRef">3129138373</gameobj>
                  <ignoreGravity dataType="Bool">false</ignoreGravity>
                  <joints />
                  <linearDamp dataType="Float">0.3</linearDamp>
                  <linearVel dataType="Struct" type="Duality.Vector2" />
                  <revolutions dataType="Float">0</revolutions>
                  <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="536646739">
                    <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="1578227046" length="4">
                      <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="929864576">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.3</friction>
                        <parent dataType="ObjectRef">1896947601</parent>
                        <restitution dataType="Float">0.3</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="3381309852">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-15.3346863</X>
                            <Y dataType="Float">-6.009781</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">14.6653137</X>
                            <Y dataType="Float">-6.009781</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">14.6653137</X>
                            <Y dataType="Float">6.990219</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-15.3346863</X>
                            <Y dataType="Float">6.990219</Y>
                          </item>
                        </vertices>
                      </item>
                    </_items>
                    <_size dataType="Int">1</_size>
                    <_version dataType="Int">18</_version>
                  </shapes>
                </item>
                <item dataType="Struct" type="Manager.AnimationManager" id="1605382061">
                  <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">chest</_x003C_animatedObjectName_x003E_k__BackingField>
                  <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="2280989887">
                    <_items dataType="Array" type="Misc.Animation[]" id="3505545646" length="4">
                      <item dataType="Struct" type="Misc.Animation" id="3404248400">
                        <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">20</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                        <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1.6</_x003C_AnimationDuration_x003E_k__BackingField>
                        <_x003C_AnimationName_x003E_k__BackingField dataType="String">open</_x003C_AnimationName_x003E_k__BackingField>
                        <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                      </item>
                      <item dataType="Struct" type="Misc.Animation" id="2666790766">
                        <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                        <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.4</_x003C_AnimationDuration_x003E_k__BackingField>
                        <_x003C_AnimationName_x003E_k__BackingField dataType="String">opened</_x003C_AnimationName_x003E_k__BackingField>
                        <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                      </item>
                      <item dataType="Struct" type="Misc.Animation" id="3815805228">
                        <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                        <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                        <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                        <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                      </item>
                    </_items>
                    <_size dataType="Int">3</_size>
                    <_version dataType="Int">3</_version>
                  </_x003C_animationDatabase_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <animationDuration dataType="Float">0</animationDuration>
                  <animationToReturnTo />
                  <animSpriteRenderer dataType="ObjectRef">2836573754</animSpriteRenderer>
                  <currentAnimationName dataType="String">idle</currentAnimationName>
                  <currentTime dataType="Float">0</currentTime>
                  <gameobj dataType="ObjectRef">3129138373</gameobj>
                </item>
                <item dataType="Struct" type="Behavior.WeaponChest" id="974710640">
                  <_x003C_weaponToGive_x003E_k__BackingField dataType="Enum" type="Behavior.WeaponChest+WeaponToGive" name="Peastol" value="0" />
                  <active dataType="Bool">true</active>
                  <alreadyOpened dataType="Bool">false</alreadyOpened>
                  <animationManager dataType="ObjectRef">1605382061</animationManager>
                  <gameobj dataType="ObjectRef">3129138373</gameobj>
                  <player dataType="ObjectRef">787527030</player>
                  <playerWeapon dataType="ObjectRef">301082255</playerWeapon>
                </item>
              </_items>
              <_size dataType="Int">5</_size>
              <_version dataType="Int">5</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3603377379" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="629739428">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">891076794</item>
                  <item dataType="ObjectRef">12090110</item>
                  <item dataType="ObjectRef">198886202</item>
                  <item dataType="Type" id="1018545348" value="Behavior.WeaponChest" />
                </keys>
                <values dataType="Array" type="System.Object[]" id="2188992278">
                  <item dataType="ObjectRef">1194486009</item>
                  <item dataType="ObjectRef">2836573754</item>
                  <item dataType="ObjectRef">1896947601</item>
                  <item dataType="ObjectRef">1605382061</item>
                  <item dataType="ObjectRef">974710640</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">1194486009</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="2990452384">O5RZOds+L0iWmL1BlPcU0Q==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Ammocrate</name>
            <parent dataType="ObjectRef">2103822484</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="3925785397">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2242278849">
              <_items dataType="Array" type="Duality.Component[]" id="175797422" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="1991133033">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">3925785397</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-519.198059</X>
                    <Y dataType="Float">-100</Y>
                    <Z dataType="Float">-1</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-519.198059</X>
                    <Y dataType="Float">-100</Y>
                    <Z dataType="Float">-1</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2693594625">
                  <active dataType="Bool">true</active>
                  <angularDamp dataType="Float">0.3</angularDamp>
                  <angularVel dataType="Float">0</angularVel>
                  <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                  <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
                  <colFilter />
                  <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                  <continous dataType="Bool">false</continous>
                  <explicitInertia dataType="Float">0</explicitInertia>
                  <explicitMass dataType="Float">0</explicitMass>
                  <fixedAngle dataType="Bool">true</fixedAngle>
                  <gameobj dataType="ObjectRef">3925785397</gameobj>
                  <ignoreGravity dataType="Bool">true</ignoreGravity>
                  <joints />
                  <linearDamp dataType="Float">0.3</linearDamp>
                  <linearVel dataType="Struct" type="Duality.Vector2" />
                  <revolutions dataType="Float">0</revolutions>
                  <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3807920561">
                    <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3516916270">
                      <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2853839696">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.3</friction>
                        <parent dataType="ObjectRef">2693594625</parent>
                        <restitution dataType="Float">0.3</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="921842620">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">532.5348</X>
                            <Y dataType="Float">284.384125</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">619.08374</X>
                            <Y dataType="Float">283.858154</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">622.1467</X>
                            <Y dataType="Float">291.7473</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">534.2348</X>
                            <Y dataType="Float">292.0103</Y>
                          </item>
                        </vertices>
                      </item>
                    </_items>
                    <_size dataType="Int">1</_size>
                    <_version dataType="Int">1</_version>
                  </shapes>
                </item>
                <item dataType="Struct" type="Behavior.Teleporter" id="3933077659">
                  <_x003C_LocationToGoTo_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">85.72</X>
                    <Y dataType="Float">67.62</Y>
                  </_x003C_LocationToGoTo_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">3925785397</gameobj>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="4170297568" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="3618991627">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">12090110</item>
                  <item dataType="Type" id="222474358" value="Behavior.Teleporter" />
                </keys>
                <values dataType="Array" type="System.Object[]" id="3012772552">
                  <item dataType="ObjectRef">1991133033</item>
                  <item dataType="ObjectRef">2693594625</item>
                  <item dataType="ObjectRef">3933077659</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">1991133033</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="3416388033">O/bitRxLAE2yTRfBim8YDg==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">LeftHoleTeleporter</name>
            <parent dataType="ObjectRef">2103822484</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="2000276485">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2952913521">
              <_items dataType="Array" type="Duality.Component[]" id="3404281518" length="8">
                <item dataType="Struct" type="Duality.Components.Transform" id="65624121">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">2000276485</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">518</X>
                    <Y dataType="Float">27</Y>
                    <Z dataType="Float">-1</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">518</X>
                    <Y dataType="Float">27</Y>
                    <Z dataType="Float">-1</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1707711866">
                  <active dataType="Bool">true</active>
                  <animDuration dataType="Float">0.8</animDuration>
                  <animFirstFrame dataType="Int">0</animFirstFrame>
                  <animFrameCount dataType="Int">1</animFrameCount>
                  <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                  <animPaused dataType="Bool">false</animPaused>
                  <animTime dataType="Float">0</animTime>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customFrameSequence />
                  <customMat />
                  <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                  <gameobj dataType="ObjectRef">2000276485</gameobj>
                  <offset dataType="Int">0</offset>
                  <pixelGrid dataType="Bool">false</pixelGrid>
                  <rect dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">20</H>
                    <W dataType="Float">30</W>
                    <X dataType="Float">-15</X>
                    <Y dataType="Float">-13</Y>
                  </rect>
                  <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                  <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Object_AmmoCrate.Material.res</contentPath>
                  </sharedMat>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="768085713">
                  <active dataType="Bool">true</active>
                  <angularDamp dataType="Float">0.3</angularDamp>
                  <angularVel dataType="Float">0</angularVel>
                  <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                  <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1, Cat2" value="3" />
                  <colFilter />
                  <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                  <continous dataType="Bool">false</continous>
                  <explicitInertia dataType="Float">0</explicitInertia>
                  <explicitMass dataType="Float">0</explicitMass>
                  <fixedAngle dataType="Bool">false</fixedAngle>
                  <gameobj dataType="ObjectRef">2000276485</gameobj>
                  <ignoreGravity dataType="Bool">false</ignoreGravity>
                  <joints />
                  <linearDamp dataType="Float">0.3</linearDamp>
                  <linearVel dataType="Struct" type="Duality.Vector2" />
                  <revolutions dataType="Float">0</revolutions>
                  <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3408070113">
                    <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3069968750">
                      <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3973445712">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.3</friction>
                        <parent dataType="ObjectRef">768085713</parent>
                        <restitution dataType="Float">0.3</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="13474236">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-15</X>
                            <Y dataType="Float">-6</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">14</X>
                            <Y dataType="Float">-6</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">14</X>
                            <Y dataType="Float">6</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-15</X>
                            <Y dataType="Float">6</Y>
                          </item>
                        </vertices>
                      </item>
                    </_items>
                    <_size dataType="Int">1</_size>
                    <_version dataType="Int">9</_version>
                  </shapes>
                </item>
                <item dataType="Struct" type="Manager.AnimationManager" id="476520173">
                  <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">chest</_x003C_animatedObjectName_x003E_k__BackingField>
                  <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="268894989">
                    <_items dataType="Array" type="Misc.Animation[]" id="3079933734" length="4">
                      <item dataType="Struct" type="Misc.Animation" id="2199381248">
                        <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">20</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                        <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1.6</_x003C_AnimationDuration_x003E_k__BackingField>
                        <_x003C_AnimationName_x003E_k__BackingField dataType="String">open</_x003C_AnimationName_x003E_k__BackingField>
                        <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                      </item>
                      <item dataType="Struct" type="Misc.Animation" id="1710236110">
                        <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                        <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.4</_x003C_AnimationDuration_x003E_k__BackingField>
                        <_x003C_AnimationName_x003E_k__BackingField dataType="String">opened</_x003C_AnimationName_x003E_k__BackingField>
                        <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                      </item>
                      <item dataType="Struct" type="Misc.Animation" id="1198696348">
                        <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                        <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                        <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                        <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                      </item>
                    </_items>
                    <_size dataType="Int">3</_size>
                    <_version dataType="Int">3</_version>
                  </_x003C_animationDatabase_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <animationDuration dataType="Float">0</animationDuration>
                  <animationToReturnTo />
                  <animSpriteRenderer dataType="ObjectRef">1707711866</animSpriteRenderer>
                  <currentAnimationName dataType="String">idle</currentAnimationName>
                  <currentTime dataType="Float">0</currentTime>
                  <gameobj dataType="ObjectRef">2000276485</gameobj>
                </item>
                <item dataType="Struct" type="Behavior.WeaponChest" id="4140816048">
                  <_x003C_weaponToGive_x003E_k__BackingField dataType="Enum" type="Behavior.WeaponChest+WeaponToGive" name="LemonadeLauncher" value="1" />
                  <active dataType="Bool">true</active>
                  <alreadyOpened dataType="Bool">false</alreadyOpened>
                  <animationManager dataType="ObjectRef">476520173</animationManager>
                  <gameobj dataType="ObjectRef">2000276485</gameobj>
                  <player dataType="ObjectRef">787527030</player>
                  <playerWeapon dataType="ObjectRef">301082255</playerWeapon>
                </item>
              </_items>
              <_size dataType="Int">5</_size>
              <_version dataType="Int">5</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="578371296" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="1055505371">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">891076794</item>
                  <item dataType="ObjectRef">12090110</item>
                  <item dataType="ObjectRef">198886202</item>
                  <item dataType="ObjectRef">1018545348</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="1623765864">
                  <item dataType="ObjectRef">65624121</item>
                  <item dataType="ObjectRef">1707711866</item>
                  <item dataType="ObjectRef">768085713</item>
                  <item dataType="ObjectRef">476520173</item>
                  <item dataType="ObjectRef">4140816048</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">65624121</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="3383626513">DYAeVCey4UyczpH83JRb6Q==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">lower ammocrate</name>
            <parent dataType="ObjectRef">2103822484</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="3889717313">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="670416677">
              <_items dataType="Array" type="Duality.Component[]" id="3441478294" length="8">
                <item dataType="Struct" type="Duality.Components.Transform" id="1955064949">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">3889717313</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-207</X>
                    <Y dataType="Float">169</Y>
                    <Z dataType="Float">-1</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-207</X>
                    <Y dataType="Float">169</Y>
                    <Z dataType="Float">-1</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3597152694">
                  <active dataType="Bool">true</active>
                  <animDuration dataType="Float">0.8</animDuration>
                  <animFirstFrame dataType="Int">0</animFirstFrame>
                  <animFrameCount dataType="Int">1</animFrameCount>
                  <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                  <animPaused dataType="Bool">false</animPaused>
                  <animTime dataType="Float">0</animTime>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customFrameSequence />
                  <customMat />
                  <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                  <gameobj dataType="ObjectRef">3889717313</gameobj>
                  <offset dataType="Int">0</offset>
                  <pixelGrid dataType="Bool">false</pixelGrid>
                  <rect dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">20</H>
                    <W dataType="Float">30</W>
                    <X dataType="Float">-15</X>
                    <Y dataType="Float">-13</Y>
                  </rect>
                  <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                  <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Object_AmmoCrate.Material.res</contentPath>
                  </sharedMat>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2657526541">
                  <active dataType="Bool">true</active>
                  <angularDamp dataType="Float">0.3</angularDamp>
                  <angularVel dataType="Float">0</angularVel>
                  <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                  <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1, Cat2" value="3" />
                  <colFilter />
                  <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                  <continous dataType="Bool">false</continous>
                  <explicitInertia dataType="Float">0</explicitInertia>
                  <explicitMass dataType="Float">0</explicitMass>
                  <fixedAngle dataType="Bool">false</fixedAngle>
                  <gameobj dataType="ObjectRef">3889717313</gameobj>
                  <ignoreGravity dataType="Bool">false</ignoreGravity>
                  <joints />
                  <linearDamp dataType="Float">0.3</linearDamp>
                  <linearVel dataType="Struct" type="Duality.Vector2" />
                  <revolutions dataType="Float">0</revolutions>
                  <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="626484189">
                    <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2928462950">
                      <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="77977472">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.3</friction>
                        <parent dataType="ObjectRef">2657526541</parent>
                        <restitution dataType="Float">0.3</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2081546652">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-15</X>
                            <Y dataType="Float">-6</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">15</X>
                            <Y dataType="Float">-6</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">15</X>
                            <Y dataType="Float">7</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">-15</X>
                            <Y dataType="Float">7</Y>
                          </item>
                        </vertices>
                      </item>
                    </_items>
                    <_size dataType="Int">1</_size>
                    <_version dataType="Int">7</_version>
                  </shapes>
                </item>
                <item dataType="Struct" type="Manager.AnimationManager" id="2365961001">
                  <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">chest</_x003C_animatedObjectName_x003E_k__BackingField>
                  <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="1494399561">
                    <_items dataType="Array" type="Misc.Animation[]" id="2751174030" length="4">
                      <item dataType="Struct" type="Misc.Animation" id="2776647888">
                        <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">20</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                        <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1.6</_x003C_AnimationDuration_x003E_k__BackingField>
                        <_x003C_AnimationName_x003E_k__BackingField dataType="String">open</_x003C_AnimationName_x003E_k__BackingField>
                        <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                      </item>
                      <item dataType="Struct" type="Misc.Animation" id="2635678318">
                        <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                        <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.4</_x003C_AnimationDuration_x003E_k__BackingField>
                        <_x003C_AnimationName_x003E_k__BackingField dataType="String">opened</_x003C_AnimationName_x003E_k__BackingField>
                        <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                      </item>
                      <item dataType="Struct" type="Misc.Animation" id="24331948">
                        <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                        <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                        <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                        <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                      </item>
                    </_items>
                    <_size dataType="Int">3</_size>
                    <_version dataType="Int">3</_version>
                  </_x003C_animationDatabase_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <animationDuration dataType="Float">0</animationDuration>
                  <animationToReturnTo />
                  <animSpriteRenderer dataType="ObjectRef">3597152694</animSpriteRenderer>
                  <currentAnimationName dataType="String">idle</currentAnimationName>
                  <currentTime dataType="Float">0</currentTime>
                  <gameobj dataType="ObjectRef">3889717313</gameobj>
                </item>
                <item dataType="Struct" type="Behavior.WeaponChest" id="1735289580">
                  <_x003C_weaponToGive_x003E_k__BackingField dataType="Enum" type="Behavior.WeaponChest+WeaponToGive" name="Pepperthrower" value="2" />
                  <active dataType="Bool">true</active>
                  <alreadyOpened dataType="Bool">false</alreadyOpened>
                  <animationManager dataType="ObjectRef">2365961001</animationManager>
                  <gameobj dataType="ObjectRef">3889717313</gameobj>
                  <player dataType="ObjectRef">787527030</player>
                  <playerWeapon dataType="ObjectRef">301082255</playerWeapon>
                </item>
              </_items>
              <_size dataType="Int">5</_size>
              <_version dataType="Int">5</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1839852904" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="86589903">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">891076794</item>
                  <item dataType="ObjectRef">12090110</item>
                  <item dataType="ObjectRef">198886202</item>
                  <item dataType="ObjectRef">1018545348</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="3403127392">
                  <item dataType="ObjectRef">1955064949</item>
                  <item dataType="ObjectRef">3597152694</item>
                  <item dataType="ObjectRef">2657526541</item>
                  <item dataType="ObjectRef">2365961001</item>
                  <item dataType="ObjectRef">1735289580</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">1955064949</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="1441200669">cSLP9RfOa0KwE9yw9Zp3Ww==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">lowest ammocrate</name>
            <parent dataType="ObjectRef">2103822484</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="1111954877">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="980469641">
              <_items dataType="Array" type="Duality.Component[]" id="3076781454" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="3472269809">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">1111954877</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-519.199036</X>
                    <Y dataType="Float">-100</Y>
                    <Z dataType="Float">-1</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-519.199036</X>
                    <Y dataType="Float">-100</Y>
                    <Z dataType="Float">-1</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="4174731401">
                  <active dataType="Bool">true</active>
                  <angularDamp dataType="Float">0.3</angularDamp>
                  <angularVel dataType="Float">0</angularVel>
                  <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                  <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
                  <colFilter />
                  <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                  <continous dataType="Bool">false</continous>
                  <explicitInertia dataType="Float">0</explicitInertia>
                  <explicitMass dataType="Float">0</explicitMass>
                  <fixedAngle dataType="Bool">true</fixedAngle>
                  <gameobj dataType="ObjectRef">1111954877</gameobj>
                  <ignoreGravity dataType="Bool">true</ignoreGravity>
                  <joints />
                  <linearDamp dataType="Float">0.3</linearDamp>
                  <linearVel dataType="Struct" type="Duality.Vector2" />
                  <revolutions dataType="Float">0</revolutions>
                  <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3988477561">
                    <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3298886478">
                      <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="331582160">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.3</friction>
                        <parent dataType="ObjectRef">4174731401</parent>
                        <restitution dataType="Float">0.3</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="3821145788">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">933.5348</X>
                            <Y dataType="Float">251.384125</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">1020.08374</X>
                            <Y dataType="Float">250.858185</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">1023.14673</X>
                            <Y dataType="Float">258.747345</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">935.2348</X>
                            <Y dataType="Float">259.010345</Y>
                          </item>
                        </vertices>
                      </item>
                    </_items>
                    <_size dataType="Int">1</_size>
                    <_version dataType="Int">1</_version>
                  </shapes>
                </item>
                <item dataType="Struct" type="Behavior.Teleporter" id="1119247139">
                  <_x003C_LocationToGoTo_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">497.04</X>
                    <Y dataType="Float">21.66</Y>
                  </_x003C_LocationToGoTo_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">1111954877</gameobj>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2566654272" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="162139715">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">12090110</item>
                  <item dataType="ObjectRef">222474358</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="980687544">
                  <item dataType="ObjectRef">3472269809</item>
                  <item dataType="ObjectRef">4174731401</item>
                  <item dataType="ObjectRef">1119247139</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">3472269809</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="2590589545">mFjPHYizFU2BZ+wh9ERlMA==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">RightHoleTeleporter</name>
            <parent dataType="ObjectRef">2103822484</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="3716996444">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3379190852">
              <_items dataType="Array" type="Duality.Component[]" id="4240494148" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="1782344080">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">3716996444</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-519.2</X>
                    <Y dataType="Float">-165.6</Y>
                    <Z dataType="Float">-1</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-519.2</X>
                    <Y dataType="Float">-165.6</Y>
                    <Z dataType="Float">-1</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2484805672">
                  <active dataType="Bool">true</active>
                  <angularDamp dataType="Float">0.3</angularDamp>
                  <angularVel dataType="Float">0</angularVel>
                  <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                  <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
                  <colFilter />
                  <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                  <continous dataType="Bool">false</continous>
                  <explicitInertia dataType="Float">0</explicitInertia>
                  <explicitMass dataType="Float">0</explicitMass>
                  <fixedAngle dataType="Bool">true</fixedAngle>
                  <gameobj dataType="ObjectRef">3716996444</gameobj>
                  <ignoreGravity dataType="Bool">true</ignoreGravity>
                  <joints />
                  <linearDamp dataType="Float">0.3</linearDamp>
                  <linearVel dataType="Struct" type="Duality.Vector2" />
                  <revolutions dataType="Float">0</revolutions>
                  <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="1237686240">
                    <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3723013084" length="4">
                      <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="586816196">
                        <density dataType="Float">1</density>
                        <friction dataType="Float">0.3</friction>
                        <parent dataType="ObjectRef">2484805672</parent>
                        <restitution dataType="Float">0.3</restitution>
                        <sensor dataType="Bool">false</sensor>
                        <vertices dataType="Array" type="Duality.Vector2[]" id="2931498820">
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">25</X>
                            <Y dataType="Float">-4.175215</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">69.14906</X>
                            <Y dataType="Float">-4.70115852</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">69.41204</X>
                            <Y dataType="Float">3.18799615</Y>
                          </item>
                          <item dataType="Struct" type="Duality.Vector2">
                            <X dataType="Float">25</X>
                            <Y dataType="Float">3.450968</Y>
                          </item>
                        </vertices>
                      </item>
                    </_items>
                    <_size dataType="Int">1</_size>
                    <_version dataType="Int">5</_version>
                  </shapes>
                </item>
                <item dataType="Struct" type="Behavior.Teleporter" id="3724288706">
                  <_x003C_LocationToGoTo_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-535.3</X>
                    <Y dataType="Float">-290</Y>
                  </_x003C_LocationToGoTo_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">3716996444</gameobj>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="55418518" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="585439694">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">12090110</item>
                  <item dataType="ObjectRef">222474358</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="2549964106">
                  <item dataType="ObjectRef">1782344080</item>
                  <item dataType="ObjectRef">2484805672</item>
                  <item dataType="ObjectRef">3724288706</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">1782344080</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="1898944894">QJYiK66Y6UuGNG5UvkTG9Q==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">TopHoleTeleporter</name>
            <parent dataType="ObjectRef">2103822484</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="713924857">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2961844029">
              <_items dataType="Array" type="Duality.Component[]" id="268306982" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="3074239789">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">713924857</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-408</X>
                    <Y dataType="Float">-243</Y>
                    <Z dataType="Float">0</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-408</X>
                    <Y dataType="Float">-243</Y>
                    <Z dataType="Float">0</Z>
                  </posAbs>
                  <scale dataType="Float">0.5</scale>
                  <scaleAbs dataType="Float">0.5</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="2456553679">
                  <active dataType="Bool">true</active>
                  <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customMat />
                  <gameobj dataType="ObjectRef">713924857</gameobj>
                  <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                  <offset dataType="Int">0</offset>
                  <text dataType="Struct" type="Duality.Drawing.FormattedText" id="3974809487">
                    <flowAreas />
                    <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="3593814702">
                      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                        <contentPath dataType="String">Default:Font:GenericMonospace10</contentPath>
                      </item>
                    </fonts>
                    <icons />
                    <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                    <maxHeight dataType="Int">0</maxHeight>
                    <maxWidth dataType="Int">0</maxWidth>
                    <sourceText dataType="String">Touch the crate to get a weapon</sourceText>
                    <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                  </text>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="823218360" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="800157783">
                  <item dataType="ObjectRef">1715766886</item>
                  <item dataType="ObjectRef">3750530768</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="3139464128">
                  <item dataType="ObjectRef">3074239789</item>
                  <item dataType="ObjectRef">2456553679</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">3074239789</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="3527418997">weFLEcYsMUG83RcQhWBPCQ==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Weaponcrate</name>
            <parent dataType="ObjectRef">2103822484</parent>
            <prefabLink />
          </item>
        </_items>
        <_size dataType="Int">7</_size>
        <_version dataType="Int">7</_version>
      </children>
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2802228186">
        <_items dataType="ObjectRef">466742873</_items>
        <_size dataType="Int">0</_size>
        <_version dataType="Int">0</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1068119818" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="596532032" length="0" />
          <values dataType="Array" type="System.Object[]" id="1800373838" length="0" />
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="565972444">6IjUN7FbvUCtqhHy88H1wQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">INANIMATE OBJECTS</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="ObjectRef">4157747002</item>
    <item dataType="ObjectRef">1132339076</item>
    <item dataType="ObjectRef">1182226281</item>
    <item dataType="ObjectRef">266672357</item>
    <item dataType="ObjectRef">3981402552</item>
    <item dataType="ObjectRef">3937918291</item>
    <item dataType="ObjectRef">3150687493</item>
    <item dataType="ObjectRef">3129138373</item>
    <item dataType="ObjectRef">3925785397</item>
    <item dataType="ObjectRef">2000276485</item>
    <item dataType="ObjectRef">3889717313</item>
    <item dataType="ObjectRef">1111954877</item>
    <item dataType="ObjectRef">3716996444</item>
    <item dataType="ObjectRef">713924857</item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
