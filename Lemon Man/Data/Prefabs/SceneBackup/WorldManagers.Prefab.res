﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="1910379577">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="596441714">
      <_items dataType="Array" type="Duality.Component[]" id="1173457104" length="4">
        <item dataType="Struct" type="Manager.WeaponDatabaseManager" id="2687757744">
          <_x003C_weaponDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Weapon]]" id="2016133952">
            <_items dataType="Array" type="Misc.Weapon[]" id="2776285468" length="4">
              <item dataType="Struct" type="Misc.Weapon" id="341372868">
                <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">0</_x003C_BottomLeft_x003E_k__BackingField>
                <_x003C_BottomRight_x003E_k__BackingField dataType="Float">0</_x003C_BottomRight_x003E_k__BackingField>
                <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                <_x003C_ID_x003E_k__BackingField dataType="Int">0</_x003C_ID_x003E_k__BackingField>
                <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">3</_x003C_Inaccuracy_x003E_k__BackingField>
                <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabXOffset_x003E_k__BackingField>
                <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabYOffset_x003E_k__BackingField>
                <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">3</_x003C_RateOfFire_x003E_k__BackingField>
                <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                <_x003C_Slug_x003E_k__BackingField dataType="String">peastol</_x003C_Slug_x003E_k__BackingField>
                <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                  <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\peastol.Texture.res</contentPath>
                </_x003C_Sprite_x003E_k__BackingField>
                <_x003C_Title_x003E_k__BackingField dataType="String">Peastol</_x003C_Title_x003E_k__BackingField>
                <_x003C_TopLeft_x003E_k__BackingField dataType="Float">0</_x003C_TopLeft_x003E_k__BackingField>
                <_x003C_TopRight_x003E_k__BackingField dataType="Float">0</_x003C_TopRight_x003E_k__BackingField>
                <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
              </item>
              <item dataType="Struct" type="Misc.Weapon" id="3306325398">
                <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">0</_x003C_BottomLeft_x003E_k__BackingField>
                <_x003C_BottomRight_x003E_k__BackingField dataType="Float">0</_x003C_BottomRight_x003E_k__BackingField>
                <_x003C_BurstCount_x003E_k__BackingField dataType="Int">5</_x003C_BurstCount_x003E_k__BackingField>
                <_x003C_ID_x003E_k__BackingField dataType="Int">1</_x003C_ID_x003E_k__BackingField>
                <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">10</_x003C_Inaccuracy_x003E_k__BackingField>
                <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabXOffset_x003E_k__BackingField>
                <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabYOffset_x003E_k__BackingField>
                <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">5</_x003C_RateOfFire_x003E_k__BackingField>
                <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Heavy" value="2" />
                <_x003C_Slug_x003E_k__BackingField dataType="String">shotgun</_x003C_Slug_x003E_k__BackingField>
                <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                  <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\shotgun.Texture.res</contentPath>
                </_x003C_Sprite_x003E_k__BackingField>
                <_x003C_Title_x003E_k__BackingField dataType="String">Shotgun</_x003C_Title_x003E_k__BackingField>
                <_x003C_TopLeft_x003E_k__BackingField dataType="Float">0</_x003C_TopLeft_x003E_k__BackingField>
                <_x003C_TopRight_x003E_k__BackingField dataType="Float">0</_x003C_TopRight_x003E_k__BackingField>
                <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
              </item>
            </_items>
            <_size dataType="Int">2</_size>
            <_version dataType="Int">2</_version>
          </_x003C_weaponDatabase_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">1910379577</gameobj>
        </item>
      </_items>
      <_size dataType="Int">1</_size>
      <_version dataType="Int">1</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="4159600714" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="1939281512">
          <item dataType="Type" id="1122475052" value="Manager.WeaponDatabaseManager" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1228690206">
          <item dataType="ObjectRef">2687757744</item>
        </values>
      </body>
    </compMap>
    <compTransform />
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="2281806548">T1DaAwTp9kS8F5vbHKougA==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">WorldManagers</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
