﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="476509570">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1536878085">
      <_items dataType="Array" type="Duality.Component[]" id="729549398">
        <item dataType="Struct" type="Duality.Components.Transform" id="2836824502">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">476509570</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">-0.1</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">-0.1</Z>
          </posAbs>
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Manager.AnimationManager" id="3247720554">
          <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peastol</_x003C_animatedObjectName_x003E_k__BackingField>
          <_x003C_animationDatabase_x003E_k__BackingField />
          <active dataType="Bool">true</active>
          <animationDuration dataType="Float">0</animationDuration>
          <animationToReturnTo />
          <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="183944951">
            <active dataType="Bool">true</active>
            <animDuration dataType="Float">5</animDuration>
            <animFirstFrame dataType="Int">0</animFirstFrame>
            <animFrameCount dataType="Int">0</animFrameCount>
            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
            <animPaused dataType="Bool">false</animPaused>
            <animTime dataType="Float">0</animTime>
            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
              <A dataType="Byte">255</A>
              <B dataType="Byte">255</B>
              <G dataType="Byte">255</G>
              <R dataType="Byte">255</R>
            </colorTint>
            <customFrameSequence />
            <customMat />
            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
            <gameobj dataType="ObjectRef">476509570</gameobj>
            <offset dataType="Int">0</offset>
            <pixelGrid dataType="Bool">false</pixelGrid>
            <rect dataType="Struct" type="Duality.Rect">
              <H dataType="Float">12</H>
              <W dataType="Float">27</W>
              <X dataType="Float">-13.5</X>
              <Y dataType="Float">-6</Y>
            </rect>
            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\heldWeapon.Material.res</contentPath>
            </sharedMat>
            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
          </animSpriteRenderer>
          <currentAnimationName />
          <currentTime dataType="Float">0</currentTime>
          <gameobj dataType="ObjectRef">476509570</gameobj>
        </item>
        <item dataType="Struct" type="Behavior.HeldWeapon" id="104779605">
          <_x003C_CurrentWeapon_x003E_k__BackingField dataType="Struct" type="Misc.Weapon" id="2748451301">
            <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">0</_x003C_BottomLeft_x003E_k__BackingField>
            <_x003C_BottomRight_x003E_k__BackingField dataType="Float">0</_x003C_BottomRight_x003E_k__BackingField>
            <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
            <_x003C_ID_x003E_k__BackingField dataType="Int">0</_x003C_ID_x003E_k__BackingField>
            <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">3</_x003C_Inaccuracy_x003E_k__BackingField>
            <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabXOffset_x003E_k__BackingField>
            <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabYOffset_x003E_k__BackingField>
            <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">3</_x003C_RateOfFire_x003E_k__BackingField>
            <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
            <_x003C_Slug_x003E_k__BackingField dataType="String">peastol</_x003C_Slug_x003E_k__BackingField>
            <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\peastol.Texture.res</contentPath>
            </_x003C_Sprite_x003E_k__BackingField>
            <_x003C_Title_x003E_k__BackingField dataType="String">Peastol</_x003C_Title_x003E_k__BackingField>
            <_x003C_TopLeft_x003E_k__BackingField dataType="Float">0</_x003C_TopLeft_x003E_k__BackingField>
            <_x003C_TopRight_x003E_k__BackingField dataType="Float">0</_x003C_TopRight_x003E_k__BackingField>
            <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
            <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
            <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
          </_x003C_CurrentWeapon_x003E_k__BackingField>
          <_x003C_prefabSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
          <_x003C_weaponOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
            <X dataType="Float">10.5</X>
            <Y dataType="Float">3.2</Y>
          </_x003C_weaponOffset_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <animationState dataType="Enum" type="Behavior.WeaponAnimationState" name="Idle" value="0" />
          <firingDelay dataType="Float">300</firingDelay>
          <firingDelayCounter dataType="Float">0</firingDelayCounter>
          <gameobj dataType="ObjectRef">476509570</gameobj>
          <heldWeaponAnimRenderer dataType="ObjectRef">183944951</heldWeaponAnimRenderer>
          <hydroLaserPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
          <idleAnimationDelayCounter dataType="Float">0</idleAnimationDelayCounter>
          <lemonadePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
          <peaPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
          <pepperFlamePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
          <player dataType="Struct" type="Duality.GameObject" id="787527030">
            <active dataType="Bool">true</active>
            <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="2396019089">
              <_items dataType="Array" type="Duality.GameObject[]" id="642429422" length="4" />
              <_size dataType="Int">0</_size>
              <_version dataType="Int">2</_version>
            </children>
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3191840416">
              <_items dataType="Array" type="Duality.Component[]" id="3393272379" length="8">
                <item dataType="Struct" type="Duality.Components.Transform" id="3147841962">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-26.2237949</Y>
                    <Z dataType="Float">0</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-26.2237949</Y>
                    <Z dataType="Float">0</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">1.33163428</Y>
                    <Z dataType="Float">0</Z>
                  </vel>
                  <velAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">1.33163428</Y>
                    <Z dataType="Float">0</Z>
                  </velAbs>
                </item>
                <item />
                <item dataType="Struct" type="Player.PlayerController" id="1575548244">
                  <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
                  <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
                  <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">4</X>
                    <Y dataType="Float">0.1</Y>
                    <Z dataType="Float">-0.1</Z>
                  </_x003C_BulletSpawnOffset_x003E_k__BackingField>
                  <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Player.FacingDirection" name="Left" value="0" />
                  <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">300</_x003C_FiringDelay_x003E_k__BackingField>
                  <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
                  <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
                  <_x003C_PlayerAnimationState_x003E_k__BackingField dataType="Enum" type="Player.PlayerAnimationState" name="Falling" value="3" />
                  <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <animationManager dataType="Struct" type="Manager.AnimationManager" id="3558738014">
                    <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">LemonMan</_x003C_animatedObjectName_x003E_k__BackingField>
                    <_x003C_animationDatabase_x003E_k__BackingField />
                    <active dataType="Bool">true</active>
                    <animationDuration dataType="Float">0</animationDuration>
                    <animationToReturnTo />
                    <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="494962411">
                      <active dataType="Bool">true</active>
                      <animDuration dataType="Float">2</animDuration>
                      <animFirstFrame dataType="Int">6</animFirstFrame>
                      <animFrameCount dataType="Int">4</animFrameCount>
                      <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                      <animPaused dataType="Bool">false</animPaused>
                      <animTime dataType="Float">0</animTime>
                      <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                        <A dataType="Byte">255</A>
                        <B dataType="Byte">255</B>
                        <G dataType="Byte">255</G>
                        <R dataType="Byte">255</R>
                      </colorTint>
                      <customFrameSequence />
                      <customMat />
                      <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="Horizontal" value="1" />
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                      <offset dataType="Int">0</offset>
                      <pixelGrid dataType="Bool">false</pixelGrid>
                      <rect dataType="Struct" type="Duality.Rect">
                        <H dataType="Float">26</H>
                        <W dataType="Float">24</W>
                        <X dataType="Float">-12</X>
                        <Y dataType="Float">-13</Y>
                      </rect>
                      <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                      <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                        <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                      </sharedMat>
                      <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                    </animSpriteRenderer>
                    <currentAnimationName />
                    <currentTime dataType="Float">0</currentTime>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                  </animationManager>
                  <firingDelayCounter dataType="Float">186.807968</firingDelayCounter>
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <gravity dataType="Float">444.444458</gravity>
                  <heldWeapon dataType="Struct" type="Behavior.HeldWeapon" id="971760278">
                    <_x003C_CurrentWeapon_x003E_k__BackingField dataType="Struct" type="Misc.Weapon" id="1948619764">
                      <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">0</_x003C_BottomLeft_x003E_k__BackingField>
                      <_x003C_BottomRight_x003E_k__BackingField dataType="Float">0</_x003C_BottomRight_x003E_k__BackingField>
                      <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                      <_x003C_ID_x003E_k__BackingField dataType="Int">0</_x003C_ID_x003E_k__BackingField>
                      <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">3</_x003C_Inaccuracy_x003E_k__BackingField>
                      <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabXOffset_x003E_k__BackingField>
                      <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabYOffset_x003E_k__BackingField>
                      <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">3</_x003C_RateOfFire_x003E_k__BackingField>
                      <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                      <_x003C_Slug_x003E_k__BackingField dataType="String">peastol</_x003C_Slug_x003E_k__BackingField>
                      <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                        <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\peastol.Texture.res</contentPath>
                      </_x003C_Sprite_x003E_k__BackingField>
                      <_x003C_Title_x003E_k__BackingField dataType="String">Peastol</_x003C_Title_x003E_k__BackingField>
                      <_x003C_TopLeft_x003E_k__BackingField dataType="Float">0</_x003C_TopLeft_x003E_k__BackingField>
                      <_x003C_TopRight_x003E_k__BackingField dataType="Float">0</_x003C_TopRight_x003E_k__BackingField>
                      <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                      <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                      <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
                    </_x003C_CurrentWeapon_x003E_k__BackingField>
                    <_x003C_prefabSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                    <_x003C_weaponOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">10.5</X>
                      <Y dataType="Float">3.2</Y>
                    </_x003C_weaponOffset_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationState dataType="Enum" type="Behavior.WeaponAnimationState" name="Idle" value="0" />
                    <firingDelay dataType="Float">300</firingDelay>
                    <firingDelayCounter dataType="Float">0</firingDelayCounter>
                    <gameobj dataType="Struct" type="Duality.GameObject" id="1343490243">
                      <active dataType="Bool">true</active>
                      <children />
                      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="945565631">
                        <_items dataType="Array" type="Duality.Component[]" id="1299951022">
                          <item dataType="Struct" type="Duality.Components.Transform" id="3703805175">
                            <active dataType="Bool">true</active>
                            <angle dataType="Float">0</angle>
                            <angleAbs dataType="Float">0</angleAbs>
                            <angleVel dataType="Float">0</angleVel>
                            <angleVelAbs dataType="Float">0</angleVelAbs>
                            <deriveAngle dataType="Bool">true</deriveAngle>
                            <gameobj dataType="ObjectRef">1343490243</gameobj>
                            <ignoreParent dataType="Bool">false</ignoreParent>
                            <parentTransform />
                            <pos dataType="Struct" type="Duality.Vector3">
                              <X dataType="Float">0</X>
                              <Y dataType="Float">0</Y>
                              <Z dataType="Float">-0.1</Z>
                            </pos>
                            <posAbs dataType="Struct" type="Duality.Vector3">
                              <X dataType="Float">0</X>
                              <Y dataType="Float">0</Y>
                              <Z dataType="Float">-0.1</Z>
                            </posAbs>
                            <scale dataType="Float">1</scale>
                            <scaleAbs dataType="Float">1</scaleAbs>
                            <vel dataType="Struct" type="Duality.Vector3" />
                            <velAbs dataType="Struct" type="Duality.Vector3" />
                          </item>
                          <item dataType="ObjectRef">971760278</item>
                          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1050925624">
                            <active dataType="Bool">true</active>
                            <animDuration dataType="Float">5</animDuration>
                            <animFirstFrame dataType="Int">0</animFirstFrame>
                            <animFrameCount dataType="Int">0</animFrameCount>
                            <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                            <animPaused dataType="Bool">false</animPaused>
                            <animTime dataType="Float">0</animTime>
                            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                              <A dataType="Byte">255</A>
                              <B dataType="Byte">255</B>
                              <G dataType="Byte">255</G>
                              <R dataType="Byte">255</R>
                            </colorTint>
                            <customFrameSequence />
                            <customMat />
                            <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                            <gameobj dataType="ObjectRef">1343490243</gameobj>
                            <offset dataType="Int">0</offset>
                            <pixelGrid dataType="Bool">false</pixelGrid>
                            <rect dataType="Struct" type="Duality.Rect">
                              <H dataType="Float">12</H>
                              <W dataType="Float">27</W>
                              <X dataType="Float">-13.5</X>
                              <Y dataType="Float">-6</Y>
                            </rect>
                            <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                            <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\heldWeapon.Material.res</contentPath>
                            </sharedMat>
                            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                          </item>
                          <item dataType="Struct" type="Manager.AnimationManager" id="4114701227">
                            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peastol</_x003C_animatedObjectName_x003E_k__BackingField>
                            <_x003C_animationDatabase_x003E_k__BackingField />
                            <active dataType="Bool">true</active>
                            <animationDuration dataType="Float">0</animationDuration>
                            <animationToReturnTo />
                            <animSpriteRenderer dataType="ObjectRef">1050925624</animSpriteRenderer>
                            <currentAnimationName />
                            <currentTime dataType="Float">0</currentTime>
                            <gameobj dataType="ObjectRef">1343490243</gameobj>
                          </item>
                        </_items>
                        <_size dataType="Int">4</_size>
                        <_version dataType="Int">12</_version>
                      </compList>
                      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2813791712" surrogate="true">
                        <header />
                        <body>
                          <keys dataType="Array" type="System.Object[]" id="2022285941">
                            <item dataType="Type" id="3737149558" value="Duality.Components.Transform" />
                            <item dataType="Type" id="1411866906" value="Manager.AnimationManager" />
                            <item dataType="Type" id="2437026710" value="Behavior.HeldWeapon" />
                            <item dataType="Type" id="2375575290" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                          </keys>
                          <values dataType="Array" type="System.Object[]" id="700109512">
                            <item dataType="ObjectRef">3703805175</item>
                            <item dataType="ObjectRef">4114701227</item>
                            <item dataType="ObjectRef">971760278</item>
                            <item dataType="ObjectRef">1050925624</item>
                          </values>
                        </body>
                      </compMap>
                      <compTransform dataType="ObjectRef">3703805175</compTransform>
                      <identifier dataType="Struct" type="System.Guid" surrogate="true">
                        <header>
                          <data dataType="Array" type="System.Byte[]" id="1722933439">A0P5xu17sEuEt+3rLf8XcA==</data>
                        </header>
                        <body />
                      </identifier>
                      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                      <name dataType="String">Weapon</name>
                      <parent />
                      <prefabLink />
                    </gameobj>
                    <heldWeaponAnimRenderer dataType="ObjectRef">1050925624</heldWeaponAnimRenderer>
                    <hydroLaserPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                    <idleAnimationDelayCounter dataType="Float">0</idleAnimationDelayCounter>
                    <lemonadePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                    <peaPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                    <pepperFlamePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                    <player dataType="ObjectRef">787527030</player>
                    <playerAnimRenderer dataType="ObjectRef">494962411</playerAnimRenderer>
                    <playerController dataType="ObjectRef">1575548244</playerController>
                    <prefabToBeFired dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Pea.Prefab.res</contentPath>
                    </prefabToBeFired>
                    <soundManager />
                    <transform dataType="ObjectRef">3703805175</transform>
                    <transparentWeaponTex dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]" />
                    <weaponAnimationManager dataType="ObjectRef">4114701227</weaponAnimationManager>
                    <weaponDatabase dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Weapon]]" id="3574393590">
                      <_items dataType="Array" type="Misc.Weapon[]" id="1064981470" length="4">
                        <item dataType="ObjectRef">1948619764</item>
                        <item dataType="Struct" type="Misc.Weapon" id="533078288">
                          <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">0</_x003C_BottomLeft_x003E_k__BackingField>
                          <_x003C_BottomRight_x003E_k__BackingField dataType="Float">0</_x003C_BottomRight_x003E_k__BackingField>
                          <_x003C_BurstCount_x003E_k__BackingField dataType="Int">5</_x003C_BurstCount_x003E_k__BackingField>
                          <_x003C_ID_x003E_k__BackingField dataType="Int">1</_x003C_ID_x003E_k__BackingField>
                          <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">10</_x003C_Inaccuracy_x003E_k__BackingField>
                          <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabXOffset_x003E_k__BackingField>
                          <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabYOffset_x003E_k__BackingField>
                          <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">5</_x003C_RateOfFire_x003E_k__BackingField>
                          <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Heavy" value="2" />
                          <_x003C_Slug_x003E_k__BackingField dataType="String">shotgun</_x003C_Slug_x003E_k__BackingField>
                          <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                            <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\shotgun.Texture.res</contentPath>
                          </_x003C_Sprite_x003E_k__BackingField>
                          <_x003C_Title_x003E_k__BackingField dataType="String">Shotgun</_x003C_Title_x003E_k__BackingField>
                          <_x003C_TopLeft_x003E_k__BackingField dataType="Float">0</_x003C_TopLeft_x003E_k__BackingField>
                          <_x003C_TopRight_x003E_k__BackingField dataType="Float">0</_x003C_TopRight_x003E_k__BackingField>
                          <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                          <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                          <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
                        </item>
                      </_items>
                      <_size dataType="Int">2</_size>
                      <_version dataType="Int">2</_version>
                    </weaponDatabase>
                    <weaponDatabaseObject dataType="Struct" type="Duality.GameObject" id="2834260959">
                      <active dataType="Bool">true</active>
                      <children />
                      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2355899891">
                        <_items dataType="Array" type="Duality.Component[]" id="752489254" length="4">
                          <item dataType="Struct" type="Manager.WeaponDatabaseManager" id="3611639126">
                            <_x003C_weaponDatabase_x003E_k__BackingField dataType="ObjectRef">3574393590</_x003C_weaponDatabase_x003E_k__BackingField>
                            <active dataType="Bool">true</active>
                            <gameobj dataType="ObjectRef">2834260959</gameobj>
                          </item>
                        </_items>
                        <_size dataType="Int">1</_size>
                        <_version dataType="Int">11</_version>
                      </compList>
                      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2583485880" surrogate="true">
                        <header />
                        <body>
                          <keys dataType="Array" type="System.Object[]" id="4208288921">
                            <item dataType="Type" id="3256482382" value="Manager.WeaponDatabaseManager" />
                          </keys>
                          <values dataType="Array" type="System.Object[]" id="503588992">
                            <item dataType="ObjectRef">3611639126</item>
                          </values>
                        </body>
                      </compMap>
                      <compTransform />
                      <identifier dataType="Struct" type="System.Guid" surrogate="true">
                        <header>
                          <data dataType="Array" type="System.Byte[]" id="3404880859">biwph8Z5kk+28lYMuemNIw==</data>
                        </header>
                        <body />
                      </identifier>
                      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                      <name dataType="String">WorldManagers</name>
                      <parent />
                      <prefabLink />
                    </weaponDatabaseObject>
                  </heldWeapon>
                  <jumpVelocity dataType="Float">200</jumpVelocity>
                  <rigidBody />
                  <spriteRenderer dataType="ObjectRef">494962411</spriteRenderer>
                  <velocity dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">83.02579</Y>
                  </velocity>
                </item>
                <item dataType="Struct" type="Behavior.EntityStats" id="1425810874">
                  <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">35</_x003C_CurrentHealth_x003E_k__BackingField>
                  <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
                  <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">35</_x003C_MaxHealth_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <animationManager />
                  <damageEffectLength dataType="Float">0.1</damageEffectLength>
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                  <soundManager />
                  <timer />
                </item>
                <item dataType="ObjectRef">494962411</item>
                <item dataType="ObjectRef">3558738014</item>
              </_items>
              <_size dataType="Int">6</_size>
              <_version dataType="Int">20</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2085781507" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="3846792228">
                  <item dataType="ObjectRef">3737149558</item>
                  <item dataType="ObjectRef">2375575290</item>
                  <item dataType="Type" id="2492177092" value="Player.PlayerController" />
                  <item dataType="Type" id="366449558" value="Behavior.EntityStats" />
                  <item dataType="ObjectRef">1411866906</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="4004096278">
                  <item dataType="ObjectRef">3147841962</item>
                  <item dataType="ObjectRef">494962411</item>
                  <item dataType="ObjectRef">1575548244</item>
                  <item dataType="ObjectRef">1425810874</item>
                  <item dataType="ObjectRef">3558738014</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">3147841962</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="1500726816">UPF8jIwOT0SjSKyQzVYuaQ==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Player</name>
            <parent />
            <prefabLink />
          </player>
          <playerAnimRenderer dataType="ObjectRef">494962411</playerAnimRenderer>
          <playerController dataType="ObjectRef">1575548244</playerController>
          <prefabToBeFired dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
            <contentPath dataType="String">Data\Prefabs\Pea.Prefab.res</contentPath>
          </prefabToBeFired>
          <soundManager />
          <transform dataType="ObjectRef">2836824502</transform>
          <transparentWeaponTex dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]" />
          <weaponAnimationManager dataType="ObjectRef">3247720554</weaponAnimationManager>
          <weaponDatabase dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Weapon]]" id="591492456">
            <_items dataType="Array" type="Misc.Weapon[]" id="1793307791" length="4">
              <item dataType="ObjectRef">2748451301</item>
              <item dataType="Struct" type="Misc.Weapon" id="2357949614">
                <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">0</_x003C_BottomLeft_x003E_k__BackingField>
                <_x003C_BottomRight_x003E_k__BackingField dataType="Float">0</_x003C_BottomRight_x003E_k__BackingField>
                <_x003C_BurstCount_x003E_k__BackingField dataType="Int">5</_x003C_BurstCount_x003E_k__BackingField>
                <_x003C_ID_x003E_k__BackingField dataType="Int">1</_x003C_ID_x003E_k__BackingField>
                <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">10</_x003C_Inaccuracy_x003E_k__BackingField>
                <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabXOffset_x003E_k__BackingField>
                <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">0</_x003C_PrefabYOffset_x003E_k__BackingField>
                <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">5</_x003C_RateOfFire_x003E_k__BackingField>
                <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Heavy" value="2" />
                <_x003C_Slug_x003E_k__BackingField dataType="String">shotgun</_x003C_Slug_x003E_k__BackingField>
                <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                  <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\shotgun.Texture.res</contentPath>
                </_x003C_Sprite_x003E_k__BackingField>
                <_x003C_Title_x003E_k__BackingField dataType="String">Shotgun</_x003C_Title_x003E_k__BackingField>
                <_x003C_TopLeft_x003E_k__BackingField dataType="Float">0</_x003C_TopLeft_x003E_k__BackingField>
                <_x003C_TopRight_x003E_k__BackingField dataType="Float">0</_x003C_TopRight_x003E_k__BackingField>
                <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
              </item>
            </_items>
            <_size dataType="Int">2</_size>
            <_version dataType="Int">2</_version>
          </weaponDatabase>
          <weaponDatabaseObject dataType="ObjectRef">2834260959</weaponDatabaseObject>
        </item>
        <item dataType="ObjectRef">183944951</item>
      </_items>
      <_size dataType="Int">4</_size>
      <_version dataType="Int">6</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1727690664" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="2871505391">
          <item dataType="ObjectRef">3737149558</item>
          <item dataType="ObjectRef">1411866906</item>
          <item dataType="ObjectRef">2437026710</item>
          <item dataType="ObjectRef">2375575290</item>
        </keys>
        <values dataType="Array" type="System.Object[]" id="863457696">
          <item dataType="ObjectRef">2836824502</item>
          <item dataType="ObjectRef">3247720554</item>
          <item dataType="ObjectRef">104779605</item>
          <item dataType="ObjectRef">183944951</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">2836824502</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="1837524093">7b3El6JqBEi6OeFxQ0c2aw==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Weapon</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
