﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="3549953109">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2430568790">
      <_items dataType="Array" type="Duality.Component[]" id="3723255328" length="8">
        <item dataType="Struct" type="Duality.Components.Transform" id="1615300745">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">3549953109</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">-5.2</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">-5.2</Z>
          </posAbs>
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3257388490">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">0.4</animDuration>
          <animFirstFrame dataType="Int">0</animFirstFrame>
          <animFrameCount dataType="Int">8</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
          <animPaused dataType="Bool">false</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence />
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">3549953109</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">48</H>
            <W dataType="Float">48</W>
            <X dataType="Float">-24</X>
            <Y dataType="Float">-24</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Misc\Lemonade_Explosion.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="Manager.AnimationManager" id="2026196797">
          <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">lemonade_explosion</_x003C_animatedObjectName_x003E_k__BackingField>
          <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="2887283977">
            <_items dataType="Array" type="Misc.Animation[]" id="1322797710" length="4">
              <item dataType="Struct" type="Misc.Animation" id="3129280720">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">8</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.4</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">explode</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
              </item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">1</_version>
          </_x003C_animationDatabase_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <animationDuration dataType="Float">0</animationDuration>
          <animationToReturnTo />
          <animSpriteRenderer dataType="ObjectRef">3257388490</animSpriteRenderer>
          <currentAnimationName dataType="String">explode</currentAnimationName>
          <currentTime dataType="Float">0</currentTime>
          <gameobj dataType="ObjectRef">3549953109</gameobj>
        </item>
        <item dataType="Struct" type="Behavior.LemonExplosion" id="502128021">
          <active dataType="Bool">true</active>
          <animationManager dataType="ObjectRef">2026196797</animationManager>
          <blastRadius dataType="Float">18.5</blastRadius>
          <currentTime dataType="Float">0</currentTime>
          <damageMultiplier dataType="Float">20</damageMultiplier>
          <explosionPosition dataType="ObjectRef">1615300745</explosionPosition>
          <gameobj dataType="ObjectRef">3549953109</gameobj>
          <maxDamage dataType="Float">10</maxDamage>
          <soundManager />
          <stats />
          <timeUntilExplode dataType="Float">0.4</timeUntilExplode>
        </item>
        <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2317762337">
          <active dataType="Bool">true</active>
          <angularDamp dataType="Float">0.3</angularDamp>
          <angularVel dataType="Float">0</angularVel>
          <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
          <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
          <colFilter />
          <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
          <continous dataType="Bool">false</continous>
          <explicitInertia dataType="Float">0</explicitInertia>
          <explicitMass dataType="Float">0</explicitMass>
          <fixedAngle dataType="Bool">true</fixedAngle>
          <gameobj dataType="ObjectRef">3549953109</gameobj>
          <ignoreGravity dataType="Bool">true</ignoreGravity>
          <joints />
          <linearDamp dataType="Float">0.3</linearDamp>
          <linearVel dataType="Struct" type="Duality.Vector2" />
          <revolutions dataType="Float">0</revolutions>
          <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="390940485">
            <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2723685590">
              <item dataType="Struct" type="Duality.Components.Physics.CircleShapeInfo" id="1309438240">
                <density dataType="Float">1</density>
                <friction dataType="Float">0.3</friction>
                <parent dataType="ObjectRef">2317762337</parent>
                <position dataType="Struct" type="Duality.Vector2" />
                <radius dataType="Float">18.5</radius>
                <restitution dataType="Float">0.3</restitution>
                <sensor dataType="Bool">true</sensor>
              </item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">18</_version>
          </shapes>
        </item>
      </_items>
      <_size dataType="Int">5</_size>
      <_version dataType="Int">5</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="623251674" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="1887313188">
          <item dataType="Type" id="3460148932" value="Duality.Components.Transform" />
          <item dataType="Type" id="2121163670" value="Duality.Components.Renderers.AnimSpriteRenderer" />
          <item dataType="Type" id="665482112" value="Manager.AnimationManager" />
          <item dataType="Type" id="13616674" value="Behavior.LemonExplosion" />
          <item dataType="Type" id="2939577052" value="Duality.Components.Physics.RigidBody" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1385139478">
          <item dataType="ObjectRef">1615300745</item>
          <item dataType="ObjectRef">3257388490</item>
          <item dataType="ObjectRef">2026196797</item>
          <item dataType="ObjectRef">502128021</item>
          <item dataType="ObjectRef">2317762337</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">1615300745</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="1179996960">/rZW7cHw7kq40s76vE0LRQ==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Lemonade_Explosion</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
