﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="262094440">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1148914559">
      <_items dataType="Array" type="Duality.Component[]" id="948524334" length="8">
        <item dataType="Struct" type="Duality.Components.Transform" id="2622409372">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">262094440</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">0.3</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">0.3</Z>
          </posAbs>
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="4264497117">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">0.6</animDuration>
          <animFirstFrame dataType="Int">0</animFirstFrame>
          <animFrameCount dataType="Int">7</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
          <animPaused dataType="Bool">false</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence />
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">262094440</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">14</H>
            <W dataType="Float">16</W>
            <X dataType="Float">-8</X>
            <Y dataType="Float">-10</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\FlamethrowerParticle.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="Manager.AnimationManager" id="3033305424">
          <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">flameParticle</_x003C_animatedObjectName_x003E_k__BackingField>
          <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="2771756476">
            <_items dataType="Array" type="Misc.Animation[]" id="1238051396" length="4">
              <item dataType="Struct" type="Misc.Animation" id="1289669188">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">7</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.6</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">burn</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
              </item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">1</_version>
          </_x003C_animationDatabase_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <animationDuration dataType="Float">0</animationDuration>
          <animationToReturnTo />
          <animSpriteRenderer dataType="ObjectRef">4264497117</animSpriteRenderer>
          <currentAnimationName dataType="String">burn</currentAnimationName>
          <currentTime dataType="Float">0</currentTime>
          <gameobj dataType="ObjectRef">262094440</gameobj>
        </item>
        <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3324870964">
          <active dataType="Bool">true</active>
          <angularDamp dataType="Float">0.3</angularDamp>
          <angularVel dataType="Float">0</angularVel>
          <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
          <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
          <colFilter />
          <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
          <continous dataType="Bool">true</continous>
          <explicitInertia dataType="Float">0</explicitInertia>
          <explicitMass dataType="Float">0</explicitMass>
          <fixedAngle dataType="Bool">true</fixedAngle>
          <gameobj dataType="ObjectRef">262094440</gameobj>
          <ignoreGravity dataType="Bool">true</ignoreGravity>
          <joints />
          <linearDamp dataType="Float">0.3</linearDamp>
          <linearVel dataType="Struct" type="Duality.Vector2" />
          <revolutions dataType="Float">0</revolutions>
          <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3266044960">
            <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3364389852">
              <item dataType="Struct" type="Duality.Components.Physics.CircleShapeInfo" id="3505785540">
                <density dataType="Float">1</density>
                <friction dataType="Float">0.3</friction>
                <parent dataType="ObjectRef">3324870964</parent>
                <position dataType="Struct" type="Duality.Vector2">
                  <X dataType="Float">0</X>
                  <Y dataType="Float">1</Y>
                </position>
                <radius dataType="Float">4</radius>
                <restitution dataType="Float">0.3</restitution>
                <sensor dataType="Bool">true</sensor>
              </item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">6</_version>
          </shapes>
        </item>
        <item dataType="Struct" type="Behavior.PepperFlameParticle" id="259482577">
          <_x003C_Creator_x003E_k__BackingField />
          <_x003C_LinearVelocityToSet_x003E_k__BackingField dataType="Struct" type="Duality.Vector2" />
          <_x003C_PepperSpriteRenderer_x003E_k__BackingField dataType="ObjectRef">4264497117</_x003C_PepperSpriteRenderer_x003E_k__BackingField>
          <_x003C_RotatingBreakPeriod_x003E_k__BackingField dataType="Float">0.1</_x003C_RotatingBreakPeriod_x003E_k__BackingField>
          <_x003C_StartRotation_x003E_k__BackingField dataType="Bool">false</_x003C_StartRotation_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <animationManager dataType="ObjectRef">3033305424</animationManager>
          <currentTime dataType="Float">0</currentTime>
          <gameobj dataType="ObjectRef">262094440</gameobj>
          <hasCollided dataType="Bool">false</hasCollided>
          <heldWeapon dataType="Struct" type="Behavior.HeldWeapon" id="301082255">
            <_x003C_CurrentWeapon_x003E_k__BackingField dataType="Struct" type="Misc.Weapon" id="70536464">
              <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">29</_x003C_BottomLeft_x003E_k__BackingField>
              <_x003C_BottomRight_x003E_k__BackingField dataType="Float">19</_x003C_BottomRight_x003E_k__BackingField>
              <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
              <_x003C_ID_x003E_k__BackingField dataType="Int">2</_x003C_ID_x003E_k__BackingField>
              <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">0</_x003C_Inaccuracy_x003E_k__BackingField>
              <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
              <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">2.8</_x003C_PrefabYOffset_x003E_k__BackingField>
              <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">0.5</_x003C_RateOfFire_x003E_k__BackingField>
              <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
              <_x003C_Slug_x003E_k__BackingField dataType="String">pepper_thrower</_x003C_Slug_x003E_k__BackingField>
              <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\pepper_thrower.Texture.res</contentPath>
              </_x003C_Sprite_x003E_k__BackingField>
              <_x003C_Title_x003E_k__BackingField dataType="String">Pepper Thrower</_x003C_Title_x003E_k__BackingField>
              <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-14.5</_x003C_TopLeft_x003E_k__BackingField>
              <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9.5</_x003C_TopRight_x003E_k__BackingField>
              <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pepper" value="2" />
              <_x003C_XOffset_x003E_k__BackingField dataType="Float">14.5</_x003C_XOffset_x003E_k__BackingField>
              <_x003C_YOffset_x003E_k__BackingField dataType="Float">2.4</_x003C_YOffset_x003E_k__BackingField>
            </_x003C_CurrentWeapon_x003E_k__BackingField>
            <_x003C_prefabSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">8</X>
              <Y dataType="Float">2.8</Y>
              <Z dataType="Float">0.3</Z>
            </_x003C_prefabSpawnOffset_x003E_k__BackingField>
            <_x003C_weaponOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">14.5</X>
              <Y dataType="Float">2.4</Y>
            </_x003C_weaponOffset_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <animationState dataType="Enum" type="Behavior.WeaponAnimationState" name="Idle" value="0" />
            <firingDelay dataType="Float">50</firingDelay>
            <firingDelayCounter dataType="Float">0</firingDelayCounter>
            <gameobj dataType="Struct" type="Duality.GameObject" id="672812220">
              <active dataType="Bool">true</active>
              <children />
              <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="4175316008">
                <_items dataType="Array" type="Duality.Component[]" id="3201545644">
                  <item dataType="Struct" type="Duality.Components.Transform" id="3033127152">
                    <active dataType="Bool">true</active>
                    <angle dataType="Float">0</angle>
                    <angleAbs dataType="Float">0</angleAbs>
                    <angleVel dataType="Float">0</angleVel>
                    <angleVelAbs dataType="Float">0</angleVelAbs>
                    <deriveAngle dataType="Bool">true</deriveAngle>
                    <gameobj dataType="ObjectRef">672812220</gameobj>
                    <ignoreParent dataType="Bool">false</ignoreParent>
                    <parentTransform />
                    <pos dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">0</X>
                      <Y dataType="Float">0</Y>
                      <Z dataType="Float">-0.1</Z>
                    </pos>
                    <posAbs dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">0</X>
                      <Y dataType="Float">0</Y>
                      <Z dataType="Float">-0.1</Z>
                    </posAbs>
                    <scale dataType="Float">1</scale>
                    <scaleAbs dataType="Float">1</scaleAbs>
                    <vel dataType="Struct" type="Duality.Vector3" />
                    <velAbs dataType="Struct" type="Duality.Vector3" />
                  </item>
                  <item dataType="Struct" type="Manager.AnimationManager" id="3444023204">
                    <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">pepper_thrower</_x003C_animatedObjectName_x003E_k__BackingField>
                    <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="3865229956">
                      <_items dataType="Array" type="Misc.Animation[]" id="1813521476" length="4">
                        <item dataType="Struct" type="Misc.Animation" id="2801565252">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">10</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">11</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                        <item dataType="Struct" type="Misc.Animation" id="2047539862">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">10</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">fire</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">21</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                        <item dataType="Struct" type="Misc.Animation" id="1284537344">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">10</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">playerMoving</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                      </_items>
                      <_size dataType="Int">3</_size>
                      <_version dataType="Int">3</_version>
                    </_x003C_animationDatabase_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationDuration dataType="Float">0</animationDuration>
                    <animationToReturnTo />
                    <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="380247601">
                      <active dataType="Bool">true</active>
                      <animDuration dataType="Float">5</animDuration>
                      <animFirstFrame dataType="Int">0</animFirstFrame>
                      <animFrameCount dataType="Int">0</animFrameCount>
                      <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                      <animPaused dataType="Bool">false</animPaused>
                      <animTime dataType="Float">0</animTime>
                      <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                        <A dataType="Byte">255</A>
                        <B dataType="Byte">255</B>
                        <G dataType="Byte">255</G>
                        <R dataType="Byte">255</R>
                      </colorTint>
                      <customFrameSequence />
                      <customMat />
                      <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                      <gameobj dataType="ObjectRef">672812220</gameobj>
                      <offset dataType="Int">0</offset>
                      <pixelGrid dataType="Bool">false</pixelGrid>
                      <rect dataType="Struct" type="Duality.Rect">
                        <H dataType="Float">19</H>
                        <W dataType="Float">29</W>
                        <X dataType="Float">-14.5</X>
                        <Y dataType="Float">-9.5</Y>
                      </rect>
                      <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                      <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                        <contentPath dataType="String">Data\Sprites &amp; Spritesheets\heldWeapon.Material.res</contentPath>
                      </sharedMat>
                      <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                    </animSpriteRenderer>
                    <currentAnimationName />
                    <currentTime dataType="Float">0</currentTime>
                    <gameobj dataType="ObjectRef">672812220</gameobj>
                  </item>
                  <item dataType="ObjectRef">301082255</item>
                  <item dataType="ObjectRef">380247601</item>
                </_items>
                <_size dataType="Int">4</_size>
                <_version dataType="Int">4</_version>
              </compList>
              <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="284934814" surrogate="true">
                <header />
                <body>
                  <keys dataType="Array" type="System.Object[]" id="690029034">
                    <item dataType="Type" id="2577056032" value="Duality.Components.Transform" />
                    <item dataType="Type" id="1043132302" value="Manager.AnimationManager" />
                    <item dataType="Type" id="3635152956" value="Behavior.HeldWeapon" />
                    <item dataType="Type" id="3855268626" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                  </keys>
                  <values dataType="Array" type="System.Object[]" id="1824785370">
                    <item dataType="ObjectRef">3033127152</item>
                    <item dataType="ObjectRef">3444023204</item>
                    <item dataType="ObjectRef">301082255</item>
                    <item dataType="ObjectRef">380247601</item>
                  </values>
                </body>
              </compMap>
              <compTransform dataType="ObjectRef">3033127152</compTransform>
              <identifier dataType="Struct" type="System.Guid" surrogate="true">
                <header>
                  <data dataType="Array" type="System.Byte[]" id="2360204874">ww7ZFsN1MUKIHkuvVynjTg==</data>
                </header>
                <body />
              </identifier>
              <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
              <name dataType="String">Weapon</name>
              <parent />
              <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1336753428">
                <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1024273992">
                  <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1642359916" length="4">
                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4052669864">
                        <_items dataType="Array" type="System.Int32[]" id="2966898348"></_items>
                        <_size dataType="Int">0</_size>
                        <_version dataType="Int">1</_version>
                      </childIndex>
                      <componentType dataType="ObjectRef">2577056032</componentType>
                      <prop dataType="MemberInfo" id="3931510686" value="P:Duality.Components.Transform:RelativePos" />
                      <val dataType="Struct" type="Duality.Vector3">
                        <X dataType="Float">-22.4</X>
                        <Y dataType="Float">0</Y>
                        <Z dataType="Float">-0.1</Z>
                      </val>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">391</_version>
                </changes>
                <obj dataType="ObjectRef">672812220</obj>
                <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                  <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                </prefab>
              </prefabLink>
            </gameobj>
            <heldWeaponAnimRenderer dataType="ObjectRef">380247601</heldWeaponAnimRenderer>
            <hydroLaserPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
            <idleAnimationDelayCounter dataType="Float">0</idleAnimationDelayCounter>
            <lemonadePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\Weapons\Lemonade.Prefab.res</contentPath>
            </lemonadePrefab>
            <peaPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\Weapons\Pea.Prefab.res</contentPath>
            </peaPrefab>
            <pepperFlamePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\Weapons\FlamethrowerParticleOld.Prefab.res</contentPath>
            </pepperFlamePrefab>
            <player dataType="Struct" type="Duality.GameObject" id="787527030">
              <active dataType="Bool">true</active>
              <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="2833676682">
                <_items dataType="Array" type="Duality.GameObject[]" id="2845878240" length="4" />
                <_size dataType="Int">0</_size>
                <_version dataType="Int">2</_version>
              </children>
              <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="182864154">
                <_items dataType="Array" type="Duality.Component[]" id="2498070640">
                  <item dataType="Struct" type="Duality.Components.Transform" id="3147841962">
                    <active dataType="Bool">true</active>
                    <angle dataType="Float">0</angle>
                    <angleAbs dataType="Float">0</angleAbs>
                    <angleVel dataType="Float">0</angleVel>
                    <angleVelAbs dataType="Float">0</angleVelAbs>
                    <deriveAngle dataType="Bool">true</deriveAngle>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <ignoreParent dataType="Bool">false</ignoreParent>
                    <parentTransform />
                    <pos dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">-726.270752</X>
                      <Y dataType="Float">-235.351654</Y>
                      <Z dataType="Float">-1</Z>
                    </pos>
                    <posAbs dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">-726.270752</X>
                      <Y dataType="Float">-235.351654</Y>
                      <Z dataType="Float">-1</Z>
                    </posAbs>
                    <scale dataType="Float">1</scale>
                    <scaleAbs dataType="Float">1</scaleAbs>
                    <vel dataType="Struct" type="Duality.Vector3" />
                    <velAbs dataType="Struct" type="Duality.Vector3" />
                  </item>
                  <item dataType="Struct" type="Player.PlayerController" id="1575548244">
                    <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
                    <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
                    <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">4</X>
                      <Y dataType="Float">0.1</Y>
                      <Z dataType="Float">-0.1</Z>
                    </_x003C_BulletSpawnOffset_x003E_k__BackingField>
                    <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Player.FacingDirection" name="Left" value="0" />
                    <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">300</_x003C_FiringDelay_x003E_k__BackingField>
                    <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
                    <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
                    <_x003C_PlayerAnimationState_x003E_k__BackingField dataType="Enum" type="Player.PlayerAnimationState" name="Idle" value="0" />
                    <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationManager dataType="Struct" type="Manager.AnimationManager" id="3558738014">
                      <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">LemonMan</_x003C_animatedObjectName_x003E_k__BackingField>
                      <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="918931390">
                        <_items dataType="Array" type="Misc.Animation[]" id="45209616">
                          <item dataType="Struct" type="Misc.Animation" id="3992796988">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">6</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="3095313302">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">4</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.5</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">7</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="3658807784">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">jump</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="2588565618">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">fall</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                        </_items>
                        <_size dataType="Int">4</_size>
                        <_version dataType="Int">4</_version>
                      </_x003C_animationDatabase_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <animationDuration dataType="Float">0</animationDuration>
                      <animationToReturnTo />
                      <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="494962411">
                        <active dataType="Bool">true</active>
                        <animDuration dataType="Float">2</animDuration>
                        <animFirstFrame dataType="Int">6</animFirstFrame>
                        <animFrameCount dataType="Int">4</animFrameCount>
                        <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                        <animPaused dataType="Bool">false</animPaused>
                        <animTime dataType="Float">0</animTime>
                        <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                          <A dataType="Byte">255</A>
                          <B dataType="Byte">255</B>
                          <G dataType="Byte">255</G>
                          <R dataType="Byte">255</R>
                        </colorTint>
                        <customFrameSequence />
                        <customMat />
                        <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                        <gameobj dataType="ObjectRef">787527030</gameobj>
                        <offset dataType="Int">0</offset>
                        <pixelGrid dataType="Bool">false</pixelGrid>
                        <rect dataType="Struct" type="Duality.Rect">
                          <H dataType="Float">26</H>
                          <W dataType="Float">24</W>
                          <X dataType="Float">-12</X>
                          <Y dataType="Float">-13</Y>
                        </rect>
                        <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                        <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                          <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                        </sharedMat>
                        <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                      </animSpriteRenderer>
                      <currentAnimationName />
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                    </animationManager>
                    <firingDelayCounter dataType="Float">0</firingDelayCounter>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <gravity dataType="Float">444.444458</gravity>
                    <heldWeapon dataType="ObjectRef">301082255</heldWeapon>
                    <jumpVelocity dataType="Float">200</jumpVelocity>
                    <rigidBody />
                    <spriteRenderer dataType="ObjectRef">494962411</spriteRenderer>
                    <velocity dataType="Struct" type="Duality.Vector2" />
                  </item>
                  <item dataType="Struct" type="Behavior.EntityStats" id="1425810874">
                    <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">35</_x003C_CurrentHealth_x003E_k__BackingField>
                    <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
                    <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">35</_x003C_MaxHealth_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationManager dataType="ObjectRef">3558738014</animationManager>
                    <damageEffectLength dataType="Float">0.1</damageEffectLength>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                    </material>
                    <soundManager />
                    <timer dataType="Struct" type="Misc.Timer" id="856554177">
                      <_x003C_LengthOfCycle_x003E_k__BackingField dataType="Float">0.1</_x003C_LengthOfCycle_x003E_k__BackingField>
                      <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                      <timerStart dataType="Bool">false</timerStart>
                    </timer>
                  </item>
                  <item dataType="ObjectRef">494962411</item>
                  <item dataType="ObjectRef">3558738014</item>
                  <item dataType="Struct" type="Manager.WeaponSpawner" id="1877854119">
                    <active dataType="Bool">true</active>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <heldWeaponPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                    </heldWeaponPrefab>
                  </item>
                  <item dataType="Struct" type="Behavior.Controller2D" id="3211926186">
                    <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                    <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="Player" value="0" />
                    <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
                    <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
                    <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">14.4</X>
                      <Y dataType="Float">10.12</Y>
                    </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
                    <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">7.4</X>
                      <Y dataType="Float">10</Y>
                    </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
                    <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <bounds dataType="Struct" type="Duality.Rect">
                      <H dataType="Float">15.88</H>
                      <W dataType="Float">9.6</W>
                      <X dataType="Float">-730.8707</X>
                      <Y dataType="Float">-238.351654</Y>
                    </bounds>
                    <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
                    <enemyController />
                    <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <horizontalRaySpacing dataType="Float">7.925</horizontalRaySpacing>
                    <maxClimbAngle dataType="Float">60</maxClimbAngle>
                    <playerController dataType="ObjectRef">1575548244</playerController>
                    <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
                    <rigidBody />
                    <verticalRaySpacing dataType="Float">4.78500032</verticalRaySpacing>
                  </item>
                  <item dataType="ObjectRef">856554177</item>
                </_items>
                <_size dataType="Int">8</_size>
                <_version dataType="Int">24</_version>
              </compList>
              <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1959540330" surrogate="true">
                <header />
                <body>
                  <keys dataType="Array" type="System.Object[]" id="219337152">
                    <item dataType="ObjectRef">2577056032</item>
                    <item dataType="ObjectRef">3855268626</item>
                    <item dataType="Type" id="3822107420" value="Player.PlayerController" />
                    <item dataType="Type" id="2970369558" value="Behavior.EntityStats" />
                    <item dataType="ObjectRef">1043132302</item>
                    <item dataType="Type" id="215607432" value="Manager.WeaponSpawner" />
                    <item dataType="Type" id="4019301298" value="Behavior.Controller2D" />
                    <item dataType="Type" id="1087601076" value="Misc.Timer" />
                  </keys>
                  <values dataType="Array" type="System.Object[]" id="899667022">
                    <item dataType="ObjectRef">3147841962</item>
                    <item dataType="ObjectRef">494962411</item>
                    <item dataType="ObjectRef">1575548244</item>
                    <item dataType="ObjectRef">1425810874</item>
                    <item dataType="ObjectRef">3558738014</item>
                    <item dataType="ObjectRef">1877854119</item>
                    <item dataType="ObjectRef">3211926186</item>
                    <item dataType="ObjectRef">856554177</item>
                  </values>
                </body>
              </compMap>
              <compTransform dataType="ObjectRef">3147841962</compTransform>
              <identifier dataType="Struct" type="System.Guid" surrogate="true">
                <header>
                  <data dataType="Array" type="System.Byte[]" id="43797852">UPF8jIwOT0SjSKyQzVYuaQ==</data>
                </header>
                <body />
              </identifier>
              <initState dataType="Enum" type="Duality.InitState" name="Disposed" value="3" />
              <name dataType="String">Player</name>
              <parent />
              <prefabLink />
            </player>
            <playerAnimRenderer dataType="ObjectRef">494962411</playerAnimRenderer>
            <playerController dataType="ObjectRef">1575548244</playerController>
            <prefabToBeFired dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\Weapons\FlamethrowerParticleOld.Prefab.res</contentPath>
            </prefabToBeFired>
            <soundManager />
            <transform dataType="ObjectRef">3033127152</transform>
            <transparentWeaponTex dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]" />
            <weaponAnimationManager dataType="ObjectRef">3444023204</weaponAnimationManager>
            <weaponDatabase dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Weapon]]" id="1221722350">
              <_items dataType="Array" type="Misc.Weapon[]" id="4208000866" length="4">
                <item dataType="Struct" type="Misc.Weapon" id="450825104">
                  <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">27</_x003C_BottomLeft_x003E_k__BackingField>
                  <_x003C_BottomRight_x003E_k__BackingField dataType="Float">12</_x003C_BottomRight_x003E_k__BackingField>
                  <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                  <_x003C_ID_x003E_k__BackingField dataType="Int">0</_x003C_ID_x003E_k__BackingField>
                  <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">3</_x003C_Inaccuracy_x003E_k__BackingField>
                  <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                  <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                  <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">3</_x003C_RateOfFire_x003E_k__BackingField>
                  <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                  <_x003C_Slug_x003E_k__BackingField dataType="String">peastol</_x003C_Slug_x003E_k__BackingField>
                  <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\peastol.Texture.res</contentPath>
                  </_x003C_Sprite_x003E_k__BackingField>
                  <_x003C_Title_x003E_k__BackingField dataType="String">Peastol</_x003C_Title_x003E_k__BackingField>
                  <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-13.5</_x003C_TopLeft_x003E_k__BackingField>
                  <_x003C_TopRight_x003E_k__BackingField dataType="Float">-6</_x003C_TopRight_x003E_k__BackingField>
                  <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                  <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                  <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
                </item>
                <item dataType="Struct" type="Misc.Weapon" id="621411054">
                  <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">33</_x003C_BottomLeft_x003E_k__BackingField>
                  <_x003C_BottomRight_x003E_k__BackingField dataType="Float">18</_x003C_BottomRight_x003E_k__BackingField>
                  <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                  <_x003C_ID_x003E_k__BackingField dataType="Int">1</_x003C_ID_x003E_k__BackingField>
                  <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">5</_x003C_Inaccuracy_x003E_k__BackingField>
                  <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">17.9</_x003C_PrefabXOffset_x003E_k__BackingField>
                  <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                  <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">5</_x003C_RateOfFire_x003E_k__BackingField>
                  <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Heavy" value="2" />
                  <_x003C_Slug_x003E_k__BackingField dataType="String">lemonade_launcher</_x003C_Slug_x003E_k__BackingField>
                  <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\lemonade_launcher.Texture.res</contentPath>
                  </_x003C_Sprite_x003E_k__BackingField>
                  <_x003C_Title_x003E_k__BackingField dataType="String">Lemonade Launcher</_x003C_Title_x003E_k__BackingField>
                  <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-16.5</_x003C_TopLeft_x003E_k__BackingField>
                  <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9</_x003C_TopRight_x003E_k__BackingField>
                  <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Lemonade" value="1" />
                  <_x003C_XOffset_x003E_k__BackingField dataType="Float">8.45</_x003C_XOffset_x003E_k__BackingField>
                  <_x003C_YOffset_x003E_k__BackingField dataType="Float">2</_x003C_YOffset_x003E_k__BackingField>
                </item>
                <item dataType="ObjectRef">70536464</item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </weaponDatabase>
            <weaponDatabaseObject dataType="Struct" type="Duality.GameObject" id="2834260959">
              <active dataType="Bool">true</active>
              <children />
              <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="4218840495">
                <_items dataType="Array" type="Duality.Component[]" id="754514926" length="4">
                  <item dataType="Struct" type="Manager.WeaponDatabaseManager" id="3611639126">
                    <_x003C_weaponDatabase_x003E_k__BackingField dataType="ObjectRef">1221722350</_x003C_weaponDatabase_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <gameobj dataType="ObjectRef">2834260959</gameobj>
                  </item>
                </_items>
                <_size dataType="Int">1</_size>
                <_version dataType="Int">11</_version>
              </compList>
              <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="789097632" surrogate="true">
                <header />
                <body>
                  <keys dataType="Array" type="System.Object[]" id="3696278149">
                    <item dataType="Type" id="3583574870" value="Manager.WeaponDatabaseManager" />
                  </keys>
                  <values dataType="Array" type="System.Object[]" id="3377770664">
                    <item dataType="ObjectRef">3611639126</item>
                  </values>
                </body>
              </compMap>
              <compTransform />
              <identifier dataType="Struct" type="System.Guid" surrogate="true">
                <header>
                  <data dataType="Array" type="System.Byte[]" id="900741007">biwph8Z5kk+28lYMuemNIw==</data>
                </header>
                <body />
              </identifier>
              <initState dataType="Enum" type="Duality.InitState" name="Disposed" value="3" />
              <name dataType="String">WorldManagers</name>
              <parent />
              <prefabLink />
            </weaponDatabaseObject>
          </heldWeapon>
          <lifetimeCounter dataType="Float">0</lifetimeCounter>
          <player dataType="ObjectRef">787527030</player>
          <playerController dataType="ObjectRef">1575548244</playerController>
          <prefabDamage dataType="Int">1</prefabDamage>
          <prefabLifetime dataType="Float">0.6</prefabLifetime>
          <rigidBody dataType="ObjectRef">3324870964</rigidBody>
          <shapeInfo dataType="ObjectRef">3505785540</shapeInfo>
          <stats />
          <transform dataType="ObjectRef">2622409372</transform>
        </item>
      </_items>
      <_size dataType="Int">5</_size>
      <_version dataType="Int">5</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="611690336" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="3370268853">
          <item dataType="ObjectRef">2577056032</item>
          <item dataType="ObjectRef">3855268626</item>
          <item dataType="ObjectRef">1043132302</item>
          <item dataType="Type" id="3199108854" value="Duality.Components.Physics.RigidBody" />
          <item dataType="Type" id="1649519130" value="Behavior.PepperFlameParticle" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="4241908552">
          <item dataType="ObjectRef">2622409372</item>
          <item dataType="ObjectRef">4264497117</item>
          <item dataType="ObjectRef">3033305424</item>
          <item dataType="ObjectRef">3324870964</item>
          <item dataType="ObjectRef">259482577</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">2622409372</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="3411919871">E3yogzf+0UKQQ3KP2iIcqg==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">FlamethrowerParticle</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
