﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="2090108664">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2869479407">
      <_items dataType="Array" type="Duality.Component[]" id="3187516654">
        <item dataType="Struct" type="Duality.Components.Transform" id="155456300">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">2090108664</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">-6.6</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">-6.6</Z>
          </posAbs>
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1797544045">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">0.4</animDuration>
          <animFirstFrame dataType="Int">0</animFirstFrame>
          <animFrameCount dataType="Int">4</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
          <animPaused dataType="Bool">false</animPaused>
          <animTime dataType="Float">0.139033318</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence />
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">2090108664</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">30</H>
            <W dataType="Float">30</W>
            <X dataType="Float">-15</X>
            <Y dataType="Float">-15</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Projectiles\Pea_Hit.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="Manager.AnimationManager" id="566352352">
          <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">peaHit</_x003C_animatedObjectName_x003E_k__BackingField>
          <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="4254238508">
            <_items dataType="Array" type="Misc.Animation[]" id="778578660" length="4">
              <item dataType="Struct" type="Misc.Animation" id="2128304068">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">4</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.4</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">hit</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
              </item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">1</_version>
          </_x003C_animationDatabase_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <animationDuration dataType="Float">0</animationDuration>
          <animationToReturnTo />
          <animSpriteRenderer dataType="ObjectRef">1797544045</animSpriteRenderer>
          <currentAnimationName dataType="String">hit</currentAnimationName>
          <currentTime dataType="Float">0</currentTime>
          <gameobj dataType="ObjectRef">2090108664</gameobj>
        </item>
        <item dataType="Struct" type="Behavior.PeaHit" id="3782035477">
          <active dataType="Bool">true</active>
          <animationManager dataType="ObjectRef">566352352</animationManager>
          <checkForDelete dataType="Bool">true</checkForDelete>
          <deleteDelay dataType="Float">250</deleteDelay>
          <deleteDelayCounter dataType="Float">0</deleteDelayCounter>
          <gameobj dataType="ObjectRef">2090108664</gameobj>
          <targetDeleteTime dataType="Float">250</targetDeleteTime>
        </item>
      </_items>
      <_size dataType="Int">4</_size>
      <_version dataType="Int">4</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="684816800" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="856763973">
          <item dataType="Type" id="711008470" value="Duality.Components.Transform" />
          <item dataType="Type" id="2331028442" value="Duality.Components.Renderers.AnimSpriteRenderer" />
          <item dataType="Type" id="1291151862" value="Manager.AnimationManager" />
          <item dataType="Type" id="1721623674" value="Behavior.PeaHit" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="3260339240">
          <item dataType="ObjectRef">155456300</item>
          <item dataType="ObjectRef">1797544045</item>
          <item dataType="ObjectRef">566352352</item>
          <item dataType="ObjectRef">3782035477</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">155456300</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="546123727">WM5YvcwfgkiRw97jEizsIg==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Pea_Hit</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
