﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="15345171">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="948577312">
      <_items dataType="Array" type="Duality.Component[]" id="1179550684" length="16">
        <item dataType="Struct" type="Duality.Components.Transform" id="2375660103">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">15345171</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">85</X>
            <Y dataType="Float">-34</Y>
            <Z dataType="Float">0</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">85</X>
            <Y dataType="Float">-34</Y>
            <Z dataType="Float">0</Z>
          </posAbs>
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="4017747848">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">5</animDuration>
          <animFirstFrame dataType="Int">0</animFirstFrame>
          <animFrameCount dataType="Int">51</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
          <animPaused dataType="Bool">false</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence />
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">15345171</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">39</H>
            <W dataType="Float">39</W>
            <X dataType="Float">-19.5</X>
            <Y dataType="Float">-19.5</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="Manager.AnimationManager" id="2786556155">
          <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">greenAppowl</_x003C_animatedObjectName_x003E_k__BackingField>
          <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="8294231">
            <_items dataType="Array" type="Misc.Animation[]" id="676662286" length="8">
              <item dataType="Struct" type="Misc.Animation" id="2934891472">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">14</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1.12</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
              </item>
              <item dataType="Struct" type="Misc.Animation" id="3945976430">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">startRun</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
              </item>
              <item dataType="Struct" type="Misc.Animation" id="688815020">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">9</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.72</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
              </item>
              <item dataType="Struct" type="Misc.Animation" id="611291666">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">12</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.96</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">death</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">34</_x003C_StartFrame_x003E_k__BackingField>
              </item>
              <item dataType="Struct" type="Misc.Animation" id="4019359112">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">10</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">deathStill</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">45</_x003C_StartFrame_x003E_k__BackingField>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </_x003C_animationDatabase_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <animationDuration dataType="Float">0</animationDuration>
          <animationToReturnTo />
          <animSpriteRenderer dataType="ObjectRef">4017747848</animSpriteRenderer>
          <currentAnimationName />
          <currentTime dataType="Float">0</currentTime>
          <gameobj dataType="ObjectRef">15345171</gameobj>
        </item>
        <item dataType="Struct" type="Enemy.EnemyController" id="2268790195">
          <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
          <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
          <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">4</X>
            <Y dataType="Float">0.1</Y>
            <Z dataType="Float">-0.1</Z>
          </_x003C_BulletSpawnOffset_x003E_k__BackingField>
          <_x003C_EnemyAnimationState_x003E_k__BackingField dataType="Enum" type="Enemy.EnemyAnimationState" name="Idle" value="0" />
          <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
          <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">10</_x003C_FiringDelay_x003E_k__BackingField>
          <_x003C_HorizontalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.HorizontalMovement" name="None" value="2" />
          <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
          <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
          <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
          <_x003C_VerticalMovement_x003E_k__BackingField dataType="Enum" type="Enemy.VerticalMovement" name="None" value="2" />
          <active dataType="Bool">true</active>
          <animationManager dataType="ObjectRef">2786556155</animationManager>
          <checkLeftMovement dataType="Bool">false</checkLeftMovement>
          <checkRightMovement dataType="Bool">false</checkRightMovement>
          <currentXPosition dataType="Float">0</currentXPosition>
          <entityStats dataType="Struct" type="Behavior.EntityStats" id="653629015">
            <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">50</_x003C_CurrentHealth_x003E_k__BackingField>
            <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
            <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">50</_x003C_MaxHealth_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <animationManager dataType="ObjectRef">2786556155</animationManager>
            <currentTime dataType="Float">0</currentTime>
            <damageEffectLength dataType="Float">0.1</damageEffectLength>
            <gameobj dataType="ObjectRef">15345171</gameobj>
            <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Enemies\Enemy_GreenAppowl.Material.res</contentPath>
            </material>
            <soundManager dataType="Struct" type="Manager.SoundManager" id="243598982">
              <active dataType="Bool">true</active>
              <eightiesTrack dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                <contentPath dataType="String">Data\Sound\80s.Sound.res</contentPath>
              </eightiesTrack>
              <gameobj dataType="Struct" type="Duality.GameObject" id="2834260959">
                <active dataType="Bool">true</active>
                <children />
                <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3653415615">
                  <_items dataType="Array" type="Duality.Component[]" id="2882939310" length="4">
                    <item dataType="Struct" type="Manager.WeaponDatabaseManager" id="3611639126">
                      <_x003C_weaponDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Weapon]]" id="107933034">
                        <_items dataType="Array" type="Misc.Weapon[]" id="1804743200" length="4">
                          <item dataType="Struct" type="Misc.Weapon" id="4035113948">
                            <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">27</_x003C_BottomLeft_x003E_k__BackingField>
                            <_x003C_BottomRight_x003E_k__BackingField dataType="Float">12</_x003C_BottomRight_x003E_k__BackingField>
                            <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                            <_x003C_ID_x003E_k__BackingField dataType="Int">0</_x003C_ID_x003E_k__BackingField>
                            <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">3</_x003C_Inaccuracy_x003E_k__BackingField>
                            <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                            <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                            <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">3</_x003C_RateOfFire_x003E_k__BackingField>
                            <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                            <_x003C_Slug_x003E_k__BackingField dataType="String">peastol</_x003C_Slug_x003E_k__BackingField>
                            <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\peastol.Texture.res</contentPath>
                            </_x003C_Sprite_x003E_k__BackingField>
                            <_x003C_Title_x003E_k__BackingField dataType="String">Peastol</_x003C_Title_x003E_k__BackingField>
                            <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-13.5</_x003C_TopLeft_x003E_k__BackingField>
                            <_x003C_TopRight_x003E_k__BackingField dataType="Float">-6</_x003C_TopRight_x003E_k__BackingField>
                            <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                            <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                            <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Weapon" id="4113668374">
                            <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">33</_x003C_BottomLeft_x003E_k__BackingField>
                            <_x003C_BottomRight_x003E_k__BackingField dataType="Float">18</_x003C_BottomRight_x003E_k__BackingField>
                            <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                            <_x003C_ID_x003E_k__BackingField dataType="Int">1</_x003C_ID_x003E_k__BackingField>
                            <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">5</_x003C_Inaccuracy_x003E_k__BackingField>
                            <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">17.9</_x003C_PrefabXOffset_x003E_k__BackingField>
                            <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                            <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">5</_x003C_RateOfFire_x003E_k__BackingField>
                            <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Heavy" value="2" />
                            <_x003C_Slug_x003E_k__BackingField dataType="String">lemonade_launcher</_x003C_Slug_x003E_k__BackingField>
                            <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\lemonade_launcher.Texture.res</contentPath>
                            </_x003C_Sprite_x003E_k__BackingField>
                            <_x003C_Title_x003E_k__BackingField dataType="String">Lemonade Launcher</_x003C_Title_x003E_k__BackingField>
                            <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-16.5</_x003C_TopLeft_x003E_k__BackingField>
                            <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9</_x003C_TopRight_x003E_k__BackingField>
                            <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Lemonade" value="1" />
                            <_x003C_XOffset_x003E_k__BackingField dataType="Float">8.45</_x003C_XOffset_x003E_k__BackingField>
                            <_x003C_YOffset_x003E_k__BackingField dataType="Float">2</_x003C_YOffset_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Weapon" id="3640833352">
                            <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">29</_x003C_BottomLeft_x003E_k__BackingField>
                            <_x003C_BottomRight_x003E_k__BackingField dataType="Float">19</_x003C_BottomRight_x003E_k__BackingField>
                            <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                            <_x003C_ID_x003E_k__BackingField dataType="Int">2</_x003C_ID_x003E_k__BackingField>
                            <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">0</_x003C_Inaccuracy_x003E_k__BackingField>
                            <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                            <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">2.8</_x003C_PrefabYOffset_x003E_k__BackingField>
                            <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">0.5</_x003C_RateOfFire_x003E_k__BackingField>
                            <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                            <_x003C_Slug_x003E_k__BackingField dataType="String">pepper_thrower</_x003C_Slug_x003E_k__BackingField>
                            <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\pepper_thrower.Texture.res</contentPath>
                            </_x003C_Sprite_x003E_k__BackingField>
                            <_x003C_Title_x003E_k__BackingField dataType="String">Pepper Thrower</_x003C_Title_x003E_k__BackingField>
                            <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-14.5</_x003C_TopLeft_x003E_k__BackingField>
                            <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9.5</_x003C_TopRight_x003E_k__BackingField>
                            <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pepper" value="2" />
                            <_x003C_XOffset_x003E_k__BackingField dataType="Float">14.5</_x003C_XOffset_x003E_k__BackingField>
                            <_x003C_YOffset_x003E_k__BackingField dataType="Float">2.4</_x003C_YOffset_x003E_k__BackingField>
                          </item>
                        </_items>
                        <_size dataType="Int">3</_size>
                        <_version dataType="Int">3</_version>
                      </_x003C_weaponDatabase_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <gameobj dataType="ObjectRef">2834260959</gameobj>
                    </item>
                    <item dataType="ObjectRef">243598982</item>
                  </_items>
                  <_size dataType="Int">2</_size>
                  <_version dataType="Int">12</_version>
                </compList>
                <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1585322464" surrogate="true">
                  <header />
                  <body>
                    <keys dataType="Array" type="System.Object[]" id="1668136309">
                      <item dataType="Type" id="3820735094" value="Manager.WeaponDatabaseManager" />
                      <item dataType="Type" id="448506138" value="Manager.SoundManager" />
                    </keys>
                    <values dataType="Array" type="System.Object[]" id="2514059464">
                      <item dataType="ObjectRef">3611639126</item>
                      <item dataType="ObjectRef">243598982</item>
                    </values>
                  </body>
                </compMap>
                <compTransform />
                <identifier dataType="Struct" type="System.Guid" surrogate="true">
                  <header>
                    <data dataType="Array" type="System.Byte[]" id="1420799935">biwph8Z5kk+28lYMuemNIw==</data>
                  </header>
                  <body />
                </identifier>
                <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                <name dataType="String">WorldManagers</name>
                <parent />
                <prefabLink />
              </gameobj>
              <happy dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                <contentPath dataType="String">Data\Sound\Happy.Sound.res</contentPath>
              </happy>
              <hurt dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                <contentPath dataType="String">Data\Sound\Hurt.Sound.res</contentPath>
              </hurt>
              <lemonade dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                <contentPath dataType="String">Data\Sound\LemonLauncher1.Sound.res</contentPath>
              </lemonade>
              <lemonLauncher dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                <contentPath dataType="String">Data\Sound\LemonlauncherShoot1.Sound.res</contentPath>
              </lemonLauncher>
              <peastol dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                <contentPath dataType="String">Data\Sound\Peastol.Sound.res</contentPath>
              </peastol>
              <pop dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                <contentPath dataType="String">Data\Sound\PopMaybe.Sound.res</contentPath>
              </pop>
            </soundManager>
            <timer dataType="Struct" type="Misc.Timer" id="84372318">
              <_x003C_LengthOfCycle_x003E_k__BackingField dataType="Float">0.1</_x003C_LengthOfCycle_x003E_k__BackingField>
              <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
              <active dataType="Bool">true</active>
              <currentTime dataType="Float">0</currentTime>
              <gameobj dataType="ObjectRef">15345171</gameobj>
              <timerStart dataType="Bool">false</timerStart>
            </timer>
          </entityStats>
          <firingDelayCounter dataType="Float">0</firingDelayCounter>
          <gameobj dataType="ObjectRef">15345171</gameobj>
          <gravity dataType="Float">444.444458</gravity>
          <heldWeapon dataType="Struct" type="Behavior.HeldWeapon" id="301082255">
            <_x003C_CurrentWeapon_x003E_k__BackingField />
            <_x003C_prefabSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">17.9</X>
              <Y dataType="Float">-0.6</Y>
              <Z dataType="Float">0.3</Z>
            </_x003C_prefabSpawnOffset_x003E_k__BackingField>
            <_x003C_weaponOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
              <X dataType="Float">8.45</X>
              <Y dataType="Float">2</Y>
            </_x003C_weaponOffset_x003E_k__BackingField>
            <active dataType="Bool">true</active>
            <animationState dataType="Enum" type="Behavior.WeaponAnimationState" name="Idle" value="0" />
            <firingDelay dataType="Float">500</firingDelay>
            <firingDelayCounter dataType="Float">0</firingDelayCounter>
            <gameobj dataType="Struct" type="Duality.GameObject" id="672812220">
              <active dataType="Bool">true</active>
              <children />
              <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1806813658">
                <_items dataType="Array" type="Duality.Component[]" id="2293641472">
                  <item dataType="Struct" type="Duality.Components.Transform" id="3033127152">
                    <active dataType="Bool">true</active>
                    <angle dataType="Float">0</angle>
                    <angleAbs dataType="Float">0</angleAbs>
                    <angleVel dataType="Float">0</angleVel>
                    <angleVelAbs dataType="Float">0</angleVelAbs>
                    <deriveAngle dataType="Bool">true</deriveAngle>
                    <gameobj dataType="ObjectRef">672812220</gameobj>
                    <ignoreParent dataType="Bool">false</ignoreParent>
                    <parentTransform />
                    <pos dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">0</X>
                      <Y dataType="Float">0</Y>
                      <Z dataType="Float">-0.8</Z>
                    </pos>
                    <posAbs dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">0</X>
                      <Y dataType="Float">0</Y>
                      <Z dataType="Float">-0.8</Z>
                    </posAbs>
                    <scale dataType="Float">1</scale>
                    <scaleAbs dataType="Float">1</scaleAbs>
                    <vel dataType="Struct" type="Duality.Vector3" />
                    <velAbs dataType="Struct" type="Duality.Vector3" />
                  </item>
                  <item dataType="Struct" type="Manager.AnimationManager" id="3444023204">
                    <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">lemonade_launcher</_x003C_animatedObjectName_x003E_k__BackingField>
                    <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="1023175788">
                      <_items dataType="Array" type="Misc.Animation[]" id="2193069924" length="4">
                        <item dataType="Struct" type="Misc.Animation" id="1582973892">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                        <item dataType="Struct" type="Misc.Animation" id="4003267990">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">2</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.2</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">fire</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">2</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                        <item dataType="Struct" type="Misc.Animation" id="3068900992">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">playerMoving</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                      </_items>
                      <_size dataType="Int">3</_size>
                      <_version dataType="Int">3</_version>
                    </_x003C_animationDatabase_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationDuration dataType="Float">0</animationDuration>
                    <animationToReturnTo />
                    <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="380247601">
                      <active dataType="Bool">true</active>
                      <animDuration dataType="Float">5</animDuration>
                      <animFirstFrame dataType="Int">0</animFirstFrame>
                      <animFrameCount dataType="Int">0</animFrameCount>
                      <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                      <animPaused dataType="Bool">false</animPaused>
                      <animTime dataType="Float">0</animTime>
                      <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                        <A dataType="Byte">255</A>
                        <B dataType="Byte">255</B>
                        <G dataType="Byte">255</G>
                        <R dataType="Byte">255</R>
                      </colorTint>
                      <customFrameSequence />
                      <customMat />
                      <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                      <gameobj dataType="ObjectRef">672812220</gameobj>
                      <offset dataType="Int">0</offset>
                      <pixelGrid dataType="Bool">false</pixelGrid>
                      <rect dataType="Struct" type="Duality.Rect">
                        <H dataType="Float">18</H>
                        <W dataType="Float">33</W>
                        <X dataType="Float">-16.5</X>
                        <Y dataType="Float">-9</Y>
                      </rect>
                      <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                      <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                        <contentPath dataType="String">Data\Sprites &amp; Spritesheets\heldWeapon.Material.res</contentPath>
                      </sharedMat>
                      <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                    </animSpriteRenderer>
                    <currentAnimationName />
                    <currentTime dataType="Float">0</currentTime>
                    <gameobj dataType="ObjectRef">672812220</gameobj>
                  </item>
                  <item dataType="ObjectRef">301082255</item>
                  <item dataType="ObjectRef">380247601</item>
                </_items>
                <_size dataType="Int">4</_size>
                <_version dataType="Int">4</_version>
              </compList>
              <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="15255226" surrogate="true">
                <header />
                <body>
                  <keys dataType="Array" type="System.Object[]" id="836075552">
                    <item dataType="Type" id="3998833628" value="Duality.Components.Transform" />
                    <item dataType="Type" id="2824972566" value="Manager.AnimationManager" />
                    <item dataType="Type" id="374336840" value="Behavior.HeldWeapon" />
                    <item dataType="Type" id="2665464242" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                  </keys>
                  <values dataType="Array" type="System.Object[]" id="2502583182">
                    <item dataType="ObjectRef">3033127152</item>
                    <item dataType="ObjectRef">3444023204</item>
                    <item dataType="ObjectRef">301082255</item>
                    <item dataType="ObjectRef">380247601</item>
                  </values>
                </body>
              </compMap>
              <compTransform dataType="ObjectRef">3033127152</compTransform>
              <identifier dataType="Struct" type="System.Guid" surrogate="true">
                <header>
                  <data dataType="Array" type="System.Byte[]" id="3926010684">ww7ZFsN1MUKIHkuvVynjTg==</data>
                </header>
                <body />
              </identifier>
              <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
              <name dataType="String">Weapon</name>
              <parent />
              <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2457239258">
                <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2124883456">
                  <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1286077596" length="4">
                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3783400648">
                        <_items dataType="Array" type="System.Int32[]" id="1985753708"></_items>
                        <_size dataType="Int">0</_size>
                        <_version dataType="Int">1</_version>
                      </childIndex>
                      <componentType dataType="ObjectRef">3998833628</componentType>
                      <prop dataType="MemberInfo" id="985422558" value="P:Duality.Components.Transform:RelativePos" />
                      <val dataType="Struct" type="Duality.Vector3">
                        <X dataType="Float">0</X>
                        <Y dataType="Float">0</Y>
                        <Z dataType="Float">-1</Z>
                      </val>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">393</_version>
                </changes>
                <obj dataType="ObjectRef">672812220</obj>
                <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                  <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                </prefab>
              </prefabLink>
            </gameobj>
            <heldWeaponAnimRenderer dataType="ObjectRef">380247601</heldWeaponAnimRenderer>
            <hydroLaserPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
            <idleAnimationDelayCounter dataType="Float">0</idleAnimationDelayCounter>
            <lemonadePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\Weapons\Lemonade.Prefab.res</contentPath>
            </lemonadePrefab>
            <lemonadeSpeed dataType="Int">4</lemonadeSpeed>
            <peaPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\Weapons\Pea.Prefab.res</contentPath>
            </peaPrefab>
            <peaSpeed dataType="Int">8</peaSpeed>
            <pepperFlamePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\Weapons\FlamethrowerParticle.Prefab.res</contentPath>
            </pepperFlamePrefab>
            <pepperSpeed dataType="Int">5</pepperSpeed>
            <player dataType="Struct" type="Duality.GameObject" id="787527030">
              <active dataType="Bool">true</active>
              <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="338670200">
                <_items dataType="Array" type="Duality.GameObject[]" id="1540409708" length="4" />
                <_size dataType="Int">0</_size>
                <_version dataType="Int">2</_version>
              </children>
              <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="553765342">
                <_items dataType="Array" type="Duality.Component[]" id="1096519354" length="16">
                  <item dataType="Struct" type="Duality.Components.Transform" id="3147841962">
                    <active dataType="Bool">true</active>
                    <angle dataType="Float">0</angle>
                    <angleAbs dataType="Float">0</angleAbs>
                    <angleVel dataType="Float">0</angleVel>
                    <angleVelAbs dataType="Float">0</angleVelAbs>
                    <deriveAngle dataType="Bool">true</deriveAngle>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <ignoreParent dataType="Bool">false</ignoreParent>
                    <parentTransform />
                    <pos dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">-685</X>
                      <Y dataType="Float">-241</Y>
                      <Z dataType="Float">-1</Z>
                    </pos>
                    <posAbs dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">-685</X>
                      <Y dataType="Float">-241</Y>
                      <Z dataType="Float">-1</Z>
                    </posAbs>
                    <scale dataType="Float">1</scale>
                    <scaleAbs dataType="Float">1</scaleAbs>
                    <vel dataType="Struct" type="Duality.Vector3" />
                    <velAbs dataType="Struct" type="Duality.Vector3" />
                  </item>
                  <item dataType="Struct" type="Player.PlayerController" id="1575548244">
                    <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
                    <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
                    <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">4</X>
                      <Y dataType="Float">0.1</Y>
                      <Z dataType="Float">-0.1</Z>
                    </_x003C_BulletSpawnOffset_x003E_k__BackingField>
                    <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Player.FacingDirection" name="Left" value="0" />
                    <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">300</_x003C_FiringDelay_x003E_k__BackingField>
                    <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
                    <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
                    <_x003C_PlayerAnimationState_x003E_k__BackingField dataType="Enum" type="Player.PlayerAnimationState" name="Idle" value="0" />
                    <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationManager dataType="Struct" type="Manager.AnimationManager" id="3558738014">
                      <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">LemonMan</_x003C_animatedObjectName_x003E_k__BackingField>
                      <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="2566329426">
                        <_items dataType="Array" type="Misc.Animation[]" id="774805840">
                          <item dataType="Struct" type="Misc.Animation" id="3669679036">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">6</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="2822499990">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">4</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.5</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">7</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="3966763368">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">jump</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                          <item dataType="Struct" type="Misc.Animation" id="4239207282">
                            <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                            <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                            <_x003C_AnimationName_x003E_k__BackingField dataType="String">fall</_x003C_AnimationName_x003E_k__BackingField>
                            <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                          </item>
                        </_items>
                        <_size dataType="Int">4</_size>
                        <_version dataType="Int">4</_version>
                      </_x003C_animationDatabase_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <animationDuration dataType="Float">0</animationDuration>
                      <animationToReturnTo />
                      <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="494962411">
                        <active dataType="Bool">true</active>
                        <animDuration dataType="Float">2</animDuration>
                        <animFirstFrame dataType="Int">6</animFirstFrame>
                        <animFrameCount dataType="Int">4</animFrameCount>
                        <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                        <animPaused dataType="Bool">false</animPaused>
                        <animTime dataType="Float">0</animTime>
                        <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                          <A dataType="Byte">255</A>
                          <B dataType="Byte">255</B>
                          <G dataType="Byte">255</G>
                          <R dataType="Byte">255</R>
                        </colorTint>
                        <customFrameSequence />
                        <customMat />
                        <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                        <gameobj dataType="ObjectRef">787527030</gameobj>
                        <offset dataType="Int">0</offset>
                        <pixelGrid dataType="Bool">false</pixelGrid>
                        <rect dataType="Struct" type="Duality.Rect">
                          <H dataType="Float">26</H>
                          <W dataType="Float">24</W>
                          <X dataType="Float">-12</X>
                          <Y dataType="Float">-13</Y>
                        </rect>
                        <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                        <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                          <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                        </sharedMat>
                        <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                      </animSpriteRenderer>
                      <currentAnimationName />
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                    </animationManager>
                    <firingDelayCounter dataType="Float">0</firingDelayCounter>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <gravity dataType="Float">444.444458</gravity>
                    <heldWeapon dataType="ObjectRef">301082255</heldWeapon>
                    <jumpVelocity dataType="Float">200</jumpVelocity>
                    <rigidBody dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3850303554">
                      <active dataType="Bool">true</active>
                      <angularDamp dataType="Float">0.3</angularDamp>
                      <angularVel dataType="Float">0</angularVel>
                      <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                      <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1, Cat2" value="3" />
                      <colFilter />
                      <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
                      <continous dataType="Bool">false</continous>
                      <explicitInertia dataType="Float">0</explicitInertia>
                      <explicitMass dataType="Float">0</explicitMass>
                      <fixedAngle dataType="Bool">true</fixedAngle>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                      <ignoreGravity dataType="Bool">true</ignoreGravity>
                      <joints />
                      <linearDamp dataType="Float">0.3</linearDamp>
                      <linearVel dataType="Struct" type="Duality.Vector2" />
                      <revolutions dataType="Float">0</revolutions>
                      <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="1928631462">
                        <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="1132016128" length="4">
                          <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2309164188">
                            <density dataType="Float">1</density>
                            <friction dataType="Float">0.3</friction>
                            <parent dataType="ObjectRef">3850303554</parent>
                            <restitution dataType="Float">0.3</restitution>
                            <sensor dataType="Bool">false</sensor>
                            <vertices dataType="Array" type="Duality.Vector2[]" id="2618602436">
                              <item dataType="Struct" type="Duality.Vector2">
                                <X dataType="Float">-5</X>
                                <Y dataType="Float">-3</Y>
                              </item>
                              <item dataType="Struct" type="Duality.Vector2">
                                <X dataType="Float">5</X>
                                <Y dataType="Float">-3</Y>
                              </item>
                              <item dataType="Struct" type="Duality.Vector2">
                                <X dataType="Float">5</X>
                                <Y dataType="Float">9</Y>
                              </item>
                              <item dataType="Struct" type="Duality.Vector2">
                                <X dataType="Float">-5</X>
                                <Y dataType="Float">9</Y>
                              </item>
                            </vertices>
                          </item>
                        </_items>
                        <_size dataType="Int">1</_size>
                        <_version dataType="Int">11</_version>
                      </shapes>
                    </rigidBody>
                    <spriteRenderer dataType="ObjectRef">494962411</spriteRenderer>
                    <velocity dataType="Struct" type="Duality.Vector2" />
                  </item>
                  <item dataType="Struct" type="Behavior.EntityStats" id="1425810874">
                    <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">35</_x003C_CurrentHealth_x003E_k__BackingField>
                    <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
                    <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">35</_x003C_MaxHealth_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationManager dataType="ObjectRef">3558738014</animationManager>
                    <currentTime dataType="Float">0</currentTime>
                    <damageEffectLength dataType="Float">0.1</damageEffectLength>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                    </material>
                    <soundManager dataType="ObjectRef">243598982</soundManager>
                    <timer dataType="Struct" type="Misc.Timer" id="856554177">
                      <_x003C_LengthOfCycle_x003E_k__BackingField dataType="Float">0.1</_x003C_LengthOfCycle_x003E_k__BackingField>
                      <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                      <active dataType="Bool">true</active>
                      <currentTime dataType="Float">0</currentTime>
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                      <timerStart dataType="Bool">false</timerStart>
                    </timer>
                  </item>
                  <item dataType="ObjectRef">494962411</item>
                  <item dataType="ObjectRef">3558738014</item>
                  <item dataType="Struct" type="Manager.WeaponSpawner" id="1877854119">
                    <active dataType="Bool">true</active>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <heldWeaponPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                    </heldWeaponPrefab>
                  </item>
                  <item dataType="Struct" type="Behavior.Controller2D" id="3211926186">
                    <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                    <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="Player" value="0" />
                    <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
                    <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
                    <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">14.4</X>
                      <Y dataType="Float">10.12</Y>
                    </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
                    <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">7.4</X>
                      <Y dataType="Float">10</Y>
                    </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
                    <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <bounds dataType="Struct" type="Duality.Rect">
                      <H dataType="Float">15.88</H>
                      <W dataType="Float">9.6</W>
                      <X dataType="Float">-689.6</X>
                      <Y dataType="Float">-244</Y>
                    </bounds>
                    <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
                    <enemyController />
                    <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <horizontalRaySpacing dataType="Float">7.925</horizontalRaySpacing>
                    <maxClimbAngle dataType="Float">60</maxClimbAngle>
                    <playerController dataType="ObjectRef">1575548244</playerController>
                    <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
                    <rigidBody dataType="ObjectRef">3850303554</rigidBody>
                    <verticalRaySpacing dataType="Float">4.78500032</verticalRaySpacing>
                  </item>
                  <item dataType="ObjectRef">856554177</item>
                  <item dataType="ObjectRef">3850303554</item>
                </_items>
                <_size dataType="Int">9</_size>
                <_version dataType="Int">29</_version>
              </compList>
              <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1477164836" surrogate="true">
                <header />
                <body>
                  <keys dataType="Array" type="System.Object[]" id="1240092072">
                    <item dataType="ObjectRef">3998833628</item>
                    <item dataType="ObjectRef">2665464242</item>
                    <item dataType="Type" id="3309438636" value="Player.PlayerController" />
                    <item dataType="Type" id="302731702" value="Behavior.EntityStats" />
                    <item dataType="ObjectRef">2824972566</item>
                    <item dataType="Type" id="2617258232" value="Manager.WeaponSpawner" />
                    <item dataType="Type" id="1397245330" value="Behavior.Controller2D" />
                    <item dataType="Type" id="857336036" value="Misc.Timer" />
                    <item dataType="Type" id="70970638" value="Duality.Components.Physics.RigidBody" />
                  </keys>
                  <values dataType="Array" type="System.Object[]" id="2721372062">
                    <item dataType="ObjectRef">3147841962</item>
                    <item dataType="ObjectRef">494962411</item>
                    <item dataType="ObjectRef">1575548244</item>
                    <item dataType="ObjectRef">1425810874</item>
                    <item dataType="ObjectRef">3558738014</item>
                    <item dataType="ObjectRef">1877854119</item>
                    <item dataType="ObjectRef">3211926186</item>
                    <item dataType="ObjectRef">856554177</item>
                    <item dataType="ObjectRef">3850303554</item>
                  </values>
                </body>
              </compMap>
              <compTransform dataType="ObjectRef">3147841962</compTransform>
              <identifier dataType="Struct" type="System.Guid" surrogate="true">
                <header>
                  <data dataType="Array" type="System.Byte[]" id="1397108628">UPF8jIwOT0SjSKyQzVYuaQ==</data>
                </header>
                <body />
              </identifier>
              <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
              <name dataType="String">Player</name>
              <parent />
              <prefabLink />
            </player>
            <playerAnimRenderer dataType="ObjectRef">494962411</playerAnimRenderer>
            <playerController dataType="ObjectRef">1575548244</playerController>
            <prefabToBeFired dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\Weapons\Lemonade.Prefab.res</contentPath>
            </prefabToBeFired>
            <soundManager dataType="ObjectRef">243598982</soundManager>
            <transform dataType="ObjectRef">3033127152</transform>
            <transparentWeaponTex dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
              <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Misc\TransparentTex.Texture.res</contentPath>
            </transparentWeaponTex>
            <weaponAnimationManager dataType="ObjectRef">3444023204</weaponAnimationManager>
            <weaponDatabase dataType="ObjectRef">107933034</weaponDatabase>
            <weaponDatabaseObject dataType="ObjectRef">2834260959</weaponDatabaseObject>
          </heldWeapon>
          <jumpVelocity dataType="Float">200</jumpVelocity>
          <shapeInfo />
          <spriteRenderer dataType="ObjectRef">4017747848</spriteRenderer>
          <targetXPosition dataType="Float">0</targetXPosition>
          <velocity dataType="Struct" type="Duality.Vector2" />
        </item>
        <item dataType="Struct" type="Behavior.Controller2D" id="2439744327">
          <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
          <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="GreenAppowl" value="1" />
          <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
          <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
          <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
            <X dataType="Float">14.4</X>
            <Y dataType="Float">17</Y>
          </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
          <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
            <X dataType="Float">7.4</X>
            <Y dataType="Float">10</Y>
          </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
          <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <bounds dataType="Struct" type="Duality.Rect">
            <H dataType="Float">22</H>
            <W dataType="Float">24.6</W>
            <X dataType="Float">72.9</X>
            <Y dataType="Float">-43.5</Y>
          </bounds>
          <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
          <enemyController dataType="ObjectRef">2268790195</enemyController>
          <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
          <gameobj dataType="ObjectRef">15345171</gameobj>
          <horizontalRaySpacing dataType="Float">10.985</horizontalRaySpacing>
          <maxClimbAngle dataType="Float">60</maxClimbAngle>
          <playerController />
          <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
          <rigidBody dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3078121695">
            <active dataType="Bool">true</active>
            <angularDamp dataType="Float">0.3</angularDamp>
            <angularVel dataType="Float">0</angularVel>
            <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Static" value="0" />
            <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
            <colFilter />
            <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
            <continous dataType="Bool">false</continous>
            <explicitInertia dataType="Float">0</explicitInertia>
            <explicitMass dataType="Float">0</explicitMass>
            <fixedAngle dataType="Bool">true</fixedAngle>
            <gameobj dataType="ObjectRef">15345171</gameobj>
            <ignoreGravity dataType="Bool">true</ignoreGravity>
            <joints />
            <linearDamp dataType="Float">0.3</linearDamp>
            <linearVel dataType="Struct" type="Duality.Vector2" />
            <revolutions dataType="Float">0</revolutions>
            <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3204766506">
              <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="1690333472">
                <item dataType="Struct" type="Duality.Components.Physics.CircleShapeInfo" id="1187829724">
                  <density dataType="Float">1</density>
                  <friction dataType="Float">0.3</friction>
                  <parent dataType="ObjectRef">3078121695</parent>
                  <position dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-1</X>
                    <Y dataType="Float">2</Y>
                  </position>
                  <radius dataType="Float">8.944272</radius>
                  <restitution dataType="Float">0.3</restitution>
                  <sensor dataType="Bool">false</sensor>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">8</_version>
            </shapes>
          </rigidBody>
          <verticalRaySpacing dataType="Float">12.285</verticalRaySpacing>
        </item>
        <item dataType="ObjectRef">653629015</item>
        <item dataType="ObjectRef">3078121695</item>
        <item dataType="ObjectRef">84372318</item>
        <item dataType="Struct" type="Enemy.GreenAppowl" id="894606163">
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">15345171</gameobj>
          <stats />
        </item>
      </_items>
      <_size dataType="Int">9</_size>
      <_version dataType="Int">9</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1427843982" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="2207844594">
          <item dataType="ObjectRef">3998833628</item>
          <item dataType="ObjectRef">2665464242</item>
          <item dataType="ObjectRef">2824972566</item>
          <item dataType="Type" id="1393898960" value="Enemy.EnemyController" />
          <item dataType="ObjectRef">1397245330</item>
          <item dataType="ObjectRef">302731702</item>
          <item dataType="ObjectRef">70970638</item>
          <item dataType="ObjectRef">857336036</item>
          <item dataType="Type" id="3431028334" value="Enemy.GreenAppowl" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="3445209418">
          <item dataType="ObjectRef">2375660103</item>
          <item dataType="ObjectRef">4017747848</item>
          <item dataType="ObjectRef">2786556155</item>
          <item dataType="ObjectRef">2268790195</item>
          <item dataType="ObjectRef">2439744327</item>
          <item dataType="ObjectRef">653629015</item>
          <item dataType="ObjectRef">3078121695</item>
          <item dataType="ObjectRef">84372318</item>
          <item dataType="ObjectRef">894606163</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">2375660103</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="3253160898">wWbUfD4muECzJ1r8Vl6Jag==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">GreenAppowl</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
