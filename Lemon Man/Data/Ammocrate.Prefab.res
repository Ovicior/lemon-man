﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="1863247237">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3592068902">
      <_items dataType="Array" type="Duality.Component[]" id="899122432" length="8">
        <item dataType="Struct" type="Duality.Components.Transform" id="4223562169">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">1863247237</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">-1</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">-1</Z>
          </posAbs>
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1570682618">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">0.8</animDuration>
          <animFirstFrame dataType="Int">0</animFirstFrame>
          <animFrameCount dataType="Int">1</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
          <animPaused dataType="Bool">false</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence />
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">1863247237</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">20</H>
            <W dataType="Float">30</W>
            <X dataType="Float">-15</X>
            <Y dataType="Float">-13</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Object_AmmoCrate.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="631056465">
          <active dataType="Bool">true</active>
          <angularDamp dataType="Float">0.3</angularDamp>
          <angularVel dataType="Float">0</angularVel>
          <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
          <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1, Cat2" value="3" />
          <colFilter />
          <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
          <continous dataType="Bool">false</continous>
          <explicitInertia dataType="Float">0</explicitInertia>
          <explicitMass dataType="Float">0</explicitMass>
          <fixedAngle dataType="Bool">false</fixedAngle>
          <gameobj dataType="ObjectRef">1863247237</gameobj>
          <ignoreGravity dataType="Bool">false</ignoreGravity>
          <joints />
          <linearDamp dataType="Float">0.3</linearDamp>
          <linearVel dataType="Struct" type="Duality.Vector2" />
          <revolutions dataType="Float">0</revolutions>
          <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="2514296021">
            <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3704979958">
              <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3873824480">
                <density dataType="Float">1</density>
                <friction dataType="Float">0.3</friction>
                <parent dataType="ObjectRef">631056465</parent>
                <restitution dataType="Float">0.3</restitution>
                <sensor dataType="Bool">false</sensor>
                <vertices dataType="Array" type="Duality.Vector2[]" id="2369320924">
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-15</X>
                    <Y dataType="Float">-6</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">15</X>
                    <Y dataType="Float">-6</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">15</X>
                    <Y dataType="Float">7</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-15</X>
                    <Y dataType="Float">7</Y>
                  </item>
                </vertices>
              </item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">6</_version>
          </shapes>
        </item>
        <item dataType="Struct" type="Manager.AnimationManager" id="339490925">
          <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">chest</_x003C_animatedObjectName_x003E_k__BackingField>
          <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="1115715865">
            <_items dataType="Array" type="Misc.Animation[]" id="2124751182" length="4">
              <item dataType="Struct" type="Misc.Animation" id="630584016">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">20</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">1.6</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">open</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
              </item>
              <item dataType="Struct" type="Misc.Animation" id="2243970670">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.4</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">opened</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
              </item>
              <item dataType="Struct" type="Misc.Animation" id="2769940652">
                <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
              </item>
            </_items>
            <_size dataType="Int">3</_size>
            <_version dataType="Int">3</_version>
          </_x003C_animationDatabase_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <animationDuration dataType="Float">0</animationDuration>
          <animationToReturnTo />
          <animSpriteRenderer dataType="ObjectRef">1570682618</animSpriteRenderer>
          <currentAnimationName dataType="String">idle</currentAnimationName>
          <currentTime dataType="Float">0</currentTime>
          <gameobj dataType="ObjectRef">1863247237</gameobj>
        </item>
        <item dataType="Struct" type="Behavior.WeaponChest" id="4003786800">
          <_x003C_weaponToGive_x003E_k__BackingField dataType="Enum" type="Behavior.WeaponChest+WeaponToGive" name="Peastol" value="0" />
          <active dataType="Bool">true</active>
          <alreadyOpened dataType="Bool">false</alreadyOpened>
          <animationManager dataType="ObjectRef">339490925</animationManager>
          <gameobj dataType="ObjectRef">1863247237</gameobj>
          <player dataType="Struct" type="Duality.GameObject" id="787527030">
            <active dataType="Bool">true</active>
            <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="1013530714">
              <_items dataType="Array" type="Duality.GameObject[]" id="2373223424" length="4" />
              <_size dataType="Int">0</_size>
              <_version dataType="Int">2</_version>
            </children>
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="253788602">
              <_items dataType="Array" type="Duality.Component[]" id="3627426720" length="16">
                <item dataType="Struct" type="Duality.Components.Transform" id="3147841962">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-685</X>
                    <Y dataType="Float">-241</Y>
                    <Z dataType="Float">-1</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-685</X>
                    <Y dataType="Float">-241</Y>
                    <Z dataType="Float">-1</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Player.PlayerController" id="1575548244">
                  <_x003C_AccelerationAirborne_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationAirborne_x003E_k__BackingField>
                  <_x003C_AccelerationGrounded_x003E_k__BackingField dataType="Float">100</_x003C_AccelerationGrounded_x003E_k__BackingField>
                  <_x003C_BulletSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">4</X>
                    <Y dataType="Float">0.1</Y>
                    <Z dataType="Float">-0.1</Z>
                  </_x003C_BulletSpawnOffset_x003E_k__BackingField>
                  <_x003C_FacingDirection_x003E_k__BackingField dataType="Enum" type="Player.FacingDirection" name="Left" value="0" />
                  <_x003C_FiringDelay_x003E_k__BackingField dataType="Float">300</_x003C_FiringDelay_x003E_k__BackingField>
                  <_x003C_JumpHeight_x003E_k__BackingField dataType="Float">45</_x003C_JumpHeight_x003E_k__BackingField>
                  <_x003C_MoveSpeed_x003E_k__BackingField dataType="Float">72</_x003C_MoveSpeed_x003E_k__BackingField>
                  <_x003C_PlayerAnimationState_x003E_k__BackingField dataType="Enum" type="Player.PlayerAnimationState" name="Idle" value="0" />
                  <_x003C_TimeToJumpApex_x003E_k__BackingField dataType="Float">0.45</_x003C_TimeToJumpApex_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <animationManager dataType="Struct" type="Manager.AnimationManager" id="3558738014">
                    <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">LemonMan</_x003C_animatedObjectName_x003E_k__BackingField>
                    <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="1389354846">
                      <_items dataType="Array" type="Misc.Animation[]" id="1683248912">
                        <item dataType="Struct" type="Misc.Animation" id="1646491452">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">6</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.8</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                        <item dataType="Struct" type="Misc.Animation" id="2738170774">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">4</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.5</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">run</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">7</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                        <item dataType="Struct" type="Misc.Animation" id="847503848">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">jump</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">19</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                        <item dataType="Struct" type="Misc.Animation" id="1594318962">
                          <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                          <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                          <_x003C_AnimationName_x003E_k__BackingField dataType="String">fall</_x003C_AnimationName_x003E_k__BackingField>
                          <_x003C_StartFrame_x003E_k__BackingField dataType="Int">20</_x003C_StartFrame_x003E_k__BackingField>
                        </item>
                      </_items>
                      <_size dataType="Int">4</_size>
                      <_version dataType="Int">4</_version>
                    </_x003C_animationDatabase_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationDuration dataType="Float">0</animationDuration>
                    <animationToReturnTo />
                    <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="494962411">
                      <active dataType="Bool">true</active>
                      <animDuration dataType="Float">2</animDuration>
                      <animFirstFrame dataType="Int">6</animFirstFrame>
                      <animFrameCount dataType="Int">4</animFrameCount>
                      <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                      <animPaused dataType="Bool">false</animPaused>
                      <animTime dataType="Float">0</animTime>
                      <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                        <A dataType="Byte">255</A>
                        <B dataType="Byte">255</B>
                        <G dataType="Byte">255</G>
                        <R dataType="Byte">255</R>
                      </colorTint>
                      <customFrameSequence />
                      <customMat />
                      <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                      <gameobj dataType="ObjectRef">787527030</gameobj>
                      <offset dataType="Int">0</offset>
                      <pixelGrid dataType="Bool">false</pixelGrid>
                      <rect dataType="Struct" type="Duality.Rect">
                        <H dataType="Float">26</H>
                        <W dataType="Float">24</W>
                        <X dataType="Float">-12</X>
                        <Y dataType="Float">-13</Y>
                      </rect>
                      <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                      <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                        <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                      </sharedMat>
                      <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                    </animSpriteRenderer>
                    <currentAnimationName />
                    <currentTime dataType="Float">0</currentTime>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                  </animationManager>
                  <firingDelayCounter dataType="Float">0</firingDelayCounter>
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <gravity dataType="Float">444.444458</gravity>
                  <heldWeapon dataType="Struct" type="Behavior.HeldWeapon" id="301082255">
                    <_x003C_CurrentWeapon_x003E_k__BackingField />
                    <_x003C_prefabSpawnOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                      <X dataType="Float">17.9</X>
                      <Y dataType="Float">-0.6</Y>
                      <Z dataType="Float">0.3</Z>
                    </_x003C_prefabSpawnOffset_x003E_k__BackingField>
                    <_x003C_weaponOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                      <X dataType="Float">8.45</X>
                      <Y dataType="Float">2</Y>
                    </_x003C_weaponOffset_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <animationState dataType="Enum" type="Behavior.WeaponAnimationState" name="Idle" value="0" />
                    <firingDelay dataType="Float">500</firingDelay>
                    <firingDelayCounter dataType="Float">0</firingDelayCounter>
                    <gameobj dataType="Struct" type="Duality.GameObject" id="672812220">
                      <active dataType="Bool">true</active>
                      <children />
                      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="407624485">
                        <_items dataType="Array" type="Duality.Component[]" id="782144150">
                          <item dataType="Struct" type="Duality.Components.Transform" id="3033127152">
                            <active dataType="Bool">true</active>
                            <angle dataType="Float">0</angle>
                            <angleAbs dataType="Float">0</angleAbs>
                            <angleVel dataType="Float">0</angleVel>
                            <angleVelAbs dataType="Float">0</angleVelAbs>
                            <deriveAngle dataType="Bool">true</deriveAngle>
                            <gameobj dataType="ObjectRef">672812220</gameobj>
                            <ignoreParent dataType="Bool">false</ignoreParent>
                            <parentTransform />
                            <pos dataType="Struct" type="Duality.Vector3">
                              <X dataType="Float">0</X>
                              <Y dataType="Float">0</Y>
                              <Z dataType="Float">-0.8</Z>
                            </pos>
                            <posAbs dataType="Struct" type="Duality.Vector3">
                              <X dataType="Float">0</X>
                              <Y dataType="Float">0</Y>
                              <Z dataType="Float">-0.8</Z>
                            </posAbs>
                            <scale dataType="Float">1</scale>
                            <scaleAbs dataType="Float">1</scaleAbs>
                            <vel dataType="Struct" type="Duality.Vector3" />
                            <velAbs dataType="Struct" type="Duality.Vector3" />
                          </item>
                          <item dataType="Struct" type="Manager.AnimationManager" id="3444023204">
                            <_x003C_animatedObjectName_x003E_k__BackingField dataType="String">lemonade_launcher</_x003C_animatedObjectName_x003E_k__BackingField>
                            <_x003C_animationDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Animation]]" id="642797216">
                              <_items dataType="Array" type="Misc.Animation[]" id="2238176476" length="4">
                                <item dataType="Struct" type="Misc.Animation" id="318651076">
                                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">idle</_x003C_AnimationName_x003E_k__BackingField>
                                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                                </item>
                                <item dataType="Struct" type="Misc.Animation" id="3899904918">
                                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">2</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.2</_x003C_AnimationDuration_x003E_k__BackingField>
                                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">fire</_x003C_AnimationName_x003E_k__BackingField>
                                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">2</_x003C_StartFrame_x003E_k__BackingField>
                                </item>
                                <item dataType="Struct" type="Misc.Animation" id="743225216">
                                  <_x003C_AmountOfAnimationFrames_x003E_k__BackingField dataType="Int">1</_x003C_AmountOfAnimationFrames_x003E_k__BackingField>
                                  <_x003C_AnimationDuration_x003E_k__BackingField dataType="Float">0.1</_x003C_AnimationDuration_x003E_k__BackingField>
                                  <_x003C_AnimationName_x003E_k__BackingField dataType="String">playerMoving</_x003C_AnimationName_x003E_k__BackingField>
                                  <_x003C_StartFrame_x003E_k__BackingField dataType="Int">1</_x003C_StartFrame_x003E_k__BackingField>
                                </item>
                              </_items>
                              <_size dataType="Int">3</_size>
                              <_version dataType="Int">3</_version>
                            </_x003C_animationDatabase_x003E_k__BackingField>
                            <active dataType="Bool">true</active>
                            <animationDuration dataType="Float">0</animationDuration>
                            <animationToReturnTo />
                            <animSpriteRenderer dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="380247601">
                              <active dataType="Bool">true</active>
                              <animDuration dataType="Float">5</animDuration>
                              <animFirstFrame dataType="Int">0</animFirstFrame>
                              <animFrameCount dataType="Int">0</animFrameCount>
                              <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                              <animPaused dataType="Bool">false</animPaused>
                              <animTime dataType="Float">0</animTime>
                              <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                                <A dataType="Byte">255</A>
                                <B dataType="Byte">255</B>
                                <G dataType="Byte">255</G>
                                <R dataType="Byte">255</R>
                              </colorTint>
                              <customFrameSequence />
                              <customMat />
                              <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                              <gameobj dataType="ObjectRef">672812220</gameobj>
                              <offset dataType="Int">0</offset>
                              <pixelGrid dataType="Bool">false</pixelGrid>
                              <rect dataType="Struct" type="Duality.Rect">
                                <H dataType="Float">18</H>
                                <W dataType="Float">33</W>
                                <X dataType="Float">-16.5</X>
                                <Y dataType="Float">-9</Y>
                              </rect>
                              <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                              <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                                <contentPath dataType="String">Data\Sprites &amp; Spritesheets\heldWeapon.Material.res</contentPath>
                              </sharedMat>
                              <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                            </animSpriteRenderer>
                            <currentAnimationName />
                            <currentTime dataType="Float">0</currentTime>
                            <gameobj dataType="ObjectRef">672812220</gameobj>
                          </item>
                          <item dataType="ObjectRef">301082255</item>
                          <item dataType="ObjectRef">380247601</item>
                        </_items>
                        <_size dataType="Int">4</_size>
                        <_version dataType="Int">4</_version>
                      </compList>
                      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3185702248" surrogate="true">
                        <header />
                        <body>
                          <keys dataType="Array" type="System.Object[]" id="1965905359">
                            <item dataType="Type" id="718568494" value="Duality.Components.Transform" />
                            <item dataType="Type" id="2001091786" value="Manager.AnimationManager" />
                            <item dataType="Type" id="1689783710" value="Behavior.HeldWeapon" />
                            <item dataType="Type" id="4236547290" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                          </keys>
                          <values dataType="Array" type="System.Object[]" id="2517633632">
                            <item dataType="ObjectRef">3033127152</item>
                            <item dataType="ObjectRef">3444023204</item>
                            <item dataType="ObjectRef">301082255</item>
                            <item dataType="ObjectRef">380247601</item>
                          </values>
                        </body>
                      </compMap>
                      <compTransform dataType="ObjectRef">3033127152</compTransform>
                      <identifier dataType="Struct" type="System.Guid" surrogate="true">
                        <header>
                          <data dataType="Array" type="System.Byte[]" id="2532087325">ww7ZFsN1MUKIHkuvVynjTg==</data>
                        </header>
                        <body />
                      </identifier>
                      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                      <name dataType="String">Weapon</name>
                      <parent />
                      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="635084271">
                        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="4117192932">
                          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="2406395844" length="4">
                            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4022387016">
                                <_items dataType="Array" type="System.Int32[]" id="826755180"></_items>
                                <_size dataType="Int">0</_size>
                                <_version dataType="Int">1</_version>
                              </childIndex>
                              <componentType dataType="ObjectRef">718568494</componentType>
                              <prop dataType="MemberInfo" id="1150215390" value="P:Duality.Components.Transform:RelativePos" />
                              <val dataType="Struct" type="Duality.Vector3">
                                <X dataType="Float">0</X>
                                <Y dataType="Float">0</Y>
                                <Z dataType="Float">-1</Z>
                              </val>
                            </item>
                          </_items>
                          <_size dataType="Int">1</_size>
                          <_version dataType="Int">393</_version>
                        </changes>
                        <obj dataType="ObjectRef">672812220</obj>
                        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                          <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                        </prefab>
                      </prefabLink>
                    </gameobj>
                    <heldWeaponAnimRenderer dataType="ObjectRef">380247601</heldWeaponAnimRenderer>
                    <hydroLaserPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                    <idleAnimationDelayCounter dataType="Float">0</idleAnimationDelayCounter>
                    <lemonadePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Weapons\Lemonade.Prefab.res</contentPath>
                    </lemonadePrefab>
                    <lemonadeSpeed dataType="Int">4</lemonadeSpeed>
                    <peaPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Weapons\Pea.Prefab.res</contentPath>
                    </peaPrefab>
                    <peaSpeed dataType="Int">8</peaSpeed>
                    <pepperFlamePrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Weapons\FlamethrowerParticle.Prefab.res</contentPath>
                    </pepperFlamePrefab>
                    <pepperSpeed dataType="Int">5</pepperSpeed>
                    <player dataType="ObjectRef">787527030</player>
                    <playerAnimRenderer dataType="ObjectRef">494962411</playerAnimRenderer>
                    <playerController dataType="ObjectRef">1575548244</playerController>
                    <prefabToBeFired dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Prefabs\Weapons\Lemonade.Prefab.res</contentPath>
                    </prefabToBeFired>
                    <soundManager dataType="Struct" type="Manager.SoundManager" id="243598982">
                      <active dataType="Bool">true</active>
                      <eightiesTrack dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\80s.Sound.res</contentPath>
                      </eightiesTrack>
                      <gameobj dataType="Struct" type="Duality.GameObject" id="2834260959">
                        <active dataType="Bool">true</active>
                        <children />
                        <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3404464222">
                          <_items dataType="Array" type="Duality.Component[]" id="1891845392" length="4">
                            <item dataType="Struct" type="Manager.WeaponDatabaseManager" id="3611639126">
                              <_x003C_weaponDatabase_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Misc.Weapon]]" id="4164971350">
                                <_items dataType="Array" type="Misc.Weapon[]" id="2818696736" length="4">
                                  <item dataType="Struct" type="Misc.Weapon" id="1290599388">
                                    <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">27</_x003C_BottomLeft_x003E_k__BackingField>
                                    <_x003C_BottomRight_x003E_k__BackingField dataType="Float">12</_x003C_BottomRight_x003E_k__BackingField>
                                    <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                                    <_x003C_ID_x003E_k__BackingField dataType="Int">0</_x003C_ID_x003E_k__BackingField>
                                    <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">3</_x003C_Inaccuracy_x003E_k__BackingField>
                                    <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                                    <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                                    <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">3</_x003C_RateOfFire_x003E_k__BackingField>
                                    <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                                    <_x003C_Slug_x003E_k__BackingField dataType="String">peastol</_x003C_Slug_x003E_k__BackingField>
                                    <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\peastol.Texture.res</contentPath>
                                    </_x003C_Sprite_x003E_k__BackingField>
                                    <_x003C_Title_x003E_k__BackingField dataType="String">Peastol</_x003C_Title_x003E_k__BackingField>
                                    <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-13.5</_x003C_TopLeft_x003E_k__BackingField>
                                    <_x003C_TopRight_x003E_k__BackingField dataType="Float">-6</_x003C_TopRight_x003E_k__BackingField>
                                    <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pea" value="0" />
                                    <_x003C_XOffset_x003E_k__BackingField dataType="Float">10.5</_x003C_XOffset_x003E_k__BackingField>
                                    <_x003C_YOffset_x003E_k__BackingField dataType="Float">3.2</_x003C_YOffset_x003E_k__BackingField>
                                  </item>
                                  <item dataType="Struct" type="Misc.Weapon" id="3522980118">
                                    <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">33</_x003C_BottomLeft_x003E_k__BackingField>
                                    <_x003C_BottomRight_x003E_k__BackingField dataType="Float">18</_x003C_BottomRight_x003E_k__BackingField>
                                    <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                                    <_x003C_ID_x003E_k__BackingField dataType="Int">1</_x003C_ID_x003E_k__BackingField>
                                    <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">5</_x003C_Inaccuracy_x003E_k__BackingField>
                                    <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">17.9</_x003C_PrefabXOffset_x003E_k__BackingField>
                                    <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">-0.6</_x003C_PrefabYOffset_x003E_k__BackingField>
                                    <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">5</_x003C_RateOfFire_x003E_k__BackingField>
                                    <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Heavy" value="2" />
                                    <_x003C_Slug_x003E_k__BackingField dataType="String">lemonade_launcher</_x003C_Slug_x003E_k__BackingField>
                                    <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\lemonade_launcher.Texture.res</contentPath>
                                    </_x003C_Sprite_x003E_k__BackingField>
                                    <_x003C_Title_x003E_k__BackingField dataType="String">Lemonade Launcher</_x003C_Title_x003E_k__BackingField>
                                    <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-16.5</_x003C_TopLeft_x003E_k__BackingField>
                                    <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9</_x003C_TopRight_x003E_k__BackingField>
                                    <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Lemonade" value="1" />
                                    <_x003C_XOffset_x003E_k__BackingField dataType="Float">8.45</_x003C_XOffset_x003E_k__BackingField>
                                    <_x003C_YOffset_x003E_k__BackingField dataType="Float">2</_x003C_YOffset_x003E_k__BackingField>
                                  </item>
                                  <item dataType="Struct" type="Misc.Weapon" id="2598391112">
                                    <_x003C_BottomLeft_x003E_k__BackingField dataType="Float">29</_x003C_BottomLeft_x003E_k__BackingField>
                                    <_x003C_BottomRight_x003E_k__BackingField dataType="Float">19</_x003C_BottomRight_x003E_k__BackingField>
                                    <_x003C_BurstCount_x003E_k__BackingField dataType="Int">1</_x003C_BurstCount_x003E_k__BackingField>
                                    <_x003C_ID_x003E_k__BackingField dataType="Int">2</_x003C_ID_x003E_k__BackingField>
                                    <_x003C_Inaccuracy_x003E_k__BackingField dataType="Float">0</_x003C_Inaccuracy_x003E_k__BackingField>
                                    <_x003C_PrefabXOffset_x003E_k__BackingField dataType="Float">8</_x003C_PrefabXOffset_x003E_k__BackingField>
                                    <_x003C_PrefabYOffset_x003E_k__BackingField dataType="Float">2.8</_x003C_PrefabYOffset_x003E_k__BackingField>
                                    <_x003C_RateOfFire_x003E_k__BackingField dataType="Float">0.5</_x003C_RateOfFire_x003E_k__BackingField>
                                    <_x003C_RecoilAmount_x003E_k__BackingField dataType="Enum" type="Misc.RecoilAmount" name="Light" value="0" />
                                    <_x003C_Slug_x003E_k__BackingField dataType="String">pepper_thrower</_x003C_Slug_x003E_k__BackingField>
                                    <_x003C_Sprite_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Weapons\pepper_thrower.Texture.res</contentPath>
                                    </_x003C_Sprite_x003E_k__BackingField>
                                    <_x003C_Title_x003E_k__BackingField dataType="String">Pepper Thrower</_x003C_Title_x003E_k__BackingField>
                                    <_x003C_TopLeft_x003E_k__BackingField dataType="Float">-14.5</_x003C_TopLeft_x003E_k__BackingField>
                                    <_x003C_TopRight_x003E_k__BackingField dataType="Float">-9.5</_x003C_TopRight_x003E_k__BackingField>
                                    <_x003C_TypeOfProjectile_x003E_k__BackingField dataType="Enum" type="Misc.ProjectileType" name="Pepper" value="2" />
                                    <_x003C_XOffset_x003E_k__BackingField dataType="Float">14.5</_x003C_XOffset_x003E_k__BackingField>
                                    <_x003C_YOffset_x003E_k__BackingField dataType="Float">2.4</_x003C_YOffset_x003E_k__BackingField>
                                  </item>
                                </_items>
                                <_size dataType="Int">3</_size>
                                <_version dataType="Int">3</_version>
                              </_x003C_weaponDatabase_x003E_k__BackingField>
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">2834260959</gameobj>
                            </item>
                            <item dataType="ObjectRef">243598982</item>
                          </_items>
                          <_size dataType="Int">2</_size>
                          <_version dataType="Int">12</_version>
                        </compList>
                        <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2380823818" surrogate="true">
                          <header />
                          <body>
                            <keys dataType="Array" type="System.Object[]" id="3196493692">
                              <item dataType="Type" id="1525914692" value="Manager.WeaponDatabaseManager" />
                              <item dataType="Type" id="390930070" value="Manager.SoundManager" />
                            </keys>
                            <values dataType="Array" type="System.Object[]" id="332054678">
                              <item dataType="ObjectRef">3611639126</item>
                              <item dataType="ObjectRef">243598982</item>
                            </values>
                          </body>
                        </compMap>
                        <compTransform />
                        <identifier dataType="Struct" type="System.Guid" surrogate="true">
                          <header>
                            <data dataType="Array" type="System.Byte[]" id="2122395432">biwph8Z5kk+28lYMuemNIw==</data>
                          </header>
                          <body />
                        </identifier>
                        <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                        <name dataType="String">WorldManagers</name>
                        <parent />
                        <prefabLink />
                      </gameobj>
                      <happy dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\Happy.Sound.res</contentPath>
                      </happy>
                      <hurt dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\Hurt.Sound.res</contentPath>
                      </hurt>
                      <lemonade dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\LemonLauncher1.Sound.res</contentPath>
                      </lemonade>
                      <lemonLauncher dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\LemonlauncherShoot1.Sound.res</contentPath>
                      </lemonLauncher>
                      <peastol dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\Peastol.Sound.res</contentPath>
                      </peastol>
                      <pop dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                        <contentPath dataType="String">Data\Sound\PopMaybe.Sound.res</contentPath>
                      </pop>
                    </soundManager>
                    <transform dataType="ObjectRef">3033127152</transform>
                    <transparentWeaponTex dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Texture]]">
                      <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Misc\TransparentTex.Texture.res</contentPath>
                    </transparentWeaponTex>
                    <weaponAnimationManager dataType="ObjectRef">3444023204</weaponAnimationManager>
                    <weaponDatabase dataType="ObjectRef">4164971350</weaponDatabase>
                    <weaponDatabaseObject dataType="ObjectRef">2834260959</weaponDatabaseObject>
                  </heldWeapon>
                  <jumpVelocity dataType="Float">200</jumpVelocity>
                  <rigidBody dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3850303554">
                    <active dataType="Bool">true</active>
                    <angularDamp dataType="Float">0.3</angularDamp>
                    <angularVel dataType="Float">0</angularVel>
                    <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                    <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1, Cat2" value="3" />
                    <colFilter />
                    <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat2" value="2" />
                    <continous dataType="Bool">false</continous>
                    <explicitInertia dataType="Float">0</explicitInertia>
                    <explicitMass dataType="Float">0</explicitMass>
                    <fixedAngle dataType="Bool">true</fixedAngle>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <ignoreGravity dataType="Bool">true</ignoreGravity>
                    <joints />
                    <linearDamp dataType="Float">0.3</linearDamp>
                    <linearVel dataType="Struct" type="Duality.Vector2" />
                    <revolutions dataType="Float">0</revolutions>
                    <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="2258298906">
                      <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2395629952" length="4">
                        <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="877952412">
                          <density dataType="Float">1</density>
                          <friction dataType="Float">0.3</friction>
                          <parent dataType="ObjectRef">3850303554</parent>
                          <restitution dataType="Float">0.3</restitution>
                          <sensor dataType="Bool">false</sensor>
                          <vertices dataType="Array" type="Duality.Vector2[]" id="2303475140">
                            <item dataType="Struct" type="Duality.Vector2">
                              <X dataType="Float">-5</X>
                              <Y dataType="Float">-4</Y>
                            </item>
                            <item dataType="Struct" type="Duality.Vector2">
                              <X dataType="Float">4</X>
                              <Y dataType="Float">-4</Y>
                            </item>
                            <item dataType="Struct" type="Duality.Vector2">
                              <X dataType="Float">5</X>
                              <Y dataType="Float">10</Y>
                            </item>
                            <item dataType="Struct" type="Duality.Vector2">
                              <X dataType="Float">-4</X>
                              <Y dataType="Float">10</Y>
                            </item>
                          </vertices>
                        </item>
                      </_items>
                      <_size dataType="Int">1</_size>
                      <_version dataType="Int">5</_version>
                    </shapes>
                  </rigidBody>
                  <spriteRenderer dataType="ObjectRef">494962411</spriteRenderer>
                  <velocity dataType="Struct" type="Duality.Vector2" />
                </item>
                <item dataType="Struct" type="Behavior.EntityStats" id="1425810874">
                  <_x003C_CurrentHealth_x003E_k__BackingField dataType="Int">35</_x003C_CurrentHealth_x003E_k__BackingField>
                  <_x003C_Dead_x003E_k__BackingField dataType="Bool">false</_x003C_Dead_x003E_k__BackingField>
                  <_x003C_MaxHealth_x003E_k__BackingField dataType="Int">35</_x003C_MaxHealth_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <animationManager dataType="ObjectRef">3558738014</animationManager>
                  <damageEffectLength dataType="Float">0.1</damageEffectLength>
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <material dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Sprites &amp; Spritesheets\Player\Player_Lemonman.Material.res</contentPath>
                  </material>
                  <soundManager dataType="ObjectRef">243598982</soundManager>
                  <timer dataType="Struct" type="Misc.Timer" id="856554177">
                    <_x003C_LengthOfCycle_x003E_k__BackingField dataType="Float">0.1</_x003C_LengthOfCycle_x003E_k__BackingField>
                    <_x003C_TimeReached_x003E_k__BackingField dataType="Bool">false</_x003C_TimeReached_x003E_k__BackingField>
                    <active dataType="Bool">true</active>
                    <currentTime dataType="Float">0</currentTime>
                    <gameobj dataType="ObjectRef">787527030</gameobj>
                    <timerStart dataType="Bool">false</timerStart>
                  </timer>
                </item>
                <item dataType="ObjectRef">494962411</item>
                <item dataType="ObjectRef">3558738014</item>
                <item dataType="Struct" type="Manager.WeaponSpawner" id="1877854119">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <heldWeaponPrefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                    <contentPath dataType="String">Data\Prefabs\Weapons\Weapon.Prefab.res</contentPath>
                  </heldWeaponPrefab>
                </item>
                <item dataType="Struct" type="Behavior.Controller2D" id="3211926186">
                  <_x003C_CollidesWith_x003E_k__BackingField dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                  <_x003C_entityType_x003E_k__BackingField dataType="Enum" type="Behavior.Controller2D+EntityType" name="Player" value="0" />
                  <_x003C_HorizontalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_HorizontalRayCount_x003E_k__BackingField>
                  <_x003C_SkinWidth_x003E_k__BackingField dataType="Float">0.015</_x003C_SkinWidth_x003E_k__BackingField>
                  <_x003C_SpriteBoundsOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">14.4</X>
                    <Y dataType="Float">10.12</Y>
                  </_x003C_SpriteBoundsOffset_x003E_k__BackingField>
                  <_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">7.4</X>
                    <Y dataType="Float">10</Y>
                  </_x003C_SpriteBoundsPositionOffset_x003E_k__BackingField>
                  <_x003C_VerticalRayCount_x003E_k__BackingField dataType="Int">3</_x003C_VerticalRayCount_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <bounds dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">15.88</H>
                    <W dataType="Float">9.6</W>
                    <X dataType="Float">-689.6</X>
                    <Y dataType="Float">-244</Y>
                  </bounds>
                  <collisions dataType="Struct" type="Behavior.Controller2D+CollisionInfo" />
                  <enemyController />
                  <facingDirection dataType="Enum" type="Enemy.FacingDirection" name="Left" value="0" />
                  <gameobj dataType="ObjectRef">787527030</gameobj>
                  <horizontalRaySpacing dataType="Float">7.925</horizontalRaySpacing>
                  <maxClimbAngle dataType="Float">60</maxClimbAngle>
                  <playerController dataType="ObjectRef">1575548244</playerController>
                  <raycastOrigins dataType="Struct" type="Behavior.Controller2D+RayCastOrigins" />
                  <rigidBody dataType="ObjectRef">3850303554</rigidBody>
                  <verticalRaySpacing dataType="Float">4.78500032</verticalRaySpacing>
                </item>
                <item dataType="ObjectRef">856554177</item>
                <item dataType="ObjectRef">3850303554</item>
              </_items>
              <_size dataType="Int">9</_size>
              <_version dataType="Int">29</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="403012698" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="492939264">
                  <item dataType="ObjectRef">718568494</item>
                  <item dataType="ObjectRef">4236547290</item>
                  <item dataType="Type" id="1835772060" value="Player.PlayerController" />
                  <item dataType="Type" id="812775958" value="Behavior.EntityStats" />
                  <item dataType="ObjectRef">2001091786</item>
                  <item dataType="Type" id="847386376" value="Manager.WeaponSpawner" />
                  <item dataType="Type" id="397470642" value="Behavior.Controller2D" />
                  <item dataType="Type" id="2629419828" value="Misc.Timer" />
                  <item dataType="Type" id="4143874446" value="Duality.Components.Physics.RigidBody" />
                </keys>
                <values dataType="Array" type="System.Object[]" id="3820219342">
                  <item dataType="ObjectRef">3147841962</item>
                  <item dataType="ObjectRef">494962411</item>
                  <item dataType="ObjectRef">1575548244</item>
                  <item dataType="ObjectRef">1425810874</item>
                  <item dataType="ObjectRef">3558738014</item>
                  <item dataType="ObjectRef">1877854119</item>
                  <item dataType="ObjectRef">3211926186</item>
                  <item dataType="ObjectRef">856554177</item>
                  <item dataType="ObjectRef">3850303554</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">3147841962</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="2302300316">UPF8jIwOT0SjSKyQzVYuaQ==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Player</name>
            <parent />
            <prefabLink />
          </player>
          <playerWeapon dataType="ObjectRef">301082255</playerWeapon>
        </item>
      </_items>
      <_size dataType="Int">5</_size>
      <_version dataType="Int">5</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3114986170" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="3634178196">
          <item dataType="ObjectRef">718568494</item>
          <item dataType="ObjectRef">4236547290</item>
          <item dataType="ObjectRef">4143874446</item>
          <item dataType="ObjectRef">2001091786</item>
          <item dataType="Type" id="3220268900" value="Behavior.WeaponChest" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="204162102">
          <item dataType="ObjectRef">4223562169</item>
          <item dataType="ObjectRef">1570682618</item>
          <item dataType="ObjectRef">631056465</item>
          <item dataType="ObjectRef">339490925</item>
          <item dataType="ObjectRef">4003786800</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">4223562169</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="1304752432">DKU5zzfg+ECd90C/nc+vcQ==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Ammocrate</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
