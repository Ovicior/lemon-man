[Core] Msg: Running DualityLauncher with flags: 
[Core] Msg: Using 'DefaultPluginLoader' to load plugins.
[Core] Msg: Environment Info: 
              Current Directory: D:\Documents\Game Development\Repositories\lemon-man\Lemon Man\Lemon Man
              Command Line: "D:\Documents\Game Development\Repositories\lemon-man\Lemon Man\Lemon Man\DualityLauncher.exe" 
              Operating System: Microsoft Windows NT 6.2.9200.0 (Windows 8)
              64 Bit OS: True
              64 Bit Process: True
              CLR Version: 4.0.30319.42000
              Processor Count: 8
[Core] Msg: Currently Loaded Assemblies:
              mscorlib 4.0.0.0
              DualityLauncher 2.3.1.0
              System.Core 4.0.0.0
              System 4.0.0.0
              Duality 2.9.3.0
              System.Runtime 4.0.0.0
              System.IO 4.0.0.0
              System.Reflection 4.0.0.0
              DualityPrimitives 2.0.3.0
              System.Collections 4.0.0.0
              System.Linq 4.0.0.0
              System.Runtime.Extensions 4.0.0.0
              System.Globalization 4.0.0.0
              System.Diagnostics.Debug 4.0.0.0
[Core] Msg: Plugin Base Directories:
              Plugins
[Core] Msg: Available Assembly Paths:
              Plugins\CamView.editor.dll
              Plugins\Compatibility.core.dll
              Plugins\DefaultOpenTKBackend.core.dll
              Plugins\DefaultOpenTKBackend.editor.dll
              Plugins\DotNetFrameworkBackend.core.dll
              Plugins\EditorBase.editor.dll
              Plugins\GamePlugin.core.dll
              Plugins\HelpAdvisor.editor.dll
              Plugins\LogView.editor.dll
              Plugins\Newtonsoft.Json.dll
              Plugins\ObjectInspector.editor.dll
              Plugins\OpenALSoft32.dll
              Plugins\OpenALSoft64.dll
              Plugins\PackageManagerFrontend.editor.dll
              Plugins\ProjectView.editor.dll
              Plugins\SceneView.editor.dll
              Plugins\SnowyPeak.Duality.Data.core.dll
              Plugins\SnowyPeak.Duality.Data.editor.dll
              Plugins\Tilemaps.core.dll
              Plugins\Tilemaps.editor.dll
[Core] Msg: Assembly loaded: System.Threading 4.0.0.0
[Core] Msg: Scanning for core plugins...
[Core] Msg:   Plugins\Compatibility.core.dll...
[Core] Msg:     Assembly loaded: Compatibility.core 2.0.2.0
[Core] Msg:     Assembly loaded: System.Linq.Expressions 4.0.0.0
[Core] Msg:     Assembly loaded: Anonymously Hosted DynamicMethods Assembly 0.0.0.0
[Core] Msg:   Plugins\DefaultOpenTKBackend.core.dll...
[Core] Msg:     Assembly loaded: DefaultOpenTKBackend.core 2.0.15.0
[Core] Msg:     Assembly loaded: OpenTK 1.1.6.0
[Core] Msg:   Plugins\DotNetFrameworkBackend.core.dll...
[Core] Msg:     Assembly loaded: DotNetFrameworkBackend.core 2.1.1.0
[Core] Msg:   Plugins\GamePlugin.core.dll...
[Core] Msg:     Assembly loaded: GamePlugin.core 1.0.0.0
[Core] Msg:   Plugins\SnowyPeak.Duality.Data.core.dll...
[Core] Msg:     Assembly loaded: SnowyPeak.Duality.Data.core 2.0.0.0
[Core] Msg:   Plugins\Tilemaps.core.dll...
[Core] Msg:     Assembly loaded: Tilemaps.core 1.1.11.0
[Core] Msg: Loading auxiliary libraries...
[Core] Msg:   Assembly loaded: Newtonsoft.Json 9.0.0.0
[Core] Msg: Initializing ISystemBackend...
[Core] Msg:   Assembly loaded: System.Text.RegularExpressions 4.0.0.0
[Core] Msg:   .Net Framework...
[Core] Msg: Assembly loaded: System.Xml.ReaderWriter 4.0.0.0
[Core] Msg: Assembly loaded: System.Xml 4.0.0.0
[Core] Msg: Assembly loaded: System.Xml.XDocument 4.0.0.0
[Core] Msg: Assembly loaded: System.Xml.Linq 4.0.0.0
[Core] Msg: Assembly loaded: System.Text.Encoding 4.0.0.0
[Core] Msg: Assembly loaded: System.Reflection.Extensions 4.0.0.0
[Core] Msg: Initializing IGraphicsBackend...
[Core] Msg:   OpenGL 2.1 (OpenTK)...
[Core] Msg:     Initializing OpenTK...
[Core] Msg:       Platform Backend: PreferNative
                  EnableHighResolution: True
[Core] Msg:     Assembly loaded: System.Drawing 4.0.0.0
[Core] Msg:     Available display devices:
[Core] Msg:       First : 1920x1080 at 144 Hz, 32 bpp, pos [   0,   0] (Primary)
[Core] Msg: Initializing IAudioBackend...
[Core] Msg:   OpenAL (OpenTK)...
[Core] Msg:     OpenAL Drivers installed in C:\Windows\system32\OpenAL32.dll
[Core] Msg:     Available devices:
                  Generic Software on Realtek HD Audio 2nd output (Realtek High Definition Audio) (Default)
                  Generic Software on Speakers (Realtek High Definition Audio)
                  Generic Software on Acer XB241H-8 (NVIDIA High Definition Audio)
                  Generic Software on Realtek Digital Output(Optical) (Realtek High Definition Audio)
                  Generic Software on Realtek Digital Output (Realtek High Definition Audio)
[Core] Msg:     Current device: Generic Software
[Core] Msg:     OpenAL Version: 1.1
                Vendor: Creative Labs Inc.
                Renderer: Software
                Effects: True
[Core] Msg:     256 sources available
[Core] Msg: Initializing core plugins...
[Core] Msg:   Compatibility.core...
[Core] Msg:   DefaultOpenTKBackend.core...
[Core] Msg:   DotNetFrameworkBackend.core...
[Core] Msg:   GamePlugin.core...
[Core] Msg:   SnowyPeak.Duality.Data.core...
[Core] Msg:   Tilemaps.core...
[Core] Msg: DualityApp initialized
            Debug Mode: False
            Command line arguments: 
[Core] Msg: Opening Window...
[Core] Msg:   Window Specification: 
                Buffers: 2
                Samples: 2
                ColorFormat: 32 (8888)
                AccumFormat: 64 (16161616)
                Depth: 24
                Stencil: 0
                VSync: On
                SwapInterval: 1
[Core] Msg:   OpenGL Version: 4.5.0 NVIDIA 381.65
              Vendor: NVIDIA Corporation
              Renderer: GeForce GTX 1070/PCIe/SSE2
              Shader Version: 4.50 NVIDIA
[Core] Msg: Initializing default content...
[Core] Msg:   Assembly loaded: System.Runtime.InteropServices 4.0.0.0
[Core] Msg:   Assembly loaded: System.Collections.Concurrent 4.0.0.0
[Core] Msg:   Assembly loaded: System.Threading.Tasks.Parallel 4.0.0.0
[Core] Msg:   Assembly loaded: NVorbis 0.7.5.0
[Core] Msg:   ...done!
[Core] Msg: Assembly loaded: FarseerDuality 4.1.1.0
[Core] Msg: Loading Resource 'Data\Levels\Main Menu.Scene.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Backgrounds\Grasslands Background.Material.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Backgrounds\Grasslands Background.Texture.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Backgrounds\Grasslands Background.Pixmap.res'
[Core] Msg: Determining OpenGL context capabilities...
[Core] Msg: Loading Resource 'Data\Levels\Beta.Scene.res'
[Core] Msg:   Assembly loaded: System.IO.Compression 4.0.0.0
[Core] Msg:   Loading Resource 'Data\Prefabs\Weapons\Weapon.Prefab.res'
[Core] Msg:   Assembly loaded: System.Runtime.Serialization.Primitives 4.0.0.0
[Core] Msg:   Assembly loaded: System.Dynamic.Runtime 4.0.0.0
[Core] Msg:   Assembly loaded: System.Runtime.Serialization 4.0.0.0
[Core] Msg:   Assembly loaded: System.ObjectModel 4.0.0.0
[Core] Msg:   Loading Resource 'Data\Tilesets\Grasslands.Tileset.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Tiles\Grasslands.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Player\Player_Lemonman.Material.res'
[Core] Msg:   Loading Resource 'Data\Shaders\TakeDamageEffect.DrawTechnique.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Player\Player_Lemonman.Texture.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Player\Player_Lemonman.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Sound\Happy.Sound.res'
[Core] Msg:   Loading Resource 'Data\Sound\Happy.AudioData.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\heldWeapon.Material.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Misc\TransparentTex.Texture.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Misc\TransparentTex.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Enemies\Enemy_GreenAppowl.Material.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Enemies\Enemy_GreenAppowl.Texture.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Enemies\Enemy_GreenAppowl.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Object_AmmoCrate.Material.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Object_AmmoCrate.Texture.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Object_AmmoCrate.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Shaders\TakeDamageEffectProgram.ShaderProgram.res'
[Core] Msg:   Loading Resource 'Data\Shaders\TakeDamageEffect.FragmentShader.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Weapons\peastol.Texture.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Weapons\peastol.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Prefabs\Weapons\Pea.Prefab.res'
[Core] Msg: Loading Resource 'Data\Sound\Peastol.Sound.res'
[Core] Msg:   Loading Resource 'Data\Sound\Peastol.AudioData.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Projectiles\Pea.Material.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Projectiles\Pea.Texture.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Projectiles\Pea.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Sound\Hurt.Sound.res'
[Core] Msg:   Loading Resource 'Data\Sound\Hurt.AudioData.res'
[Core] Msg: Loading Resource 'Data\Prefabs\Weapons\Pea_Hit.Prefab.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Projectiles\Pea_Hit.Material.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Projectiles\Pea_Hit.Texture.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Projectiles\Pea_Hit.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Weapons\lemonade_launcher.Texture.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Weapons\lemonade_launcher.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Prefabs\Weapons\Lemonade.Prefab.res'
[Core] Msg: Loading Resource 'Data\Sound\LemonlauncherShoot1.Sound.res'
[Core] Msg:   Loading Resource 'Data\Sound\LemonlauncherShoot1.AudioData.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Projectiles\Lemonade.Material.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Projectiles\Lemonade.Texture.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Projectiles\Lemonade.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Prefabs\Misc\Lemonade_Explosion.Prefab.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Misc\Lemonade_Explosion.Material.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Misc\Lemonade_Explosion.Texture.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Misc\Lemonade_Explosion.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Sound\LemonLauncher1.Sound.res'
[Core] Msg:   Loading Resource 'Data\Sound\LemonLauncher1.AudioData.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Weapons\pepper_thrower.Texture.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Weapons\pepper_thrower.Pixmap.res'
[Core] Msg: Loading Resource 'Data\Prefabs\Weapons\FlamethrowerParticle.Prefab.res'
[Core] Msg: Loading Resource 'Data\Sprites & Spritesheets\Projectiles\FlamethrowerParticle.Material.res'
[Core] Msg:   Loading Resource 'Data\Sprites & Spritesheets\Projectiles\FlamethrowerParticle.Texture.res'
[Core] Msg:     Loading Resource 'Data\Sprites & Spritesheets\Projectiles\FlamethrowerParticle.Pixmap.res'
[Core] Msg: Shutting down OpenGL 2.1 (OpenTK)...
[Core] Msg: Shutting down OpenAL (OpenTK)...
[Core] Msg: Shutting down .Net Framework...
[Core] Msg: DualityApp terminated
