﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseClasses;

namespace Behavior
{
    public class PeaHit : ShootableObjectHit
    {
        public override void OnInit(InitContext context)
        {
            base.OnInit(context); //Other script already checks for activation.
            if (context == InitContext.Activate)
            {
                deleteDelay = 250f;
                targetDeleteTime = deleteDelayCounter + deleteDelay;
                deleteDelayCounter = 0;
                checkForDelete = true;
                animationManager.PlayAnimationContinuously("hit");
            }
        }
    }
}
