﻿using Duality;
using Duality.Resources;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Misc
{
    public enum ProjectileType
    {
        Pea,
        Lemonade,
        Pepper,
        Hydro
    }

    public enum RecoilAmount
    {
        Light,
        Medium,
        Heavy
    }

    public class Weapon
    {
        public int ID { get; private set; }
        public string Title { get; private set; }
        public string Slug { get; private set; }

        //Load using slug and by placing all sprites in one folder
        public ContentRef<Texture> Sprite { get; private set; }

        public int BurstCount { get; private set; }
        public ProjectileType TypeOfProjectile { get; private set; }
        public RecoilAmount RecoilAmount { get; private set; }
        public float RateOfFire { get; private set; }
        public float Inaccuracy { get; private set; }

        //Offset of where weapon is attached to player
        public float XOffset { get; private set; }
        public float YOffset { get; private set; }

        //Offset of where particles (Flames, bullets, etc.) are spawned (Relative to the weapon, I believe).
        public float PrefabXOffset { get; private set; }
        public float PrefabYOffset { get; private set; }

        //Renderer 'Rect' values for the individual weapon
        public float BottomLeft { get; private set; }
        public float BottomRight { get; private set; }
        public float TopLeft { get; private set; }
        public float TopRight { get; private set; }

        public Weapon(int id, string title, string slug, int burstCount, ProjectileType typeOfProjectile, 
                      RecoilAmount recoilAmount, float rateOfFire, float inaccuracy, float xOffset, float yOffset,
                      float prefabXOffset, float prefabYOffset, float bottomLeft, float bottomRight, float topLeft,
                      float topRight)
        {
            ID = id;
            Title = title;
            Slug = slug;
            Sprite = new ContentRef<Texture>(null, @"Data\Sprites & Spritesheets\Weapons\" + slug + @".Texture.res");
            BurstCount = burstCount;
            TypeOfProjectile = typeOfProjectile;
            RecoilAmount = recoilAmount;
            RateOfFire = rateOfFire;
            Inaccuracy = inaccuracy;
            XOffset = xOffset;
            YOffset = yOffset;
            PrefabXOffset = prefabXOffset;
            PrefabYOffset = prefabYOffset;
            BottomLeft = bottomLeft;
            BottomRight = bottomRight;
            TopLeft = topLeft;
            TopRight = topRight;
        }
    }
}