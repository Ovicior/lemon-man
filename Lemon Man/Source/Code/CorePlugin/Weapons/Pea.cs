﻿using System.Collections.Generic;
using BaseClasses;
using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Components.Renderers;
using Duality.Resources;
using Player;

namespace Behavior
{
    /// <summary>
    ///     Gives objects basic bullet properties. They damage enemies on impact, and disappear after a certain amount of
    ///     time.
    ///     These bullets are meant to be fired by the player, as they do not damage the player.
    /// </summary>
    public class Pea : ShootableObject
    {
        public SpriteRenderer PeaSpriteRenderer { get; set; }

        private ContentRef<Prefab> hitAnimation;

        public override void OnInit(InitContext context)
        {
            base.OnInit(context);
            if (context == InitContext.Activate)
            {
                prefabDamage = 3;
                prefabLifetime = 6.0f;
                Speed = 8;
                PeaSpriteRenderer = GameObj.GetComponent<SpriteRenderer>();
                hitAnimation = new ContentRef<Prefab>(null, @"Data\Prefabs\Weapons\Pea_Hit.Prefab.res");
            }
        }

        public override void ExecuteCollisionCode(Component sender, CollisionEventArgs args)
        {
            //If the object has health, remove some from it.
            if (stats != null)
            {
                stats.InflictDamage(prefabDamage);
            }
            else //Only generation splat animation if hitting a wall.
            {
                int _angle = MathF.Rnd.Next(0, 361); //Random angle to make it prettier.
                GameObject _animation = hitAnimation.Res.Instantiate(new Vector3(GameObj.Transform.Pos.X, GameObj.Transform.Pos.Y, -6f), _angle);
                Scene.Current.AddObject(_animation);
            }
            
            GameObj.DisposeLater();
        }
    }
}