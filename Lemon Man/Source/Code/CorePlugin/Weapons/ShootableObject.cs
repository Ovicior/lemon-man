﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Behavior;
using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Components.Renderers;
using Player;

namespace BaseClasses
{
    /// <summary>
    /// Meant to give basic properties of shootable prefabs to classes that inherit from this class.
    /// </summary>
    [RequiredComponent(typeof(Transform))]
    [RequiredComponent(typeof(RigidBody))]
    [RequiredComponent(typeof(SpriteRenderer))]
    public abstract class ShootableObject : Component ,ICmpInitializable, ICmpUpdatable, ICmpCollisionListener
    {
        public float Speed;
        public Vector2 LinearVelocityToSet { get; set; }
        public GameObject Creator { get; set; }

        protected float lifetimeCounter = 0;
        protected PlayerController playerController;
        protected RigidBody rigidBody;
        protected Transform transform;
        protected HeldWeapon heldWeapon; //Reference to weapon used by player.
        protected GameObject player; //Reference to player.
        protected int prefabDamage;
        protected EntityStats stats;
        protected float prefabLifetime; //In seconds.
        protected bool hasCollided; //Bool set true when bullet has already hit the entity.

        //Prefab damage, prefab lifetime, and prefab speed must be set in init.
        public virtual void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                transform = GameObj.GetComponent<Transform>();
                rigidBody = GameObj.GetComponent<RigidBody>();
                player = GameObj.ParentScene.FindGameObject<PlayerController>();
                playerController = player.GetComponent<PlayerController>();
                rigidBody.LinearVelocity = LinearVelocityToSet; //Set object's velocity to whatever HeldWeapon passes in
                heldWeapon = GameObj.ParentScene.FindGameObject<HeldWeapon>().GetComponent<HeldWeapon>(); //Find heldWeapon and get a reference to it.
            }
        }

        public virtual void OnShutdown(ShutdownContext context)
        {
        }

        public virtual void OnUpdate()
        {
            lifetimeCounter += Time.TimeMult * Time.SPFMult;
            if (lifetimeCounter >= prefabLifetime) //Make sure bullet does not last forever.
                GameObj.DisposeLater();
        }

        public virtual void OnCollisionBegin(Component sender, CollisionEventArgs args)
        {
            if (args.CollideWith == Creator)
                return; //Cancel the collision if it is with the creator of the bullet.
            if (hasCollided) //We've already collided, don't check again.
                return;
            else
            {
                hasCollided = true; //Haven't collided yet, record that we just collided.
            }

            //We cast to RigidBodyCollisionEventArgs to get access to the info about the shapes involved.
            var rigidBodyArgs = args as RigidBodyCollisionEventArgs;
            if ((rigidBodyArgs != null) && rigidBodyArgs.OtherShape.IsSensor) return; //Don't do anything if a sensor

            stats = args.CollideWith.GetComponent<EntityStats>(); //Access health variable of hit object

            ExecuteCollisionCode(sender, args);
        }

        public virtual void OnCollisionEnd(Component sender, CollisionEventArgs args)
        {
        }

        public virtual void OnCollisionSolve(Component sender, CollisionEventArgs args)
        {
        }

        /// <summary>
        /// Whenever a collision occurs, this method will be executed. Place special behavior here.
        /// </summary>
        public virtual void ExecuteCollisionCode(Component sender, CollisionEventArgs args)
        {
        }
    }
}
