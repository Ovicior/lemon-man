﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Duality;
using Duality.Components;
using Duality.Components.Renderers;
using Manager;

namespace Behavior
{
    [RequiredComponent(typeof(AnimSpriteRenderer))]
    public class LemonExplosion : Component, ICmpInitializable, ICmpUpdatable, ICmpCollisionListener
    {
        private float timeUntilExplode;
        private float currentTime;
        private float damageMultiplier = 20f;
        private float maxDamage = 10;
        private float blastRadius = 18.5f;
        private AnimationManager animationManager;
        private Transform explosionPosition;
        private EntityStats stats; //Stats of anything in the explosion.
        private SoundManager soundManager;

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                animationManager = GameObj.GetComponent<AnimationManager>();
                animationManager.PlayAnimationContinuously("explode");
                timeUntilExplode = animationManager.animationDatabase[0].AnimationDuration; //Get length of explode animation, set object lifetime to it.
                explosionPosition = GameObj.GetComponent<Transform>();
                soundManager = GameObj.ParentScene.FindGameObject<SoundManager>().GetComponent<SoundManager>();
            }
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            currentTime += Time.TimeMult * Time.SPFMult; //Counter in seconds
            
            if (currentTime >= timeUntilExplode)
                GameObj.DisposeLater();
        }

        public void OnCollisionBegin(Component sender, CollisionEventArgs args)
        {
            stats = args.CollideWith.GetComponent<EntityStats>();
            //Log.Game.Write("Name of the collided with object is {0}", args.CollideWith.Name);
            Transform _hitObjectTransform = args.CollideWith.GetComponent<Transform>();

            if (stats != null)
            {
                float _distance = MathF.Distance(explosionPosition.Pos.X, explosionPosition.Pos.Y,
                                                 _hitObjectTransform.Pos.X, _hitObjectTransform.Pos.Y);

                float _damageAmount = MathF.RoundToInt(((blastRadius - _distance) / blastRadius) * damageMultiplier);
                MathF.Clamp(_damageAmount, 0, maxDamage); //Make sure value is not below zero and not over the max.

                //Log.Game.Write("Distance is {0} and damageAmount is {1}", _distance, _damageAmount);
                stats.InflictDamage((int)_damageAmount);
            }
            soundManager.PlaySound(Sound.Lemonade);
        }

        public void OnCollisionEnd(Component sender, CollisionEventArgs args)
        {
        }

        public void OnCollisionSolve(Component sender, CollisionEventArgs args)
        {
        }
    }
}
