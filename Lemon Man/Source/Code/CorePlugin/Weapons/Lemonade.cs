﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseClasses;
using Duality;
using Duality.Components;
using Duality.Components.Renderers;
using Duality.Resources;
using Player;

namespace Behavior
{
    [RequiredComponent(typeof(SpriteRenderer))]
    [RequiredComponent(typeof(Transform))]
    public class Lemonade : ShootableObject, ICmpInitializable, ICmpUpdatable, ICmpCollisionListener
    {
        public SpriteRenderer LemonadeSpriteRenderer { get; set; }

        private ContentRef<Prefab> lemonExplosionPrefab;
        private Transform gameObjTransform;
        private int entityCollisionCount = 0;
        private float grenadeLifetime = 2.5f;
        private float currentTime;

        public override void OnCollisionBegin(Component sender, CollisionEventArgs args)
        {
            //Object can be damaged, and the grenade has not yet collided with anything
            if (args.CollideWith.GetComponent<EntityStats>() != null && entityCollisionCount <= 0)
            {
                Explode();
            }
            //Otherwise collided with object could not be damaged, meaning touching the grenade should not trigger it as of now.
            else
            {
                entityCollisionCount++;
            }
        }

        public override void OnCollisionEnd(Component sender, CollisionEventArgs args)
        {
        }

        public override void OnCollisionSolve(Component sender, CollisionEventArgs args)
        {
        }

        public override void OnInit(InitContext context)
        {
            base.OnInit(context);
            if (context == InitContext.Activate)
            {
                lemonExplosionPrefab = new ContentRef<Prefab>(null, @"Data\Prefabs\Misc\Lemonade_Explosion.Prefab.res");
                gameObjTransform = GameObj.GetComponent<Transform>();
                LemonadeSpriteRenderer = GameObj.GetComponent<SpriteRenderer>();
                Speed = 5;
            }
        }

        public override void OnShutdown(ShutdownContext context)
        {
        }

        public override void OnUpdate()
        {
            currentTime += Time.TimeMult * Time.SPFMult;

            if (currentTime >= grenadeLifetime)
                Explode();
        }

        /// <summary>
        /// Instantiates lemon explosion at location of lemonade while also deleting the lemonade.
        /// </summary>
        private void Explode()
        {
            GameObject _explosion;

            _explosion = lemonExplosionPrefab.Res.Instantiate(new Vector3(gameObjTransform.Pos.X, gameObjTransform.Pos.Y, -1f));
            Scene.Current.AddObject(_explosion);

            GameObj.DisposeLater();
        }
    }
}
