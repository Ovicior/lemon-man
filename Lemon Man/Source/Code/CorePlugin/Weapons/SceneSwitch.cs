﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Duality;
using Duality.Input;
using Duality.Resources;

namespace Misc
{
    public class SceneSwitch : Component, ICmpInitializable, ICmpUpdatable
    {
        private ContentRef<Scene> firstLevel;

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                firstLevel = new ContentRef<Scene>(null, @"Data\Levels\Beta.Scene.res");
            }
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            if (DualityApp.Keyboard.KeyHit(Key.X))
                Scene.SwitchTo(firstLevel);
        }
    }
}
