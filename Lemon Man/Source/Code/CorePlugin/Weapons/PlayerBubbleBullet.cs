﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseClasses;
using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Components.Renderers;

namespace Behavior
{
    /// <summary>
    /// Bubble bullet properties. Bubble bullets are meant to bounce off walls a single time, and are much slower than regular bullets. 
    /// </summary>
    [RequiredComponent(typeof(Transform))]
    [RequiredComponent(typeof(RigidBody))]
    [RequiredComponent(typeof(SpriteRenderer))]
    public class PlayerBubbleBullet : ShootableObject
    {
        private int timesToBounce = 1;
        private int bounces = 0;

        public override void OnInit(InitContext context)
        {
            base.OnInit(context);
            prefabDamage = 3;
            Speed = 3;
        }

        public override void ExecuteCollisionCode(Component sender, CollisionEventArgs args)
        {
            //Collision occurred with damageable object, delete prefab and damage object.
            if (stats != null)
            {
                GameObj.DisposeLater();
                stats.InflictDamage(prefabDamage);
            }
            //Collision occurred with non-damageable object, bounce off.
            else
            {
                bounces++;
                if (bounces > timesToBounce) //Once it has bounced enough, it is deleted.
                    GameObj.DisposeLater();
            }
        }
    }
}
