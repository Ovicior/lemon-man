﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseClasses;
using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Components.Renderers;
using Manager;

namespace Behavior
{
    public class PepperFlameParticle : ShootableObject
    {
        public AnimSpriteRenderer PepperSpriteRenderer { get; set; }
        public bool StartRotation { get; set; } = false;
        public float RotatingBreakPeriod { get; set; } = 0.1f;

        private CircleShapeInfo shapeInfo;
        private AnimationManager animationManager;
        private float currentTime;

        public override void OnInit(InitContext context)
        {
            base.OnInit(context);
            PepperSpriteRenderer = GameObj.GetComponent<AnimSpriteRenderer>(); //Probably shouldn't be outside of context check, but it works for now.
            if (context == InitContext.Activate)
            {
                shapeInfo = (CircleShapeInfo)GameObj.GetComponent<RigidBody>().Shapes.First(); //Get the first (and only) shape info in the rigidbody.
                animationManager = GameObj.GetComponent<AnimationManager>();
                prefabLifetime = 0.6f;
                prefabDamage = 1;
                shapeInfo.IsSensor = true;
                animationManager.PlayAnimationContinuously("burn");
            }
        }

        public override void OnUpdate()
        {
            base.OnUpdate();
            AdjustCollider();

            currentTime += Time.TimeMult * Time.SPFMult;

            if (StartRotation)
                Rotate();
        }

        /// <summary>
        /// Changes the radius and position of the circle collider of the flame particle depending on the current frame of animation.
        /// </summary>
        public void AdjustCollider()
        {
            int _currentAnimationFrame = PepperSpriteRenderer.CurrentFrame + 1;
            switch (_currentAnimationFrame)
            {
                case 1:
                    shapeInfo.Position = new Vector2(-0.3f, -0.5f);
                    shapeInfo.Radius = 3.5f;
                    break;

                case 2:
                    shapeInfo.Position = new Vector2(-0.5f, -3);
                    shapeInfo.Radius = 4.8f;
                    break;

                case 3:
                    shapeInfo.Position = new Vector2(-1, -3);
                    shapeInfo.Radius = 6.7f;
                    break;

                //Flame particle has dissipitated into smoke in all of these frames. Can no longer damage things
                case 4:
                    shapeInfo.Position = new Vector2(-1, -3);
                    shapeInfo.IsSensor = false; //Collide off walls now, for smoke effect
                    shapeInfo.Radius = 6f;
                    break;
                case 5:
                case 6:
                case 7:
                    break;
            }
        }

        public override void ExecuteCollisionCode(Component sender, CollisionEventArgs args)
        {
            base.ExecuteCollisionCode(sender, args);
            if (stats != null && shapeInfo.IsSensor) //IsSensor is true, meaning particle is still a flame.
            {
                stats.InflictDamage(prefabDamage);
                GameObj.DisposeLater();
            }
            if (PepperSpriteRenderer.CurrentFrame + 1 <= 3 && args.CollideWith.GetComponent<PepperFlameParticle>() == null) //Still a flame, collided with something.
            {                                                                                                              //that is not a flame.
                GameObj.DisposeLater();
            }
        }
    
        public void Rotate()
        {
            Transform transform = GameObj.GetComponent<Transform>();

            if (currentTime >= RotatingBreakPeriod)
            {
                transform.Angle += MathF.Rnd.Next(0, 4);
                currentTime = 0;
            }
        }
    }
}
