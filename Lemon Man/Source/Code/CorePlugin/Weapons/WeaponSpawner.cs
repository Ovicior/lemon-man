﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Behavior;
using Duality;
using Duality.Resources;

namespace Manager
{
    public class WeaponSpawner : Component, ICmpInitializable
    {
        private ContentRef<Prefab> heldWeaponPrefab;

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                GameObject _weapon = GameObj.ParentScene.FindGameObject<HeldWeapon>();

                //Spawn weapon if there is currently not one in the level.
                if (_weapon == null)
                {
                    heldWeaponPrefab = new ContentRef<Prefab>(null, @"Data\Prefabs\Weapons\Weapon.Prefab.res");
                    SpawnHeldWeapon();
                }
            }
        }

        public void OnShutdown(ShutdownContext context)
        {
            if (context == ShutdownContext.Deactivate)
            {
                GameObject _weapon;
                _weapon = GameObj.ParentScene.FindGameObject<HeldWeapon>();

                //There is a weapon in the scene, remove it. 
                if (_weapon != null)
                    Scene.Current.RemoveObject(_weapon);
            }
        }

        /// <summary>
        /// Spawns held weapon which will move itself onto the player.
        /// </summary>
        public void SpawnHeldWeapon()
        {
            GameObject _weapon;
            _weapon = heldWeaponPrefab.Res.Instantiate(new Vector3(0, 0, 0.1f));
            Scene.Current.AddObject(_weapon);
        }
    }
}
