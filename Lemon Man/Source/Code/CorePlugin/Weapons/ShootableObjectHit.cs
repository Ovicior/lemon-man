﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Duality;
using Manager;

namespace BaseClasses
{
    public class ShootableObjectHit : Component, ICmpInitializable, ICmpUpdatable
    {
        protected AnimationManager animationManager;
        protected float deleteDelayCounter;
        protected float targetDeleteTime;
        protected float deleteDelay;
        protected bool checkForDelete;

        public virtual void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                animationManager = GameObj.GetComponent<AnimationManager>();
            }
        }

        public virtual void OnShutdown(ShutdownContext context)
        {
        }

        public virtual void OnUpdate()
        {
            deleteDelayCounter += Time.MsPFMult * Time.TimeMult;

            if (checkForDelete)
            {
                if (deleteDelayCounter >= targetDeleteTime)
                    GameObj.DisposeLater();
            }
        }
    }
}
