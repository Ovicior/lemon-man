﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Duality;
using Duality.Components;

namespace Camera
{
    public enum SpriteType
    {
        Background, //Acting as background, no colliders.
        Tile, //Tile, on top of background.
        Enemy, //Enemy sprites.
        Projectile, //Projectiles on top of enemies, so they don't get behind them.
        Gun, //Projectiles made below guns, but guns are still underneath player sprite.
        Player //Player sprite. Above everything.
    }

    public class SpriteOrganizer : Component, ICmpInitializable
    {
        public SpriteType SpriteType { get; set; }
        private Transform transform;

        public void OnInit(InitContext context)
        {
            transform = GameObj.GetComponent<Transform>();

            switch (SpriteType)
            {
                case SpriteType.Background:
                    transform.Pos = new Vector3(transform.Pos.X, transform.Pos.Y, 0.6f); //Give new position, original X + Y but new Z.
                    break;
                
                case SpriteType.Tile:
                    transform.Pos = new Vector3(transform.Pos.X, transform.Pos.Y, 0.5f);
                    break;

                case SpriteType.Enemy:
                    transform.Pos = new Vector3(transform.Pos.X, transform.Pos.Y, 0.4f);
                    break;

                case SpriteType.Projectile:
                    transform.Pos = new Vector3(transform.Pos.X, transform.Pos.Y, 0.3f);
                    break;

                case SpriteType.Gun:
                    transform.Pos = new Vector3(transform.Pos.X, transform.Pos.Y, 0.2f);
                    break;

                case SpriteType.Player:
                    transform.Pos = new Vector3(transform.Pos.X, transform.Pos.Y, 0.1f);
                    break;
            }
        }

        public void OnShutdown(ShutdownContext context)
        {

        }
    }
}
