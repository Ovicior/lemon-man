﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Duality;
using Duality.Components;

namespace Misc
{
    public class TilemapPositionMaintainer : Component, ICmpInitializable, ICmpUpdatable
    {
        public Vector3 positionToMaintain { get; set; }
        private Transform transform;

        public void OnInit(InitContext context)
        {
            transform = GameObj.GetComponent<Transform>();
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            if (transform.Pos != positionToMaintain)
                transform.Pos = positionToMaintain;
        }
    }
}
