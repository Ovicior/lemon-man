﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Duality;
using Manager;
using Player;

namespace Behavior
{
    public class WeaponChest : Component, ICmpInitializable, ICmpCollisionListener
    {
        public enum WeaponToGive { Peastol, LemonadeLauncher, Pepperthrower, Random}

        public WeaponToGive weaponToGive { get; set; }
        private AnimationManager animationManager;
        private GameObject player;
        private HeldWeapon playerWeapon;
        private bool alreadyOpened;

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                animationManager = GameObj.GetComponent<AnimationManager>();
                animationManager.PlayAnimationContinuously("idle");
                player = GameObj.ParentScene.FindGameObject<PlayerController>();
                playerWeapon = GameObj.ParentScene.FindGameObject<HeldWeapon>().GetComponent<HeldWeapon>();
            }
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnCollisionBegin(Component sender, CollisionEventArgs args)
        {
            if (args.CollideWith == player)
            {
                if (alreadyOpened == false) //Player hasn't yet opened this chest.
                {
                    animationManager.PlayAnimationOnce("open", "opened");

                    switch (weaponToGive)
                    {
                        case WeaponToGive.Peastol:
                            playerWeapon.ChangeHeldWeapon(0);
                            break;

                        case WeaponToGive.LemonadeLauncher:
                            playerWeapon.ChangeHeldWeapon(1);
                            break;

                        case WeaponToGive.Pepperthrower:
                            playerWeapon.ChangeHeldWeapon(2);
                            break;

                        case WeaponToGive.Random:
                            playerWeapon.ChangeHeldWeapon(MathF.Rnd.Next(0, 3)); //Generate number between 0 - 2.
                            break;
                    }
                    alreadyOpened = true;
                }
            }
        }

        public void OnCollisionEnd(Component sender, CollisionEventArgs args)
        {
        }

        public void OnCollisionSolve(Component sender, CollisionEventArgs args)
        {
        }
    }
}
