﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Duality;
using Duality.Components;

namespace Behavior
{
    public class Teleporter : Component, ICmpCollisionListener
    {
        public Vector2 LocationToGoTo { get; set; }

        public void OnCollisionBegin(Component sender, CollisionEventArgs args)
        {
            args.CollideWith.GetComponent<Transform>().Pos = new Vector3(LocationToGoTo, args.CollideWith.GetComponent<Transform>().Pos.Z);
        }

        public void OnCollisionEnd(Component sender, CollisionEventArgs args)
        {
        }

        public void OnCollisionSolve(Component sender, CollisionEventArgs args)
        {
        }
    }
}
