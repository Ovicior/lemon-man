﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Duality;

namespace Misc
{
    public class Timer : Component, ICmpUpdatable
    {
        //Bool that is set to true when the current time is atleast the cycle's length.
        public bool TimeReached { get; private set; }

        //The amount of time taken before it is indicated that the time has been reached.
        public float LengthOfCycle { get; set; }

        private float currentTime;
        private bool timerStart;

        public void OnUpdate()
        {
            if (timerStart)
            {
                currentTime += Time.SPFMult * Time.TimeMult;
            }
            if (LengthOfCycle > 0)
            {
                if (currentTime >= LengthOfCycle)
                {
                    TimeReached = true;
                }
            }
        }

        /// <summary>
        /// Starts timer.
        /// </summary>
        public void Start()
        {
            timerStart = true;
            if (LengthOfCycle <= 0)
                Log.Game.WriteError("CycleLength must have a value higher than 0.");
        }

        /// <summary>
        /// Stops timer.
        /// </summary>
        public void Stop()
        {
            timerStart = false;
        }

        /// <summary>
        /// Resets current timer value to zero.
        /// </summary>
        public void Reset()
        {
            currentTime = 0;
            TimeReached = false;
        }
    }
}
