﻿using System.Linq;
using Behavior;
using Duality;
using Duality.Components.Physics;
using Duality.Components.Renderers;
using Duality.Input;
using Duality.Resources;
using Manager;
using Misc;

namespace Enemy
{
    public enum FacingDirection
    {
        Left,
        Right
    }

    public enum EnemyAnimationState
    {
        Idle,
        Running,
        Death,
    }

    public enum HorizontalMovement
    {
        Left,
        Right,
        None
    }

    public enum VerticalMovement
    {
        Up,
        Down,
        None
    }

    /// <summary>
    /// Utilizes Controller2D to perform calculations and take input from the player for movement.
    /// </summary>
    [RequiredComponent(typeof(Controller2D))]
    public class EnemyController : Component, ICmpUpdatable, ICmpInitializable
    {

        public EnemyAnimationState EnemyAnimationState { get; private set; }
        public FacingDirection FacingDirection { get; private set; }
        public HorizontalMovement HorizontalMovement { get; private set; }
        public VerticalMovement VerticalMovement { get; private set; }
        public float FiringDelay { get; set; } = 10f;
        public Vector3 BulletSpawnOffset { get; set; } = new Vector3(4, 0.1f, -0.1f);

        private float firingDelayCounter;
        private float gravity;
        private HeldWeapon heldWeapon;
        private EntityStats entityStats;
        private float jumpVelocity;
        private SpriteRenderer spriteRenderer;
        private ShapeInfo shapeInfo;
        private Vector2 velocity;
        private float MoveSpeed { get; } = 72;
        private float JumpHeight { get; } = 45;
        private float TimeToJumpApex { get; } = .45f;
        private float AccelerationGrounded { get; } = 100;
        private float AccelerationAirborne { get; set; } = 100;
        private bool checkLeftMovement = false;
        private bool checkRightMovement = false;
        private float currentXPosition;
        private float targetXPosition;

        private Controller2D Controller
        {
            get { return GameObj.GetComponent<Controller2D>(); }
        }

        private AnimationManager animationManager;

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                spriteRenderer = GameObj.GetComponent<SpriteRenderer>();
                heldWeapon = GameObj.ParentScene.FindGameObject<HeldWeapon>().GetComponent<HeldWeapon>();
                animationManager = GameObj.GetComponent<AnimationManager>();
                entityStats = GameObj.GetComponent<EntityStats>();
                gravity = 2 * JumpHeight / TimeToJumpApex / TimeToJumpApex;
                jumpVelocity = MathF.Abs(gravity) * TimeToJumpApex;
                EnemyAnimationState = EnemyAnimationState.Idle;
                HorizontalMovement = HorizontalMovement.None;
                VerticalMovement = VerticalMovement.None;
            }
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            firingDelayCounter += Time.MsPFMult * Time.TimeMult;
            if (Controller.Collisions.below || Controller.Collisions.above)
                velocity.Y = 0;

            var _input = Vector2.Zero;
            if (entityStats.Dead != true) //Not dead so animation allowed
            {
                if (HorizontalMovement == HorizontalMovement.Left)
                {
                    _input.X = -1;
                    FacingDirection = FacingDirection.Left;
                    EnemyAnimationState = EnemyAnimationState.Running;
                }
                else if (HorizontalMovement == HorizontalMovement.Right)
                {
                    _input.X = 1;
                    FacingDirection = FacingDirection.Right;
                    EnemyAnimationState = EnemyAnimationState.Running;
                }
                else if (HorizontalMovement == HorizontalMovement.None)
                {
                    //No input, so enemy must not be moving.
                    EnemyAnimationState = EnemyAnimationState.Idle;
                }

                if (VerticalMovement == VerticalMovement.Up)
                    _input.Y = -1;
                else if (VerticalMovement == VerticalMovement.Down)
                    _input.Y = 1;
            }
            else if (entityStats.Dead)
            {
                EnemyAnimationState = EnemyAnimationState.Death;
                //animationManager.PlayAnimationOnce("death", "deathStill"); TODO: FIX THIS
            }

            /*if (DualityApp.Keyboard.KeyHit(Key.Z) && Controller.Collisions.below)
                velocity.Y = -jumpVelocity; Old jump related mechanisms*/

            /*if (velocity.Y < 0) //Velocity means player is gaining height
                EnemyAnimationState = PlayerAnimationState.Jumping;
            else if (velocity.Y > 0) //Velocity means player is losing height
                PlayerAnimationState = PlayerAnimationState.Falling; Not really gonna need these for enemies*/

            var _targetVelocityX = _input.X * MoveSpeed;
            velocity.X = MathF.Lerp(velocity.X, _targetVelocityX, AccelerationGrounded / 100);
            velocity.Y += gravity * Time.TimeMult / 60f;
            Controller.Move(velocity * Time.TimeMult / 60f);
            UpdateSprite();
            UpdateAnimation();
            CheckIfMovementOver();
        }

        /// <summary>
        /// Flips sprite based on state. Correspondingly adjusts RigidBody shape.
        /// </summary>
        private void UpdateSprite()
        {
            switch (FacingDirection)
            {
                case FacingDirection.Left:
                    spriteRenderer.Flip = SpriteRenderer.FlipMode.Horizontal;
                    break;
                case FacingDirection.Right:
                    spriteRenderer.Flip = SpriteRenderer.FlipMode.None;
                    break;
            }
        }

        /// <summary>
        /// Changes player's animation based on state machine.
        /// </summary>
        private void UpdateAnimation()
        {
            switch (EnemyAnimationState)
            {
                case EnemyAnimationState.Idle:
                    animationManager.PlayAnimationContinuously("idle");
                    break;

                case EnemyAnimationState.Running:
                    animationManager.PlayAnimationContinuously("run");
                    break;
            }
        }

        /// <summary>
        /// Move left by the inputted distance.
        /// </summary>
        /// <param name="distance"></param>
        public void MoveLeft(float distance)
        {
            currentXPosition = GameObj.Transform.Pos.X; //Get current position.
            targetXPosition = currentXPosition - distance; //Set destination based on starting position.
            HorizontalMovement = HorizontalMovement.Left; //Start movement.
            checkLeftMovement = true; //Begin checking to see if we have arrived at the destination.
            checkRightMovement = false; //Do not check for both left and right movement at the same time.
        }

        /// <summary>
        /// Move right by the inputted distance.
        /// </summary>
        /// <param name="distance"></param>
        public void MoveRight(float distance)
        {
            currentXPosition = GameObj.Transform.Pos.X;
            targetXPosition = currentXPosition + distance;
            HorizontalMovement = HorizontalMovement.Right;
            checkLeftMovement = false;
            checkRightMovement = true;
        }

        /// <summary>
        /// Checks to see if movement designated in the methods MoveLeft() and MoveRight() has been completed.
        /// </summary>
        public void CheckIfMovementOver()
        {
            currentXPosition = GameObj.Transform.Pos.X; //Update position every frame.

            if (checkLeftMovement)
            {
                if (currentXPosition <= targetXPosition)
                {
                    HorizontalMovement = HorizontalMovement.None; //Stop movement.
                    checkLeftMovement = false; //Don't check for movement anymore.
                }
            }
            else if (checkRightMovement)
            {
                if (currentXPosition >= targetXPosition)
                {
                    HorizontalMovement = HorizontalMovement.None;
                    checkRightMovement = false;
                }
            }
        }
    }
}