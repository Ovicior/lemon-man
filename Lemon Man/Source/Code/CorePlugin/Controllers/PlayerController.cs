﻿using Behavior;
using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Components.Renderers;
using Duality.Input;
using Duality.Resources;
using Manager;
using Misc;

namespace Player
{
    public enum FacingDirection
    {
        Left,
        Right
    }

    public enum PlayerAnimationState
    {
        Idle,
        Running,
        Jumping,
        Falling,
        Other
    }

    /// <summary>
    /// Utilizes Controller2D to perform calculations and take input from the player for movement.
    /// </summary>
    [RequiredComponent(typeof(Controller2D))]
    public class PlayerController : Component, ICmpUpdatable, ICmpInitializable
    {
        public PlayerAnimationState PlayerAnimationState { get; private set; }

        public FacingDirection FacingDirection { get; private set; }

        public float FiringDelay { get; set; } = 10f;
        public Vector3 BulletSpawnOffset { get; set; } = new Vector3(4, 0.1f, -0.1f);
        public Vector2 velocity;

        private float firingDelayCounter;
        private float gravity;
        private HeldWeapon heldWeapon;
        private float jumpVelocity;
        private SpriteRenderer spriteRenderer;
        private float MoveSpeed { get; } = 72;
        private float JumpHeight { get; } = 45;
        private float TimeToJumpApex { get; } = .45f;
        private float AccelerationGrounded { get; } = 100;
        private float AccelerationAirborne { get; set; } = 100;
        private RigidBody rigidBody; //For collision detection.

        private Controller2D Controller
        {
            get { return GameObj.GetComponent<Controller2D>(); }
        }

        private AnimationManager animationManager;

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                spriteRenderer = GameObj.GetComponent<SpriteRenderer>();
                heldWeapon = GameObj.ParentScene.FindGameObject<HeldWeapon>().GetComponent<HeldWeapon>();
                animationManager = GameObj.GetComponent<AnimationManager>();
                gravity = 2 * JumpHeight / TimeToJumpApex / TimeToJumpApex;
                jumpVelocity = MathF.Abs(gravity) * TimeToJumpApex;
                PlayerAnimationState = PlayerAnimationState.Idle;
                rigidBody = GameObj.GetComponent<RigidBody>();
            }
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            firingDelayCounter += Time.MsPFMult * Time.TimeMult;
            if (Controller.Collisions.below || Controller.Collisions.above)
                velocity.Y = 0;

            var _input = Vector2.Zero;
            if (DualityApp.Keyboard[Key.Left])
            {
                _input.X = -1;
                FacingDirection = FacingDirection.Left;
                PlayerAnimationState = PlayerAnimationState.Running;
            }
            else if (DualityApp.Keyboard[Key.Right])
            {
                _input.X = 1;
                FacingDirection = FacingDirection.Right;
                PlayerAnimationState = PlayerAnimationState.Running;
            }
            else
            {
                //No input, so player must not be moving.
                PlayerAnimationState = PlayerAnimationState.Idle;
            }

            if (DualityApp.Keyboard[Key.Up])
                _input.Y = -1;
            else if (DualityApp.Keyboard[Key.Down])
                _input.Y = 1;

            if (DualityApp.Keyboard.KeyHit(Key.Z) && Controller.Collisions.below)
                velocity.Y = -jumpVelocity;

            if (DualityApp.Keyboard.KeyHit(Key.R))
                GameObj.GetComponent<Transform>().Pos = new Vector3(-726.27f, -235.35f, -1f);

            if (velocity.Y < 0) //Velocity means player is gaining height
                PlayerAnimationState = PlayerAnimationState.Jumping;
            else if (velocity.Y > 0) //Velocity means player is losing height
                PlayerAnimationState = PlayerAnimationState.Falling;

            var _targetVelocityX = _input.X * MoveSpeed;
            velocity.X = MathF.Lerp(velocity.X, _targetVelocityX, AccelerationGrounded / 100);
            velocity.Y += gravity * Time.TimeMult / 60f;
            Controller.Move(velocity * Time.TimeMult / 60f);
            UpdateSprite();
            UpdateAnimation();

        }

        /// <summary>
        /// Flips sprite based on arrow key input.
        /// </summary>
        private void UpdateSprite()
        {
            switch (FacingDirection)
            {
                case FacingDirection.Left:
                    spriteRenderer.Flip = SpriteRenderer.FlipMode.Horizontal;
                    break;
                case FacingDirection.Right:
                    spriteRenderer.Flip = SpriteRenderer.FlipMode.None;
                    break;
            }
        }

        /// <summary>
        /// Changes player's animation based on state machine.
        /// </summary>
        private void UpdateAnimation()
        {
            switch (PlayerAnimationState)
            {
                case PlayerAnimationState.Idle:
                    animationManager.PlayAnimationContinuously("idle");
                    break;

                case PlayerAnimationState.Running:
                    animationManager.PlayAnimationContinuously("run");
                    break;

                case PlayerAnimationState.Jumping:
                    animationManager.PlayAnimationContinuously("jump");
                    break;

                case PlayerAnimationState.Falling:
                    animationManager.PlayAnimationContinuously("fall");
                    break;
            }
        }
    }
}