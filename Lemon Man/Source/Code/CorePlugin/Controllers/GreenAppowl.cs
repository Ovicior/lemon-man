﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Behavior;
using Duality;

namespace Enemy
{
    public class GreenAppowl : Component, ICmpCollisionListener
    {
        private EntityStats stats;

        public void OnCollisionBegin(Component sender, CollisionEventArgs args)
        {
            stats = args.CollideWith.GetComponent<EntityStats>();

            if (stats != null)
            {
                stats.InflictDamage(4);
            }
        }

        public void OnCollisionEnd(Component sender, CollisionEventArgs args)
        {
        }

        public void OnCollisionSolve(Component sender, CollisionEventArgs args)
        {
        }
    }
}
