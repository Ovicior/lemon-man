﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Duality;
using Duality.Resources;

namespace Manager
{
    public enum Sound
    {
        Eighties,
        Hurt,
        Lemonade,
        LemonLauncher,
        Peastol,
        Pop,
        Happy
    }

    public class SoundManager : Component, ICmpInitializable, ICmpUpdatable
    {
        private ContentRef<Duality.Resources.Sound> eightiesTrack;
        private ContentRef<Duality.Resources.Sound> hurt;
        private ContentRef<Duality.Resources.Sound> lemonade;
        private ContentRef<Duality.Resources.Sound> lemonLauncher;
        private ContentRef<Duality.Resources.Sound> peastol;
        private ContentRef<Duality.Resources.Sound> pop;
        private ContentRef<Duality.Resources.Sound> happy;

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                eightiesTrack = new ContentRef<Duality.Resources.Sound>(null, @"Data\Sound\80s.Sound.res");
                hurt = new ContentRef<Duality.Resources.Sound>(null, @"Data\Sound\Hurt.Sound.res");
                lemonade = new ContentRef<Duality.Resources.Sound>(null, @"Data\Sound\LemonLauncher1.Sound.res");
                lemonLauncher = new ContentRef<Duality.Resources.Sound>(null, @"Data\Sound\LemonlauncherShoot1.Sound.res");
                peastol = new ContentRef<Duality.Resources.Sound>(null, @"Data\Sound\Peastol.Sound.res");
                pop = new ContentRef<Duality.Resources.Sound>(null, @"Data\Sound\PopMaybe.Sound.res");
                happy = new ContentRef<Duality.Resources.Sound>(null, @"Data\Sound\Happy.Sound.res");

                if (DualityApp.ExecContext == DualityApp.ExecutionContext.Game)
                    DualityApp.Sound.PlaySound(happy);
            }
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
        }

        public void PlaySound(Sound sound)
        {
            switch (sound)
            {
                case Sound.Eighties:
                    DualityApp.Sound.PlaySound(eightiesTrack);
                    break;

                case Sound.Hurt:
                    DualityApp.Sound.PlaySound(hurt);
                    break;

                case Sound.LemonLauncher:
                    DualityApp.Sound.PlaySound(lemonLauncher);
                    break;

                case Sound.Lemonade:
                    DualityApp.Sound.PlaySound(lemonade);
                    break;

                case Sound.Peastol:
                    DualityApp.Sound.PlaySound(peastol);
                    break;

                case Sound.Pop:
                    DualityApp.Sound.PlaySound(pop);
                    break;

                case Sound.Happy:
                    DualityApp.Sound.PlaySound(happy);
                    break;
            }
        }
    }
}
