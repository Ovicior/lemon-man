﻿using Duality;
using Duality.Components;
using Duality.Components.Renderers;
using Duality.Drawing;
using Duality.Resources;
using Manager;
using Misc;
using Sound = Manager.Sound;

namespace Behavior
{
    [RequiredComponent(typeof(Transform))]
    [RequiredComponent(typeof(Timer))]
    public class EntityStats : Component, ICmpUpdatable, ICmpInitializable
    {
        public int MaxHealth { get; set; }
        public int CurrentHealth { get; private set; }
        public bool Dead { get; private set; }

        private AnimationManager animationManager;
        private Timer timer;
        private ContentRef<Material> material;
        private float damageEffectLength = 0.1f;
        private float currentTime;
        private SoundManager soundManager;

        void ICmpInitializable.OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                CurrentHealth = MaxHealth;
                animationManager = GameObj.GetComponent<AnimationManager>();
                timer = GameObj.GetComponent<Timer>();
                soundManager = GameObj.ParentScene.FindGameObject<SoundManager>().GetComponent<SoundManager>();
                timer.LengthOfCycle = damageEffectLength;

                if (GameObj.GetComponent<SpriteRenderer>() == null && GameObj.GetComponent<AnimSpriteRenderer>() == null)
                    Log.Game.WriteError("Object must have some sort of renderer attached in order to have shader function properly.");

                if (GameObj.GetComponent<AnimSpriteRenderer>() != null) //There's an AnimSpriteRenderer.
                {
                    material = GameObj.GetComponent<AnimSpriteRenderer>().SharedMaterial; //Get reference to material.
                }
                else //There's not an AnimSpriteRenderer, so there must be a SpriteRenderer.
                {
                    material = GameObj.GetComponent<SpriteRenderer>().SharedMaterial;
                }

                material.Res.SetUniform("shaderActive", 0, 0f); //Reset value in editor.
            }
        }

        void ICmpInitializable.OnShutdown(ShutdownContext context)
        {
        }

        void ICmpUpdatable.OnUpdate()
        {
            if (CurrentHealth > MaxHealth)
                CurrentHealth = MaxHealth;

            if (CurrentHealth <= 0)
            {
                Dead = true;
            }

            if (Dead)
            {
                currentTime += Time.SPFMult * Time.TimeMult;
                animationManager.PlayAnimationContinuously("death");

                if (currentTime >= animationManager.GetAnimation("death").AnimationDuration)
                    animationManager.PlayAnimationContinuously("deathStill");
            }

            if (timer.TimeReached)
            {
                material.Res.SetUniform("shaderActive", 0, 0f);
                timer.Stop();
                timer.Reset();
            }
        }

        /// <summary>
        /// Damages entity for given amount.
        /// </summary>
        /// <param name="damageAmount"></param>
        public void InflictDamage(int damageAmount)
        {
            if (Dead == false) //Don't do damage or effects if dead.
            {
                CurrentHealth -= damageAmount;
                material.Res.SetUniform("shaderActive", 0, 1.0f);
                timer.Reset();
                timer.Start();
                soundManager.PlaySound(Sound.Hurt);
            }
        }
    }
}