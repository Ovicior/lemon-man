﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misc
{
    public class Animation
    {
        public int StartFrame { get; private set; } //Frame at which animation begins in spritesheet
        public int AmountOfAnimationFrames { get; private set; } //Length of animation in frames (So the final frame's location can be calculated)
        public float AnimationDuration { get; private set; } //How long the animation should last in seconds
        public string AnimationName { get; private set; } //Name of the animation

        public Animation(int startFrame, int amountOfAnimationFrames, float animationDuration, string animationName)
        {
            StartFrame = startFrame;
            AmountOfAnimationFrames = amountOfAnimationFrames;
            AnimationDuration = animationDuration;
            AnimationName = animationName;
        }
    }
}
