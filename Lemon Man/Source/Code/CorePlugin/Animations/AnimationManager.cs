﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Duality;
using Duality.Components.Renderers;
using Misc;
using Duality.IO;
using System.IO;
using Newtonsoft.Json;

namespace Manager
{
    /// <summary>
    /// Builds database with animation data from JSON file and compiles it into a list.
    /// Also allows for animations to be played 
    /// </summary>
    [RequiredComponent(typeof(AnimSpriteRenderer))]
    public class AnimationManager : Component, ICmpInitializable, ICmpUpdatable
    {
        public string animatedObjectName { get; set; }
        public List<Animation> animationDatabase { get; private set; }

        private AnimSpriteRenderer animSpriteRenderer;
        private string currentAnimationName;
        private float currentTime;
        private float animationDuration;
        private string animationToReturnTo;

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                if (animatedObjectName != null)
                    ConstructAnimationDatabase();

                animSpriteRenderer = GameObj.GetComponent<AnimSpriteRenderer>();
            }
        }

        public void OnShutdown(ShutdownContext context)
        {
        }

        public void OnUpdate()
        {
            currentTime += Time.SPFMult * Time.TimeMult;

            //Continuously check if animation has been played once.
            if (currentTime >= animationDuration)
            {
                PlayAnimationContinuously(animationToReturnTo);
                currentTime = 0;
            }
        }

        /// <summary>
        /// Changes values of the AnimSpriteRenderer component to continously loop an animation.
        /// Animation data must be entered in a .json file before it can be used.
        /// </summary>
        /// <param name="animationName"></param>
        public void PlayAnimationContinuously(string animationName)
        {
            foreach (Animation _animation in animationDatabase)
            {
                if (_animation.AnimationName == animationName)
                {
                    currentAnimationName = animationName;
                    animSpriteRenderer.AnimFirstFrame = _animation.StartFrame - 1; //Minus one to account for zero-based indexing
                    animSpriteRenderer.AnimFrameCount = _animation.AmountOfAnimationFrames;
                    animSpriteRenderer.AnimDuration = _animation.AnimationDuration;
                }
            }
        }

        /// <summary>
        /// Play an animation a single time, and then return to looping another animation.
        /// </summary>
        /// <param name="animationName"></param>
        /// <param name="animationToReturnTo"></param>
        public void PlayAnimationOnce(string animationName, string animationToReturnTo)
        {
            foreach (Animation _animation in animationDatabase)
            {
                if (_animation.AnimationName == animationName)
                {
                    currentTime = 0;
                    animationDuration = _animation.AnimationDuration;
                    this.animationToReturnTo = animationToReturnTo;
                    PlayAnimationContinuously(animationName);
                    
                    break; //Break out of for-each loop if animation is found.
                }
            }
        }

        public Animation GetAnimation(string animationName)
        {
            foreach (Animation _animation in animationDatabase)
            {
                if (_animation.AnimationName == animationName) //If the current animation is the animation we are seeking
                {
                    return _animation;
                }
            }
            return null;
        }

        /// <summary>
        /// Constructs database of all animation data belonging to the object it is attached to.
        /// Can be executed a second time after changing animatedObjectName to load a different database.
        /// </summary>
        public void ConstructAnimationDatabase()
        {
            animationDatabase = null; //Clear out list before doing anything to it.
            string _path = @"Data\Misc\" + animatedObjectName + @"Animations.json";
            using (var s = FileOp.Open(_path, FileAccessMode.Read)) //Opens file.
            {
                using (var r = new StreamReader(s)) //Read opened file.
                {
                    var json = r.ReadToEnd(); //Variable containing the contents of the .json file
                    animationDatabase = JsonConvert.DeserializeObject<List<Animation>>(json); //JSON.NET handles the rest :D
                }
            }
        }
    }
}
