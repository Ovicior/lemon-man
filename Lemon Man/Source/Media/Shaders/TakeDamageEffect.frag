uniform sampler2D mainTex;
uniform float shaderActive;

void main()
{
    vec4 color = texture2D(mainTex, gl_TexCoord[0].st);

    if (shaderActive == 1)
    {
        /*
        if (color.a == 0)
        {
            gl_FragColor = vec4(0, 0, 0, 0);
        }
        else
        {
            gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
        */
        gl_FragColor = vec4(1.0, 1.0, 1.0, color.a);
    }
    else
    {
        gl_FragColor = gl_Color * texture2D(mainTex, gl_TexCoord[0].st);
    }
}